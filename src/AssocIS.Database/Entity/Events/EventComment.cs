﻿using AssocIS.Database.Entity.BaseEntities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity.Events
{
    [Table(nameof(EventComment))]
    public class EventComment : LoggedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long EventId { get; set; }

        [ForeignKey(nameof(EventId))]
        public Event Event { get; set; }

        public string Text { get; set; }
    }
}
