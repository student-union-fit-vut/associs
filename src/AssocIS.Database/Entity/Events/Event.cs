﻿
using AssocIS.Database.Entity.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity.Events
{
    public class Event : LoggedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public DateTime From { get; set; }

        // If contains null, event takes only one day.
        public DateTime? To { get; set; }

        public string Description { get; set; }

        public int ExpectedCount { get; set; }

        public int? RealCount { get; set; }

        public string Note { get; set; }

        [StringLength(20)]
        public string DiscordChannelId { get; set; }

        public ISet<EventComment> Comments { get; set; }

        public string CalendarEventId { get; set; }

        public Event()
        {
            Comments = new HashSet<EventComment>();
        }
    }
}
