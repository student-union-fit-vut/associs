﻿using AssocIS.Database.Entity.BaseEntities;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    [Table(nameof(AccessMember))]
    public class AccessMember : LoggedEntity
    {
        public long AccessDefinitionId { get; set; }

        [ForeignKey(nameof(AccessDefinitionId))]
        public AccessDefinition AccessDefinition { get; set; }

        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public User User { get; set; }

        public bool Allowed { get; set; }

        public string Note { get; set; }
    }
}
