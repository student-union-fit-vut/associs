﻿using AssocIS.Database.Entity.BaseEntities;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    public class Meeting : LoggedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public DateTime At { get; set; }

        public DateTime ExpectedEnd { get; set; }

        public DateTime? LastNotifiedAt { get; set; }

        public string Report { get; set; }

        public MeetingAttendanceType AttendanceType { get; set; }

        public MeetingState State { get; set; }

        public string Location { get; set; }

        public string Message { get; set; }

        public string CalendarEventId { get; set; }

        public ISet<MeetingPresence> Presences { get; set; }
        public ISet<MeetingAgendaItem> Agenda { get; set; }

        public Meeting()
        {
            Presences = new HashSet<MeetingPresence>();
            Agenda = new HashSet<MeetingAgendaItem>();
        }
    }
}
