﻿using AssocIS.Database.Entity.BaseEntities;
using AssocIS.Database.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    public class AccessDefinition : LoggedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public AccessType Type { get; set; }

        public string Description { get; set; }

        public ISet<AccessMember> Members { get; set; }

        public AccessDefinition()
        {
            Members = new HashSet<AccessMember>();
        }
    }
}
