﻿using System;

namespace AssocIS.Database.Entity.BaseEntities
{
    public abstract class LoggedEntity
    {
        public long? CreatedById { get; set; }

        public User CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public long? LastModifiedById { get; set; }

        public User LastModifiedBy { get; set; }

        public DateTime? LastModifiedAt { get; set; }

        public void MarkAsModified(long modifiedBy)
        {
            LastModifiedById = modifiedBy;
            LastModifiedAt = DateTime.Now;
        }
    }
}
