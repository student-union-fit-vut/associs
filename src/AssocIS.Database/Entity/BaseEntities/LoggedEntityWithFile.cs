﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.IO.Compression;

namespace AssocIS.Database.Entity.BaseEntities
{
    public abstract class LoggedEntityWithFile : LoggedEntity
    {
        public byte[] Content { get; set; }

        [NotMapped]
        public byte[] DecompressedData
        {
            get => GetData();
            set => StoreData(value);
        }

        private void StoreData(byte[] data)
        {
            if(data == null || data.Length == 0)
            {
                Content = null;
                return;
            }

            using var memStream = new MemoryStream();
            using var originalMemStream = new MemoryStream(data);
            using var gzipStream = new GZipStream(memStream, CompressionMode.Compress);
            originalMemStream.CopyTo(gzipStream);

            Content = memStream.ToArray();
        }

        private byte[] GetData()
        {
            if (Content == null || Content.Length == 0) return new byte[0];

            using var compressedStream = new MemoryStream();

            compressedStream.Write(Content, 0, Content.Length);
            compressedStream.Position = 0;

            using var gzipStream = new GZipStream(compressedStream, CompressionMode.Decompress, true);
            using var decompressedStream = new MemoryStream();
            gzipStream.CopyTo(decompressedStream);

            return decompressedStream.ToArray();
        }
    }
}
