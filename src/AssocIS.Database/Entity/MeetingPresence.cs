﻿using AssocIS.Database.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    public class MeetingPresence
    {
        public long MeetingId { get; set; }

        [ForeignKey("MeetingId")]
        public Meeting Meeting { get; set; }

        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public MeetingPresenceType PresenceType { get; set; }
    }
}
