﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    [Table("TeamMember")]
    public class TeamMember
    {
        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public long TeamId { get; set; }

        [ForeignKey("TeamId")]
        public Team Team { get; set; }

        public bool IsLeader { get; set; }
    }
}
