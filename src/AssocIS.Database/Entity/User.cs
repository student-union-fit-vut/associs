﻿using AssocIS.Database.Entity.BaseEntities;
using AssocIS.Database.Enums;
using AssocIS.Database.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    public class User : LoggedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(100)]
        public string Username { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Surname { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public DateTime JoinedAt { get; set; }

        public DateTime? LeavedAt { get; set; }

        [StringLength(100)]
        public string StudyProgram { get; set; }

        #region ProfileImage

        public long? ProfileImageId { get; set; }

        [ForeignKey("ProfileImageId")]
        public ProfilePicture ProfileImage { get; set; }

        #endregion

        public DateTime? TestPeriodEnd { get; set; }

        public string GlobalNote { get; set; }

        public ISet<UserNote> UserNotes { get; set; }

        public UserState State { get; set; }

        [DataType(DataType.Password)]
        [StringLength(512)]
        public string Password { get; set; }

        public long Permissions { get; set; }

        [StringLength(30)]
        public string DiscordId { get; set; }

        public string GooglePrimaryEmail { get; set; }

        public DateTime? LastLogin { get; set; }

        public long Flags { get; set; }

        public ISet<TeamMember> Teams { get; set; }
        public ISet<Interview> Interviews { get; set; }
        public ISet<RefreshToken> RefreshTokens { get; set; }
        public ISet<MeetingPresence> MeetingPresences { get; set; }
        public ISet<AccessMember> Accesses { get; set; }

        public User()
        {
            UserNotes = new HashSet<UserNote>();
            Teams = new HashSet<TeamMember>();
            RefreshTokens = new HashSet<RefreshToken>();
            Interviews = new HashSet<Interview>();
            MeetingPresences = new HashSet<MeetingPresence>();
            Accesses = new HashSet<AccessMember>();
        }

        #region GDPR

        public void Anonymize(IAssocISRepository repository)
        {
            DiscordId = null;
            Email = "anonymized@anonymized.com";
            GlobalNote = null;
            GooglePrimaryEmail = null;
            LastLogin = null;
            Name = "Anonymized";
            Password = null;
            Permissions = 0;
            PhoneNumber = null;
            State = UserState.BasicMember;
            StudyProgram = null;
            Surname = "Anonymized";
            Teams.Clear();
            Username = DateTime.Now.ToString("yyyyMMddHHmmss");

            Flags |= (long)UserFlags.Anonymized;
            Flags &= ~(long)UserFlags.Admin;

            if (ProfileImage != null)
            {
                repository.Remove(ProfileImage);

                ProfileImage = null;
                ProfileImageId = null;
            }

            repository.RemoveCollection(RefreshTokens);
        }

        #endregion
    }
}
