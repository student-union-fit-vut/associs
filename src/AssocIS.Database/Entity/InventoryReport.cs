﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    [Table("InventoryReport")]
    public class InventoryReport
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public long AssetId { get; set; }

        [ForeignKey("AssetId")]
        public Asset Asset { get; set; }

        public DateTime At { get; set; }

        public string Note { get; set; }

        public int Count { get; set; }
    }
}
