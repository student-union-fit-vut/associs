﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    public class MeetingAgendaItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long MeetingId { get; set; }

        [ForeignKey(nameof(MeetingId))]
        public Meeting Meeting { get; set; }

        public string Content { get; set; }
    }
}
