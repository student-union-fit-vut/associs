﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    public class Team
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }

        [StringLength(20)]
        public string DiscordRoleId { get; set; }

        public DateTime CreatedAt { get; set; }

        public ISet<TeamMember> Members { get; set; }
        public ISet<TeamMailingList> GoogleGroups { get; set; }

        public Team()
        {
            Members = new HashSet<TeamMember>();
            GoogleGroups = new HashSet<TeamMailingList>();
        }
    }
}
