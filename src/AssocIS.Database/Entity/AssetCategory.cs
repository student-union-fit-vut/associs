﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    [Table("AssetCategory")]
    public class AssetCategory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public string Icon { get; set; }

        public ISet<Asset> Assets { get; set; }

        public AssetCategory()
        {
            Assets = new HashSet<Asset>();
        }
    }
}
