﻿using AssocIS.Database.Entity.BaseEntities;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity.Finance
{
    public class Transaction : LoggedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public DateTime ProcessedAt { get; set; }

        public decimal Amount { get; set; }

        public TransactionType Type { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public long AccountId { get; set; }

        [ForeignKey(nameof(AccountId))]
        public Account Account { get; set; }

        public long? SupplierId { get; set; }

        [ForeignKey(nameof(SupplierId))]
        public Supplier Supplier { get; set; }

        public ISet<TransactionCategory> Categories { get; set; }
        public ISet<TransactionAttachment> Attachments { get; set; }

        public Transaction()
        {
            Categories = new HashSet<TransactionCategory>();
            Attachments = new HashSet<TransactionAttachment>();
        }
    }
}
