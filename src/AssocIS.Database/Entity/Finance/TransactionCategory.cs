﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity.Finance
{
    public class TransactionCategory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public string Icon { get; set; }

        public ISet<Transaction> Transactions { get; set; }

        public TransactionCategory()
        {
            Transactions = new HashSet<Transaction>();
        }
    }
}
