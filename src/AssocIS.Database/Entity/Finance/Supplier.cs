﻿using AssocIS.Database.Entity.BaseEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity.Finance
{
    public class Supplier : LoggedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Name { get; set; }

        public string Contact { get; set; }

        public string WebLink { get; set; }

        public string Note { get; set; }

        public ISet<Transaction> Transactions { get; set; }

        public Supplier()
        {
            Transactions = new HashSet<Transaction>();
        }
    }
}
