﻿using AssocIS.Database.Entity.BaseEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity.Finance
{
    public class Account : LoggedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        [StringLength(50)]
        public string AccountNumber { get; set; }

        [StringLength(10)]
        public string BankCode { get; set; }

        public ISet<Transaction> Transactions { get; set; }

        public Account()
        {
            Transactions = new HashSet<Transaction>();
        }
    }
}
