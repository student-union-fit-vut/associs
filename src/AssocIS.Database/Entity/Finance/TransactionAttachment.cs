﻿using AssocIS.Database.Entity.BaseEntities;
using AssocIS.Database.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace AssocIS.Database.Entity.Finance
{
    [Table(nameof(TransactionAttachment))]
    public class TransactionAttachment : LoggedEntityWithFile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Name { get; set; }

        public TransactionAttachmentType Type { get; set; }

        public long TransactionId { get; set; }

        [ForeignKey(nameof(TransactionId))]
        public Transaction Transaction { get; set; }

        public static Expression<Func<TransactionAttachment, TransactionAttachment>> GetExpressionWithoutData()
        {
            return o => new TransactionAttachment()
            {
                Name = o.Name,
                TransactionId = o.TransactionId,
                Type = o.Type,
                CreatedAt = o.CreatedAt,
                CreatedBy = o.CreatedBy,
                CreatedById = o.CreatedById,
                Id = o.Id,
                LastModifiedAt = o.LastModifiedAt,
                LastModifiedBy = o.LastModifiedBy,
                LastModifiedById = o.LastModifiedById
            };
        }
    }
}
