﻿using AssocIS.Database.Entity.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AssocIS.Database.Entity
{
    public class Asset : LoggedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public int Count { get; set; }

        public long Flags { get; set; }

        public ISet<AssetCategory> Categories { get; set; }
        public ISet<AssetLoan> Loans { get; set; }
        public ISet<InventoryReport> InventoryReports { get; set; }

        public Asset()
        {
            Categories = new HashSet<AssetCategory>();
            Loans = new HashSet<AssetLoan>();
            InventoryReports = new HashSet<InventoryReport>();
        }
    }
}
