﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssocIS.Database.Entity
{
    [Table("Interview")]
    public class Interview
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public long InterviewLeaderId { get; set; }

        [ForeignKey("InterviewLeaderId")]
        public User InterviewLeader { get; set; }

        public DateTime At { get; set; }

        public string Note { get; set; }
    }
}
