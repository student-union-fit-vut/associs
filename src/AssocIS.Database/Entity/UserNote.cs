﻿namespace AssocIS.Database.Entity
{
    public class UserNote
    {
        public long Id { get; set; }

        public long AuthorId { get; set; }
        public User Author { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }

        public string Content { get; set; }
    }
}
