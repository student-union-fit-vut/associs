﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using AssocIS.Database.Entity;
using AssocIS.Database.Services;
using System;
using System.Linq;
using AssocIS.Database.Enums;

namespace AssocIS.Database;

public static class DatabaseExtensions
{
    public static IServiceCollection AddDatabase(this IServiceCollection services, Action<DbContextOptionsBuilder> builderAction)
    {
        services.AddDbContext<AppDbContext>(builderAction);

        return services
            .AddScoped<IAssocISRepository, AssocISRepository>();
    }

    public static void CreateDefaultUser(this IApplicationBuilder builder)
    {
        using var scope = builder.ApplicationServices.CreateScope();
        var repository = scope.ServiceProvider.GetService<IAssocISRepository>();

        var anyAdminExists = repository.User.GetUsersForSynchronizationQuery()
            .Any(o => o.State == Enums.UserState.Chairman || (o.Flags & (int)UserFlags.Admin) != 0);

        if (!anyAdminExists)
        {
            var user = new User()
            {
                Username = "admin",
                CreatedAt = DateTime.Now,
                Email = "admin@admin.com",
                JoinedAt = DateTime.Now,
                Name = "Administrator",
                Surname = "Admin",
                Password = BCrypt.Net.BCrypt.HashPassword("Admin123"),
                State = Enums.UserState.BasicMember,
                Flags = (int)UserFlags.Admin
            };

            repository.Add(user);
        }

        var anyClientExists = repository.Client.GetClientsQuery(null)
            .Any(o => o.ClientType == Enums.ClientType.WebClient || o.ClientId == "default");

        if (!anyClientExists)
        {
            var client = new Client()
            {
                CreatedAt = DateTime.Now,
                Name = "Default client for development",
                ClientId = "default",
                ClientType = Enums.ClientType.WebClient
            };

            repository.Add(client);
        }

        if(!anyAdminExists || !anyClientExists) repository.Commit();
    }
}
