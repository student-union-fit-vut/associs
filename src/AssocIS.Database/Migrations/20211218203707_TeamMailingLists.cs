﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AssocIS.Database.Migrations
{
    public partial class TeamMailingLists : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TeamMailingList_Teams_TeamId",
                table: "TeamMailingList");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TeamMailingList",
                table: "TeamMailingList");

            migrationBuilder.RenameTable(
                name: "TeamMailingList",
                newName: "TeamMailingLists");

            migrationBuilder.RenameIndex(
                name: "IX_TeamMailingList_TeamId",
                table: "TeamMailingLists",
                newName: "IX_TeamMailingLists_TeamId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TeamMailingLists",
                table: "TeamMailingLists",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TeamMailingLists_Teams_TeamId",
                table: "TeamMailingLists",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TeamMailingLists_Teams_TeamId",
                table: "TeamMailingLists");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TeamMailingLists",
                table: "TeamMailingLists");

            migrationBuilder.RenameTable(
                name: "TeamMailingLists",
                newName: "TeamMailingList");

            migrationBuilder.RenameIndex(
                name: "IX_TeamMailingLists_TeamId",
                table: "TeamMailingList",
                newName: "IX_TeamMailingList_TeamId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TeamMailingList",
                table: "TeamMailingList",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TeamMailingList_Teams_TeamId",
                table: "TeamMailingList",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
