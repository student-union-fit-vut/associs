﻿namespace AssocIS.Database.Enums
{
    /// <summary>
    /// Types of attendence at the meeting.
    /// </summary>
    public enum MeetingAttendanceType
    {
        /// <summary>
        /// Mandatory attendance at the meeting
        /// </summary>
        Required = 0,

        /// <summary>
        /// Optional attendance at the meeting
        /// </summary>
        Optional = 1,

        /// <summary>
        /// Mandatory attendance at the meeting for the expected.
        /// </summary>
        RequiredForExpected = 2
    }
}
