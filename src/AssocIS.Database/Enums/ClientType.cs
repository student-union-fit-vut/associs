﻿namespace AssocIS.Database.Enums
{
    /// <summary>
    /// Type of external application.
    /// </summary>
    public enum ClientType
    {
        /// <summary>
        /// Third party application.
        /// </summary>
        ThirdPartyApplication = 0,

        /// <summary>
        /// Web application (such as browser client, ...)
        /// </summary>
        WebClient = 1
    }
}
