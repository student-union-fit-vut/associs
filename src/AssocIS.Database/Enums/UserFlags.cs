﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssocIS.Database.Enums
{
    /// <summary>
    /// Internal user flags that cannot modify from UI/API.
    /// </summary>
    [Flags]
    public enum UserFlags
    {
        /// <summary>
        /// Member have full permissions. Overrides permissions and user state.
        /// </summary>
        Admin = 1,

        /// <summary>
        /// [GDPR] Member was anonymized.
        /// </summary>
        Anonymized = 2
    }
}
