﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssocIS.Database.Enums
{
    public enum AccessType
    {
        /// <summary>
        /// Some place, such as office, ...
        /// </summary>
        Area,

        /// <summary>
        /// Access to facebook (group, profile, ...)
        /// </summary>
        Facebook,

        /// <summary>
        /// Access to google services (GSuite, Calendar, ...)
        /// </summary>
        Google
    }
}
