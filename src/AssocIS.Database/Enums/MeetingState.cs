﻿namespace AssocIS.Database.Enums
{
    /// <summary>
    /// Meeting status.
    /// </summary>
    public enum MeetingState
    {
        /// <summary>
        /// Meeting was created but not notified.
        /// </summary>
        Drafted = 0,

        /// <summary>
        /// Users was notified about meeting. It cannot modify base properties. Can edit only report, attendance or cancel.
        /// </summary>
        Summoned = 1,

        /// <summary>
        /// Meeting was cancelled.
        /// </summary>
        Cancelled = 2
    }
}
