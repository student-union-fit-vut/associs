﻿using System;

namespace AssocIS.Database.Enums
{
    /// <summary>
    /// Special permissions for user.
    /// </summary>
    [Flags]
    public enum UserPermission
    {
        None = 0,

        /// <summary>
        /// User can work with users.
        /// </summary>
        UserManager = 1,

        /// <summary>
        /// User have full permissions on to-do lists.
        /// </summary>
        TodoListsManager = 2,

        /// <summary>
        /// User have full permissions on events.
        /// </summary>
        EventManager = 4,

        /// <summary>
        /// User have full permissions on meetings.
        /// </summary>
        MeetingManager = 8,

        /// <summary>
        /// User have full permissions on properties.
        /// </summary>
        PropertyManager = 16,

        /// <summary>
        /// User have full permissions on finances.
        /// </summary>
        FinanceManager = 32,

        /// <summary>
        /// User have full permissions on work items.
        /// </summary>
        WorkItemsManager = 64,

        /// <summary>
        /// User have full permissions on jobs.
        /// </summary>
        JobsManager = 128,

        /// <summary>
        /// User have full permissions on application management (diagnostics, ...)
        /// </summary>
        AppManager = 256
    }
}
