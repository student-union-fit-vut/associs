﻿namespace AssocIS.Database.Enums
{
    public enum TransactionType
    {
        /// <summary>
        /// Incoming transactions
        /// </summary>
        Incoming,

        /// <summary>
        /// Outgoing transactions
        /// </summary>
        Outgoing
    }
}
