﻿using System;

#pragma warning disable S2344 // Enumeration type names should not have "Flags" or "Enum" suffixes
namespace AssocIS.Database.Enums
{
    /// <summary>
    /// Flags defined in Asset.Flags property.
    /// </summary>
    [Flags]
    public enum AssetFlags
    {
        /// <summary>
        /// Unused
        /// </summary>
        None = 0,

        /// <summary>
        /// Loans are disabled for this item.
        /// </summary>
        LoansDisabled = 1,

        /// <summary>
        /// Item is removed and not available to loan.
        /// </summary>
        Removed = 2
    }
}
