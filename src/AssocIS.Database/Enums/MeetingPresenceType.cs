﻿namespace AssocIS.Database.Enums
{
    /// <summary>
    /// Types of presence of user at the meetings.
    /// </summary>
    public enum MeetingPresenceType
    {
        /// <summary>
        /// We dont know users presence.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Absent at the meeting.
        /// </summary>
        Absent = 1,

        /// <summary>
        /// Present at the meeting.
        /// </summary>
        Present = 2,

        /// <summary>
        /// Expected at the meeting.
        /// </summary>
        Expected = 3,

        /// <summary>
        /// Apologized for the meegting.
        /// </summary>
        Apologized = 4,

        /// <summary>
        /// Optional presence (for active collaborators)
        /// </summary>
        Optional = 5
    }
}
