﻿namespace AssocIS.Database.Enums
{
    /// <summary>
    /// User types.
    /// </summary>
    public enum UserState
    {
        /// <summary>
        /// Basic member.
        /// </summary>
        BasicMember,

        /// <summary>
        /// Honorary member.
        /// </summary>
        HonoraryMember,

        /// <summary>
        /// Chairman.
        /// </summary>
        Chairman,

        /// <summary>
        /// Vice-chairman.
        /// </summary>
        ViceChairman,

        /// <summary>
        /// Verified member. Applicable if user left association.
        /// </summary>
        VerifiedMember,

        /// <summary>
        /// Active collaborators
        /// </summary>
        ActiveCollaborator
    }
}
