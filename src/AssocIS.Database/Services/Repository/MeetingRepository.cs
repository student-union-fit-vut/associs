﻿using AssocIS.Database.Entity;
using AssocIS.Database.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class MeetingRepository : RepositoryBase
    {
        public MeetingRepository(AppDbContext context) : base(context)
        {
        }

        public Task<Meeting> FindMeetingByIdAsync(long id)
        {
            return Context.Meetings
                .Include(o => o.Presences)
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .Include(o => o.Agenda)
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public IQueryable<Meeting> GetMeetingsQuery(string nameQuery, DateTime? from, DateTime? to, List<MeetingAttendanceType> attendanceTypes, List<MeetingState> meetingStates,
            string agendaQuery, string reportQuery, long? authorId, string messageQuery)
        {
            var query = Context.Meetings
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .Include(o => o.Presences)
                .Include(o => o.Agenda)
                .AsQueryable();

            if (!string.IsNullOrEmpty(nameQuery))
                query = query.Where(o => o.Name.Contains(nameQuery));

            if (from != null)
                query = query.Where(o => o.At >= from.Value);

            if (to != null)
                query = query.Where(o => o.At < to.Value);

            if (attendanceTypes != null)
                query = query.Where(o => attendanceTypes.Contains(o.AttendanceType));

            if (meetingStates != null)
                query = query.Where(o => meetingStates.Contains(o.State));

            if (!string.IsNullOrEmpty(agendaQuery))
                query = query.Where(o => o.Agenda.Any(o => o.Content.Contains(agendaQuery)));

            if (!string.IsNullOrEmpty(reportQuery))
                query = query.Where(o => o.Report.Contains(reportQuery));

            if (authorId != null)
                query = query.Where(o => o.CreatedById.Value == authorId);

            if (messageQuery != null)
                query = query.Where(o => o.Message.Contains(messageQuery));

            return query.OrderBy(o => o.Id);
        }

        public IQueryable<Meeting> GetReportableMeetingsQuery()
        {
            return Context.Meetings
                .Include(o => o.Presences)
                .Include(o => o.Agenda)
                .Where(o => o.State == MeetingState.Summoned)
                .AsQueryable();
        }

        public IQueryable<User> GetExpectedUsersQuery(Meeting meeting)
        {
            var query = Context.Users
                .Where(o => (o.Flags & (long)UserFlags.Anonymized) == 0)
                .AsQueryable();

            if (meeting.AttendanceType == MeetingAttendanceType.RequiredForExpected || meeting.AttendanceType == MeetingAttendanceType.Optional)
            {
                var expectedUserIds = meeting.Presences.Select(o => o.UserId).ToArray();
                return query.Where(o => expectedUserIds.Contains(o.Id));
            }
            else if (meeting.AttendanceType == MeetingAttendanceType.Required)
            {
                var expectedUserIds = meeting.Presences.Select(o => o.UserId).ToArray();
                // User not left association or is expected.
                return query.Where(o => o.LeavedAt == null || o.LeavedAt > DateTime.Now || expectedUserIds.Contains(o.Id));
            }

            return query.Where(_ => false);
        }
    }
}
