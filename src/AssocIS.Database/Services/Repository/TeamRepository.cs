﻿using AssocIS.Database.Entity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class TeamRepository : RepositoryBase
    {
        public TeamRepository(AppDbContext context) : base(context)
        {
        }

        private IQueryable<Team> GetBaseQuery(bool includeMembers, bool includeMailingLists)
        {
            var query = Context.Teams.AsQueryable();

            if (includeMembers)
                query = query.Include(o => o.Members).ThenInclude(o => o.User);

            if (includeMailingLists)
                query = query.Include(o => o.GoogleGroups);

            return query;
        }

        public IQueryable<Team> GetTeamsQuery(string searchQuery, bool onlyEmptyTeams)
        {
            var query = GetBaseQuery(true, false);

            if (!string.IsNullOrEmpty(searchQuery))
                query = query.Where(o => o.Name.Contains(searchQuery));

            if (onlyEmptyTeams)
                query.Where(o => o.Members.Count == 0 || (o.Members.Count == 1 && o.Members.First().IsLeader));

            return query;
        }

        public Task<Team> FindTeamByIdAsync(long id)
        {
            return GetBaseQuery(true, true)
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public Task<bool> TeamExistsAsync(long id)
        {
            return GetBaseQuery(false, false)
                .AnyAsync(o => o.Id == id);
        }

        public IQueryable<TeamMember> GetTeamMembersQuery(long teamId)
        {
            return Context.TeamMembers
                .Include(o => o.User)
                .Where(o => o.TeamId == teamId);
        }

        public IQueryable<TeamMailingList> GetMailingListsQuery(long? teamId)
        {
            var query = Context.TeamMailingLists.AsQueryable();

            if (teamId != null)
                query = query.Where(o => o.TeamId == teamId);

            return query;
        }
    }
}
