﻿using AssocIS.Database.Entity;
using AssocIS.Database.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class UserRepository : RepositoryBase
    {
        public UserRepository(AppDbContext context) : base(context)
        {
        }

        private IQueryable<User> GetBaseQuery(bool includeRefreshTokens)
        {
            var query = Context.Users.AsSplitQuery()
                .Include(o => o.Teams).ThenInclude(o => o.Team)
                .AsQueryable();

            if (includeRefreshTokens)
                query = query.Include(o => o.RefreshTokens);

            return query;
        }

        public User FindUserByUsername(string username, bool includeRefreshTokens = false)
        {
            return GetBaseQuery(includeRefreshTokens)
                .SingleOrDefault(o => o.Username == username);
        }

        public Task<User> FindUserByUsernameAsync(string username, bool includeRefreshTokens = false)
        {
            return GetBaseQuery(includeRefreshTokens)
                .SingleOrDefaultAsync(o => o.Username == username);
        }

        public bool UserExists(long id)
        {
            return GetBaseQuery(false)
                .Any(o => o.Id == id);
        }

        public Task<bool> UserExistsAsync(long id)
        {
            return GetBaseQuery(false)
                .AnyAsync(o => o.Id == id);
        }

        public Task<User> FindUserByIdAsync(long id, bool includeProfilePic = false, bool includeRefreshTokens = false)
        {
            var query = GetBaseQuery(includeRefreshTokens)
                .Include(o => o.UserNotes)
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .Include(o => o.Interviews)
                .AsQueryable();

            if (includeProfilePic)
                query = query.Include(o => o.ProfileImage);

            return query
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public IQueryable<User> GetUsersQuery(string nameQuery, bool ignoreLeftUsers, bool ignoreTestPeriodUsers,
            bool ignoreAnonymized, List<UserState> states)
        {
            var query = GetBaseQuery(false);

            if (!string.IsNullOrEmpty(nameQuery))
                query = query.Where(o => o.Name.Contains(nameQuery) || o.Surname.Contains(nameQuery) || o.Username.Contains(nameQuery));

            if (ignoreLeftUsers)
                query = query.Where(o => o.LeavedAt == null || o.LeavedAt > DateTime.Now);

            if (ignoreTestPeriodUsers)
                query = query.Where(o => o.TestPeriodEnd == null || o.TestPeriodEnd > DateTime.Now);

            if (ignoreAnonymized)
                query = query.Where(o => (o.Flags & (long)UserFlags.Anonymized) == 0);

            if (states != null)
                query = query.Where(o => states.Contains(o.State));

            return query.OrderBy(o => o.Id);
        }

        public Task<ProfilePicture> FindProfilePictureByUserId(long userId)
        {
            return GetBaseQuery(false)
                .Include(o => o.ProfileImage)
                .Where(o => o.Id == userId)
                .Select(o => o.ProfileImage)
                .FirstOrDefaultAsync();
        }

        public IQueryable<User> GetUsersForSynchronizationQuery()
        {
            return GetBaseQuery(false);
        }

        public Task<User> FindUserByDiscordIdAsync(ulong discordId, bool includeRefreshTokens = false)
        {
            return GetBaseQuery(includeRefreshTokens)
                .FirstOrDefaultAsync(o => o.DiscordId == discordId.ToString());
        }
    }
}
