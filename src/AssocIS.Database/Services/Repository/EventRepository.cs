﻿using AssocIS.Database.Entity.Events;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class EventRepository : RepositoryBase
    {
        public EventRepository(AppDbContext context) : base(context)
        {
        }

        public IQueryable<Event> GetEventsListQuery(string nameQuery, bool excludeFuture, bool excludeOngoing, bool excludePast)
        {
            var query = Context.Events.AsQueryable();

            if (!string.IsNullOrEmpty(nameQuery))
                query = query.Where(o => o.Name.Contains(nameQuery));

            if (excludeFuture)
                query = query.Where(o => o.From <= DateTime.Now);

            if (excludeOngoing)
                query = query.Where(o => !(o.From >= DateTime.Now && ((o.To != null && o.To < DateTime.Now) || (new DateTime(o.From.Year, o.From.Month, o.From.Day, 23, 59, 59) < DateTime.Now))));

            if (excludePast)
                query = query.Where(o => o.From > DateTime.Now);

            return query
                .OrderByDescending(o => o.From);
        }

        public Task<Event> GetEventByIdAsync(long id, bool includeComments = false)
        {
            var query = Context.Events
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .AsQueryable();

            if (includeComments)
                query = query.Include(o => o.Comments);

            return query.FirstOrDefaultAsync(o => o.Id == id);
        }

        public Task<bool> CanUserManageEventAsync(long eventId, long userId)
        {
            return Context.Events.AsNoTracking().AnyAsync(o => o.Id == eventId && o.CreatedById == userId);
        }

        public Task<bool> EventExistsAsync(long id)
        {
            return Context.Events.AsNoTracking().AnyAsync(o => o.Id == id);
        }

        public Task<EventComment> GetCommentByIdAsync(long eventId, long commentId)
        {
            return Context.EventComments
                .Include(o => o.CreatedBy)
                .FirstOrDefaultAsync(o => o.EventId == eventId && o.Id == commentId);
        }

        public Task<EventComment> GetCommentByIdAsync(long commentId)
        {
            return Context.EventComments
                .Include(o => o.CreatedBy)
                .FirstOrDefaultAsync(o => o.Id == commentId);
        }

        public Task<bool> ExistsCommentAsync(long eventId, long commentId, long? userId)
        {
            var query = Context.EventComments
                .Where(o => o.EventId == eventId && o.Id == commentId);

            if (userId != null)
                query = query.Where(o => o.CreatedById == userId);

            return query.AnyAsync();
        }

        public IQueryable<EventComment> GetCommentsQuery(long eventId, long? authorId, string textQuery)
        {
            var query = Context.EventComments
                .Include(o => o.CreatedBy)
                .Where(o => o.EventId == eventId);

            if (authorId != null)
                query = query.Where(o => o.CreatedById == authorId);

            if (!string.IsNullOrEmpty(textQuery))
                query = query.Where(o => o.Text.Contains(textQuery));

            return query;
        }
    }
}
