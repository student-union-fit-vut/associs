﻿using AssocIS.Database.Entity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class ClientRepository : RepositoryBase
    {
        public ClientRepository(AppDbContext context) : base(context)
        {
        }

        public Client FindClientByClientId(string clientId)
        {
            return Context.Clients
                .FirstOrDefault(o => o.ClientId == clientId);
        }

        public Task<Client> FindClientByClientIdAsync(string clientId)
        {
            return Context.Clients
                .FirstOrDefaultAsync(o => o.ClientId == clientId);
        }

        public IQueryable<Client> GetClientsQuery(string nameSearch)
        {
            var query = Context.Clients.AsQueryable();

            if (!string.IsNullOrEmpty(nameSearch))
                query = query.Where(o => o.Name.Contains(nameSearch));

            return query;
        }

        public Task<Client> FindClientByIdAsync(long id)
        {
            return Context.Clients
                .FirstOrDefaultAsync(o => o.ID == id);
        }

        public Task<bool> ClientExistsAsync(long id)
        {
            return Context.Clients
                .AnyAsync(o => o.ID == id);
        }

        public Task<bool> ClientExistsAsync(string clientId)
        {
            return Context.Clients
                .AnyAsync(o => o.ClientId == clientId);
        }
    }
}
