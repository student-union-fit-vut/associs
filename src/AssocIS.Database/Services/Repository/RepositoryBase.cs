﻿namespace AssocIS.Database.Services.Repository
{
    public abstract class RepositoryBase
    {
        protected AppDbContext Context { get; }

        protected RepositoryBase(AppDbContext context)
        {
            Context = context;
        }
    }
}
