﻿using AssocIS.Database.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class AuthRepository : RepositoryBase
    {
        public AuthRepository(AppDbContext context) : base(context)
        {
        }

        public Task<RefreshToken> FindRefreshTokenAsync(string refreshToken)
        {
            return Context.RefreshTokens
                .Include(o => o.User)
                .SingleOrDefaultAsync(o => o.Token == refreshToken && o.ExpiresAt > DateTime.Now);
        }

        public IQueryable<RefreshToken> FindExpiredTokens()
        {
            return Context.RefreshTokens
                .Where(o => o.ExpiresAt <= DateTime.Now);
        }

        public IQueryable<RefreshToken> FindRefreshTokensOfUser(long userId, bool onlyExpired = false)
        {
            if(onlyExpired)
                return FindExpiredTokens().Where(o => o.UserId == userId);

            return Context.RefreshTokens.Where(o => o.UserId == userId);
        }
    }
}
