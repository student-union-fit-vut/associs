﻿using AssocIS.Database.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class InterviewRepository : RepositoryBase
    {
        public InterviewRepository(AppDbContext context) : base(context)
        {
        }

        private IQueryable<Interview> GetBaseQuery()
        {
            return Context.Interviews.AsQueryable()
                .Include(o => o.InterviewLeader)
                .Include(o => o.User);
        }

        public IQueryable<Interview> GetInterviewsQuery(long? userId, long? leaderId, DateTime? from, DateTime? to)
        {
            var query = GetBaseQuery();

            if (userId != null)
                query = query.Where(o => o.UserId == userId.Value);

            if (leaderId != null)
                query = query.Where(o => o.InterviewLeaderId == leaderId.Value);

            if (from != null)
                query = query.Where(o => o.At >= from.Value);

            if (to != null)
                query = query.Where(o => o.At < to.Value);

            return query
                .OrderByDescending(o => o.Id);
        }

        public Task<Interview> GetInterviewByIdAsync(long id)
        {
            return GetBaseQuery()
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public Task<bool> ExistsAsync(long id)
        {
            return Context.Interviews.AsQueryable()
                .AnyAsync(o => o.Id == id);
        }
    }
}
