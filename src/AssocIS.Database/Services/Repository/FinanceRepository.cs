﻿using AssocIS.Database.Entity.Finance;
using AssocIS.Database.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class FinanceRepository : RepositoryBase
    {
        public FinanceRepository(AppDbContext context) : base(context)
        {
        }

        public IQueryable<TransactionCategory> GetCategoriesQuery()
        {
            return Context.TransactionCategories
                .OrderBy(o => o.Id)
                .AsQueryable();
        }

        public IQueryable<Tuple<TransactionCategory, int>> GetCategoriesQueryWithCount()
        {
            return GetCategoriesQuery()
                .Select(o => Tuple.Create(o, o.Transactions.Count))
                .AsQueryable();
        }

        public Task<TransactionCategory> GetCategoryByIdAsync(long id)
        {
            return Context.TransactionCategories
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public Task<int> GetTransactionCountInCategoryAsync(long categoryId)
        {
            return Context.TransactionCategories
                .AsNoTracking()
                .Where(o => o.Id == categoryId)
                .Select(o => o.Transactions.Count)
                .FirstOrDefaultAsync();
        }

        public IQueryable<Supplier> GetSuppliersQuery(string nameQuery)
        {
            var query = Context.Suppliers
                .OrderBy(o => o.Id)
                .AsQueryable();

            if (!string.IsNullOrEmpty(nameQuery))
                query = query.Where(o => o.Name.Contains(nameQuery));

            return query;
        }

        public Task<Supplier> GetSupplierByIdAsync(long id)
        {
            return Context.Suppliers
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public Task<bool> AnyTransactionOfSupplierAsync(long id)
        {
            return Context.Transactions.AnyAsync(o => o.SupplierId == id);
        }

        public Task<bool> SupplierExistsAsync(long id)
        {
            return Context.Suppliers.AnyAsync(o => o.Id == id);
        }

        public Task<Account> GetAccountByIdAsync(long id)
        {
            return Context.Accounts
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public IQueryable<Account> GetAccountsQuery(string nameQuery, string accountNumberQuery, string bankCode)
        {
            var query = Context.Accounts
                .AsQueryable();

            if (!string.IsNullOrEmpty(nameQuery))
                query = query.Where(o => o.Name.Contains(nameQuery));

            if (!string.IsNullOrEmpty(accountNumberQuery))
                query = query.Where(o => o.AccountNumber.StartsWith(accountNumberQuery));

            if (!string.IsNullOrEmpty(bankCode))
                query = query.Where(o => o.BankCode == bankCode);

            return query
                .OrderBy(o => o.Id);
        }

        public Task<int> GetTransactionCountAtAccountAsync(long id)
        {
            return Context.Transactions.CountAsync(o => o.AccountId == id);
        }

        public async Task<Tuple<int, decimal>> CalculateAccountStateAsync(long id)
        {
            var query = Context.Transactions
                .Where(o => o.AccountId == id);

            var count = await query.CountAsync();
            var state = await query.SumAsync(o => o.Type == TransactionType.Outgoing ? -o.Amount : o.Amount);

            return Tuple.Create(count, state);
        }

        public Task<bool> AnyTransactionOnAccountAsync(long id)
        {
            return Context.Transactions.AnyAsync(o => o.AccountId == id);
        }

        public Task<bool> AccountExistsAsync(long id)
        {
            return Context.Accounts.AnyAsync(o => o.Id == id);
        }

        public IQueryable<Transaction> GetTransactionsQuery() => GetTransactionsQuery(null, null, null, null, null, null, null, null, null, false);

        public IQueryable<Transaction> GetTransactionsQuery(DateTime? processedFrom, DateTime? processedTo, decimal? amountFrom, decimal? amountTo, TransactionType[] transactionTypes,
            string nameQuery, long[] accountIds, long[] supplierIds, long[] categoryIds, bool onlyWithAttachments)
        {
            var query = Context.Transactions
                .Include(o => o.Account)
                .Include(o => o.Supplier)
                .Include(o => o.Categories)
                .AsQueryable();

            if (processedFrom != null)
                query = query.Where(o => o.ProcessedAt >= processedFrom.Value);

            if (processedTo != null)
                query = query.Where(o => o.ProcessedAt < processedTo.Value);

            if (amountFrom != null)
                query = query.Where(o => o.Amount >= amountFrom.Value);

            if (amountTo != null)
                query = query.Where(o => o.Amount < amountTo.Value);

            if (transactionTypes != null)
                query = query.Where(o => transactionTypes.Contains(o.Type));

            if (nameQuery != null)
                query = query.Where(o => o.Name.Contains(nameQuery));

            if (accountIds != null)
                query = query.Where(o => accountIds.Contains(o.AccountId));

            if (supplierIds != null)
                query = query.Where(o => supplierIds.Contains(o.SupplierId.Value));

            if (categoryIds != null)
                query = query.Where(o => o.Categories.Any(x => categoryIds.Contains(x.Id)));

            if (onlyWithAttachments)
                query = query.Where(o => o.Attachments.Count > 0);

            return query.OrderByDescending(o => o.Id);
        }

        public async Task<Transaction> GetTransactionByIdAsync(long id, long? userId = null)
        {
            var query = Context.Transactions
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .Include(o => o.Account)
                .Include(o => o.Supplier)
                .Include(o => o.Categories)
                .AsQueryable();

            if (userId != null)
                query = query.Where(o => o.CreatedById == userId.Value);

            var entity = await query.FirstOrDefaultAsync(o => o.Id == id);
            if (entity == null)
                return null;

            entity.Attachments = Context.TransactionAttachments
                .Where(o => o.TransactionId == id)
                .Select(TransactionAttachment.GetExpressionWithoutData())
                .ToHashSet();

            return entity;
        }

        public Task<TransactionAttachment> GetTransactionAttachmentByIdAsync(long transactionId, long attachmentId, long? userId, bool includes, bool noContent)
        {
            var query = Context.TransactionAttachments
                .Where(o => o.Transaction.Id == transactionId && o.Id == attachmentId);

            if (userId != null)
                query = query.Where(o => o.Transaction.CreatedById == userId);

            if (includes)
            {
                query = query
                    .Include(o => o.CreatedBy)
                    .Include(o => o.LastModifiedBy);
            }

            if (noContent)
                query = query.Select(TransactionAttachment.GetExpressionWithoutData());

            return query.FirstOrDefaultAsync();
        }

        public Task<bool> TransactionCategoryNameExistsAsync(string name)
        {
            return Context.TransactionCategories.AsNoTracking()
                .AnyAsync(o => o.Name == name);
        }
    }
}
