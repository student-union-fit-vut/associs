﻿using AssocIS.Database.Entity;
using AssocIS.Database.Enums;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class AssetRepository : RepositoryBase
    {
        public AssetRepository(AppDbContext context) : base(context)
        {
        }

        public IQueryable<AssetCategory> GetAssetCategoriesQuery()
        {
            return Context.AssetCategories
                .OrderBy(o => o.Id)
                .AsQueryable();
        }

        public Task<AssetCategory> GetAssetCategoryAsync(long id)
        {
            return GetAssetCategoriesQuery()
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public Task<Asset> GetAssetAsync(long id, bool @readonly = false)
        {
            var query = Context.Assets
                .Include(o => o.Categories)
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .Include(o => o.Loans)
                .AsQueryable();

            if (@readonly)
                query = query.AsNoTracking();

            return query
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public IQueryable<Asset> GetAssetsQuery(string nameQuery, bool? isAvailable, List<long> categories, bool includeRemoved)
        {
            var query = Context.Assets
                .Include(o => o.Categories)
                .Include(o => o.Loans)
                .AsQueryable();

            if (!string.IsNullOrEmpty(nameQuery))
                query = query.Where(o => o.Name.Contains(nameQuery));

            if (isAvailable != null)
            {
                if (isAvailable == true)
                    query = query.Where(o => (o.Flags & (long)(AssetFlags.LoansDisabled | AssetFlags.Removed)) == 0 && o.Loans.Count < o.Count);
                else
                    query = query.Where(o => (o.Flags & (long)AssetFlags.LoansDisabled) != 0 || o.Loans.Count >= o.Count || (o.Flags & (long)AssetFlags.Removed) != 0);
            }

            if (categories?.Count > 0)
                query = query.Where(o => o.Categories.Any(x => categories.Contains(x.Id)));

            if (!includeRemoved)
                query = query.Where(o => (o.Flags & (long)AssetFlags.Removed) == 0);

            return query.OrderBy(o => o.Id);
        }

        public Task<AssetLoan> GetAssetLoanAsync(long id)
        {
            return Context.AssetLoans
                .Include(o => o.Asset)
                .ThenInclude(o => o.Categories)
                .Include(o => o.User)
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public IQueryable<AssetLoan> GetLoansQuery(long assetId, bool onlyActive, long? ofUser)
        {
            var query = Context.AssetLoans
                .Include(o => o.Asset)
                .ThenInclude(o => o.Categories)
                .Include(o => o.User)
                .Where(o => o.AssetId == assetId)
                .AsQueryable();

            if (onlyActive)
                query = query.Where(o => o.ReturnedAt == null);

            if (ofUser != null)
                query = query.Where(o => o.UserId == ofUser.Value);

            return query.OrderBy(o => o.Id);
        }

        public IQueryable<InventoryReport> GetInventoryReportsQuery(long assetId, long? userId)
        {
            var query = Context.InventoryReports
                .Include(o => o.User)
                .Where(o => o.AssetId == assetId)
                .AsQueryable();

            if (userId != null)
                query = query.Where(o => o.UserId == userId);

            return query.OrderBy(o => o.Id);
        }

        public Task<bool> AssetCategoryNameExistsAsync(string name)
        {
            return Context.AssetCategories.AsNoTracking()
                .AnyAsync(o => o.Name == name);
        }
    }
}
