﻿using AssocIS.Database.Entity;
using AssocIS.Database.Enums;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services.Repository
{
    public class AccessRepository : RepositoryBase
    {
        public AccessRepository(AppDbContext context) : base(context)
        {
        }

        private IQueryable<AccessDefinition> GetBaseQuery(bool includeMembers = false)
        {
            var query = Context.AccessDefinitions.AsQueryable()
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .AsQueryable();

            if (includeMembers)
                query = query.Include(o => o.Members);

            return query;
        }

        private IQueryable<AccessMember> GetAccessMembersBaseQuery(bool includeAccess)
        {
            var query = Context.AccessMembers.AsQueryable()
                .Include(o => o.CreatedBy)
                .Include(o => o.LastModifiedBy)
                .Include(o => o.User)
                .AsQueryable();

            if (includeAccess)
                query = query.Include(o => o.AccessDefinition);

            return query;
        }

        public Task<AccessDefinition> GetAccessDefinitionByIdAsync(long id, bool includeMembers = false)
        {
            return GetBaseQuery(includeMembers)
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public IQueryable<AccessDefinition> GetAccessDefinitionsQuery(AccessType? accessType, long? createdById, string descriptionContains)
        {
            var query = GetBaseQuery();

            if (accessType != null)
                query = query.Where(o => o.Type == accessType.Value);

            if (createdById != null)
                query = query.Where(o => o.CreatedById == createdById.Value);

            if (descriptionContains != null)
                query = query.Where(o => o.Description.Contains(descriptionContains));

            return query
                .OrderBy(o => o.Id);
        }

        public Task<bool> AccessDefinitionExistsAsync(long id)
        {
            return GetBaseQuery()
                .AnyAsync(o => o.Id == id);
        }

        public IQueryable<AccessMember> GetAccessMembersQuery(long id, bool includeAccess = false)
        {
            return GetAccessMembersBaseQuery(includeAccess)
                .Where(o => o.AccessDefinitionId == id);
        }

        public Task<AccessMember> GetAccessMemberByIdAsync(long accessId, long userId)
        {
            return GetAccessMembersQuery(accessId)
                .FirstOrDefaultAsync(o => o.UserId == userId);
        }

        public Task<bool> AccessMemberExistsAsync(long accessId, long userId)
        {
            return GetAccessMembersBaseQuery(false)
                .AnyAsync(o => o.AccessDefinitionId == accessId && o.UserId == userId);
        }
    }
}
