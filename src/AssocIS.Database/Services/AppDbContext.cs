﻿using Microsoft.EntityFrameworkCore;
using AssocIS.Database.Entity;
using AssocIS.Database.Entity.Finance;
using AssocIS.Database.Entity.Events;
using System;

namespace AssocIS.Database.Services
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(builder =>
            {
                builder.HasOne(o => o.ProfileImage);

                builder
                    .HasMany(o => o.UserNotes)
                    .WithOne(o => o.User);

                builder
                    .HasOne(o => o.CreatedBy)
                    .WithMany()
                    .HasForeignKey(o => o.CreatedById);

                builder
                    .HasOne(o => o.LastModifiedBy)
                    .WithMany()
                    .HasForeignKey(o => o.LastModifiedById);

                builder
                    .HasMany(o => o.RefreshTokens)
                    .WithOne(o => o.User);

                builder
                    .HasMany(o => o.MeetingPresences)
                    .WithOne(o => o.User);

                builder
                    .HasIndex(o => o.DiscordId)
                    .IsUnique();
            });

            modelBuilder.Entity<TeamMember>(builder =>
            {
                builder
                    .HasOne(o => o.Team)
                    .WithMany(o => o.Members);

                builder
                    .HasOne(o => o.User)
                    .WithMany(o => o.Teams);

                builder
                    .HasKey(o => new { o.UserId, o.TeamId });
            });

            modelBuilder.Entity<Interview>(builder =>
            {
                builder
                    .HasOne(o => o.InterviewLeader);

                builder
                    .HasOne(o => o.User)
                    .WithMany(o => o.Interviews);
            });

            modelBuilder.Entity<Client>(builder =>
            {
                builder
                    .HasIndex(o => o.ClientId)
                    .IsUnique();
            });

            modelBuilder.Entity<MeetingPresence>(builder => builder.HasKey(o => new { o.MeetingId, o.UserId }));

            modelBuilder.Entity<Meeting>(builder =>
            {
                builder.HasMany(o => o.Presences).WithOne(o => o.Meeting);

                builder
                    .HasOne(o => o.CreatedBy)
                    .WithMany()
                    .HasForeignKey(o => o.CreatedById)
                    .IsRequired();

                builder
                    .HasOne(o => o.LastModifiedBy)
                    .WithMany()
                    .HasForeignKey(o => o.LastModifiedById);

                builder
                    .HasMany(o => o.Agenda)
                    .WithOne(o => o.Meeting);
            });

            modelBuilder.Entity<Asset>(builder =>
            {
                builder
                    .HasMany(o => o.Categories)
                    .WithMany(o => o.Assets);

                builder
                    .HasMany(o => o.InventoryReports)
                    .WithOne(o => o.Asset);

                builder
                    .HasMany(o => o.Loans)
                    .WithOne(o => o.Asset);

                builder
                    .HasOne(o => o.CreatedBy)
                    .WithMany()
                    .HasForeignKey(o => o.CreatedById)
                    .IsRequired();

                builder
                    .HasOne(o => o.LastModifiedBy)
                    .WithMany()
                    .HasForeignKey(o => o.LastModifiedById);
            });

            modelBuilder.Entity<AssetLoan>(builder => builder.HasOne(o => o.User).WithMany());
            modelBuilder.Entity<InventoryReport>(builder => builder.HasOne(o => o.User).WithMany());

            modelBuilder.Entity<AccessDefinition>(builder =>
            {
                builder
                    .HasMany(o => o.Members)
                    .WithOne(o => o.AccessDefinition);

                builder
                    .HasOne(o => o.CreatedBy)
                    .WithMany()
                    .HasForeignKey(o => o.CreatedById);

                builder
                    .HasOne(o => o.LastModifiedBy)
                    .WithMany()
                    .HasForeignKey(o => o.LastModifiedById);
            });

            modelBuilder.Entity<AccessMember>(builder =>
            {
                builder
                    .HasKey(o => new { o.AccessDefinitionId, o.UserId });

                builder
                    .HasOne(o => o.User)
                    .WithMany(o => o.Accesses);

                builder
                    .HasOne(o => o.CreatedBy)
                    .WithMany()
                    .HasForeignKey(o => o.CreatedById);

                builder
                    .HasOne(o => o.LastModifiedBy)
                    .WithMany()
                    .HasForeignKey(o => o.LastModifiedById);
            });

            modelBuilder.Entity<Transaction>(builder =>
            {
                builder
                    .HasOne(o => o.Account)
                    .WithMany(o => o.Transactions);

                builder
                    .HasOne(o => o.Supplier)
                    .WithMany(o => o.Transactions);

                builder
                    .HasMany(o => o.Categories)
                    .WithMany(o => o.Transactions);

                builder
                    .HasMany(o => o.Attachments)
                    .WithOne(o => o.Transaction);

                builder
                    .HasOne(o => o.CreatedBy)
                    .WithMany()
                    .HasForeignKey(o => o.CreatedById);

                builder
                    .HasOne(o => o.LastModifiedBy)
                    .WithMany()
                    .HasForeignKey(o => o.LastModifiedById);
            });

            modelBuilder.Entity<TransactionCategory>(builder => builder.HasIndex(o => o.Name).IsUnique());

            modelBuilder.Entity<Event>(builder =>
            {
                builder
                    .HasMany(o => o.Comments)
                    .WithOne(o => o.Event);

                builder
                    .HasOne(o => o.CreatedBy)
                    .WithMany()
                    .HasForeignKey(o => o.CreatedById);

                builder
                    .HasOne(o => o.LastModifiedBy)
                    .WithMany()
                    .HasForeignKey(o => o.LastModifiedById);
            });

            modelBuilder.Entity<EventComment>(builder =>
            {
                builder
                    .HasOne(o => o.CreatedBy)
                    .WithMany()
                    .HasForeignKey(o => o.CreatedById);

                builder
                    .HasOne(o => o.LastModifiedBy)
                    .WithMany()
                    .HasForeignKey(o => o.LastModifiedById);
            });

            modelBuilder.Entity<TeamMailingList>(builder => builder.HasOne(o => o.Team).WithMany(o => o.GoogleGroups));

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<ProfilePicture> ProfilePictures { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<TeamMember> TeamMembers { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<AssetCategory> AssetCategories { get; set; }
        public DbSet<AssetLoan> AssetLoans { get; set; }
        public DbSet<InventoryReport> InventoryReports { get; set; }
        public DbSet<Interview> Interviews { get; set; }
        public DbSet<AccessDefinition> AccessDefinitions { get; set; }
        public DbSet<AccessMember> AccessMembers { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<TransactionCategory> TransactionCategories { get; set; }
        public DbSet<TransactionAttachment> TransactionAttachments { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventComment> EventComments { get; set; }
        public DbSet<TeamMailingList> TeamMailingLists { get; set; }
    }
}
