﻿using AssocIS.Database.Services.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AssocIS.Database.Services
{
    public interface IAssocISRepository
    {
        UserRepository User { get; }
        ClientRepository Client { get; }
        AuthRepository Auth { get; }
        TeamRepository Team { get; }
        MeetingRepository Meeting { get; }
        AssetRepository Assets { get; }
        InterviewRepository Interview { get; }
        AccessRepository Access { get; }
        FinanceRepository Finance { get; }
        EventRepository Event { get; }

        /// <summary>
        /// Adds new entity to EF tracking.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        void Add<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Adds new entity to EF tracking.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task AddAsync<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Removes entity.
        /// </summary>
        void Remove<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Removes collection of entities.
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="entities">Entity collection</param>
        void RemoveCollection<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;

        /// <summary>
        /// Saves transaction.
        /// </summary>
        /// <returns></returns>
        Task CommitAsync();

        /// <summary>
        /// Saves transaction.
        /// </summary>
        void Commit();
    }
}
