﻿using AssocIS.Database.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Database.Services
{
    public class AssocISRepository : IAssocISRepository
    {
        private AppDbContext Context { get; }
        private List<RepositoryBase> Repositories { get; set; }

        public AssocISRepository(AppDbContext context)
        {
            Context = context;
            Repositories = new List<RepositoryBase>();
        }

        public UserRepository User => FindOrCreateRepository<UserRepository>();
        public ClientRepository Client => FindOrCreateRepository<ClientRepository>();
        public AuthRepository Auth => FindOrCreateRepository<AuthRepository>();
        public TeamRepository Team => FindOrCreateRepository<TeamRepository>();
        public MeetingRepository Meeting => FindOrCreateRepository<MeetingRepository>();
        public AssetRepository Assets => FindOrCreateRepository<AssetRepository>();
        public InterviewRepository Interview => FindOrCreateRepository<InterviewRepository>();
        public AccessRepository Access => FindOrCreateRepository<AccessRepository>();
        public FinanceRepository Finance => FindOrCreateRepository<FinanceRepository>();
        public EventRepository Event => FindOrCreateRepository<EventRepository>();

        private TRepository FindOrCreateRepository<TRepository>() where TRepository : RepositoryBase
        {
            var repository = Repositories.OfType<TRepository>().FirstOrDefault();

            if(repository == null)
            {
                repository = (TRepository)Activator.CreateInstance(typeof(TRepository), new[] { Context });
                Repositories.Add(repository);
            }

            return repository;
        }

        #region Misc

        public void Add<TEntity>(TEntity entity) where TEntity : class
        {
            Context.Set<TEntity>().Add(entity);
        }

        public Task AddAsync<TEntity>(TEntity entity) where TEntity : class
        {
            return Context.Set<TEntity>().AddAsync(entity).AsTask();
        }

        public void Remove<TEntity>(TEntity entity) where TEntity : class
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public void RemoveCollection<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            entities = entities.Where(o => o != null);

            if (!entities.Any())
                return;

            Context.Set<TEntity>().RemoveRange(entities);
        }

        public Task CommitAsync()
        {
            return Context.SaveChangesAsync();
        }

        public void Commit()
        {
            Context.SaveChanges();
        }

        #endregion
    }
}
