﻿using NSwag.Annotations;
using System;

namespace AssocIS.Common.Attributes
{
    public class ApiOperationAttribute : OpenApiOperationAttribute
    {
        public ApiOperationAttribute(string operationId) : base(operationId)
        {
        }

        public ApiOperationAttribute(string summary, string description) : base(summary, description)
        {
        }

        public ApiOperationAttribute(string operationId, string summary, string description) : base(operationId, summary, description)
        {
        }

        public ApiOperationAttribute(Type controller, string methodName) : base(controller.Name + "_" + methodName)
        {
        }
    }
}
