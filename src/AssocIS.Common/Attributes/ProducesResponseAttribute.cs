﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace AssocIS.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ProducesResponseAttribute : ProducesResponseTypeAttribute
    {
        public ProducesResponseAttribute(int statusCode) : base(statusCode)
        {
        }

        public ProducesResponseAttribute(Type type, int statusCode) : base(type, statusCode)
        {
        }

        public ProducesResponseAttribute(Type type, HttpStatusCode statusCode) : base(type, (int)statusCode)
        {
        }

        public ProducesResponseAttribute(HttpStatusCode statusCode) : base((int)statusCode)
        {
        }
    }
}
