﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace AssocIS.Common.Helpers
{
    public static class StringHelpers
    {
        /// <summary>
        /// Taken from GrillBot OpenSource repository from same author.
        /// Source: https://github.com/Misha12/GrillBot/blob/master/src/Grillbot/Helpers/StringHelper.cs#L9
        /// </summary>
        public static string CreateRandomString(int length)
        {
            if (length == 0) return "";

            const string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890@#!_-/";
            var str = new StringBuilder();
            var random = new Random();

            int randomValue;
            for (int i = 0; i < length; i++)
            {
                randomValue = random.Next(0, alphabet.Length);
                str.Append(alphabet[randomValue]);
            }

            return str.ToString();
        }

        /// <summary>
        /// Creates safe string to password usage.
        /// </summary>
        public static string CreateSafeString(int bytesCount)
        {
            using var rng = RandomNumberGenerator.Create();

            var bytes = new byte[bytesCount];
            rng.GetBytes(bytes);

            return Convert.ToBase64String(bytes);
        }
    }
}
