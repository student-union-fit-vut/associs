﻿using System;

namespace AssocIS.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime GetEndOfDay(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59);
        }
    }
}
