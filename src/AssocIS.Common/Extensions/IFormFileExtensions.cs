﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace AssocIS.Common.Extensions
{
    public static class IFormFileExtensions
    {
        public static bool IsImage(this IFormFile file)
        {
            var imageContentTypes = new[]
            {
                "image/jpg",
                "image/jpeg",
                "image/pjpeg",
                "image/gif",
                "image/x-png",
                "image/png"
            };

            return imageContentTypes.Any(o => string.Equals(o, file.ContentType, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
