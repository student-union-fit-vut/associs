﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AssocIS.Common.Extensions
{
    /// <summary>
    /// Extensions for string type.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Checks if template contains environment information and replaces it.
        /// Format of variables in Template is `{PropertyName}`.
        /// </summary>
        /// <see cref="Environment"/>
        public static string TryFillEnvironment(this string template)
        {
            if (string.IsNullOrEmpty(template))
                return null;

            var values = typeof(Environment)
                .GetProperties(BindingFlags.Public | BindingFlags.Static)
                .ToDictionary(o => o.Name, o => o.GetValue(null, null)?.ToString());

            foreach (var value in values)
            {
                template = template.Replace($"{value.Key}", value.Value);
            }

            return template;
        }

        /// <summary>
        /// Splits big string into multiple smaller strings.
        /// </summary>
        /// <remarks>
        /// Taken from GrillBot project from same author:
        /// https://github.com/Misha12/GrillBot/blob/master/src/Grillbot/Extensions/StringExtensions.cs#L9
        /// </remarks>
        public static IEnumerable<string> SplitInParts(this string str, int partLength)
        {
            if (str == null)
                throw new ArgumentNullException(nameof(str));
            if (partLength <= 0)
                throw new ArgumentException("Part length has to be positive.", nameof(partLength));

            return SplitParts(str, partLength);
        }

        /// <summary>
        /// Splits big string into multiple smaller strings.
        /// </summary>
        /// <remarks>
        /// Taken from GrillBot project from same author:
        /// https://github.com/Misha12/GrillBot/blob/master/src/Grillbot/Extensions/StringExtensions.cs#L19
        /// </remarks>
        public static IEnumerable<string> SplitParts(this string s, int partLength)
        {
            for (var i = 0; i < s.Length; i += partLength)
                yield return s[i..Math.Min(partLength, s.Length - i)];
        }
    }
}
