﻿using System;
using System.Collections.Generic;

namespace AssocIS.Common.Extensions
{
    public static class ExceptionExtensions
    {
        /// <summary>
        /// Gets all messages from exception and inner exceptions.
        /// </summary>
        /// <param name="exception">Root exception.</param>
        public static List<string> GetExceptionMessages(this Exception exception)
        {
            var messages = new List<string>();

            Exception ex = exception;
            while (ex != null)
            {
                messages.Add(ex.Message);
                ex = ex.InnerException;
            }

            return messages;
        }
    }
}
