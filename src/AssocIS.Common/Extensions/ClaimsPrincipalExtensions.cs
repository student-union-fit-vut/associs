﻿using System;
using System.Security.Claims;

namespace AssocIS.Common.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        /// <summary>
        /// Gets user ID from user identity.
        /// </summary>
        /// <param name="claimsPrincipal">Identity</param>
        public static long GetUserId(this ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal == null)
                return 0;

            var claim = claimsPrincipal.FindFirst(ClaimTypes.NameIdentifier);
            return claim == null ? 0 : Convert.ToInt64(claim.Value);
        }
    }
}
