using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AssocIS.Database;
using AssocIS.Database.Services;
using System.Linq;
using Microsoft.AspNetCore.HttpOverrides;
using AssocIS.Main.Services;
using AssocIS.Main.Infrastructure;
using AssocIS.Main.Services.External;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;

namespace AssocIS.Main
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("Default");

            services
                .AddConfiguration(Configuration)
                .AddDatabase(builder => builder.UseNpgsql(connectionString))
                .AddOpenApi()
                .AddAuth(Configuration)
                .AddLocalization(opt => opt.ResourcesPath = "")
                .AddCors()
                .AddHttpClient()
                .AddMemoryCache()
                .AddApiVersioning(setup =>
                {
                    setup.ApiVersionReader = new UrlSegmentApiVersionReader();
                    setup.DefaultApiVersion = new ApiVersion(1, 0);
                    setup.AssumeDefaultVersionWhenUnspecified = true;
                })
                .AddExternalServices()
                .AddQuartz();

            services
                .AddFeatures()
                .AddControllers()
                .AddNewtonsoftJson();

            services.Configure<ForwardedHeadersOptions>(opt => opt.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedHost);
            services.AddHealthChecks().AddNpgSql(connectionString);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, AppDbContext context)
        {
            if (context.Database.GetPendingMigrations().Any())
                context.Database.Migrate();

            app.CreateDefaultUser();
            app.UseExceptionHandler(new ExceptionHandlerOptions() { ExceptionHandler = new JsonExceptionMiddleware().InvokeAsync });

            app
                .UseHealthChecks("/health")
                .UseForwardedHeaders()
                .UseDefaultFiles()
                .UseStaticFiles()
                .UseCors(o => o.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader())
                .UseSwagger(Configuration)
                .UseLocalization(Configuration)
                .UseRouting()
                .UseAuthentication()
                .UseAuthorization()
                .UseMiddleware<RequestsCounterMiddleware>()
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                    endpoints.MapFallbackToFile("/index.html");
                });
        }
    }
}
