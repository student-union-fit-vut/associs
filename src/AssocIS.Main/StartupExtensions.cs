﻿using AssocIS.Data.Infrastructure;
using AssocIS.Main.Infrastructure;
using AssocIS.Main.Infrastructure.OpenApi;
using AssocIS.Main.Models.Configuration;
using AssocIS.Main.Models.Configuration.External.Discord;
using AssocIS.Main.Models.Configuration.External.Google;
using AssocIS.Main.Services;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.External;
using Discord.Rest;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using NSwag;
using NSwag.AspNetCore;
using NSwag.Generation.AspNetCore;
using NSwag.Generation.Processors.Security;
using Quartz;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace AssocIS.Main
{
    public static class StartupExtensions
    {
        public static IServiceCollection AddOpenApi(this IServiceCollection services)
        {
            services
                .AddVersionedApiExplorer(o =>
                {
                    o.GroupNameFormat = "'v'VVV";
                    o.SubstituteApiVersionInUrl = true;
                })
                .AddOpenApiDocument(doc => SetupOpenapiDocument(doc, "v1", "AssocIS", "Association Information System"));

            return services;
        }

        private static void SetupOpenapiDocument(AspNetCoreOpenApiDocumentGeneratorSettings doc, string version, string title, string description)
        {
            doc.DocumentName = version;
            doc.ApiGroupNames = new[] { version };

            doc.AddSecurity(JwtBearerDefaults.AuthenticationScheme, new OpenApiSecurityScheme()
            {
                BearerFormat = "JWT",
                Description = "JWT Authentication token",
                Name = "JWT",
                Scheme = JwtBearerDefaults.AuthenticationScheme,
                Type = OpenApiSecuritySchemeType.Http,
                In = OpenApiSecurityApiKeyLocation.Header
            });

            doc.OperationProcessors.Add(new CultureOperationProcessor());
            doc.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor(JwtBearerDefaults.AuthenticationScheme));

            doc.PostProcess = document =>
            {
                document.Info = new OpenApiInfo
                {
                    Title = title,
                    Description = description,
                    Version = version,

                    License = new OpenApiLicense
                    {
                        Name = "MIT",
                        Url = "https://opensource.org/licenses/MIT"
                    }
                };
            };
        }

        public static IApplicationBuilder UseSwagger(this IApplicationBuilder builder, IConfiguration config)
        {
            builder
                .UseOpenApi(configure =>
                {
                    configure.PostProcess = (document, _) =>
                    {
                        var configuration = config.GetSection("OpenApi").Get<OpenApiConfiguration>();
                        if (configuration?.Servers?.Length > 0)
                        {
                            foreach (var server in configuration.Servers)
                            {
                                document.Servers.Add(new OpenApiServer() { Url = server });
                            }
                        }
                    };
                })
                .UseSwaggerUi3(settings =>
                {
                    settings.SwaggerRoutes.Add(new SwaggerUi3Route("v1", "/swagger/v1/swagger.json"));

                    settings.TransformToExternalPath = (route, request) =>
                    {
                        string pathBase = request.Headers["X-Forwarded-PathBase"].FirstOrDefault();

                        if (!string.IsNullOrEmpty(pathBase))
                            return pathBase + route;

                        return route;
                    };
                });

            return builder;
        }

        public static IServiceCollection AddAuth(this IServiceCollection services, IConfiguration config)
        {
            var configuration = config.GetSection("Jwt").Get<JwtConfiguration>();

            services
                .AddAuthorization(opt => Policies.Set(opt))
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(o =>
                {
                    o.RequireHttpsMetadata = false;
                    o.SaveToken = true;
                    o.IncludeErrorDetails = true;

                    o.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,

                        ValidIssuer = configuration.Issuer,
                        ValidAudience = configuration.Audience,
                        IssuerSigningKey = configuration.GetSecret()
                    };
                });

            services
                .AddScoped<AuthService>()
                .AddScoped<ClientsService>()
                .AddScoped<PermissionsGenerator>();

            return services;
        }

        public static IApplicationBuilder UseLocalization(this IApplicationBuilder builder, IConfiguration configuration)
        {
            var defaultLocale = configuration["DefaultLocale"];

            builder.UseRequestLocalization(new RequestLocalizationOptions()
            {
                DefaultRequestCulture = new RequestCulture(new CultureInfo(defaultLocale)),
                SupportedCultures = new List<CultureInfo>()
                {
                    new CultureInfo("cs"),
                    new CultureInfo("en")
                }
            });

            builder.UseMiddleware<CultureMiddleware>();

            return builder;
        }

        public static IServiceCollection AddConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .Configure<JwtConfiguration>(configuration.GetSection("Jwt"))
                .Configure<ResetPasswordConfiguration>(configuration.GetSection("ResetPassword"))
                .Configure<EmailConfiguration>(configuration.GetSection("Email"))
                .Configure<MeetingConfiguration>(configuration.GetSection("Meeting"))
                .Configure<OpenApiConfiguration>(configuration.GetSection("OpenApi"))
                .Configure<OAuth2Configuration>(configuration.GetSection("OAuth"))
                .Configure<ClientConfiguration>(configuration.GetSection("Client"))
                .Configure<DiscordConfiguration>(configuration.GetSection("Discord"))
                .Configure<GoogleConfiguration>(configuration.GetSection("Google"));

            return services;
        }

        public static IServiceCollection AddFeatures(this IServiceCollection services)
        {
            var toRegister = Assembly.GetExecutingAssembly().GetTypes()
                .Where(o => o.GetCustomAttribute<ServiceAttribute>() != null);

            foreach (var service in toRegister)
            {
                var attribute = service.GetCustomAttribute<ServiceAttribute>();

                switch (attribute.Lifetime)
                {
                    case ServiceLifetime.Scoped when attribute.InterfaceType == null:
                        services.AddScoped(service);
                        break;
                    case ServiceLifetime.Scoped when attribute.InterfaceType != null:
                        services.AddScoped(attribute.InterfaceType, service);
                        break;
                    case ServiceLifetime.Singleton when attribute.InterfaceType == null:
                        services.AddSingleton(service);
                        break;
                    case ServiceLifetime.Singleton when attribute.InterfaceType != null:
                        services.AddSingleton(attribute.InterfaceType, service);
                        break;
                    case ServiceLifetime.Transient when attribute.InterfaceType == null:
                        services.AddTransient(service);
                        break;
                    case ServiceLifetime.Transient when attribute.InterfaceType != null:
                        services.AddTransient(attribute.InterfaceType, service);
                        break;
                }
            }

            return services
                .AddScoped<FileExtensionContentTypeProvider>();
        }

        public static IServiceCollection AddQuartz(this IServiceCollection services)
        {
            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();
                q.AddQuartzJob<MembersSynchronizationJob>(TimeSpan.FromHours(12));
            });

            services.AddQuartzHostedService();
            return services;
        }

        private static void AddQuartzJob<TJob>(this IServiceCollectionQuartzConfigurator quartz, TimeSpan period) where TJob : IJob
        {
            var jobName = typeof(TJob).Name;

            quartz.AddJob<TJob>(opt => opt.WithIdentity(jobName)).AddTrigger(opt => opt.ForJob(jobName)
                .WithIdentity($"{jobName}-Trigger")
                .WithSimpleSchedule(builder => builder.RepeatForever().WithInterval(period))
            );
        }
    }
}
