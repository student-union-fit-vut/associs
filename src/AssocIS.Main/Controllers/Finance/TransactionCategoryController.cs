﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Finance.Categories;
using AssocIS.Data.Resources.Finance;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.Finances;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Finance
{
    [ApiController]
    [Route("api/v{version:apiVersion}/finance/category")]
    [ApiVersion("1.0")]
    [OpenApiTag("Finance - Categories", Description = "Category management in finances.")]
    public class TransactionCategoryController : ControllerBase
    {
        private TransactionCategoryService TransactionCategoryService { get; }
        private IStringLocalizer<FinanceResources> Localizer { get; }

        public TransactionCategoryController(TransactionCategoryService transactionCategoryService, IStringLocalizer<FinanceResources> localizer)
        {
            TransactionCategoryService = transactionCategoryService;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets non paginated list of transaction categories.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(TransactionCategoryController), nameof(GetCategoriesAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<List<TransactionCategory>>> GetCategoriesAsync()
        {
            var categories = await TransactionCategoryService.GetCategoriesAsync();
            return Ok(categories);
        }

        /// <summary>
        /// Creates new transaction category.
        /// </summary>
        /// <response code="200">Success</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(TransactionCategoryController), nameof(CreateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<TransactionCategory>> CreateAsync(UpdateCategoryParams parameters)
        {
            var category = await TransactionCategoryService.CreateAsync(parameters);
            return Ok(category);
        }

        /// <summary>
        /// Updates transaction category.
        /// </summary>
        /// <param name="id">ID of transaction category.</param>
        /// <param name="parameters">Data</param>
        /// <response code="200">Success</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Category not found.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(TransactionCategoryController), nameof(UpdateCategoryAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<TransactionCategory>> UpdateCategoryAsync(long id, UpdateCategoryParams parameters)
        {
            var category = await TransactionCategoryService.UpdateCategoryAsync(id, parameters);

            if (category == null)
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.CategoryNotFound)]));

            return Ok(category);
        }

        /// <summary>
        /// Removes category.
        /// </summary>
        /// <param name="id">ID of category.</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Category not found.</response>
        [HttpDelete("{id}")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(TransactionCategoryController), nameof(RemoveCategoryAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> RemoveCategoryAsync(long id)
        {
            var result = await TransactionCategoryService.DeleteCategoryAsync(id);

            if (result == null)
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.CategoryNotFound)]));

            return Ok();
        }
    }
}
