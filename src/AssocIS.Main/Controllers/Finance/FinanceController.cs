﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Finance.Categories;
using AssocIS.Data.Models.Finance.Suppliers;
using AssocIS.Data.Models.Finance.Transactions;
using AssocIS.Data.Resources.Finance;
using AssocIS.Database.Enums;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.Finances;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Finance
{
    [ApiController]
    [Route("api/v{version:apiVersion}/finance")]
    [ApiVersion("1.0")]
    [OpenApiTag("Finance", Description = "Finance management")]
    public class FinanceController : ControllerBase
    {
        private FinanceService FinanceService { get; }
        private IStringLocalizer<FinanceResources> Localizer { get; }

        public FinanceController(FinanceService financeService, IStringLocalizer<FinanceResources> localizer)
        {
            FinanceService = financeService;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets paginated list of transactions.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(FinanceController), nameof(GetTransactionListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<TransactionListItem>>> GetTransactionListAsync([FromQuery] SearchTransactionParams parameters)
        {
            var transactions = await FinanceService.GetTransactionListAsync(parameters, User);
            return Ok(transactions);
        }

        /// <summary>
        /// Gets detailed information about transaction.
        /// </summary>
        /// <param name="id">Transaction ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Transaction not found or logged user not have permissions to this transaction.</response>
        [HttpGet("{id}")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(FinanceController), nameof(GetTransactionListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Transaction>> GetTransactionDetailAsync(long id)
        {
            var transaction = await FinanceService.GetTransactionDetailAsync(id, User);

            if (transaction == null)
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.TransactionNotFound)]));

            return Ok(transaction);
        }

        /// <summary>
        /// Creates new transaction. Everyone can create transaction.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(FinanceController), nameof(CreateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<Transaction>> CreateAsync(UpdateTransactionParams parameters)
        {
            var transaction = await FinanceService.CreateAsync(parameters, User);
            return Ok(transaction);
        }

        /// <summary>
        /// Updates existing transaction.
        /// </summary>
        /// <param name="id">Transaction ID.</param>
        /// <param name="parameters"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Transaction not found or logged user not have permissions to this transaction.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(FinanceController), nameof(UpdateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Transaction>> UpdateAsync(long id, UpdateTransactionParams parameters)
        {
            var transaction = await FinanceService.UpdateAsync(id, parameters, User);

            if (transaction == null)
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.TransactionNotFound)]));

            return Ok(transaction);
        }

        /// <summary>
        /// Hardly remove transaction from database.
        /// </summary>
        /// <param name="id">Transaction ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Transaction not found or logged user not have permissions to this transaction.</response>
        [HttpDelete("{id}")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(FinanceController), nameof(RemoveTransactionAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> RemoveTransactionAsync(long id)
        {
            if (!await FinanceService.TransactionExistsAsync(id, User))
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.TransactionNotFound)]));

            await FinanceService.RemoveTransactionAsync(id);
            return Ok();
        }

        /// <summary>
        /// Gets report by transaction type.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("report/type")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(FinanceController), nameof(GetReportByTransactionTypeAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<List<TransactionReportItem<TransactionType>>>> GetReportByTransactionTypeAsync()
        {
            var report = await FinanceService.GetReportByTransactionTypesAsync();
            return Ok(report);
        }

        /// <summary>
        /// Gets report by supplier.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("report/supplier")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(FinanceController), nameof(GetReportBySupplierAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<List<TransactionReportItem<SupplierListItem>>>> GetReportBySupplierAsync()
        {
            var report = await FinanceService.GetReportBySupplierAsync();
            return Ok(report);
        }

        /// <summary>
        /// Gets report by category.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("report/category")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(FinanceController), nameof(GetReportByCategoryAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<List<TransactionReportItem<TransactionCategory>>>> GetReportByCategoryAsync()
        {
            var report = await FinanceService.GetReportByTransactionCategoryAsync();
            return Ok(report);
        }
    }
}
