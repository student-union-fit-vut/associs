﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Finance.Transactions;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.Finance;
using AssocIS.Database.Enums;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.Finances;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Finance
{
    [ApiController]
    [Route("api/v{version:apiVersion}/finance/{id}/attachment")]
    [ApiVersion("1.0")]
    [OpenApiTag("Finance", Description = "Finance management")]
    public class TransactionAttachmentController : ControllerBase
    {
        private TransactionAttachmentService TransactionAttachmentService { get; }
        private FinanceService FinanceService { get; }
        private IStringLocalizer<FinanceResources> Localizer { get; }

        public TransactionAttachmentController(TransactionAttachmentService transactionAttachmentService, FinanceService financeService,
            IStringLocalizer<FinanceResources> localizer)
        {
            TransactionAttachmentService = transactionAttachmentService;
            FinanceService = financeService;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets binary data of transaction attachment.
        /// </summary>
        /// <param name="id">Transaction ID</param>
        /// <param name="attachmentId">Transaction attachment ID</param>
        /// <response code="200">Success</response>
        /// <response code="404">Transaction or attachment not found or logged user not have permissions to this transaction.</response>
        [HttpGet("{attachmentId}")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(TransactionAttachmentController), nameof(GetAttachmentDataAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetAttachmentDataAsync(long id, long attachmentId)
        {
            var data = await TransactionAttachmentService.GetAttachmentDataAsync(id, attachmentId, User);

            if (data == null)
                return NotFound();

            return File(data.Item1, data.Item2);
        }

        /// <summary>
        /// Uploads new attachment to transaction.
        /// </summary>
        /// <param name="id">Transaction ID</param>
        /// <param name="type">Attachment type (Invoice, ...).</param>
        /// <param name="file">File content.</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Transaction or attachment not found or logged user not have permissions to this transaction.</response>
        [HttpPost]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(TransactionAttachmentController), nameof(CreateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<TransactionAttachment>> CreateAsync(long id,
            [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
            TransactionAttachmentType type,
            [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
            IFormFile file)
        {
            if (!await FinanceService.TransactionExistsAsync(id, User))
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.TransactionNotFound)]));

            var parameters = new CreateTransactionAttachmentParams()
            {
                File = file,
                Type = type
            };

            var attachment = await TransactionAttachmentService.CreateAsync(id, parameters, User);

            if (attachment == null)
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.TransactionNotFound)]));

            return Ok(attachment);
        }

        /// <summary>
        /// Removes transaction attachment.
        /// </summary>
        /// <param name="id">Transaction ID</param>
        /// <param name="attachmentId">Attachment ID.</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Transaction or attachment not found or logged user not have permissions to this transaction.</response>
        [HttpDelete("{attachmentId}")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(TransactionAttachmentController), nameof(RemoveAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> RemoveAsync(long id, long attachmentId)
        {
            if(!await FinanceService.TransactionExistsAsync(id, User))
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.TransactionNotFound)]));

            await TransactionAttachmentService.RemoveAsync(id, attachmentId);
            return Ok();
        }
    }
}
