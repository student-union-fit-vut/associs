﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Finance.Suppliers;
using AssocIS.Data.Resources.Finance;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.Finances;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using Org.BouncyCastle.Asn1.Crmf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Finance
{
    [ApiController]
    [Route("api/v{version:apiVersion}/finance/supplier")]
    [ApiVersion("1.0")]
    [OpenApiTag("Finance - Supplier", Description = "Suppliers mamangement in finances.")]
    public class SupplierController : ControllerBase
    {
        private SupplierService SupplierService { get; set; }
        private IStringLocalizer<FinanceResources> Localizer { get; set; }

        public SupplierController(SupplierService supplierService, IStringLocalizer<FinanceResources> localizer)
        {
            SupplierService = supplierService;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets non paginated data of suppliers.
        /// </summary>
        /// <param name="nameQuery">Search query in supplier name.</param>
        /// <response code="200">Success.</response>
        [HttpGet]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(SupplierController), nameof(GetSupplierListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<List<SupplierListItem>>> GetSupplierListAsync([FromQuery] string nameQuery)
        {
            var data = await SupplierService.GetSuppliersListAsync(nameQuery);
            return Ok(data);
        }

        /// <summary>
        /// Gets detailed information about supplier.
        /// </summary>
        /// <param name="id">Supplier ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Supplier not found.</response>
        [HttpGet("{id}")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(SupplierController), nameof(GetSupplierDetailAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Supplier>> GetSupplierDetailAsync(long id)
        {
            var data = await SupplierService.GetSupplierAsync(id);

            if (data == null)
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.SupplierNotFound)]));

            return Ok(data);
        }

        /// <summary>
        /// Creates new supplier.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed</response>
        [HttpPost]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(SupplierController), nameof(CreateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<Supplier>> CreateAsync(UpdateSupplierParams parameters)
        {
            var supplier = await SupplierService.CreateAsync(parameters, User);
            return Ok(supplier);
        }

        /// <summary>
        /// Updates supplier.
        /// </summary>
        /// <param name="id">Supplier ID</param>
        /// <param name="parameters"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Supplier not found.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(SupplierController), nameof(UpdateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NoContent)]
        public async Task<ActionResult<Supplier>> UpdateAsync(long id, UpdateSupplierParams parameters)
        {
            var supplier = await SupplierService.UpdateAsync(id, parameters, User);

            if (supplier == null)
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.SupplierNotFound)]));

            return Ok(supplier);
        }

        /// <summary>
        /// Removes supplier.
        /// </summary>
        /// <param name="id">Supplier ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Supplier not found.</response>
        [HttpDelete("{id}")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(SupplierController), nameof(RemoveAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> RemoveAsync(long id)
        {
            if (!await SupplierService.SupplierExistsAsync(id))
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.SupplierNotFound)]));

            await SupplierService.RemoveSupplierAsync(id);
            return Ok();
        }
    }
}