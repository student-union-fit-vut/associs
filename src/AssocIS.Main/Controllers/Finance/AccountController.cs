﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Finance.Accounts;
using AssocIS.Data.Resources.Finance;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.Finances;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Finance
{
    [ApiController]
    [Route("api/v{version:apiVersion}/finance/account")]
    [ApiVersion("1.0")]
    [OpenApiTag("Finance - Account", Description = "Account management in finances.")]
    public class AccountController : ControllerBase
    {
        private AccountService AccountService { get; set; }
        private IStringLocalizer<FinanceResources> Localizer { get; set; }

        public AccountController(AccountService accountService, IStringLocalizer<FinanceResources> localizer)
        {
            AccountService = accountService;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets non paginated list of accounts.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(AccountController), nameof(GetAccountsListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<List<AccountListItem>>> GetAccountsListAsync([FromQuery] SearchAccountParams parameters)
        {
            var data = await AccountService.GetAccountsListAsync(parameters);
            return Ok(data);
        }

        /// <summary>
        /// Gets detailed information about account.
        /// </summary>
        /// <param name="id">Account ID.</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Account not found.</response>
        [HttpGet("{id}")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(AccountController), nameof(GetAccountDetailAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Account>> GetAccountDetailAsync(long id)
        {
            var account = await AccountService.GetAccountDetailAsync(id);

            if (account == null)
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.AccountNotFound)]));

            return Ok(account);
        }

        /// <summary>
        /// Creates new account.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(AccountController), nameof(CreateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<Account>> CreateAsync(UpdateAccountParams parameters)
        {
            var account = await AccountService.CreateAsync(parameters, User);
            return Ok(account);
        }

        /// <summary>
        /// Updates account.
        /// </summary>
        /// <param name="id">Account ID</param>
        /// <param name="parameters"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Account not found.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(AccountController), nameof(CreateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Account>> UpdateAsync(long id, UpdateAccountParams parameters)
        {
            var account = await AccountService.UpdateAsync(id, parameters, User);

            if (account == null)
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.AccountNotFound)]));

            return Ok(account);
        }

        /// <summary>
        /// Removes account.
        /// </summary>
        /// <param name="id">Account ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Account not found.</response>
        [HttpDelete("{id}")]
        [Authorize(Policies.FinanceManagement)]
        [ApiOperation(typeof(SupplierController), nameof(RemoveAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> RemoveAsync(long id)
        {
            if (!await AccountService.AccountExistsAsync(id))
                return NotFound(new MessageResponse(Localizer[nameof(FinanceResources.AccountNotFound)]));

            await AccountService.RemoveAsync(id);
            return Ok();
        }
    }
}
