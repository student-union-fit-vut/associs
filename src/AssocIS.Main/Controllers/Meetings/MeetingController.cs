﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Meetings;
using AssocIS.Data.Resources.Meeting;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.Meetings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Meetings
{
    [ApiController]
    [Route("api/v{version:apiVersion}/meeting")]
    [ApiVersion("1.0")]
    [OpenApiTag("Meetings", Description = "Meetings and meeting presence.")]
    public class MeetingController : Controller
    {
        private MeetingService MeetingService { get; }
        private IStringLocalizer<MeetingResources> Localizer { get; }

        public MeetingController(MeetingService meetingService, IStringLocalizer<MeetingResources> localizer)
        {
            MeetingService = meetingService;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets list of meetings.
        /// </summary>
        /// <param name="searchParams">Search parameters</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed</response>
        [HttpGet]
        [Authorize]
        [ApiOperation(typeof(MeetingController), nameof(GetMeetingsListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<MeetingListItem>>> GetMeetingsListAsync([FromQuery] MeetingSearchParams searchParams)
        {
            var data = await MeetingService.GetMeetingsListAsync(searchParams, User);
            return Ok(data);
        }

        /// <summary>
        /// Gets detailed information about meeting.
        /// </summary>
        /// <param name="id">Meeting ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Meeting was not found.</response>
        [HttpGet("{id}")]
        [Authorize]
        [ApiOperation(typeof(MeetingController), nameof(GetMeetingAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Meeting>> GetMeetingAsync(long id)
        {
            var meeting = await MeetingService.GetMeetingAsync(id, User);

            if (meeting == null)
                return NotFound(new MessageResponse(Localizer[nameof(MeetingResources.MeetingNotFound)]));

            return Ok(meeting);
        }

        /// <summary>
        /// Creates new meeting.
        /// </summary>
        /// <param name="meetingParams"></param>
        /// <remarks>
        /// If new meeting will be in Summoned state. Members will be notified about new meeting.
        /// </remarks>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.MeetingManagement)]
        [OpenApiOperation(nameof(MeetingController) + "_" + nameof(CreateMeetingAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<Meeting>> CreateMeetingAsync(UpdateMeetingParams meetingParams)
        {
            var meeting = await MeetingService.CreateMeetingAsync(meetingParams, User);
            return Ok(meeting);
        }

        /// <summary>
        /// Updates meeting.
        /// </summary>
        /// <param name="id">Meeting ID</param>
        /// <param name="meetingParams"></param>
        /// <remarks>
        /// If new meeting will be in Summoned or Cancelled state that members will be notified about meeting.
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Validation failed</response>
        /// <response code="404">Meeting was not found.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.MeetingManagement)]
        [OpenApiOperation(nameof(MeetingController) + "_" + nameof(UpdateMeetingAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Meeting>> UpdateMeetingAsync(long id, UpdateMeetingParams meetingParams)
        {
            var meeting = await MeetingService.UpdateMeetingAsync(id, meetingParams, User);

            if (meeting == null)
                return NotFound(new MessageResponse(Localizer[nameof(MeetingResources.MeetingNotFound)]));

            return Ok(meeting);
        }

        /// <summary>
        /// Notifies summoned meeting.
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// Meeting have to summoned state.
        /// </remarks>
        /// <response code="200">Success.</response>
        /// <response code="400">Meeting is not in summoned state.</response>
        /// <response code="404">Meeting not found.</response>
        [HttpPost("{id}/notify")]
        [Authorize(Policies.MeetingManagement)]
        [ApiOperation(typeof(MeetingController), nameof(NotifyMeetingAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Meeting>> NotifyMeetingAsync(long id)
        {
            try
            {
                var meeting = await MeetingService.NotifyMeetingAsync(id, User);

                if (meeting == null)
                    return NotFound(new MessageResponse(Localizer[nameof(MeetingResources.MeetingNotFound)]));

                return Ok(meeting);
            }
            catch (InvalidOperationException ex)
            {
                var errDict = new Dictionary<string, string[]>()
                {
                    { "State", new[] { Localizer[nameof(MeetingResources.NotSummonedState), ex.Message].ToString() } }
                };

                return BadRequest(new ValidationProblemDetails(errDict));
            }
        }

        /// <summary>
        /// Apologize current logged user from meeting.
        /// </summary>
        /// <param name="id">Meeting ID</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed. Meeting started.</response>
        /// <response code="404">Meeting not found.</response>
        [HttpPut("{id}/apologize")]
        [Authorize]
        [ApiOperation(typeof(MeetingController), nameof(ApologizeFromMeetingAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Meeting>> ApologizeFromMeetingAsync(long id)
        {
            try
            {
                var meeting = await MeetingService.ApologizeFromMeetingAsync(id, User);

                if (meeting == null)
                    return NotFound(new MessageResponse(Localizer[nameof(MeetingResources.MeetingNotFound)]));

                return Ok(meeting);
            }
            catch (InvalidOperationException ex)
            {
                var errDict = new Dictionary<string, string[]>()
                {
                    { "At", new[]{ Localizer[nameof(MeetingResources.ApologizeCannotMeetingStarted), ex.Message].ToString() } }
                };

                return BadRequest(new ValidationProblemDetails(errDict));
            }
        }

        /// <summary>
        /// Gets presence for specific meeting.
        /// </summary>
        /// <param name="id">Meeting ID</param>
        /// <response code="200">Success</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Meeting not found.</response>
        [HttpGet("{id}/presence")]
        [Authorize(Policies.MeetingManagement)]
        [ApiOperation(typeof(MeetingController), nameof(GetMeetingPresenceAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<List<MeetingPresenceListItem>>> GetMeetingPresenceAsync(long id)
        {
            var data = await MeetingService.GetMeetingPresenceAsync(id);

            if (data == null)
                return NotFound(new MessageResponse(Localizer[nameof(MeetingResources.MeetingNotFound)]));

            return Ok(data);
        }

        /// <summary>
        /// Create report of all summoned meetings.
        /// </summary>
        /// <response code="200">Success</response>
        [HttpGet("report/presence")]
        [Authorize(Policies.MeetingManagement)]
        [ApiOperation(typeof(MeetingController), nameof(GetPresenceReportAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<List<UserPresenceStatus>>> GetPresenceReportAsync()
        {
            var data = await MeetingService.GetPresenceReportAsync();
            return Ok(data);
        }

        /// <summary>
        /// Updates meeting presence.
        /// </summary>
        /// <param name="id">Meeting ID</param>
        /// <param name="presenceParams">Presence users</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Meeting not found.</response>
        [HttpPut("{id}/presence")]
        [Authorize(Policies.MeetingManagement)]
        [ApiOperation(typeof(MeetingController), nameof(SetMeetingPresenceAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Meeting>> SetMeetingPresenceAsync(long id, List<SetMeetingPresenceParams> presenceParams)
        {
            var meeting = await MeetingService.SetMeetingPresence(id, presenceParams, User);

            if (meeting == null)
                return NotFound(new MessageResponse(Localizer[nameof(MeetingResources.MeetingNotFound)]));

            return Ok(meeting);
        }
    }
}
