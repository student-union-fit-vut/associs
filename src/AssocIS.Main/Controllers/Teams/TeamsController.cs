﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Teams;
using AssocIS.Main.Services.Teams;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System.Net;
using System.Threading.Tasks;
using AssocIS.Data.Resources.Team;
using System;
using AssocIS.Main.Services.Auth;

namespace AssocIS.Main.Controllers.Teams
{
    [ApiController]
    [Route("api/v{version:apiVersion}/team")]
    [ApiVersion("1.0")]
    [OpenApiTag("Teams", Description = "Teams management")]
    public class TeamsController : ControllerBase
    {
        private TeamsService TeamsService { get; }
        private IStringLocalizer<TeamResources> Localizer { get; }

        public TeamsController(TeamsService service, IStringLocalizer<TeamResources> localizer)
        {
            TeamsService = service;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets list of teams.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet]
        [Authorize(Policies.TeamLeaders)]
        [OpenApiOperation(nameof(TeamsController) + "_" + nameof(GetTeamsListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<TeamListItem>>> GetTeamsListAsync([FromQuery] TeamSearchParams request)
        {
            var data = await TeamsService.GetTeamsListAsync(request, User);
            return Ok(data);
        }

        /// <summary>
        /// Gets team by id.
        /// </summary>
        /// <param name="id">Unique ID of team.</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Team not found</response>
        [HttpGet("{id}")]
        [Authorize(Policies.TeamLeaders)]
        [OpenApiOperation(nameof(TeamsController) + "_" + nameof(GetTeamAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Team>> GetTeamAsync(long id)
        {
            var team = await TeamsService.GetTeamAsync(id, User);

            if (team == null)
                return NotFound(new MessageResponse(Localizer[nameof(TeamResources.UnknownTeam), id]));

            return Ok(team);
        }

        /// <summary>
        /// Creates team.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.UserManagement)]
        [OpenApiOperation(nameof(TeamsController) + "_" + nameof(CreateTeamAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<Team>> CreateTeamAsync(UpdateTeamParams request)
        {
            var team = await TeamsService.CreateTeamAsync(request, User);
            return Ok(team);
        }

        /// <summary>
        /// Updates existing team.
        /// </summary>
        /// <param name="id">ID of team</param>
        /// <param name="request"></param>
        /// <response code="200">Success</response>
        /// <response code="404">Team not found.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.TeamLeaders)]
        [OpenApiOperation(nameof(TeamsController) + "_" + nameof(ModifyTeamAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Team>> ModifyTeamAsync(long id, [FromBody] UpdateTeamParams request)
        {
            var team = await TeamsService.ModifyTeamAsync(id, request, User);

            if (team == null)
                return NotFound(new MessageResponse(Localizer[nameof(TeamResources.UnknownTeam), id]));

            return Ok(team);
        }

        /// <summary>
        /// Gets list of team members.
        /// </summary>
        /// <param name="id">ID of team</param>
        /// <param name="request"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Team not found.</response>
        [HttpGet("{id}/members")]
        [Authorize(Policies.TeamLeaders)]
        [OpenApiOperation(nameof(TeamsController) + "_" + nameof(GetTeamMembersListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<PaginatedData<TeamMember>>> GetTeamMembersListAsync(long id, [FromQuery] PaginationParams request)
        {
            var data = await TeamsService.GetTeamMembersAsync(id, request);

            if (data == null)
                return NotFound(new MessageResponse(Localizer[nameof(TeamResources.UnknownTeam), id]));

            return Ok(data);
        }

        /// <summary>
        /// Deletes a team.
        /// </summary>
        /// <param name="id">ID of team.</param>
        /// <response code="200">Success</response>
        /// <response code="403">Unallowed operation.</response>
        /// <response code="404">Team not found.</response>
        [HttpDelete("{id}")]
        [Authorize(Policies.TeamLeaders)]
        [OpenApiOperation(nameof(TeamsController) + "_" + nameof(RemoveTeamAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.Forbidden)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> RemoveTeamAsync(long id)
        {
            if (!await TeamsService.TeamExistsAsync(id))
                return NotFound(new MessageResponse(Localizer[nameof(TeamResources.UnknownTeam), id]));

            if (!await TeamsService.CheckRemovableAsync(id, User))
                return StatusCode((int)HttpStatusCode.Forbidden, new MessageResponse(Localizer[nameof(TeamResources.OnlyTeamLeaderCanProcess), id]));

            await TeamsService.RemoveTeamAsync(id, User);
            return Ok();
        }

        /// <summary>
        /// Removes user from team.
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Success.</response>
        /// <response code="403">Forbidden operation (such as leader want remove self, but not have higher permissions.)</response>
        /// <response code="404">Team not found.</response>
        [HttpDelete("{teamId}/members/{userId}")]
        [Authorize(Policies.TeamLeaders)]
        [OpenApiOperation(nameof(TeamsController) + "_" + nameof(RemoveTeamMemberAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Team>> RemoveTeamMemberAsync([FromRoute] UpdateTeamMemberParams request)
        {
            if (!await TeamsService.TeamExistsAsync(request.TeamId))
                return NotFound(new MessageResponse(Localizer[nameof(TeamResources.UnknownTeam), request.TeamId]));

            try
            {
                var team = await TeamsService.RemoveTeamMemberAsync(request.TeamId, request.UserId, User);
                return Ok(team);
            }
            catch (UnauthorizedAccessException)
            {
                return StatusCode((int)HttpStatusCode.Forbidden, new MessageResponse(Localizer[nameof(TeamResources.OnlyHigherPersonCanProcess)]));
            }
        }

        /// <summary>
        /// Adds user to team.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="403">Forbidden operation. (Not team leader of team and not have permissions).</response>
        /// <response code="404">Team not found</response>
        [HttpPut("{teamId}/members/{userId}")]
        [Authorize(Policies.TeamLeaders)]
        [OpenApiOperation(nameof(TeamsController) + "_" + nameof(AddTeamMemberAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Team>> AddTeamMemberAsync([FromRoute] UpdateTeamMemberParams request)
        {
            if (!await TeamsService.TeamExistsAsync(request.TeamId))
                return NotFound(new MessageResponse(Localizer[nameof(TeamResources.UnknownTeam), request.TeamId]));

            try
            {
                var team = await TeamsService.AddTeamMemberAsync(request, User);
                return Ok(team);
            }
            catch (UnauthorizedAccessException)
            {
                return StatusCode((int)HttpStatusCode.Forbidden, new MessageResponse(Localizer[nameof(TeamResources.OnlyHigherPersonCanProcess)]));
            }
        }
    }
}
