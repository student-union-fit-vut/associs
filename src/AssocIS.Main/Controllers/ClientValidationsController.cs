﻿using AssocIS.Common.Attributes;
using AssocIS.Main.Services.Assets;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.Finances;
using AssocIS.Main.Services.Teams;
using AssocIS.Main.Services.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using NSwag.Annotations;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/validations")]
    [ApiVersion("1.0")]
    [OpenApiTag("Validations", Description = "Client side validations")]
    public class ClientValidationsController : ControllerBase
    {
        private IServiceProvider Provider { get; }

        public ClientValidationsController(IServiceProvider provider)
        {
            Provider = provider;
        }

        /// <summary>
        /// Checks if username exists.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("username/exists")]
        [Authorize]
        [ApiOperation(typeof(ClientValidationsController), nameof(UsernameExistsAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<bool>> UsernameExistsAsync(string username)
        {
            using var scope = Provider.CreateScope();
            var service = scope.ServiceProvider.GetService<UsersService>();

            var result = await service.UsernameExistsAsync(username);
            return Ok(result);
        }

        /// <summary>
        /// Checks format of phone number
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("phoneNumber")]
        [Authorize]
        [ApiOperation(typeof(ClientValidationsController), nameof(ValidatePhoneNumber))]
        [ProducesResponse(HttpStatusCode.OK)]
        public ActionResult<bool> ValidatePhoneNumber(string phoneNumber)
        {
            var validator = new PhoneAttribute();
            var result = validator.IsValid(phoneNumber);

            return Ok(result);
        }

        /// <summary>
        /// Checks if user with this ID exists.
        /// </summary>
        /// <param name="id">User ID</param>
        /// <response code="200">Success.</response>
        [HttpGet("user/{id}/exists")]
        [Authorize]
        [ApiOperation(typeof(ClientValidationsController), nameof(UserIdExistsAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<bool>> UserIdExistsAsync(long id)
        {
            using var scope = Provider.CreateScope();
            var service = scope.ServiceProvider.GetService<UsersService>();

            var result = await service.UserIdExistsAsync(id);
            return Ok(result);
        }

        /// <summary>
        /// Checks if team name exists.
        /// </summary>
        /// <param name="name">Expected team name</param>
        [HttpGet("team/exists")]
        [Authorize]
        [ApiOperation(typeof(ClientValidationsController), nameof(TeamNameExistsAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<bool>> TeamNameExistsAsync(string name)
        {
            using var scope = Provider.CreateScope();
            var service = scope.ServiceProvider.GetService<TeamsService>();

            var result = await service.TeamNameExistsAsync(name);
            return Ok(result);
        }

        /// <summary>
        /// Checks if asset category name exists.
        /// </summary>
        /// <param name="name">Expected category name</param>
        /// <response code="200">Success</response>
        [HttpGet("asset/category/exists")]
        [Authorize]
        [ApiOperation(typeof(ClientValidationsController), nameof(AssetCategoryNameExistsAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<bool>> AssetCategoryNameExistsAsync(string name)
        {
            using var scope = Provider.CreateScope();
            var service = scope.ServiceProvider.GetService<AssetCategoryService>();

            var result = await service.AssetCategoryNameExistsAsync(name);
            return Ok(result);
        }

        /// <summary>
        /// Checks if transaction category name exists.
        /// </summary>
        /// <param name="name">Expected category name</param>
        /// <response code="200">Success</response>
        [HttpGet("finance/category/exists")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(ClientValidationsController), nameof(TransactionCategoryNameExistsAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<bool>> TransactionCategoryNameExistsAsync(string name)
        {
            using var scope = Provider.CreateScope();
            var service = scope.ServiceProvider.GetService<TransactionCategoryService>();

            var result = await service.CategoryNameExistsAsync(name);
            return Ok(result);
        }
    }
}
