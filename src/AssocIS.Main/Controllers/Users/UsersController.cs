﻿using AssocIS.Common.Attributes;
using AssocIS.Common.Extensions;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Users;
using AssocIS.Data.Resources.User;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Localization;
using NSwag;
using NSwag.Annotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Users
{
    [ApiController]
    [Route("api/v{version:apiVersion}/user")]
    [ApiVersion("1.0")]
    [OpenApiTag("Users", Description = "Users management")]
    public class UsersController : Controller
    {
        private UsersService UsersService { get; }
        private IStringLocalizer<UserResources> Localizer { get; }
        private FileExtensionContentTypeProvider FileExtensionContentTypeProvider { get; }
        private UserNotesService UserNotesService { get; }
        private UserAccessService UserAccessService { get; }

        public UsersController(UsersService usersService, IStringLocalizer<UserResources> localizer, FileExtensionContentTypeProvider fileExtensionContentTypeProvider,
            UserNotesService userNotesService, UserAccessService userAccessService)
        {
            UsersService = usersService;
            Localizer = localizer;
            FileExtensionContentTypeProvider = fileExtensionContentTypeProvider;
            UserNotesService = userNotesService;
            UserAccessService = userAccessService;
        }

        /// <summary>
        /// Gets paginated list of users.
        /// </summary>
        /// <param name="searchParams">Search parameters</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet]
        [Authorize]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(GetUsersListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<User>>> GetUsersListAsync([FromQuery] UserSearchParams searchParams)
        {
            var data = await UsersService.GetUserListAsync(searchParams);
            return Ok(data);
        }

        /// <summary>
        /// Gets user detail.
        /// </summary>
        /// <param name="id">User ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">User not found.</response>
        [HttpGet("{id}")]
        [Authorize]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(GetUserDetailAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserDetail>> GetUserDetailAsync(long id)
        {
            var user = await UsersService.GetUserDetailAsync(id, User);

            if (user == null)
                return NotFound(new MessageResponse(Localizer[nameof(UserResources.UserNotFound), id]));

            return Ok(user);
        }

        /// <summary>
        /// Creates new user.
        /// </summary>
        /// <param name="userParams">User data</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.UserManagement)]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(CreateUserAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<UserDetail>> CreateUserAsync(UserParams userParams)
        {
            var user = await UsersService.CreateUserAsync(userParams, User);
            return Ok(user);
        }

        /// <summary>
        /// Updates user.
        /// </summary>
        /// <param name="id">User ID</param>
        /// <param name="userParams">New user data.</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">User not found.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.UserManagement)]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(UpdateUserAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserDetail>> UpdateUserAsync(long id, UserParams userParams)
        {
            var user = await UsersService.UpdateUserAsync(id, userParams, User);

            if (user == null)
                return NotFound(new MessageResponse(Localizer[nameof(UserResources.UserNotFound), id]));

            return Ok(user);
        }

        /// <summary>
        /// Updates current logged user.
        /// </summary>
        /// <param name="userParams">New user data.</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPut("@me")]
        [Authorize]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(UpdateCurrentUserAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<UserDetail>> UpdateCurrentUserAsync(UserParams userParams)
        {
            var currentUserId = User.GetUserId();
            return await UpdateUserAsync(currentUserId, userParams);
        }

        /// <summary>
        /// Uploads profile picture for current logged user.
        /// </summary>
        /// <param name="file">File</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed. Max 5MB per file and only images.</response>
        [HttpPost("@me/image")]
        [Authorize]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(UploadProfilePictureAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<UserDetail>> UploadProfilePictureAsync(IFormFile file)
        {
            if (file.Length > 5 * 1024 * 1024) // 5MB
            {
                var errors = new Dictionary<string, string[]>() { { "file", new[] { Localizer[nameof(UserResources.BigProfilePicture)].Value } } };
                return BadRequest(new ValidationProblemDetails(errors));
            }

            if (!file.IsImage())
            {
                var errors = new Dictionary<string, string[]>() { { "file", new[] { Localizer[nameof(UserResources.FileIsNotImage)].Value } } };
                return BadRequest(new ValidationProblemDetails(errors));
            }

            var userId = User.GetUserId();
            var user = await UsersService.UploadProfilePictureAsync(userId, file, User);
            return Ok(user);
        }

        /// <summary>
        /// Gets profile picture for user. If user not have profile picture, it returns default.
        /// </summary>
        /// <param name="id">User ID</param>
        /// <response code="200">Success.</response>
        [HttpGet("{id}/image")]
        [AllowAnonymous]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(GetProfilePictureAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<IActionResult> GetProfilePictureAsync(long id)
        {
            var profilePicture = id == -1 ? null : await UsersService.GetProfilePictureAsync(id);

            var contentType = FileExtensionContentTypeProvider.TryGetContentType(profilePicture?.Item2 ?? "DefaultProfilePicture.png", out string ct) ? ct : null;
            var file = profilePicture?.Item1 ?? UserResources.DefaultProfileImage;

            return File(file, contentType);
        }

        /// <summary>
        /// Removes profile picture for current logged user.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpDelete("@me/image")]
        [Authorize]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(DeleteProfilePictureAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<UserDetail>> DeleteProfilePictureAsync()
        {
            var user = await UsersService.DeleteProfilePictureAsync(User);
            return Ok(user);
        }

        /// <summary>
        /// Sets user note. Author is current logged user.
        /// </summary>
        /// <param name="id">Destination user ID.</param>
        /// <param name="userNoteParams">Parameters.</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Destination user not found.</response>
        [HttpPost("{id}/note")]
        [Authorize]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(SetUserNoteAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserDetail>> SetUserNoteAsync(long id, [FromBody] UserNoteParams userNoteParams)
        {
            var user = await UserNotesService.SetUserNoteAsync(id, userNoteParams.Content, User);

            if (user == null)
                return NotFound(new MessageResponse(Localizer[nameof(UserResources.UserNotFound), id]));

            return Ok(user);
        }

        /// <summary>
        /// Changes password for currently logged user.
        /// </summary>
        /// <param name="changeParams">Password change request.</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPut("@me/password")]
        [Authorize]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(SetPasswordAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<UserDetail>> SetPasswordAsync(PasswordChangeParams changeParams)
        {
            try
            {
                var user = await UserAccessService.SetCurrentUserPasswordAsync(changeParams, User);
                return Ok(user);
            }
            catch (ValidationException ex) when (ex.Message == "InvalidOldPasswords")
            {
                var errors = new Dictionary<string, string[]>()
                {
                    { nameof(changeParams.OldPassword), new[]{ Localizer[nameof(UserResources.InvalidOldPassword)].Value } }
                };

                return BadRequest(new ValidationProblemDetails(errors));
            }
        }

        /// <summary>
        /// Resets password for user.
        /// </summary>
        /// <param name="id">User ID</param>
        /// <response code="200">Success</response>
        /// <response code="404">User not found.</response>
        [HttpPut("{id}/password/reset")]
        [Authorize(Policies.UserManagement)]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(ResetPasswordAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<ResetPasswordResult>> ResetPasswordAsync(long id)
        {
            var result = await UserAccessService.ResetPasswordAsync(id, User);

            if (result == null)
                return NotFound(new MessageResponse(Localizer[nameof(UserResources.UserNotFound), id]));

            return Ok(result);
        }

        /// <summary>
        /// Deletes password for user. After this operation user cannot login with standard login and get higher permissions (if have).
        /// </summary>
        /// <param name="id">User ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">User not found.</response>
        [HttpDelete("{id}/password")]
        [Authorize(Policies.UserManagement)]
        [OpenApiOperation(nameof(UsersController) + "_" + nameof(DeletePasswordAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserDetail>> DeletePasswordAsync(long id)
        {
            var user = await UserAccessService.RemovePasswordAsync(id, User);

            if (user == null)
                return NotFound(new MessageResponse(Localizer[nameof(UserResources.UserNotFound), id]));

            return Ok(user);
        }

        /// <summary>
        /// (GDPR) Anonymize user.
        /// </summary>
        /// <param name="id">User ID</param>
        /// <response code="200">Success</response>
        /// <response code="404">User not found or left association.</response>
        [HttpDelete("{id}/anonymize")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(UsersController), nameof(AnonymizeUserAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserDetail>> AnonymizeUserAsync(long id)
        {
            var user = await UsersService.AnonymizeUserAsync(id, User);

            if (user == null)
                return NotFound(new MessageResponse(Localizer[nameof(UserResources.UserNotFoundOrNotLeft)]));

            return Ok(user);
        }
    }
}