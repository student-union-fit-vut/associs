﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Assets.Category;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.Asset;
using AssocIS.Main.Services.Assets;
using AssocIS.Main.Services.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Assets
{
    [ApiController]
    [Route("api/v{version:apiVersion}/asset/category")]
    [ApiVersion("1.0")]
    [OpenApiTag("Assets category", Description = "Asset category management.")]
    public class AssetCategoryController : Controller
    {
        private AssetCategoryService AssetCategoryService { get; }
        private IStringLocalizer<AssetResources> Localizer { get; }

        public AssetCategoryController(AssetCategoryService assetCategoryService, IStringLocalizer<AssetResources> localizer)
        {
            AssetCategoryService = assetCategoryService;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets non paginated list of asset categories.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(AssetCategoryController), nameof(GetCategoriesAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<List<AssetCategory>>> GetCategoriesAsync()
        {
            var categories = await AssetCategoryService.GetCategoriesAsync();
            return Ok(categories);
        }

        /// <summary>
        /// Creates new asset category.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.PropertyManagement)]
        [ApiOperation(typeof(AssetCategoryController), nameof(CreateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<AssetCategory>> CreateAsync(UpdateCategoryParams categoryParams)
        {
            var category = await AssetCategoryService.CreateAsync(categoryParams);
            return Ok(category);
        }

        /// <summary>
        /// Updates asset category.
        /// </summary>
        /// <param name="id">ID of asset category.</param>
        /// <param name="categoryParams">Data</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Category not found.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.PropertyManagement)]
        [ApiOperation(typeof(AssetCategoryController), nameof(UpdateCategoryAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<AssetCategory>> UpdateCategoryAsync(long id, UpdateCategoryParams categoryParams)
        {
            var category = await AssetCategoryService.UpdateCategoryAsync(id, categoryParams);

            if (category == null)
                return NotFound(new MessageResponse(Localizer[nameof(AssetResources.AssetCategoryNotFound)]));

            return Ok(category);
        }

        /// <summary>
        /// Removes category.
        /// </summary>
        /// <param name="id">ID of asset category.</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Category not found.</response>
        [HttpDelete("{id}")]
        [Authorize(Policies.PropertyManagement)]
        [ApiOperation(typeof(AssetCategoryController), nameof(RemoveCategoryAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> RemoveCategoryAsync(long id)
        {
            var result = await AssetCategoryService.DeleteCategoryAsync(id);

            if(result == null)
                return NotFound(new MessageResponse(Localizer[nameof(AssetResources.AssetCategoryNotFound)]));

            return Ok();
        }
    }
}