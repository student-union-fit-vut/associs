﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Assets;
using AssocIS.Data.Models.Assets.Inventory;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.Asset;
using AssocIS.Main.Services.Assets;
using AssocIS.Main.Services.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Assets
{
    [ApiController]
    [Route("api/v{version:apiVersion}/asset")]
    [ApiVersion("1.0")]
    [OpenApiTag("Assets", Description = "Assets and loans management")]
    public class AssetController : Controller
    {
        private AssetService AssetService { get; }
        private IStringLocalizer<AssetResources> Localizer { get; }
        private AssetInventoryService AssetInventoryService { get; }

        public AssetController(AssetService assetService, IStringLocalizer<AssetResources> localizer, AssetInventoryService assetInventoryService)
        {
            AssetService = assetService;
            Localizer = localizer;
            AssetInventoryService = assetInventoryService;
        }

        /// <summary>
        /// Gets paginated list of assets.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(AssetController), nameof(GetAssetsListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<AssetListInfo>>> GetAssetsListAsync([FromQuery] AssetSearchParams searchParams)
        {
            var list = await AssetService.GetAssetsListAsync(searchParams);
            return Ok(list);
        }

        /// <summary>
        /// Gets detail of asset.
        /// </summary>
        /// <param name="id">Asset ID.</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Asset not found</response>
        [HttpGet("{id}")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(AssetController), nameof(GetAssetDetailAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<AssetDetail>> GetAssetDetailAsync(long id)
        {
            var asset = await AssetService.GetAssetDetailAsync(id);

            if (asset == null)
                return NotFound(new MessageResponse(Localizer[nameof(AssetResources.ItemNotFound)]));

            return Ok(asset);
        }

        /// <summary>
        /// Creates new Asset.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.PropertyManagement)]
        [ApiOperation(typeof(AssetController), nameof(CreateAssetAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<AssetDetail>> CreateAssetAsync([FromBody] CreateAssetParams assetParams)
        {
            var asset = await AssetService.CreateAssetAsync(assetParams, User);
            return Ok(asset);
        }

        /// <summary>
        /// Updates asset.
        /// </summary>
        /// <param name="id">Asset ID</param>
        /// <param name="assetParams"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Asset not found.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.PropertyManagement)]
        [ApiOperation(typeof(AssetController), nameof(UpdateAssetAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<AssetDetail>> UpdateAssetAsync(long id, [FromBody] UpdateAssetParams assetParams)
        {
            var asset = await AssetService.UpdateAssetAsync(id, assetParams, User);

            if (asset == null)
                return NotFound(new MessageResponse(Localizer[nameof(AssetResources.ItemNotFound)]));

            return Ok(asset);
        }

        /// <summary>
        /// Loans item.
        /// </summary>
        /// <param name="id">Asset ID</param>
        /// <response code="200">Success.</response>
        /// <response></response>
        /// <response code="404">Asset not found.</response>
        [HttpPost("{id}/loan")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(AssetController), nameof(LoanItemAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<AssetLoanInfo>> LoanItemAsync(long id)
        {
            try
            {
                var loanInfo = await AssetService.LoanAssetAsync(id, User);

                if (loanInfo == null)
                    return NotFound(new MessageResponse(Localizer[nameof(AssetResources.ItemNotFound)]));

                return Ok(loanInfo);
            }
            catch (InvalidOperationException ex)
            {
                var errDict = new Dictionary<string, string[]>()
                {
                    { "Availability", new[] { Localizer[nameof(AssetResources.AssetIsNotAvailable), ex.Message].ToString() } }
                };

                return BadRequest(new ValidationProblemDetails(errDict));
            }
        }

        /// <summary>
        /// Returns an active loan of the asset.
        /// </summary>
        /// <param name="id">Asset ID</param>
        /// <param name="loanId">Loan ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Asset, or users active loan wasn't found.</response>
        [HttpPut("{id}/loan/{loanId}/return")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(AssetController), nameof(ReturnItemAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<AssetLoanInfo>> ReturnItemAsync(long id, long loanId)
        {
            var loanInfo = await AssetService.ReturnAssetAsync(id, loanId, User);

            if (loanInfo == null)
                return NotFound(new MessageResponse(Localizer[nameof(AssetResources.AssetOrLoanNotFound)]));

            return Ok(loanInfo);
        }

        /// <summary>
        /// Gets loans of asset.
        /// </summary>
        /// <param name="id">Asset ID</param>
        /// <param name="searchParams">Search parameters</param>
        /// <response code="200">Success</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet("{id}/loan")]
        [Authorize(Policies.RegularMembers)]
        [ApiOperation(typeof(AssetController), nameof(GetLoansAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<AssetLoanInfo>>> GetLoansAsync(long id, [FromQuery] AssetLoanSearchParams searchParams)
        {
            var list = await AssetService.GetLoansAsync(id, searchParams, User);
            return Ok(list);
        }

        /// <summary>
        /// Processes inventory.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost("inventory")]
        [Authorize(Policies.PropertyManagement)]
        [ApiOperation(typeof(AssetController), nameof(ProcessInventoryAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<List<AssetListInfo>>> ProcessInventoryAsync(CreateInventoryParams inventoryParams)
        {
            var list = await AssetInventoryService.ProcessInventoryAsync(inventoryParams);
            return Ok(list);
        }

        /// <summary>
        /// Gets paginated list of inventory reports.
        /// </summary>
        /// <param name="id">Asset ID</param>
        /// <param name="userId">Optional user ID, that processed inventory.</param>
        /// <param name="paginationParams">Pagination parameters.</param>
        [HttpGet("{id}/inventory")]
        [Authorize(Policies.PropertyManagement)]
        [ApiOperation(typeof(AssetController), nameof(GetInventoryReportsListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<InventoryReport>>> GetInventoryReportsListAsync(long id, [FromQuery] long? userId, [FromQuery] PaginationParams paginationParams)
        {
            var list = await AssetInventoryService.GetInventoryReportsAsync(id, userId, paginationParams);
            return Ok(list);
        }
    }
}
