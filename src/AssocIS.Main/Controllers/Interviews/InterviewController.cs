﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Interviews;
using AssocIS.Data.Resources.Interview;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.Interviews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Interviews
{
    [ApiController]
    [Route("api/v{version:apiVersion}/interview")]
    [ApiVersion("1.0")]
    [OpenApiTag("Interviews", Description = "Interview management")]
    public class InterviewController : ControllerBase
    {
        private InterviewService InterviewService { get; }
        private IStringLocalizer<InterviewResources> Localizer { get; }

        public InterviewController(InterviewService interviewService, IStringLocalizer<InterviewResources> localizer)
        {
            InterviewService = interviewService;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets list of interviews.
        /// </summary>
        /// <param name="parameters">Search parameters</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(InterviewController), nameof(GetInterviewsListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<InterviewListItem>>> GetInterviewsListAsync([FromQuery] InterviewSearchParams parameters)
        {
            var data = await InterviewService.GetInterviewsListAsync(parameters);
            return Ok(data);
        }

        /// <summary>
        /// Creates new interview.
        /// </summary>
        /// <param name="parameters">Data</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(InterviewController), nameof(CreateInterviewAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<Interview>> CreateInterviewAsync(UpdateInterviewParams parameters)
        {
            var interview = await InterviewService.CreateInterviewAsync(parameters, User);
            return Ok(interview);
        }

        /// <summary>
        /// Gets detailed information about meeting
        /// </summary>
        /// <param name="id">Meeting Id</param>
        /// <response code="200">Success</response>
        /// <response code="404">Interview not found.</response>
        [HttpGet("{id}")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(InterviewController), nameof(GetInterviewDetailAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Interview>> GetInterviewDetailAsync(long id)
        {
            var interview = await InterviewService.GetInterviewDetailAsync(id);

            if (interview == null)
                return NotFound(new MessageResponse(Localizer[nameof(InterviewResources.InterviewNotFound)]));

            return Ok(interview);
        }

        /// <summary>
        /// Updates existing interview.
        /// </summary>
        /// <param name="id">Interview ID</param>
        /// <param name="parameters">New interview data.</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Interview not found.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(InterviewController), nameof(UpdateInterviewAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Interview>> UpdateInterviewAsync(long id, UpdateInterviewParams parameters)
        {
            var interview = await InterviewService.UpdateInterviewAsync(id, parameters, User);

            if (interview == null)
                return NotFound(new MessageResponse(Localizer[nameof(InterviewResources.InterviewNotFound)]));

            return Ok(interview);
        }

        /// <summary>
        /// Deletes existing interview.
        /// </summary>
        /// <param name="id">Interview ID</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Interview not found.</response>
        [HttpDelete("{id}")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(InterviewController), nameof(DeleteInterviewAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.BadRequest)]
        public async Task<ActionResult> DeleteInterviewAsync(long id)
        {
            if (!await InterviewService.InterviewExistsAsync(id))
                return NotFound(new MessageResponse(Localizer[nameof(InterviewResources.InterviewNotFound)]));

            await InterviewService.RemoveInterviewAsync(id);
            return Ok();
        }
    }
}
