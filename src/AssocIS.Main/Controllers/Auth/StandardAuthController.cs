﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Auth;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.Auth;
using AssocIS.Main.Models.Auth;
using AssocIS.Main.Services.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using AssocIS.Common.Extensions;

namespace AssocIS.Main.Controllers.Auth
{
    [ApiController]
    [Route("api/v{version:apiVersion}/auth")]
    [ApiVersion("1.0")]
    [Authorize]
    [OpenApiTag("Auth", Description = "Standard authentication methods.")]
    public class StandardAuthController : ControllerBase
    {
        private AuthService AuthService { get; }
        private IStringLocalizer<AuthResources> Localizer { get; }
        private ClientsService ClientsService { get; }

        public StandardAuthController(AuthService authService, IStringLocalizer<AuthResources> localizer, ClientsService clientsService)
        {
            AuthService = authService;
            Localizer = localizer;
            ClientsService = clientsService;
        }

        /// <summary>
        /// Provides standard login via username and password.
        /// </summary>
        /// <response code="200">Success</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="401">Login is not allowed. User not found, invalid password, ...</response>
        [HttpPost("login")]
        [AllowAnonymous]
        [OpenApiOperation(nameof(StandardAuthController) + "_" + nameof(LoginAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.Unauthorized)]
        public async Task<ActionResult<JwtToken>> LoginAsync([FromBody] LoginParams request)
        {
            if (!await ClientsService.VerifyClientAsync(request.ClientId))
                return StatusCode((int)HttpStatusCode.Forbidden, new MessageResponse(Localizer[nameof(AuthResources.InvalidClientId), request.ClientId]));

            var token = await AuthService.AuthenticateAsync(request.Username, request.Password);

            if (token.Code != AuthResultCodes.OK)
                return StatusCode((int)HttpStatusCode.Forbidden, new MessageResponse(Localizer[token.GetCodeResourceName()]));

            return Ok(token.Token);
        }

        /// <summary>
        /// Creates token from provided refresh token.
        /// </summary>
        /// <param name="refreshToken">Refresh token</param>
        /// <response code="200">Success</response>
        /// <response code="404">Refresh token not exists or expired.</response>
        [HttpGet("refresh")]
        [OpenApiOperation(nameof(StandardAuthController) + "_" + nameof(RefreshTokenAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<JwtToken>> RefreshTokenAsync([FromQuery, Required] string refreshToken)
        {
            var result = await AuthService.ReauthenticateAsync(refreshToken);

            if (result.Code == AuthResultCodes.RefreshTokenNotFound)
                return NotFound(new MessageResponse(Localizer[result.GetCodeResourceName()]));

            return Ok(result.Token);
        }

        /// <summary>
        /// Removes all refresh tokens of current user.
        /// </summary>
        /// <param name="onlyExpired">Remove only expired tokens.</param>
        /// <response code="200">Success</response>
        [HttpDelete("refresh")]
        [OpenApiOperation(nameof(StandardAuthController) + "_" + nameof(DeleteRefreshTokensAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult> DeleteRefreshTokensAsync([FromQuery] bool onlyExpired = false)
        {
            var userId = User.GetUserId();
            await AuthService.ClearRefreshTokensAsync(userId, onlyExpired);

            return Ok();
        }
    }
}
