﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Enums;
using AssocIS.Data.Models.Auth;
using AssocIS.Data.Models.Auth.OAuth2;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.Auth;
using AssocIS.Data.Resources.Auth.OAuth2;
using AssocIS.Main.Models.Auth;
using AssocIS.Main.Services.Auth.OAuth2;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Auth
{
    [ApiController]
    [Route("api/v{version:apiVersion}/auth/oauth2")]
    [ApiVersion("1.0")]
    [Authorize]
    [OpenApiTag("OAuth2", Description = "OAuth2 external authentication.")]
    public class OAuth2Controller : ControllerBase
    {
        private IMemoryCache Cache { get; }
        private OAuth2ServiceFactory OAuth2ServiceFactory { get; }
        private IStringLocalizer<OAuth2Resources> OAuthLocalizer { get; }
        private IStringLocalizer<AuthResources> AuthLocalizer { get; }

        public OAuth2Controller(IMemoryCache memoryCache, OAuth2ServiceFactory oAuth2ServiceFactory, IStringLocalizer<OAuth2Resources> oAuthLocalizer,
            IStringLocalizer<AuthResources> authLocalizer)
        {
            Cache = memoryCache;
            OAuth2ServiceFactory = oAuth2ServiceFactory;
            OAuthLocalizer = oAuthLocalizer;
            AuthLocalizer = authLocalizer;
        }

        /// <summary>
        /// Checks if oauth gateways are available and generates links.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet("links")]
        [AllowAnonymous]
        [ApiOperation(typeof(OAuth2Controller), nameof(GetOAuth2LinksAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<OAuth2Links>> GetOAuth2LinksAsync([FromQuery] OAuth2GetLinksParams data)
        {
            var sessionId = Guid.NewGuid().ToString();
            using var discord = OAuth2ServiceFactory.Create(OAuth2SupportedServices.Discord);
            using var google = OAuth2ServiceFactory.Create(OAuth2SupportedServices.Google);

            var links = new OAuth2Links()
            {
                Discord = await discord.GetRedirectUrlAsync(sessionId),
                Google = await google.GetRedirectUrlAsync(sessionId)
            };

            if (links.IsAnyAvailable())
                Cache.Set(sessionId, new OAuth2SessionData(data.ClientRedirectUrl), DateTime.Now.AddHours(1));

            return Ok(links);
        }

        /// <summary>
        /// Callback from oauth gateway.
        /// </summary>
        /// <response code="302">Success, redirect to page defined in /links.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet("callback")]
        [AllowAnonymous]
        [ApiOperation(typeof(OAuth2Controller), nameof(OnOAuth2CallbackAsync))]
        [ProducesResponse(HttpStatusCode.Redirect)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult> OnOAuth2CallbackAsync([FromQuery] OAuth2CallbackParams data)
        {
            using var service = OAuth2ServiceFactory.Create(data.Service.Value);
            var url = await service.CreateRedirectUrlAsync(data.SessionId.Value, data.Code);

            return Redirect(url);
        }

        /// <summary>
        /// Creates token from oauth2 session.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="403">Unauthorized access. (Non-existent user, non-paired account, ...)</response>
        /// <response code="404">Session wasn't found in session storage.</response>
        [HttpGet("token")]
        [AllowAnonymous]
        [ApiOperation(typeof(OAuth2Controller), nameof(CreateAuthTokenAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.Forbidden)]
        public async Task<ActionResult<JwtToken>> CreateAuthTokenAsync([FromQuery] OAuth2CreateTokenParams parameters)
        {
            var sessionId = parameters.SessionId.ToString();
            if (!Cache.TryGetValue(sessionId, out OAuth2SessionData data))
                return NotFound(new MessageResponse(OAuthLocalizer[nameof(OAuth2Resources.InvalidSession)]));

            using var service = OAuth2ServiceFactory.Create(data.Service);
            var authResult = await service.CreateAuthAsync(data.UserId);

            Cache.Remove(sessionId);
            if (authResult.Code != AuthResultCodes.OK)
                return StatusCode((int)HttpStatusCode.Forbidden, new MessageResponse(AuthLocalizer[authResult.GetCodeResourceName()]));

            return Ok(authResult.Token);
        }
    }
}
