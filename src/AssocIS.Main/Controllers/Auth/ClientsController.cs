﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Auth.Clients;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.Auth;
using AssocIS.Main.Services.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Auth
{
    [ApiController]
    [Route("api/v{version:apiVersion}/auth/clients")]
    [ApiVersion("1.0")]
    [Authorize(Policies.AppManagement)]
    [OpenApiTag("Clients", Description = "Clients management. Required for authentication.")]
    public class ClientsController : ControllerBase
    {
        private IStringLocalizer<AuthResources> Localizer { get; }
        private ClientsService ClientsService { get; }

        public ClientsController(IStringLocalizer<AuthResources> localizer, ClientsService clientsService)
        {
            Localizer = localizer;
            ClientsService = clientsService;
        }

        /// <summary>
        /// Gets list of registered clients.
        /// </summary>
        /// <param name="nameSearch">Name of client, that must contains.</param>
        /// <param name="pagination">Pagination request data.</param>
        /// <response code="200">Success</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet]
        [OpenApiOperation(nameof(ClientsController) + "_" + nameof(GetClientsListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<ClientListItem>>> GetClientsListAsync([FromQuery] string nameSearch, [FromQuery] PaginationParams pagination)
        {
            var list = await ClientsService.GetClientsListAsync(nameSearch, pagination);
            return Ok(list);
        }

        /// <summary>
        /// Gets detailed information about registered client.
        /// </summary>
        /// <param name="id">ID of client</param>
        /// <response code="200">Success</response>
        /// <response code="404">Client not exists.</response>
        [HttpGet("{id}")]
        [OpenApiOperation(nameof(ClientsController) + "_" + nameof(GetClientAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Client>> GetClientAsync(long id)
        {
            var client = await ClientsService.GetClientAsync(id);

            if(client == null)
                return NotFound(new MessageResponse(Localizer[nameof(AuthResources.InvalidClientId), id]));

            return Ok(client);
        }

        /// <summary>
        /// Creates new client.
        /// </summary>
        [HttpPost]
        [OpenApiOperation(nameof(ClientsController) + "_" + nameof(CreateClientAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<Client>> CreateClientAsync(UpdateClientParams request)
        {
            var client = await ClientsService.CreateClientAsync(request);
            return Ok(client);
        }

        /// <summary>
        /// Updates client application.
        /// </summary>
        /// <param name="id">Internal client ID</param>
        /// <param name="request"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed</response>
        /// <response code="404">Client not found.</response>
        [HttpPut("{id}")]
        [OpenApiOperation(nameof(ClientsController) + "_" + nameof(UpdateClientAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Client>> UpdateClientAsync(long id, [FromBody] UpdateClientParams request)
        {
            var client = await ClientsService.UpdateClientAsync(id, request);

            if (client == null)
                return NotFound(new MessageResponse(Localizer[nameof(AuthResources.InvalidClientId), id]));

            return Ok(client);
        }

        /// <summary>
        /// Revokes old client ID and generates new.
        /// </summary>
        /// <param name="id">Internal client ID.</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Client not found.</response>
        [HttpPut("{id}/regenerate")]
        [OpenApiOperation(nameof(ClientsController) + "_" + nameof(RegenerateClientIdAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Client>> RegenerateClientIdAsync(long id)
        {
            var client = await ClientsService.RegenerateClientIdAsync(id);

            if (client == null)
                return NotFound(new MessageResponse(Localizer[nameof(AuthResources.InvalidClientId), id]));

            return Ok(client);
        }

        /// <summary>
        /// Removes client.
        /// </summary>
        /// <param name="id">Internal client ID.</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Client not found.</response>
        [HttpDelete("{id}")]
        [OpenApiOperation(nameof(ClientsController) + "_" + nameof(DeleteClientAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> DeleteClientAsync(long id)
        {
            if(!await ClientsService.ClientExistsAsync(id))
                return NotFound(new MessageResponse(Localizer[nameof(AuthResources.InvalidClientId), id]));

            await ClientsService.RemoveClientAsync(id);
            return Ok();
        }
    }
}
