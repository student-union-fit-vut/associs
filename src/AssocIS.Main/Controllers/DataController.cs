﻿using AssocIS.Common.Attributes;
using AssocIS.Main.Services.Assets;
using AssocIS.Main.Services.External;
using AssocIS.Main.Services.External.Discord;
using AssocIS.Main.Services.Teams;
using AssocIS.Main.Services.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/data")]
    [ApiVersion("1.0")]
    [OpenApiTag("Data", Description = "Data controller for filling data in forms, etc...")]
    public class DataController : Controller
    {
        private DiscordExternalService DiscordExternalService { get; set; }

        public DataController(IEnumerable<ExternalService> externalServices)
        {
            DiscordExternalService = externalServices.OfType<DiscordExternalService>().FirstOrDefault();
        }

        /// <summary>
        /// Get list of available roles on discord.
        /// </summary>
        /// <remarks>
        /// This method will take 5 seconds to clients have time to provide data.
        /// </remarks>
        /// <responses code="200">Success</responses>
        [HttpGet("roles/discord")]
        [Authorize]
        [ApiOperation(typeof(DataController), nameof(GetDiscordRolesListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<Dictionary<string, string>>> GetDiscordRolesListAsync()
        {
            var roles = await DiscordExternalService.GetHomeGuildRolesAsync();
            var availableRoles = roles.ToDictionary(o => o.Key.ToString(), o => o.Value);
            return Ok(availableRoles);
        }

        /// <summary>
        /// Gets simple list of available users.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("users")]
        [Authorize]
        [ApiOperation(typeof(DataController), nameof(GetUsersSelectListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<Dictionary<long, string>>> GetUsersSelectListAsync()
        {
            var service = HttpContext.RequestServices.GetService<UsersService>();
            var users = await service.GetUserSelectListAsync();

            return Ok(users);
        }

        /// <summary>
        /// Gets simple list of available items.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("assets")]
        [Authorize]
        [ApiOperation(typeof(DataController), nameof(GetAssetsSelectListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<Dictionary<long, string>>> GetAssetsSelectListAsync()
        {
            var service = HttpContext.RequestServices.GetService<AssetService>();
            var assets = await service.GetAssetsSelectListAsync();

            return Ok(assets);
        }

        /// <summary>
        /// Gets simple list of users available in discord.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("users/discord")]
        [Authorize]
        [ApiOperation(typeof(DataController), nameof(GetUsersInDiscordHomeGuildAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<Dictionary<string, string>>> GetUsersInDiscordHomeGuildAsync()
        {
            var users = (await DiscordExternalService.GetMembersOfHomeGuildAsync())
                .ToDictionary(o => o.Key.ToString(), o => o.Value);
            return Ok(users);
        }

        /// <summary>
        /// Get list of channels in discord home guild.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("channels/discord")]
        [Authorize]
        [ApiOperation(typeof(DataController), nameof(GetTextChannelsInDiscordHomeGuildAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<Dictionary<string, string>>> GetTextChannelsInDiscordHomeGuildAsync()
        {
            var textChannels = (await DiscordExternalService.GetTextChannelsOfHomeGuildAsync())
                .ToDictionary(o => o.Key.ToString(), o => o.Value);
            return Ok(textChannels);
        }

        /// <summary>
        /// Gets list of teams.
        /// </summary>
        /// <param name="ignoredUsers">List of members. Team is ignored if user is member.</param>
        /// <response code="200">Success</response>
        [HttpGet("teams")]
        [Authorize]
        [ApiOperation(typeof(DataController), nameof(GetTeamsSelectListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        public async Task<ActionResult<Dictionary<long, string>>> GetTeamsSelectListAsync([FromQuery] long[] ignoredUsers = null)
        {
            var service = HttpContext.RequestServices.GetService<TeamsService>();
            var teams = await service.GetTeamsSelectListAsync(ignoredUsers);

            return Ok(teams);
        }
    }
}
