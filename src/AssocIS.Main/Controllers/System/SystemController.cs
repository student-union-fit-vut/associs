﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Diagnostics;
using AssocIS.Main.Models.Configuration;
using AssocIS.Main.Models.Configuration.External.Discord;
using AssocIS.Main.Models.Configuration.External.Google;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NSwag.Annotations;
using System.Net;
using System.Reflection;
using ClientConfigurationModel = AssocIS.Data.Models.Configuration.ClientConfiguration;

namespace AssocIS.Main.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/system")]
    [ApiVersion("1.0")]
    [OpenApiTag("System", Description = "Internal methods to diagnostics, configurations, etc.")]
    public class SystemController : Controller
    {
        private RequestsCounterService RequestsCounter { get; }
        private IConfiguration Configuration { get; }

        public SystemController(RequestsCounterService requestsCounter, IConfiguration configuration)
        {
            RequestsCounter = requestsCounter;
            Configuration = configuration;
        }

        /// <summary>
        /// Gets diagnostics data of application.
        /// </summary>
        [HttpGet("diag")]
        [Authorize(Policies.AppManagement)]
        [ApiOperation(typeof(SystemController), nameof(GetDiagnosticsData))]
        [ProducesResponse(HttpStatusCode.OK)]
        public ActionResult<DiagnosticData> GetDiagnosticsData()
        {
            var data = new DiagnosticData(RequestsCounter.Count);
            return Ok(data);
        }

        /// <summary>
        /// Gets client configuration.
        /// </summary>
        /// <response code="200">Success</response>
        [HttpGet("client")]
        [AllowAnonymous]
        [ApiOperation(typeof(SystemController), nameof(GetClientConfiguration))]
        [ProducesResponse(HttpStatusCode.OK)]
        public ActionResult<ClientConfigurationModel> GetClientConfiguration()
        {
            var clientConfiguration = Configuration.GetSection("Client").Get<ClientConfiguration>();
            var discordConfiguration = Configuration.GetSection("Discord").Get<DiscordConfiguration>();
            var googleConfiguration = Configuration.GetSection("Google").Get<GoogleConfiguration>();

            return Ok(new ClientConfigurationModel()
            {
                ClientId = clientConfiguration.ClientId,
                DefaultLanguage = Configuration["DefaultLocale"],
                Modules = clientConfiguration.Modules,
                Version = Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                DiscordEnabled = discordConfiguration.Enabled,
                GoogleEnabled = googleConfiguration.Enabled
            });
        }
    }
}
