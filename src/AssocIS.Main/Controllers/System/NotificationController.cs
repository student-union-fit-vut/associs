﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Enums;
using AssocIS.Data.Models.System;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Services.External;
using AssocIS.Main.Services.External.Discord;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.System
{
    [ApiController]
    [Route("api/v{version:apiVersion}/system/notifications")]
    [ApiVersion("1.0")]
    [OpenApiTag("System", Description = "Internal system methods.")]
    public class NotificationController : ControllerBase
    {
        private IEnumerable<ExternalService> ExternalServices { get; }

        public NotificationController(IEnumerable<ExternalService> externalServices)
        {
            ExternalServices = externalServices;
        }

        /// <summary>
        /// Sends notification to all external services of specific client type.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost("external")]
        [Authorize(Policies.TeamLeaders)]
        [ApiOperation(typeof(NotificationController), nameof(NotifyExternalServicesAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult> NotifyExternalServicesAsync(NotifyExternalServicesParams parameters)
        {
            var services = ExternalServices
                .Where(o => parameters.Type == EveryoneNotificationType.Discord && o is DiscordExternalService)
                .ToList();

            await services.AggregatedExecutionAsync(service => service.EveryoneMessageAsync(parameters.Content));
            return Ok();
        }
    }
}
