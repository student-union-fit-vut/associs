﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Events;
using AssocIS.Data.Models.Events.Comments;
using AssocIS.Data.Resources.Events;
using AssocIS.Main.Services.Events;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Events
{
    [ApiController]
    [Route("api/v{version:apiVersion}/event")]
    [ApiVersion("1.0")]
    [OpenApiTag("Events", Description = "Events management")]
    public class EventController : ControllerBase
    {
        private EventService EventService { get; set; }
        private IStringLocalizer<EventResources> Localizer { get; }

        public EventController(EventService eventService, IStringLocalizer<EventResources> localizer)
        {
            EventService = eventService;
            Localizer = localizer;
        }

        /// <summary>
        /// Gets paginated list of events.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet]
        [Authorize]
        [ApiOperation(typeof(EventController), nameof(GetEventsListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<EventListItem>>> GetEventsListAsync([FromQuery] SearchEventParams parameters)
        {
            var data = await EventService.GetEventsListAsync(parameters, User);
            return Ok(data);
        }

        /// <summary>
        /// Gets detailed information about event.
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Event not found</response>
        [HttpGet("{id}")]
        [Authorize]
        [ApiOperation(typeof(EventController), nameof(GetEventDetailAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Event>> GetEventDetailAsync(long id)
        {
            var @event = await EventService.GetEventDetailAsync(id, User);

            if (@event == null)
                return NotFound(new MessageResponse(Localizer[nameof(EventResources.EventNotFound)]));

            return Ok(@event);
        }

        /// <summary>
        /// Creates new event.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize]
        [ApiOperation(typeof(EventController), nameof(CreateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<Event>> CreateAsync(UpdateEventParams parameters)
        {
            var @event = await EventService.CreateAsync(parameters, User);
            return Ok(@event);
        }

        /// <summary>
        /// Updates event.
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="parameters"></param>
        /// <reseponse code="200">Success.</reseponse>
        /// <reseponse code="400">Validation failed.</reseponse>
        /// <reseponse code="403">User not have permissions to modify event.</reseponse>
        /// <reseponse code="404">Event not found.</reseponse>
        [HttpPut("{id}")]
        [Authorize]
        [ApiOperation(typeof(EventController), nameof(CreateAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.Forbidden)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<Event>> UpdateAsync(long id, UpdateEventParams parameters)
        {
            if (!await EventService.CanManageEventAsync(id, User))
                return StatusCode((int)HttpStatusCode.Forbidden, new MessageResponse(Localizer[nameof(EventResources.ForbiddenEventAccess)]));

            var @event = await EventService.UpdateAsync(id, parameters, User);

            if (@event == null)
                return NotFound(new MessageResponse(Localizer[nameof(EventResources.EventNotFound)]));

            return Ok(@event);
        }

        /// <summary>
        /// Removes event.
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <reseponse code="200">Success.</reseponse>
        /// <reseponse code="403">User not have permissions to modify event.</reseponse>
        /// <reseponse code="404">Event not found.</reseponse>
        [HttpDelete("{id}")]
        [Authorize]
        [ApiOperation(typeof(EventController), nameof(RemoveAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.Forbidden)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> RemoveAsync(long id)
        {
            if (!await EventService.CanManageEventAsync(id, User))
                return StatusCode((int)HttpStatusCode.Forbidden, new MessageResponse(Localizer[nameof(EventResources.ForbiddenEventAccess)]));

            if (!await EventService.EventExistsAsync(id))
                return NotFound(new MessageResponse(Localizer[nameof(EventResources.EventNotFound)]));

            await EventService.RemoveAsync(id, User);
            return Ok();
        }

        /// <summary>
        /// Gets paginated list of comments.
        /// </summary>
        /// <param name="eventId">Event ID</param>
        /// <param name="parameters"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet("{eventId}/comments")]
        [Authorize]
        [ApiOperation(typeof(EventController), nameof(GetEventCommmentsListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<EventComment>>> GetEventCommmentsListAsync(long eventId, [FromQuery] SearchCommentParams parameters)
        {
            var comments = await EventService.GetCommentsAsync(eventId, parameters);
            return Ok(comments);
        }

        /// <summary>
        /// Posts new comment to event.
        /// </summary>
        /// <param name="eventId">Event ID</param>
        /// <param name="parameters"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Event not found.</response>
        [HttpPost("{eventId}/comment")]
        [Authorize]
        [ApiOperation(typeof(EventController), nameof(PostCommentAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<EventComment>> PostCommentAsync(long eventId, UpdateCommentParams parameters)
        {
            var comment = await EventService.PostCommentAsync(eventId, parameters, User);

            if (comment == null)
                return NotFound(new MessageResponse(Localizer[nameof(EventResources.EventNotFound)]));

            return Ok(comment);
        }

        /// <summary>
        /// Updates existing comment.
        /// </summary>
        /// <param name="eventId">Event ID</param>
        /// <param name="commentId">Comment ID</param>
        /// <param name="parameters"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Comment not found or user not have permission to update.</response>
        [HttpPut("{eventId}/comment/{commentId}")]
        [Authorize]
        [ApiOperation(typeof(EventController), nameof(UpdateCommentAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<EventComment>> UpdateCommentAsync(long eventId, long commentId, UpdateCommentParams parameters)
        {
            var comment = await EventService.UpdateCommentAsync(eventId, commentId, parameters, User);

            if (comment == null)
                return NotFound(new MessageResponse(Localizer[nameof(EventResources.EventCommentNotFound)]));

            return Ok(comment);
        }

        /// <summary>
        /// Removes comment.
        /// </summary>
        /// <param name="eventId">Event ID</param>
        /// <param name="commentId">Comment ID</param>
        /// <response code="200">Success.</response>
        /// <response code="403">User not have permission to remove comment.</response>
        [HttpDelete("{eventId}/comment/{commentId}")]
        [Authorize]
        [ApiOperation(typeof(EventController), nameof(RemoveCommentAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.Forbidden)]
        public async Task<ActionResult> RemoveCommentAsync(long eventId, long commentId)
        {
            if (!await EventService.CanRemoveCommentAsync(eventId, commentId, User))
                return StatusCode((int)HttpStatusCode.Forbidden, new MessageResponse(Localizer[nameof(EventResources.ForbiddenCommentAccess)]));

            await EventService.RemoveCommentAsync(eventId, commentId, User);
            return Ok();
        }
    }
}
