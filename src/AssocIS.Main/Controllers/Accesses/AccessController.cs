﻿using AssocIS.Common.Attributes;
using AssocIS.Data.Models.Accesses;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.Access;
using AssocIS.Main.Services.Accesses;
using AssocIS.Main.Services.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSwag.Annotations;
using System.Net;
using System.Threading.Tasks;

namespace AssocIS.Main.Controllers.Accesses
{
    [ApiController]
    [Route("api/v{version:apiVersion}/access")]
    [ApiVersion("1.0")]
    [OpenApiTag("Access", Description = "Access management to areas, ...")]
    public class AccessController : ControllerBase
    {
        private AccessService AccessService { get; }
        private IStringLocalizer<AccessResources> Localizer { get; }

        public AccessController(AccessService service, IStringLocalizer<AccessResources> localizer)
        {
            AccessService = service;
            Localizer = localizer;
        }

        /// <summary>
        /// Creates new access definition.
        /// </summary>
        /// <response code="200">Success</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(AccessController), nameof(CreateAccessAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<AccessDefinition>> CreateAccessAsync(UpdateAccessDefinitionParams parameters)
        {
            var access = await AccessService.CreateAccessDefinitionAsync(parameters, User);
            return Ok(access);
        }

        /// <summary>
        /// Gets detail of access definition.
        /// </summary>
        /// <param name="id">Access definition ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Access definition not found.</response>
        [HttpGet("{id}")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(AccessController), nameof(GetAccessDetailAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<AccessDefinition>> GetAccessDetailAsync(long id)
        {
            var access = await AccessService.GetAccessDefinitionDetailAsync(id);

            if (access == null)
                return NotFound(new MessageResponse(Localizer[nameof(AccessResources.AccessNotFound)]));

            return Ok(access);
        }

        /// <summary>
        /// Gets paginated list of access definitions.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        [HttpGet]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(AccessController), nameof(GetAccessListAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<AccessDefinitionListItem>>> GetAccessListAsync([FromQuery] AccessDefinitionSearchParams parameters)
        {
            var data = await AccessService.GetAccessDefinitionListAsync(parameters);
            return Ok(data);
        }

        /// <summary>
        /// Gets paginated list of all members to access.
        /// </summary>
        /// <param name="id">Access ID.</param>
        /// <param name="pagination">Pagination parameters.</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Not found.</response>
        [HttpGet("{id}/members")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(AccessController), nameof(GetAccessMembersList))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<PaginatedData<AccessMemberListItem>>> GetAccessMembersList(long id, [FromQuery] PaginationParams pagination)
        {
            var members = await AccessService.GetAccessMembersAsync(id, pagination);

            if (members == null)
                return NotFound(new MessageResponse(Localizer[nameof(AccessResources.AccessNotFound)]));

            return Ok(members);
        }

        /// <summary>
        /// Sets specific access for user.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Access definition not found.</response>
        [HttpPost("{id}/members")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(AccessController), nameof(SetAccessMemberAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        public async Task<ActionResult<AccessMember>> SetAccessMemberAsync(long id, SetAccessMemberParams parameters)
        {
            if (!await AccessService.AccessDefinitionExistsAsync(id))
                return NotFound(new MessageResponse(Localizer[nameof(AccessResources.AccessNotFound)]));

            var member = await AccessService.SetAccessMemberAsync(id, parameters, User);
            return Ok(member);
        }

        /// <summary>
        /// Gets detailed information about access for user.
        /// </summary>
        /// <param name="id">Access ID</param>
        /// <param name="userId">User ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Access for user not exists.</response>
        [HttpGet("{id}/members/{userId}")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(AccessController), nameof(GetAccessMemberDetailAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<AccessMember>> GetAccessMemberDetailAsync(long id, long userId)
        {
            var detail = await AccessService.GetAccessMemberDetailAsync(id, userId);

            if (detail == null)
                return NotFound(new MessageResponse(Localizer[nameof(AccessResources.AccessMemberNotFound)]));

            return Ok(detail);
        }

        /// <summary>
        /// Updates access definition.
        /// </summary>
        /// <param name="id">Access ID</param>
        /// <param name="parameters">New data</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Validation failed.</response>
        /// <response code="404">Access definition not found.</response>
        [HttpPut("{id}")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(AccessController), nameof(UpdateAccessDefinitionAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(ValidationProblemDetails), HttpStatusCode.BadRequest)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult<AccessDefinition>> UpdateAccessDefinitionAsync(long id, UpdateAccessDefinitionParams parameters)
        {
            var access = await AccessService.UpdateAccessDefinitionAsync(id, parameters, User);

            if (access == null)
                return NotFound(new MessageResponse(Localizer[nameof(AccessResources.AccessNotFound)]));

            return Ok(access);
        }

        /// <summary>
        /// Removes access definition and all access members.
        /// </summary>
        /// <param name="id">Access ID</param>
        /// <response code="200">Success.</response>
        /// <response code="404">Access definition not found.</response>
        [HttpDelete("{id}")]
        [Authorize(Policies.UserManagement)]
        [ApiOperation(typeof(AccessController), nameof(RemoveAccessDefinitionAsync))]
        [ProducesResponse(HttpStatusCode.OK)]
        [ProducesResponse(typeof(MessageResponse), HttpStatusCode.NotFound)]
        public async Task<ActionResult> RemoveAccessDefinitionAsync(long id)
        {
            if(!await AccessService.AccessDefinitionExistsAsync(id))
                return NotFound(new MessageResponse(Localizer[nameof(AccessResources.AccessNotFound)]));

            await AccessService.RemoveAccessDefinitionAsync(id);
            return Ok();
        }
    }
}
