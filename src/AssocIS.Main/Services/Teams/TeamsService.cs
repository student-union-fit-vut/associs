﻿using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Teams;
using AssocIS.Database.Services;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Common.Extensions;
using System;
using AssocIS.Main.Services.Auth;
using AssocIS.Main.Infrastructure.Auth;
using AssocIS.Data.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using AssocIS.Main.Models.Configuration.External.Discord;
using AssocIS.Main.Services.External.Discord;
using AssocIS.Main.Services.External;
using AssocIS.Data.Models.Users;

namespace AssocIS.Main.Services.Teams
{
    [Service(ServiceLifetime.Scoped)]
    public class TeamsService
    {
        private IAssocISRepository Repository { get; }
        private DiscordConfiguration DiscordConfiguration { get; }
        private DiscordExternalService DiscordExternalService { get; }
        private IEnumerable<ExternalService> ExternalServices { get; }

        public TeamsService(IAssocISRepository repository, IOptions<DiscordConfiguration> options, IEnumerable<ExternalService> externalServices)
        {
            Repository = repository;
            DiscordConfiguration = options.Value;
            ExternalServices = externalServices;
            DiscordExternalService = externalServices.OfType<DiscordExternalService>().FirstOrDefault();
        }

        public async Task<PaginatedData<TeamListItem>> GetTeamsListAsync(TeamSearchParams request, ClaimsPrincipal loggedUser)
        {
            var query = Repository.Team.GetTeamsQuery(request.NameQuery, request.OnlyEmptyTeams);
            return await PaginatedData<TeamListItem>.CreateAsync(query, request, entity => new TeamListItem(entity, CheckRemovable(entity, loggedUser)));
        }

        public async Task<Team> GetTeamAsync(long id, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Team.FindTeamByIdAsync(id);
            return entity == null ? null : new Team(entity, CheckRemovable(entity, loggedUser));
        }

        public async Task<Team> CreateTeamAsync(UpdateTeamParams request, ClaimsPrincipal loggedUser)
        {
            var entity = request.ToEntity();

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            var detail = await GetTeamAsync(entity.Id, loggedUser);
            await ExternalServices.AggregatedExecutionAsync(service => service.TeamCreatedAsync(detail, request.CreateDiscordRole));

            return detail;
        }

        public async Task<Team> ModifyTeamAsync(long teamId, UpdateTeamParams request, ClaimsPrincipal loggedUser)
        {
            var team = await Repository.Team.FindTeamByIdAsync(teamId);
            var teamBefore = new Team(team, CheckRemovable(team, loggedUser));

            if (team == null)
                return null; // Team not exists.

            request.UpdateEntity(team);
            await Repository.CommitAsync();

            var detail = await GetTeamAsync(teamId, loggedUser);
            await ExternalServices.AggregatedExecutionAsync(service => service.TeamUpdatedAsync(teamBefore, detail, request.CreateDiscordRole));
            return detail;
        }

        public async Task<PaginatedData<TeamMember>> GetTeamMembersAsync(long teamId, PaginationParams request)
        {
            if (!await TeamExistsAsync(teamId))
                return null;

            var query = Repository.Team.GetTeamMembersQuery(teamId);
            return await PaginatedData<TeamMember>.CreateAsync(query, request, entity => new TeamMember(entity));
        }

        public async Task<bool> TeamExistsAsync(long teamId)
        {
            return await Repository.Team.TeamExistsAsync(teamId);
        }

        public async Task<bool> CheckRemovableAsync(long id, ClaimsPrincipal loggedUser)
        {
            var team = await Repository.Team.FindTeamByIdAsync(id);
            return team != null && CheckRemovable(team, loggedUser);
        }

        private static bool CheckRemovable(Database.Entity.Team team, ClaimsPrincipal loggedUser)
        {
            if (loggedUser.IsInPolicy(Policies.UserManagement))
                return true;

            var loggedUserId = loggedUser.GetUserId();
            var leader = team.Members.FirstOrDefault(o => o.UserId == loggedUserId);
            return leader?.IsLeader == true;
        }

        public async Task RemoveTeamAsync(long teamId, ClaimsPrincipal loggedUser)
        {
            var team = await Repository.Team.FindTeamByIdAsync(teamId);

            if (team == null)
                return;

            Repository.RemoveCollection(team.Members);
            Repository.Remove(team);
            await Repository.CommitAsync();

            await ExternalServices.AggregatedExecutionAsync(
                service => service.TeamRemovedAsync(new Team(team, CheckRemovable(team, loggedUser)))
            );
        }

        public async Task<Team> RemoveTeamMemberAsync(long teamId, long userId, ClaimsPrincipal caller)
        {
            var team = await Repository.Team.FindTeamByIdAsync(teamId);
            var teamMember = team.Members.FirstOrDefault(o => o.UserId == userId);

            // User is not team member. Dont have to some do.
            if (teamMember == null)
                return new Team(team, CheckRemovable(team, caller));

            if (teamMember.IsLeader && teamMember.UserId == caller.GetUserId() && !caller.IsInPolicy(Policies.UserManagement))
                throw new UnauthorizedAccessException();

            Repository.Remove(teamMember);
            await Repository.CommitAsync();
            await ExternalServices.AggregatedExecutionAsync(
                service => service.SynchronizeMembersAsync(new List<UserDetail>() { new(teamMember.User, caller.GetUserId()) })
            );

            return await GetTeamAsync(teamId, caller);
        }

        public async Task<Team> AddTeamMemberAsync(UpdateTeamMemberParams request, ClaimsPrincipal caller)
        {
            var team = await Repository.Team.FindTeamByIdAsync(request.TeamId);
            var teamMember = team.Members.FirstOrDefault(o => o.UserId == request.UserId);

            if (teamMember != null) // Now is in team. 
                return new Team(team, CheckRemovable(team, caller));

            var callerId = caller.GetUserId();
            var leader = team.Members.FirstOrDefault(o => o.UserId == callerId && o.IsLeader);
            if (leader == null && !caller.IsInPolicy(Policies.UserManagement))
                throw new UnauthorizedAccessException();

            var entity = request.ToEntity();
            team.Members.Add(entity);
            await Repository.CommitAsync();

            teamMember = await Repository.Team.GetTeamMembersQuery(request.TeamId).FirstOrDefaultAsync(o => o.UserId == request.UserId);
            await ExternalServices.AggregatedExecutionAsync(
                service => service.SynchronizeMembersAsync(new() { new(teamMember.User, caller.GetUserId()) })
            );

            return await GetTeamAsync(request.TeamId, caller);
        }

        public async Task<bool> TeamNameExistsAsync(string teamName)
        {
            return await Repository.Team.GetTeamsQuery(null, false).AnyAsync(o => o.Name == teamName);
        }

        public async Task<Dictionary<long, string>> GetTeamsSelectListAsync(long[] ignoredUsers)
        {
            var query = Repository.Team.GetTeamsQuery(null, false);

            if (ignoredUsers?.Length > 0)
                query = query.Where(o => !o.Members.Any(x => ignoredUsers.Contains(x.UserId)));

            query.Select(o => new { o.Id, o.Name }).OrderBy(o => o.Name);
            return await query.ToDictionaryAsync(o => o.Id, o => o.Name);
        }
    }
}
