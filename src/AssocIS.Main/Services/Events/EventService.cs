﻿using AssocIS.Data.Infrastructure;
using AssocIS.Data.Models.Events;
using AssocIS.Database.Services;
using AssocIS.Common.Extensions;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Data.Models.Common;
using AssocIS.Main.Infrastructure.Auth;
using AssocIS.Main.Services.Auth;
using Microsoft.EntityFrameworkCore;
using AssocIS.Data.Models.Events.Comments;
using System.Collections.Generic;
using AssocIS.Main.Services.External;

namespace AssocIS.Main.Services.Events
{
    [Service]
    public class EventService
    {
        private IAssocISRepository Repository { get; }
        private IEnumerable<ExternalService> ExternalServices { get; }

        public EventService(IAssocISRepository repository, IEnumerable<ExternalService> externalServices)
        {
            Repository = repository;
            ExternalServices = externalServices;
        }

        public async Task<PaginatedData<EventListItem>> GetEventsListAsync(SearchEventParams parameters, ClaimsPrincipal loggedUser)
        {
            var query = Repository.Event.GetEventsListQuery(parameters.NameQuery, parameters.ExcludeFuture, parameters.ExcludeOngoing,
                parameters.ExcludePast).AsNoTracking();
            return await PaginatedData<EventListItem>.CreateAsync(query, parameters, entity => new EventListItem(entity, CanManageEvent(entity, loggedUser)));
        }

        public async Task<Event> CreateAsync(UpdateEventParams parameters, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var entity = parameters.ToEntity(loggedUserId);

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            entity = await Repository.Event.GetEventByIdAsync(entity.Id);
            // Todo. count on deactivation.
            var eventModel = new Event(entity, CanManageEvent(entity, loggedUser));
            await ExternalServices.AggregatedExecutionAsync(service => service.EventCreatedAsync(eventModel, parameters.CreateDiscordChannel));
            return eventModel;
        }

        public async Task<Event> GetEventDetailAsync(long id, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Event.GetEventByIdAsync(id);
            return entity == null ? null : new Event(entity, CanManageEvent(entity, loggedUser));
        }

        public async Task<Event> UpdateAsync(long id, UpdateEventParams parameters, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Event.GetEventByIdAsync(id);

            if (entity == null)
                return null;

            var loggedUserId = loggedUser.GetUserId();
            parameters.UpdateEntity(entity, loggedUserId);

            await Repository.CommitAsync();

            entity = await Repository.Event.GetEventByIdAsync(id);
            var eventModel = new Event(entity, CanManageEvent(entity, loggedUser));
            await ExternalServices.AggregatedExecutionAsync(service => service.EventUpdatedAsync(eventModel, parameters.CreateDiscordChannel));
            return eventModel;
        }

        public async Task RemoveAsync(long id, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Event.GetEventByIdAsync(id, true);

            if (entity == null)
                return;

            if (entity.Comments.Count > 0)
                Repository.RemoveCollection(entity.Comments);

            await ExternalServices.AggregatedExecutionAsync(service => service.EventRemovedAsync(new Event(entity, CanManageEvent(entity, loggedUser))));
            Repository.Remove(entity);
            await Repository.CommitAsync();
        }

        public async Task<bool> EventExistsAsync(long id)
        {
            return await Repository.Event.EventExistsAsync(id);
        }

        public async Task<EventComment> PostCommentAsync(long eventId, UpdateCommentParams parameters, ClaimsPrincipal loggedUser)
        {
            var @event = await Repository.Event.GetEventByIdAsync(eventId, true);

            if (@event == null)
                return null;

            var loggedUserId = loggedUser.GetUserId();
            var entity = parameters.ToEntity(loggedUserId);
            @event.Comments.Add(entity);
            await Repository.CommitAsync();

            entity = await Repository.Event.GetCommentByIdAsync(eventId, entity.Id);
            return new EventComment(entity);
        }

        public async Task<EventComment> UpdateCommentAsync(long eventId, long commentId, UpdateCommentParams parameters, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Event.GetCommentByIdAsync(eventId, commentId);

            var loggedUserId = loggedUser.GetUserId();
            if (entity == null || entity.CreatedById != loggedUserId)
                return null;

            parameters.UpdateEntity(entity, loggedUserId);
            await Repository.CommitAsync();

            return new EventComment(entity);
        }

        public async Task<bool> CanRemoveCommentAsync(long eventId, long commentId, ClaimsPrincipal loggedUser)
        {
            if (loggedUser.IsInPolicy(Policies.EventManagement))
                return true;

            var loggedUserId = loggedUser.GetUserId();
            return await Repository.Event.ExistsCommentAsync(eventId, commentId, loggedUserId);
        }

        public async Task RemoveCommentAsync(long eventId, long commentId, ClaimsPrincipal loggedUser)
        {
            if (!await CanRemoveCommentAsync(eventId, commentId, loggedUser))
                return;

            var entity = await Repository.Event.GetCommentByIdAsync(eventId, commentId);

            if (entity == null)
                return;

            Repository.Remove(entity);
            await Repository.CommitAsync();
        }

        public async Task<PaginatedData<EventComment>> GetCommentsAsync(long eventId, SearchCommentParams parameters)
        {
            var query = Repository.Event.GetCommentsQuery(eventId, parameters.CreatedById, parameters.TextQuery).AsNoTracking();
            return await PaginatedData<EventComment>.CreateAsync(query, parameters, entity => new EventComment(entity));
        }

        public async Task PairCalendarIdWithEventAsync(long eventId, string calendarEventId)
        {
            var entity = await Repository.Event.GetEventByIdAsync(eventId);

            if (entity == null)
                return;

            entity.CalendarEventId = calendarEventId;
            await Repository.CommitAsync();
        }

        public async Task PairChannelIdWithEventAsync(long eventId, string channelId)
        {
            var entity = await Repository.Event.GetEventByIdAsync(eventId);

            if (entity == null)
                return;

            entity.DiscordChannelId = channelId;
            await Repository.CommitAsync();
        }

        public async Task<bool> CanManageEventAsync(long id, ClaimsPrincipal loggedUser)
        {
            var @event = await Repository.Event.GetEventByIdAsync(id, false);
            return CanManageEvent(@event, loggedUser);
        }

        private static bool CanManageEvent(Database.Entity.Events.Event @event, ClaimsPrincipal loggedUser)
        {
            if (loggedUser.IsInPolicy(Policies.EventManagement))
                return true;

            return @event.CreatedById == loggedUser.GetUserId();
        }
    }
}
