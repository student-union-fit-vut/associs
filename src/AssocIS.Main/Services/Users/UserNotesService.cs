﻿using AssocIS.Data.Models.Users;
using AssocIS.Common.Extensions;
using AssocIS.Database.Services;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Data.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace AssocIS.Main.Services.Users
{
    [Service(ServiceLifetime.Scoped)]
    public class UserNotesService
    {
        private IAssocISRepository Repository { get; }

        public UserNotesService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<UserDetail> SetUserNoteAsync(long userId, string content, ClaimsPrincipal currentUser)
        {
            var user = await Repository.User.FindUserByIdAsync(userId);

            if (user == null)
                return null;

            var currentUserId = currentUser.GetUserId();

            var note = user.UserNotes.FirstOrDefault(o => o.AuthorId == currentUserId);
            if (note == null)
            {
                note = new Database.Entity.UserNote()
                {
                    AuthorId = currentUserId,
                    Content = content
                };

                user.UserNotes.Add(note);
            }
            else
            {
                note.Content = content;
            }

            await Repository.CommitAsync();

            return new UserDetail(user, currentUserId);
        }
    }
}
