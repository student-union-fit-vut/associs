﻿using AssocIS.Data.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace AssocIS.Main.Services.Users
{
    [Service(ServiceLifetime.Singleton)]
    public class UserProfilePictureCache
    {
        private Dictionary<long, Database.Entity.ProfilePicture> Cache { get; }
        private readonly object _locker = new object();

        public UserProfilePictureCache()
        {
            Cache = new Dictionary<long, Database.Entity.ProfilePicture>();
        }

        public void AddOrUpdate(long id, Database.Entity.ProfilePicture data)
        {
            lock (_locker)
            {
                if (Cache.ContainsKey(id))
                    Cache[id] = data;
                else
                    Cache.Add(id, data);
            }
        }

        public void TryRemove(long id)
        {
            lock (_locker)
            {
                if (!Cache.ContainsKey(id)) return;
                Cache.Remove(id);
            }
        }

        public Database.Entity.ProfilePicture Get(long id)
        {
            lock (_locker)
            {
                return Cache.TryGetValue(id, out var data) ? data : null;
            }
        }
    }
}
