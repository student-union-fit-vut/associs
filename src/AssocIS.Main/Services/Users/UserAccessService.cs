﻿using AssocIS.Data.Models.Users;
using AssocIS.Database.Services;
using AssocIS.Common.Extensions;
using System.Security.Claims;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System;
using AssocIS.Common.Helpers;
using AssocIS.Main.Models.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using AssocIS.Data.Infrastructure;

namespace AssocIS.Main.Services.Users
{
    [Service(ServiceLifetime.Scoped)]
    public class UserAccessService
    {
        private IAssocISRepository Repository { get; }
        private ResetPasswordConfiguration ResetPasswordConfig { get; }

        public UserAccessService(IAssocISRepository repository, IOptionsSnapshot<ResetPasswordConfiguration> resetPasswordConfig)
        {
            Repository = repository;
            ResetPasswordConfig = resetPasswordConfig.Value;
        }

        public async Task<UserDetail> SetCurrentUserPasswordAsync(PasswordChangeParams changeParams, ClaimsPrincipal currentUser)
        {
            var currentUserId = currentUser.GetUserId();
            var user = await Repository.User.FindUserByIdAsync(currentUserId);

            if (user == null)
                return null;

            if (!changeParams.CheckOldPassword(user))
                throw new ValidationException("InvalidOldPasswords");

            changeParams.UpdateEntity(user, currentUserId);

            await Repository.CommitAsync();
            return new UserDetail(user, currentUserId);
        }

        public async Task<ResetPasswordResult> ResetPasswordAsync(long userId, ClaimsPrincipal currentUser)
        {
            var user = await Repository.User.FindUserByIdAsync(userId);

            if (user == null)
                return null;

            var randLength = ResetPasswordConfig.GetRandBetweenValue();
            var newPassword = StringHelpers.CreateRandomString(randLength);
            var changeParams = new PasswordChangeParams() { NewPassword = newPassword };
            var currentUserId = currentUser.GetUserId();

            changeParams.UpdateEntity(user, currentUserId);
            await Repository.CommitAsync();

            return new ResetPasswordResult(new UserDetail(user, currentUserId), newPassword);
        }

        public async Task<UserDetail> RemovePasswordAsync(long userId, ClaimsPrincipal currentUser)
        {
            var user = await Repository.User.FindUserByIdAsync(userId);

            if (user == null)
                return null;

            var currentUserId = currentUser.GetUserId();

            user.MarkAsModified(currentUserId);
            user.Password = null;

            await Repository.CommitAsync();
            return new UserDetail(user, currentUserId);
        }
    }
}
