﻿using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Users;
using AssocIS.Database.Services;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Common.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using System;
using AssocIS.Main.Infrastructure.Auth;
using AssocIS.Main.Services.Auth;
using AssocIS.Data.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using AssocIS.Data.Resources.Templates.EmailTemplates;
using AssocIS.Main.Models.Configuration;
using Microsoft.Extensions.Options;
using AssocIS.Data.Extensions.Entity;
using System.Linq;
using AssocIS.Database.Enums;
using AssocIS.Main.Services.External;
using AssocIS.Main.Services.External.Google;

namespace AssocIS.Main.Services.Users
{
    [Service(ServiceLifetime.Scoped)]
    public class UsersService
    {
        private IAssocISRepository Repository { get; }
        private EmailConfiguration EmailConfiguration { get; }
        private EmailService EmailService { get; }
        private IEnumerable<ExternalService> ExternalServices { get; }
        private GoogleExternalService GoogleExternalService => ExternalServices.OfType<GoogleExternalService>().FirstOrDefault();
        private UserProfilePictureCache UserProfilePictureCache { get; }

        public UsersService(IAssocISRepository repository, IOptionsSnapshot<EmailConfiguration> options,
            EmailService emailService, IEnumerable<ExternalService> externalServices, UserProfilePictureCache userProfilePictureCache)
        {
            Repository = repository;
            EmailConfiguration = options.Value;
            EmailService = emailService;
            ExternalServices = externalServices;
            UserProfilePictureCache = userProfilePictureCache;
        }

        public async Task<PaginatedData<User>> GetUserListAsync(UserSearchParams searchParams)
        {
            var query = Repository.User.GetUsersQuery(searchParams.NameQuery, searchParams.IgnoreLeftUsers, searchParams.IgnoreTestPeriodUsers,
                searchParams.IgnoreAnonymized, searchParams.States);
            return await PaginatedData<User>.CreateAsync(query, searchParams, entity => new User(entity));
        }

        public async Task<UserDetail> GetUserDetailAsync(long id, ClaimsPrincipal currentUser)
        {
            var user = await Repository.User.FindUserByIdAsync(id);

            if (user == null)
                return null;

            var currentUserId = currentUser.GetUserId();
            return new UserDetail(user, currentUserId);
        }

        public async Task<UserDetail> CreateUserAsync(UserParams userParams, ClaimsPrincipal currentUser)
        {
            var currentLoggedUserId = currentUser.GetUserId();
            var entity = userParams.ToEntity(currentLoggedUserId, email => GoogleExternalService.CreateMailFromAnotherAddress(email).ToString());

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            var detail = await GetUserDetailAsync(entity.Id, currentUser);
            await ExternalServices.AggregatedExecutionAsync(service => service.MemberCreatedAsync(detail));

            return detail;
        }

        public async Task<UserDetail> UpdateUserAsync(long id, UserParams userParams, ClaimsPrincipal currentUser)
        {
            var entity = await Repository.User.FindUserByIdAsync(id);

            if (entity == null)
                return null;

            var currentLoggedUserId = currentUser.GetUserId();
            var isSelfLowPermsUpdate = entity.Id == currentLoggedUserId && !currentUser.IsInPolicy(Policies.UserManagement);

            userParams.UpdateEntity(entity, currentLoggedUserId, isSelfLowPermsUpdate);
            await Repository.CommitAsync();

            var detail = await GetUserDetailAsync(id, currentUser);
            await ExternalServices.AggregatedExecutionAsync(service => service.MemberUpdatedAsync(detail));

            return detail;
        }

        public async Task<UserDetail> UploadProfilePictureAsync(long userId, IFormFile file, ClaimsPrincipal currentUser)
        {
            var entity = await Repository.User.FindUserByIdAsync(userId, true);

            entity.MarkAsModified(userId);
            if (entity.ProfileImage != null)
            {
                Repository.Remove(entity.ProfileImage);
                entity.ProfileImageId = null;

                await Repository.CommitAsync();
                entity.MarkAsModified(userId);
            }

            using var ms = new MemoryStream();
            await file.CopyToAsync(ms);

            entity.ProfileImage = new Database.Entity.ProfilePicture()
            {
                Content = ms.ToArray(),
                Filename = file.FileName
            };

            await Repository.CommitAsync();
            UserProfilePictureCache.TryRemove(userId);

            return await GetUserDetailAsync(userId, currentUser);
        }

        public async Task<Tuple<byte[], string>> GetProfilePictureAsync(long userId)
        {
            var profilePicture = UserProfilePictureCache.Get(userId);
            if (profilePicture == null)
            {
                profilePicture = await Repository.User.FindProfilePictureByUserId(userId);
                UserProfilePictureCache.AddOrUpdate(userId, profilePicture);
            }

            if (profilePicture == null) return null;
            return Tuple.Create(profilePicture.Content, profilePicture.Filename);
        }

        public async Task<UserDetail> DeleteProfilePictureAsync(ClaimsPrincipal currentUser)
        {
            var userId = currentUser.GetUserId();
            var entity = await Repository.User.FindUserByIdAsync(userId, true);

            if (entity.ProfileImageId == null)
                return await GetUserDetailAsync(userId, currentUser);

            Repository.Remove(entity.ProfileImage);
            entity.MarkAsModified(userId);
            entity.ProfileImage = null;
            entity.ProfileImageId = null;

            await Repository.CommitAsync();
            UserProfilePictureCache.TryRemove(userId);

            return await GetUserDetailAsync(userId, currentUser);
        }

        public async Task<List<UserDetail>> GetUsersForSynchronizationAsync()
        {
            var query = Repository.User.GetUsersForSynchronizationQuery();
            var data = await query.ToListAsync();

            return data.ConvertAll(o => new UserDetail(o, 0));
        }

        public async Task<bool> UsernameExistsAsync(string username)
        {
            return await Repository.User.GetUsersQuery(null, false, false, true, null)
                .AnyAsync(o => o.Username == username);
        }

        public async Task<Dictionary<long, string>> GetUserSelectListAsync()
        {
            return await Repository.User.GetUsersQuery(null, false, false, true, null)
                .Select(o => new { o.Id, o.Name, o.Surname })
                .ToDictionaryAsync(o => o.Id, o => $"{o.Name} {o.Surname}");
        }

        public async Task<bool> UserIdExistsAsync(long id)
        {
            return await Repository.User.UserExistsAsync(id);
        }

        public async Task<UserDetail> AnonymizeUserAsync(long userId, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var user = await Repository.User.FindUserByIdAsync(userId, true, true);
            if (user == null || user.LeavedAt == null) return null;
            if ((user.Flags & (long)UserFlags.Anonymized) != 0) return new(user, loggedUserId);

            user.Anonymize(Repository);
            await Repository.CommitAsync();

            return new UserDetail(user, loggedUserId);
        }
    }
}
