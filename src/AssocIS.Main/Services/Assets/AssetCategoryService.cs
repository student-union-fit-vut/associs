﻿using AssocIS.Data.Infrastructure;
using AssocIS.Data.Models.Assets.Category;
using AssocIS.Database.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.Assets
{
    [Service(ServiceLifetime.Scoped)]
    public class AssetCategoryService
    {
        private IAssocISRepository Repository { get; }

        public AssetCategoryService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<AssetCategory> CreateAsync(UpdateCategoryParams categoryParams)
        {
            var entity = categoryParams.ToEntity();

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            return new AssetCategory(entity);
        }

        public async Task<List<AssetCategory>> GetCategoriesAsync()
        {
            var query = Repository.Assets.GetAssetCategoriesQuery();
            var data = await query.ToListAsync();

            return data.ConvertAll(o => new AssetCategory(o));
        }

        public async Task<AssetCategory> UpdateCategoryAsync(long id, UpdateCategoryParams categoryParams)
        {
            var entity = await Repository.Assets.GetAssetCategoryAsync(id);

            if (entity == null)
                return null;

            categoryParams.UpdateEntity(entity);
            await Repository.CommitAsync();

            return new AssetCategory(entity);
        }

        public async Task<AssetCategory> DeleteCategoryAsync(long id)
        {
            var category = await Repository.Assets.GetAssetCategoryAsync(id);

            if (category == null)
                return null;

            Repository.Remove(category);
            await Repository.CommitAsync();

            return new AssetCategory();
        }

        public async Task<bool> AssetCategoryNameExistsAsync(string name)
        {
            return await Repository.Assets.AssetCategoryNameExistsAsync(name);
        }
    }
}
