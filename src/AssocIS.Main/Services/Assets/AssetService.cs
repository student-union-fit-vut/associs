﻿using AssocIS.Data.Models.Assets;
using AssocIS.Database.Services;
using AssocIS.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Data.Models.Common;
using System;
using AssocIS.Data.Helpers;
using AssocIS.Database.Enums;
using AssocIS.Main.Infrastructure.Auth;
using AssocIS.Main.Services.Auth;
using Microsoft.Extensions.DependencyInjection;
using AssocIS.Data.Infrastructure;
using System.Collections.Generic;

namespace AssocIS.Main.Services.Assets
{
    [Service(ServiceLifetime.Scoped)]
    public class AssetService
    {
        private IAssocISRepository Repository { get; }

        public AssetService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<PaginatedData<AssetListInfo>> GetAssetsListAsync(AssetSearchParams searchParams)
        {
            var query = Repository.Assets.GetAssetsQuery(searchParams.NameQuery, searchParams.IsAvailable, searchParams.Categories, searchParams.IncludeRemoved)
                .AsNoTracking();

            return await PaginatedData<AssetListInfo>.CreateAsync(query, searchParams, entity => new AssetListInfo(entity));
        }

        public async Task<AssetDetail> CreateAssetAsync(CreateAssetParams assetParams, ClaimsPrincipal currentUser)
        {
            var categories = await Repository.Assets.GetAssetCategoriesQuery()
                .Where(o => assetParams.Categories.Contains(o.Id))
                .ToListAsync();

            var loggedUserId = currentUser.GetUserId();
            var entity = assetParams.ToEntity(loggedUserId, categories);

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            return await GetAssetDetailAsync(entity.Id);
        }

        public async Task<AssetDetail> GetAssetDetailAsync(long id)
        {
            var entity = await Repository.Assets.GetAssetAsync(id, true);
            return entity == null ? null : new AssetDetail(entity);
        }

        public async Task<AssetDetail> UpdateAssetAsync(long id, UpdateAssetParams assetParams, ClaimsPrincipal currentUser)
        {
            var entity = await Repository.Assets.GetAssetAsync(id);

            if (entity == null)
                return null;

            var categories = await Repository.Assets.GetAssetCategoriesQuery()
                .Where(o => assetParams.Categories.Contains(o.Id))
                .ToListAsync();

            var loggedUserId = currentUser.GetUserId();
            assetParams.UpdateEntity(entity, loggedUserId, categories);

            await Repository.CommitAsync();
            return new AssetDetail(entity);
        }

        public async Task<AssetLoanInfo> LoanAssetAsync(long id, ClaimsPrincipal currentUser)
        {
            var entity = await Repository.Assets.GetAssetAsync(id);

            if (entity == null || (entity.Flags & (long)AssetFlags.Removed) != 0)
                return null;

            var availableCount = AssetsHelper.CalculateAvailableCount(entity);
            if (availableCount == 0)
                throw new InvalidOperationException(entity.Name);

            var currentUserId = currentUser.GetUserId();

            var loanEntity = new Database.Entity.AssetLoan()
            {
                AssetId = id,
                LoanedAt = DateTime.Now,
                UserId = currentUserId
            };

            entity.Loans.Add(loanEntity);
            await Repository.CommitAsync();

            loanEntity = await Repository.Assets.GetAssetLoanAsync(loanEntity.Id);
            return new AssetLoanInfo(loanEntity);
        }

        public async Task<AssetLoanInfo> ReturnAssetAsync(long assetId, long loanId, ClaimsPrincipal currentUser)
        {
            var entity = await Repository.Assets.GetAssetAsync(assetId);

            if (entity == null)
                return null;

            var currentUserId = currentUser.GetUserId();
            var isAdmin = currentUser.IsInPolicy(Policies.PropertyManagement);

            var loan = entity.Loans.FirstOrDefault(o => o.Id == loanId && (isAdmin || o.UserId == currentUserId) && o.ReturnedAt == null);
            if (loan == null)
                return null;

            loan.ReturnedAt = DateTime.Now;
            await Repository.CommitAsync();

            loan = await Repository.Assets.GetAssetLoanAsync(loanId);
            return new AssetLoanInfo(loan);
        }

        public async Task<PaginatedData<AssetLoanInfo>> GetLoansAsync(long assetId, AssetLoanSearchParams searchParams, ClaimsPrincipal currentUser)
        {
            // Admin can see all loans, others only his/her.
            var ofUserId = currentUser.IsInPolicy(Policies.PropertyManagement) ? (long?)null : currentUser.GetUserId();
            var query = Repository.Assets.GetLoansQuery(assetId, searchParams.OnlyActive, ofUserId)
                .AsNoTracking();

            return await PaginatedData<AssetLoanInfo>.CreateAsync(query, searchParams, entity => new AssetLoanInfo(entity));
        }

        public async Task<Dictionary<long, string>> GetAssetsSelectListAsync()
        {
            return await Repository.Assets.GetAssetsQuery(null, null, null, false)
                .AsNoTracking()
                .Select(o => new { o.Id, o.Name })
                .ToDictionaryAsync(o => o.Id, o => o.Name);
        }
    }
}
