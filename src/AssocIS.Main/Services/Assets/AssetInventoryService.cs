﻿using AssocIS.Data.Infrastructure;
using AssocIS.Data.Models.Assets;
using AssocIS.Data.Models.Assets.Inventory;
using AssocIS.Data.Models.Common;
using AssocIS.Database.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.Assets
{
    [Service(ServiceLifetime.Scoped)]
    public class AssetInventoryService
    {
        private IAssocISRepository Repository { get; }

        public AssetInventoryService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<List<AssetListInfo>> ProcessInventoryAsync(CreateInventoryParams inventoryParams)
        {
            var result = new List<AssetListInfo>();

            foreach (var entity in inventoryParams.ToEntities())
            {
                var asset = await Repository.Assets.GetAssetAsync(entity.AssetId);

                asset.Count = entity.Count;
                asset.InventoryReports.Add(entity);

                result.Add(new AssetListInfo(asset));
            }

            await Repository.CommitAsync();
            return result;
        }

        public async Task<PaginatedData<InventoryReport>> GetInventoryReportsAsync(long assetId, long? userId, PaginationParams paginationParams)
        {
            var query = Repository.Assets.GetInventoryReportsQuery(assetId, userId)
                .AsNoTracking();

            return await PaginatedData<InventoryReport>.CreateAsync(query, paginationParams, entity => new InventoryReport(entity));
        }
    }
}
