﻿using AssocIS.Database.Entity;
using AssocIS.Database.Services;
using AssocIS.Main.Models.Auth;
using AssocIS.Main.Models.Configuration;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.Auth.OAuth2
{
    public abstract class OAuth2ServiceBase : IDisposable
    {
        protected OAuth2Configuration Configuration { get; }
        protected HttpClient HttpClient { get; }
        protected IMemoryCache Cache { get; }
        protected IAssocISRepository Repository { get; }
        protected AuthService AuthService { get; }

        protected OAuth2ServiceBase(IServiceProvider serviceProvider)
        {
            Configuration = serviceProvider.GetService<IOptionsSnapshot<OAuth2Configuration>>().Value;
            HttpClient = serviceProvider.GetService<IHttpClientFactory>().CreateClient();
            Cache = serviceProvider.GetService<IMemoryCache>();
            Repository = serviceProvider.GetService<IAssocISRepository>();
            AuthService = serviceProvider.GetService<AuthService>();
        }

        public abstract Task<bool> IsAvailableAsync();
        public abstract Task<string> GetRedirectUrlAsync(string sessionId);
        public abstract Task<string> CreateRedirectUrlAsync(Guid sessionId, string authorizationCode);
        public abstract Task<AuthResult> CreateAuthAsync(string userId);

        protected virtual string CreateAccessToken(User user, out DateTime expiresAt) => AuthService.CreateAccessToken(user, out expiresAt);
        protected virtual string CreateRefreshToken(User user, DateTime expiresAt) => AuthService.CreateRefreshToken(user, expiresAt);

        #region Disposable support

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    HttpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
