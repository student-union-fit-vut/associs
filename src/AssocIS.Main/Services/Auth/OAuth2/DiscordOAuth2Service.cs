﻿using AssocIS.Data.Enums;
using AssocIS.Data.Models.Auth;
using AssocIS.Data.Models.Auth.OAuth2;
using AssocIS.Main.Models.Auth;
using Discord;
using Discord.Rest;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.Auth.OAuth2
{
    public class DiscordOAuth2Service : OAuth2ServiceBase
    {
        public DiscordOAuth2Service(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<string> CreateRedirectUrlAsync(Guid sessionId, string authorizationCode)
        {
            var token = await CreateTokenAsync(authorizationCode);
            var userId = await GetUserIdAsync(token);

            var data = Cache.Get<OAuth2SessionData>(sessionId.ToString());

            data.UserId = userId.ToString();
            data.Service = OAuth2SupportedServices.Discord;
            Cache.Set(sessionId.ToString(), data, DateTimeOffset.Now.LocalDateTime.AddHours(1));

            var uriBuilder = new UriBuilder(data.RedirectUrl)
            {
                Query = $"sessionId={sessionId}"
            };

            return uriBuilder.ToString();
        }

        public override async Task<string> GetRedirectUrlAsync(string sessionId)
        {
            if (!await IsAvailableAsync())
                return null;

            sessionId += "|Discord";
            var builder = new UriBuilder(Configuration.Discord.AuthorizeEndpoint)
            {
                Query = string.Join("&", new[]
                {
                    $"client_id={Configuration.Discord.ClientId}",
                    $"redirect_uri={WebUtility.UrlEncode(Configuration.Discord.RedirectUrl)}",
                    "response_type=code",
                    $"scope={string.Join(" ", Configuration.Discord.Scopes)}",
                    $"state={sessionId}"
                })
            };

            return builder.ToString();
        }

        public override async Task<bool> IsAvailableAsync()
        {
            if (Configuration.Discord.Disabled)
                return false;

            var uri = new UriBuilder(Configuration.Discord.AuthorizeEndpoint) { Query = $"client_id={Configuration.Discord.ClientId}" };

            using var message = new HttpRequestMessage(HttpMethod.Get, uri.Uri);
            using var response = await HttpClient.SendAsync(message);

            return response.IsSuccessStatusCode;
        }

        private async Task<string> CreateTokenAsync(string authorizationCode)
        {
            using var message = new HttpRequestMessage(HttpMethod.Post, Configuration.Discord.TokenEndpoint)
            {
                Content = new FormUrlEncodedContent(new Dictionary<string, string>()
                {
                    { "client_id", Configuration.Discord.ClientId },
                    { "client_secret", Configuration.Discord.ClientSecret},
                    { "grant_type", "authorization_code" },
                    { "code", authorizationCode },
                    { "scope", string.Join(" ", Configuration.Discord.Scopes) },
                    { "redirect_uri", Configuration.Discord.RedirectUrl }
                })
            };

            using var response = await HttpClient.SendAsync(message);
            var json = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
                throw new WebException(json);

            return JObject.Parse(json)["access_token"].ToString();
        }

        private static async Task<ulong> GetUserIdAsync(string token)
        {
            using var client = new DiscordRestClient();
            await client.LoginAsync(TokenType.Bearer, token);

            return client.CurrentUser.Id;
        }

        public override async Task<AuthResult> CreateAuthAsync(string userId)
        {
            var userEntity = await Repository.User.FindUserByDiscordIdAsync(Convert.ToUInt64(userId), true);

            if (userEntity == null)
                return new AuthResult(AuthResultCodes.UserNotFoundOrNotPaired);

            var accessToken = CreateAccessToken(userEntity, out DateTime expiresAt);
            var refreshToken = CreateRefreshToken(userEntity, expiresAt);

            userEntity.LastLogin = DateTime.Now;
            await Repository.CommitAsync();

            var jwtToken = new JwtToken(accessToken, JwtBearerDefaults.AuthenticationScheme, expiresAt, refreshToken);
            return new AuthResult(jwtToken);
        }
    }
}
