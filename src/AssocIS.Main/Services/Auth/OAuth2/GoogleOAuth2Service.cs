﻿using AssocIS.Data.Enums;
using AssocIS.Data.Models.Auth;
using AssocIS.Data.Models.Auth.OAuth2;
using AssocIS.Main.Models.Auth;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Requests;
using Google.Apis.Oauth2.v2;
using Google.Apis.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.Auth.OAuth2
{
    public class GoogleOAuth2Service : OAuth2ServiceBase
    {
        public GoogleOAuth2Service(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<AuthResult> CreateAuthAsync(string userId)
        {
            var userEntity = await Repository.User.FindUserByUsernameAsync(userId, true);

            if (userEntity == null)
                return new AuthResult(AuthResultCodes.UserNotFoundOrNotPaired);

            var accessToken = CreateAccessToken(userEntity, out DateTime expiresAt);
            var refreshToken = CreateRefreshToken(userEntity, expiresAt);

            userEntity.LastLogin = DateTime.Now;
            await Repository.CommitAsync();

            var jwtToken = new JwtToken(accessToken, JwtBearerDefaults.AuthenticationScheme, expiresAt, refreshToken);
            return new AuthResult(jwtToken);
        }

        public override async Task<string> CreateRedirectUrlAsync(Guid sessionId, string authorizationCode)
        {
            var initializer = new AuthorizationCodeFlow.Initializer(GoogleAuthConsts.AuthorizationUrl, GoogleAuthConsts.TokenUrl)
            {
                ClientSecrets = Configuration.Google.CreateSecrets(),
                Scopes = Configuration.Google.Scopes
            };

            using var flow = new AuthorizationCodeFlow(initializer);
            var token = await flow.ExchangeCodeForTokenAsync(null, authorizationCode, Configuration.Google.RedirectUrl, CancellationToken.None);

            var oAuth2Service = new Oauth2Service(new BaseClientService.Initializer()
            {
                HttpClientInitializer = new UserCredential(flow, null, token),
                ApplicationName = "AssocIS"
            });

            var userInfo = await oAuth2Service.Userinfo.Get().ExecuteAsync();
            var emailData = new MailAddress(userInfo.Email);

            var cacheData = Cache.Get<OAuth2SessionData>(sessionId.ToString());
            cacheData.UserId = emailData.User;
            cacheData.Service = OAuth2SupportedServices.Google;
            Cache.Set(sessionId.ToString(), cacheData, DateTimeOffset.Now.LocalDateTime.AddHours(1));

            var uriBuilder = new UriBuilder(cacheData.RedirectUrl) { Query = $"sessionId={sessionId}" };
            return uriBuilder.ToString();
        }

        public override async Task<string> GetRedirectUrlAsync(string sessionId)
        {
            if (!await IsAvailableAsync())
                return null;

            var initializer = new AuthorizationCodeFlow.Initializer(GoogleAuthConsts.AuthorizationUrl, GoogleAuthConsts.TokenUrl)
            {
                ClientSecrets = Configuration.Google.CreateSecrets(),
                Scopes = Configuration.Google.Scopes
            };

            using var flow = new AuthorizationCodeFlow(initializer);

            var authUrl = flow.CreateAuthorizationCodeRequest(Configuration.Google.RedirectUrl);
            authUrl.State = $"{sessionId}|Google";

            var builder = new UriBuilder(authUrl.Build());
            var queryString = builder.Query.StartsWith("?") ? builder.Query[1..] : builder.Query;
            var queryParts = queryString.Split('&').Select(o => o.Split('=')).ToList();
            queryParts.Add(new[] { "prompt", "select_account" }); // Force account selection.
            builder.Query = $"?{string.Join("&", queryParts.Select(o => string.Join("=", o)))}";

            return builder.ToString();
        }

        public override async Task<bool> IsAvailableAsync()
        {
            if (Configuration.Google.Disabled)
                return false;

            var request = new AuthorizationCodeRequestUrl(new Uri(GoogleAuthConsts.AuthorizationUrl));

            var uri = request.Build();
            using var message = new HttpRequestMessage(HttpMethod.Get, uri);
            using var response = await HttpClient.SendAsync(message);

            return response.IsSuccessStatusCode;
        }
    }
}
