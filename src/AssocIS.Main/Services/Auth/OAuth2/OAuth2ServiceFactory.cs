﻿using AssocIS.Data.Enums;
using AssocIS.Data.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AssocIS.Main.Services.Auth.OAuth2
{
    [Service(ServiceLifetime.Transient)]
    public class OAuth2ServiceFactory
    {
        private IServiceProvider Provider { get; }

        public OAuth2ServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public OAuth2ServiceBase Create(OAuth2SupportedServices type)
        {
            return type switch
            {
                OAuth2SupportedServices.Discord => new DiscordOAuth2Service(Provider),
                OAuth2SupportedServices.Google => new GoogleOAuth2Service(Provider),
                _ => null
            };
        }
    }
}
