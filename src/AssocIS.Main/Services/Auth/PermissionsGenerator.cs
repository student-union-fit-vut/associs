﻿using AssocIS.Database.Entity;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AssocIS.Main.Services.Auth
{
    /// <summary>
    /// Permissions evaluation.
    /// </summary>
    public class PermissionsGenerator
    {
        /// <summary>
        /// Creates permissions from user.
        /// </summary>
        /// <param name="user">User entity</param>
        public List<string> CreatePemissions(User user)
        {
            if ((user.Flags & (long)UserFlags.Admin) != 0) // Fullpower administrator have same permissions as chairman.
                return new List<string>() { Permissions.Chairperson };

            var permissions = new List<string>();
            AppendStatePermission(permissions, user);

            // Chairman have full permissions.
            if (permissions.Contains(Permissions.Chairperson))
                return permissions;

            AppendTeamPermissions(permissions, user);
            AppendSpecialPermissions(permissions, user);

            return permissions;
        }

        private static void AppendStatePermission(List<string> permissions, User user)
        {
            var permission = user.State switch
            {
                UserState.BasicMember or UserState.VerifiedMember => Permissions.BasicMember,
                UserState.Chairman => Permissions.Chairperson,
                UserState.ViceChairman => Permissions.ViceChairperson,
                _ => null
            };

            if (permission != null) permissions.Add(permission);
        }

        private static void AppendTeamPermissions(List<string> permissions, User user)
        {
            if (user.Teams.Any(o => o.IsLeader))
                permissions.Add(Permissions.TeamLeader);
        }

        private static void AppendSpecialPermissions(List<string> permissions, User user)
        {
            if (user.LeavedAt != null && user.LeavedAt < DateTime.Now)
                return;

            if ((user.Permissions & (long)UserPermission.UserManager) != 0)
                permissions.Add(Permissions.UserManager);

            if ((user.Permissions & (long)UserPermission.TodoListsManager) != 0)
                permissions.Add(Permissions.TodoListsManager);

            if ((user.Permissions & (long)UserPermission.EventManager) != 0)
                permissions.Add(Permissions.EventManager);

            if ((user.Permissions & (long)UserPermission.MeetingManager) != 0)
                permissions.Add(Permissions.MeetingManager);

            if ((user.Permissions & (long)UserPermission.PropertyManager) != 0)
                permissions.Add(Permissions.PropertyManager);

            if ((user.Permissions & (long)UserPermission.FinanceManager) != 0)
                permissions.Add(Permissions.FinanceManager);

            if ((user.Permissions & (long)UserPermission.WorkItemsManager) != 0)
                permissions.Add(Permissions.WorkItemsManager);

            if ((user.Permissions & (long)UserPermission.JobsManager) != 0)
                permissions.Add(Permissions.JobsManager);

            if ((user.Permissions & (long)UserPermission.AppManager) != 0)
                permissions.Add(Permissions.AppManager);
        }
    }
}
