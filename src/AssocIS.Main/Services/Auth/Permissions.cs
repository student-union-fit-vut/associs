﻿namespace AssocIS.Main.Services.Auth
{
    /// <summary>
    /// Permission values.
    /// </summary>
    public static class Permissions
    {
        /// <summary>
        /// User is leader of some team.
        /// </summary>
        public const string TeamLeader = "team_leader";

        /// <summary>
        /// User is chairman. Have full permissions.
        /// </summary>
        public const string Chairperson = "chairperson";

        /// <summary>
        /// User is vice-chairman.
        /// </summary>
        public const string ViceChairperson = "vice_chairperson";

        /// <summary>
        /// User can work with users.
        /// </summary>
        public const string UserManager = "user";

        /// <summary>
        /// User have full permissions on to-do lists.
        /// </summary>
        public const string TodoListsManager = "todo";

        /// <summary>
        /// User have full permissions on events.
        /// </summary>
        public const string EventManager = "event";

        /// <summary>
        /// User have full permissions on meetings.
        /// </summary>
        public const string MeetingManager = "meeting";

        /// <summary>
        /// User have full permissions on properties.
        /// </summary>
        public const string PropertyManager = "property";

        /// <summary>
        /// User have full permissions on finances.
        /// </summary>
        public const string FinanceManager = "finance";

        /// <summary>
        /// User have full permissions on work items.
        /// </summary>
        public const string WorkItemsManager = "work-items";

        /// <summary>
        /// User have full permissions on jobs.
        /// </summary>
        public const string JobsManager = "jobs";

        /// <summary>
        /// User have full permissions on application management (diagnostics, ...)
        /// </summary>
        public const string AppManager = "app";

        /// <summary>
        /// Active collaborators. This members have lower permissions than basic members.
        /// </summary>
        public const string BasicMember = "basic";
    }
}
