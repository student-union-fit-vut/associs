﻿using AssocIS.Data.Models.Auth.Clients;
using AssocIS.Data.Models.Common;
using AssocIS.Database.Services;
using System;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.Auth
{
    public class ClientsService
    {
        private IAssocISRepository Repository { get; }

        public ClientsService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<PaginatedData<ClientListItem>> GetClientsListAsync(string nameSearch, PaginationParams pagination)
        {
            var query = Repository.Client.GetClientsQuery(nameSearch);
            return await PaginatedData<ClientListItem>.CreateAsync(query, pagination, entity => new ClientListItem(entity));
        }

        public async Task<bool> VerifyClientAsync(string clientId)
        {
            var client = await Repository.Client.FindClientByClientIdAsync(clientId);
            if (client == null) return false;

            client.LastAccess = DateTime.Now;
            await Repository.CommitAsync();
            return true;
        }

        public async Task<bool> ClientExistsAsync(long id)
        {
            return await Repository.Client.ClientExistsAsync(id);
        }

        public async Task<Client> GetClientAsync(long id)
        {
            var client = await Repository.Client.FindClientByIdAsync(id);
            return client == null ? null : new Client(client);
        }

        public async Task<Client> CreateClientAsync(UpdateClientParams request)
        {
            var entity = request.ToEntity();

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            return new Client(entity);
        }

        public async Task<Client> UpdateClientAsync(long id, UpdateClientParams request)
        {
            var entity = await Repository.Client.FindClientByIdAsync(id);

            if (entity == null)
                return null;

            request.UpdateEntity(entity);
            await Repository.CommitAsync();

            return new Client(entity);
        }

        public async Task<Client> RegenerateClientIdAsync(long id)
        {
            var entity = await Repository.Client.FindClientByIdAsync(id);

            if (entity == null)
                return null;

            entity.ClientId = Guid.NewGuid().ToString();
            await Repository.CommitAsync();

            return new Client(entity);
        }

        public async Task RemoveClientAsync(long id)
        {
            var entity = await Repository.Client.FindClientByIdAsync(id);

            if (entity == null)
                return;

            Repository.Remove(entity);
            await Repository.CommitAsync();
        }
    }
}
