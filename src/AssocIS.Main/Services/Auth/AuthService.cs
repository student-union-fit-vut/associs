﻿using AssocIS.Data.Models.Auth;
using AssocIS.Database.Entity;
using AssocIS.Database.Services;
using AssocIS.Main.Models.Auth;
using AssocIS.Main.Models.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.Auth
{
    public class AuthService
    {
        private IAssocISRepository Repository { get; }
        private JwtConfiguration JwtConfiguration { get; }
        private PermissionsGenerator PermissionsGenerator { get; }

        public AuthService(IAssocISRepository repository, IOptionsSnapshot<JwtConfiguration> jwtConfig, PermissionsGenerator permissionsGenerator)
        {
            Repository = repository;
            JwtConfiguration = jwtConfig.Value;
            PermissionsGenerator = permissionsGenerator;
        }

        /// <summary>
        /// Logins user with username and password.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        public async Task<AuthResult> AuthenticateAsync(string username, string password)
        {
            var user = await Repository.User.FindUserByUsernameAsync(username, true);

            if (user == null || string.IsNullOrEmpty(user.Password) || !BCrypt.Net.BCrypt.Verify(password, user.Password))
                return new AuthResult(AuthResultCodes.UserNotFoundOrInvalidPassword);

            var accessToken = CreateAccessToken(user, out var expiresAt);
            var refreshToken = CreateRefreshToken(user, expiresAt);

            user.LastLogin = DateTime.Now;
            await Repository.CommitAsync();

            var jwtToken = new JwtToken(accessToken, JwtBearerDefaults.AuthenticationScheme, expiresAt, refreshToken);
            return new AuthResult(jwtToken);
        }

        public string CreateAccessToken(User user, out DateTime expiresAt)
        {
            expiresAt = DateTime.Now.AddHours(JwtConfiguration.HoursExpiration);

            var perms = PermissionsGenerator.CreatePemissions(user);
            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Audience = JwtConfiguration.Audience,
                Expires = expiresAt,
                IssuedAt = DateTime.Now,
                Issuer = JwtConfiguration.Issuer,
                SigningCredentials = new SigningCredentials(JwtConfiguration.GetSecret(), SecurityAlgorithms.HmacSha256Signature),
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.Name),
                    new Claim(ClaimTypes.Surname, user.Surname),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.GivenName, user.Username),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                })
            };

            tokenDescriptor.Subject.AddClaims(perms.Select(o => new Claim(ClaimTypes.Role, o)));

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static string CreateRefreshToken(User user, DateTime expiresAt)
        {
            var refreshToken = new RefreshToken()
            {
                ExpiresAt = expiresAt,
                Token = Convert.ToBase64String(Encoding.ASCII.GetBytes(Guid.NewGuid().ToString())),
            };

            user.RefreshTokens.Add(refreshToken);
            return refreshToken.Token;
        }

        /// <summary>
        /// Creates token from provided refresh token.
        /// </summary>
        /// <param name="refreshToken">Refresh token</param>
        public async Task<AuthResult> ReauthenticateAsync(string refreshToken)
        {
            var token = await Repository.Auth.FindRefreshTokenAsync(refreshToken);

            if (token == null)
                return new AuthResult(AuthResultCodes.RefreshTokenNotFound);

            var accessToken = CreateAccessToken(token.User, out DateTime expiresAt);
            token.ExpiresAt = expiresAt;

            await Repository.CommitAsync();

            var jwtToken = new JwtToken(accessToken, JwtBearerDefaults.AuthenticationScheme, expiresAt, token.Token);
            return new AuthResult(jwtToken);
        }

        /// <summary>
        /// Removes all refresh tokens of user.
        /// </summary>
        /// <param name="userId">ID of user.</param>
        /// <param name="onlyExpired">Remove only expired tokens.</param>
        public async Task ClearRefreshTokensAsync(long userId, bool onlyExpired)
        {
            var entities = await Repository.Auth.FindRefreshTokensOfUser(userId, onlyExpired).ToListAsync();

            Repository.RemoveCollection(entities);
            await Repository.CommitAsync();
        }
    }
}
