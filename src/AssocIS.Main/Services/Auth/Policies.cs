﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace AssocIS.Main.Services.Auth
{
    public static class Policies
    {
        public const string TeamLeaders = "TeamLeaders";
        public const string UserManagement = "UserManagement";
        public const string AppManagement = "AppManagement";
        public const string TodoManagement = "ToDoManagement";
        public const string EventManagement = "EventManagement";
        public const string MeetingManagement = "MeetingManagement";
        public const string PropertyManagement = "PropertyManagement";
        public const string FinanceManagement = "FinanceManagement";
        public const string WorkItemsManagement = "WorkItemsManagement";
        public const string JobsManagement = "JobsManagement";
        public const string RegularMembers = "AllMembers";

        public static Dictionary<string, string[]> PolicyToRolesMapping { get; } = new Dictionary<string, string[]>()
        {
            { TeamLeaders, new[] { Permissions.Chairperson, Permissions.TeamLeader, Permissions.UserManager, Permissions.BasicMember } },
            { UserManagement, new[] { Permissions.Chairperson, Permissions.UserManager, Permissions.BasicMember } },
            { AppManagement, new[] { Permissions.Chairperson, Permissions.AppManager, Permissions.BasicMember } },
            { TodoManagement, new[] { Permissions.Chairperson, Permissions.TodoListsManager, Permissions.BasicMember } },
            { EventManagement, new[] { Permissions.Chairperson, Permissions.EventManager, Permissions.BasicMember } },
            { MeetingManagement, new[] { Permissions.Chairperson, Permissions.MeetingManager, Permissions.TeamLeader, Permissions.BasicMember } },
            { PropertyManagement, new[] { Permissions.Chairperson, Permissions.PropertyManager, Permissions.BasicMember } },
            { FinanceManagement, new[] { Permissions.Chairperson, Permissions.FinanceManager, Permissions.BasicMember } },
            { WorkItemsManagement, new[] { Permissions.Chairperson, Permissions.WorkItemsManager, Permissions.BasicMember } },
            { JobsManagement, new[] { Permissions.Chairperson, Permissions.JobsManager, Permissions.BasicMember } },
            { RegularMembers, new[] { Permissions.BasicMember, Permissions.Chairperson } }
        };

        public static void Set(AuthorizationOptions options)
        {
            // TODO: Maybe to DB?

            foreach (var mapping in PolicyToRolesMapping)
            {
                options.AddPolicy(mapping.Key, policy => ConfigurePolicy(policy, mapping.Value));
            }
        }

        private static void ConfigurePolicy(AuthorizationPolicyBuilder policyBuilder, string[] roles)
        {
            policyBuilder
                .RequireAuthenticatedUser()
                .RequireRole(roles);
        }
    }
}
