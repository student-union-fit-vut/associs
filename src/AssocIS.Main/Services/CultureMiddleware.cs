﻿using Microsoft.AspNetCore.Http;
using System.Globalization;
using System.Threading.Tasks;

namespace AssocIS.Main.Services
{
    public class CultureMiddleware
    {
        private RequestDelegate Next { get; }

        public CultureMiddleware(RequestDelegate next)
        {
            Next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var culture = context.Request.Headers["Accept-Language"].ToString();

            if (!string.IsNullOrEmpty(culture))
            {
                culture = culture.Split(';')[0].Trim().Split(',')[0];
                var cultureInfo = new CultureInfo(culture);

                CultureInfo.CurrentCulture = cultureInfo;
                CultureInfo.CurrentUICulture = cultureInfo;
            }

            await Next(context);
        }
    }
}
