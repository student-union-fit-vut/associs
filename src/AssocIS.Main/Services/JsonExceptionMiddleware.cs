﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using AssocIS.Common.Extensions;
using AssocIS.Data.Models.Common;

namespace AssocIS.Main.Services
{
    public class JsonExceptionMiddleware
    {
        public async Task InvokeAsync(HttpContext context)
        {
            var ex = context.Features.Get<IExceptionHandlerFeature>()?.Error;
            if (ex == null) return;

            var exceptionMessages = ex.GetExceptionMessages();
            var errors = new ErrorData(exceptionMessages);

            context.Response.ContentType = "application/json";

            await context.Response.WriteAsJsonAsync(errors);
        }
    }
}
