﻿using AssocIS.Data.Infrastructure;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Interviews;
using AssocIS.Common.Extensions;
using AssocIS.Database.Services;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.Interviews
{
    [Service(ServiceLifetime.Transient)]
    public class InterviewService
    {
        private IAssocISRepository Repository { get; }

        public InterviewService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<PaginatedData<InterviewListItem>> GetInterviewsListAsync(InterviewSearchParams parameters)
        {
            var query = Repository.Interview.GetInterviewsQuery(parameters.UserId, parameters.LeaderUserId, parameters.From, parameters.To);
            return await PaginatedData<InterviewListItem>.CreateAsync(query, parameters, entity => new InterviewListItem(entity));
        }

        public async Task<Interview> CreateInterviewAsync(UpdateInterviewParams parameters, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var entity = parameters.ToEntity(loggedUserId);

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            return await GetInterviewDetailAsync(entity.Id);
        }

        public async Task<Interview> GetInterviewDetailAsync(long id)
        {
            var entity = await Repository.Interview.GetInterviewByIdAsync(id);
            return entity == null ? null : new Interview(entity);
        }

        public async Task<Interview> UpdateInterviewAsync(long id, UpdateInterviewParams parameters, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Interview.GetInterviewByIdAsync(id);

            if (entity == null)
                return null;

            var userId = loggedUser.GetUserId();
            parameters.UpdateEntity(entity, userId);

            await Repository.CommitAsync();
            return new Interview(entity);
        }

        public async Task<bool> InterviewExistsAsync(long id)
        {
            return await Repository.Interview.ExistsAsync(id);
        }

        public async Task RemoveInterviewAsync(long id)
        {
            var entity = await Repository.Interview.GetInterviewByIdAsync(id);
            if (entity == null) return;

            Repository.Remove(entity);
            await Repository.CommitAsync();
        }
    }
}
