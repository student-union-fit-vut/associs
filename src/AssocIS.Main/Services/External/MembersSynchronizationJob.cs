﻿using AssocIS.Main.Services.Users;
using Quartz;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External
{
    [DisallowConcurrentExecution]
    public class MembersSynchronizationJob : IJob
    {
        private IEnumerable<ExternalService> ExternalServices { get; }
        private UsersService UsersService { get; }

        public MembersSynchronizationJob(IEnumerable<ExternalService> externalServices,
            UsersService usersService)
        {
            ExternalServices = externalServices;
            UsersService = usersService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var members = await UsersService.GetUsersForSynchronizationAsync();
            await ExternalServices.AggregatedExecutionAsync(service => service.SynchronizeMembersAsync(members));
        }
    }
}
