﻿using AssocIS.Data.Models.Events;
using Discord;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External.Discord
{
    public partial class DiscordExternalService
    {
        protected override bool IsEventProcessingEnabled() => Configuration.Enabled && Configuration.Features.Events;

        protected override async Task ProcessEventCreatedAsync(Event @event, bool createCommunicationChannel)
        {
            if (!createCommunicationChannel) return;

            var homeGuild = await GetHomeGuildAsync();

            var channelName = @event.Name.Replace(" ", "-");
            var permissions = Configuration.RoleGroups.EveryoneValid.Select(o => new Overwrite(o, PermissionTarget.Role, new OverwritePermissions(viewChannel: PermValue.Allow))).ToList();
            permissions.Add(new Overwrite(homeGuild.Id, PermissionTarget.Role, new OverwritePermissions(viewChannel: PermValue.Deny)));

            var channel = await homeGuild.CreateTextChannelAsync(channelName, o =>
            {
                o.Topic = @event.Description;
                o.CategoryId = Configuration.TemporaryChannelsCategoryId;
                o.PermissionOverwrites = permissions;
            });

            var eventEntity = await Repository.Event.GetEventByIdAsync(@event.Id);
            eventEntity.DiscordChannelId = channel.Id.ToString();
            await Repository.CommitAsync();
        }
    }
}
