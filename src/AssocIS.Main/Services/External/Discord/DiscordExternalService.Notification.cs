﻿using Discord;
using System.Threading.Tasks;
using AssocIS.Common.Extensions;
using System.Linq;

namespace AssocIS.Main.Services.External.Discord
{
    public partial class DiscordExternalService
    {
        protected override bool IsEveryoneMessageProcessingEnabled() => Configuration.Enabled && Configuration.Features.Everyone;

        public override async Task EveryoneMessageAsync(string message)
        {
            var notificationChannel = await GetNotificationsChannelAsync();
            var homeGuild = await GetHomeGuildAsync();
            if (notificationChannel == null) return;

            var partLength = DiscordConfig.MaxMessageSize - 10; // 10 characters is everyone tag.
            var parts = message.SplitInParts(partLength).ToList();
            parts[0] = $"{homeGuild.EveryoneRole.Mention}\n{parts[0]}";

            foreach (var part in parts)
            {
                await notificationChannel.SendMessageAsync(part);
            }
        }
    }
}
