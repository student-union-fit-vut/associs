﻿using AssocIS.Data.Models.Meetings;
using AssocIS.Data.Models.Users;
using AssocIS.Database.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using AssocIS.Common.Extensions;
using System;
using System.Linq;
using Discord;
using System.Text;

namespace AssocIS.Main.Services.External.Discord
{
    public partial class DiscordExternalService
    {
        protected override bool IsMeetingProcessingEnabled() => Configuration.Enabled && Configuration.Features.Meetings;

        protected override Task ProcessMeetingAsync(Meeting meeting, string calendarEventId, Dictionary<MeetingPresenceType, List<UserInfo>> expectedMembers)
        {
            return meeting.AttendanceType switch
            {
                MeetingAttendanceType.RequiredForExpected or MeetingAttendanceType.Optional => NotifyExpectatedMeetingAsync(meeting, expectedMembers),
                MeetingAttendanceType.Required => NotifyRequiredMeetingAsync(meeting),
                _ => Task.CompletedTask,
            };
        }

        private async Task NotifyRequiredMeetingAsync(Meeting meeting)
        {
            var fullMessage = BuildMessage(meeting);
            await EveryoneMessageAsync(fullMessage);
        }

        private async Task NotifyExpectatedMeetingAsync(Meeting meeting, Dictionary<MeetingPresenceType, List<UserInfo>> expectedMembers)
        {
            var fullMessage = BuildMessage(meeting);
            var parts = fullMessage.SplitInParts(1990);
            var homeGuild = await GetHomeGuildAsync();

            foreach (var expectedUser in expectedMembers.SelectMany(o => o.Value))
            {
                var user = await homeGuild.GetUserAsync(Convert.ToUInt64(expectedUser.DiscordId));
                if (user == null) continue;

                foreach (var part in parts)
                {
                    await user.SendMessageAsync(part);
                }
            }
        }

        private string BuildMessage(Meeting meeting)
        {
            if (meeting.State == MeetingState.Summoned)
                return BuildSummonedMessage(meeting);
            else
                return BuildCancellationMessage(meeting);
        }

        private string BuildSummonedMessage(Meeting meeting)
        {
            var builder = new StringBuilder();

            switch (meeting.AttendanceType)
            {
                case MeetingAttendanceType.Required:
                    builder.Append("Touto zprávou byla oficiálně svolána schůze na **").Append(meeting.At.ToString(Configuration.DateTimeFormat)).AppendLine("**.");
                    break;
                case MeetingAttendanceType.RequiredForExpected:
                    builder.Append("Touto zprávou jsi očekáván/a na schůzi, která byla naplánována na **").Append(meeting.At.ToLocalTime().ToString(Configuration.DateTimeFormat)).AppendLine("**.");
                    break;
                case MeetingAttendanceType.Optional:
                    builder.Append("Touto zprávou jsi byl pozván/a na schůzi, která byla naplánována na **").Append(meeting.At.ToLocalTime().ToString(Configuration.DateTimeFormat)).AppendLine("**.");
                    break;
            }

            builder.Append("Schůze bude probíhat na místě **").Append(meeting.Location).AppendLine("**.");
            builder.AppendLine("\nProgram schůze:");

            foreach (var item in meeting.Agenda)
            {
                builder.Append("> ").AppendLine(item);
            }

            if (!string.IsNullOrEmpty(meeting.Message))
                builder.AppendLine().AppendLine(meeting.Message);

            return builder.ToString();
        }

        private string BuildCancellationMessage(Meeting meeting)
        {
            var builder = new StringBuilder();

            switch (meeting.AttendanceType)
            {
                case MeetingAttendanceType.Required:
                    builder.Append("Schůze, která byla plánována na **").Append(meeting.At.ToLocalTime().ToString(Configuration.DateTimeFormat)).AppendLine("** byla zrušena.");
                    break;
                case MeetingAttendanceType.Optional:
                    builder.Append("Tímto ti oznamuji, že plánovaná schůze, na které jsi byl pozván/a dne **").Append(meeting.At.ToLocalTime().ToString(Configuration.DateTimeFormat)).AppendLine("** se nekoná.");
                    break;
                case MeetingAttendanceType.RequiredForExpected:
                    builder.Append("Tímto ti oznamuji, že plánovaná schůze, na které jsi byl očekáván/a dne **").Append(meeting.At.ToLocalTime().ToString(Configuration.DateTimeFormat)).AppendLine("** se nekoná.");
                    break;
            }

            builder.AppendLine("\nOmlouvám se za veškeré komplikace.");
            return builder.ToString();
        }
    }
}
