﻿using Discord;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External.Discord
{
    public partial class DiscordExternalService
    {
        public Task OnLogAsync(LogMessage message)
        {
            switch (message.Severity)
            {
                case LogSeverity.Critical:
                    Logger.LogCritical(message.Exception, message.Message);
                    break;
                case LogSeverity.Debug:
                    Logger.LogDebug(message.Message);
                    break;
                case LogSeverity.Error:
                    Logger.LogError(message.Exception, message.Message);
                    break;
                case LogSeverity.Warning when message.Exception != null:
                    Logger.LogWarning(message.Exception, message.Message);
                    break;
                case LogSeverity.Warning when message.Exception == null:
                    Logger.LogWarning(message.Message);
                    break;
                case LogSeverity.Info:
                case LogSeverity.Verbose:
                    Logger.LogInformation(message.Message);
                    break;
            }

            return Task.CompletedTask;
        }
    }
}
