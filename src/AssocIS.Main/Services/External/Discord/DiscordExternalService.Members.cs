﻿using AssocIS.Data.Models.Users;
using AssocIS.Database.Enums;
using Discord;
using Discord.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External.Discord
{
    public partial class DiscordExternalService
    {
        protected override bool IsMembersProcessingEnabled() => Configuration.Enabled && Configuration.Features.Members;
        protected override bool IsMembersSynchronizationEnabled() => Configuration.Enabled && Configuration.Features.MembersSynchronization;

        protected override async Task ProcessMemberCreatedAsync(UserDetail user)
        {
            if (string.IsNullOrEmpty(user.DiscordId)) return;

            var homeGuild = await GetHomeGuildAsync();
            var discordUser = await homeGuild.GetUserAsync(Convert.ToUInt64(user.DiscordId));
            if (discordUser == null) return;

            await SynchronizeUserRolesAsync(user, discordUser, homeGuild);
        }

        protected override Task ProcessMemberUpdatedAsync(UserDetail user)
        {
            return ProcessMemberCreatedAsync(user);
        }

        protected override async Task ProcessSynchronizeMembersAsync(List<UserDetail> users)
        {
            var homeGuild = await GetHomeGuildAsync();
            var watchedRoles = await GetWatchedRolesAsync();

            foreach (var user in users.Where(o => !string.IsNullOrEmpty(o.DiscordId)))
            {
                var discordUser = await homeGuild.GetUserAsync(Convert.ToUInt64(user.DiscordId));
                if (discordUser == null) return;

                await SynchronizeUserRolesAsync(user, discordUser, homeGuild, watchedRoles);
            }
        }

        private async Task SynchronizeUserRolesAsync(UserDetail userEntity, IGuildUser user, IGuild homeGuild)
        {
            var watchedRoles = await GetWatchedRolesAsync();
            await SynchronizeUserRolesAsync(userEntity, user, homeGuild, watchedRoles);
        }

        private async Task SynchronizeUserRolesAsync(UserDetail userEntity, IGuildUser user, IGuild homeGuild, List<ulong> watchedRoles)
        {
            var expectedRoles = CalculateExpectedUserRoles(userEntity, homeGuild);

            // First check and remove roles that user cannot have.
            var memberRoles = user.RoleIds.Select(o => homeGuild.GetRole(o)).Where(o => o.Id != homeGuild.EveryoneRole.Id).ToList();
            foreach (var role in memberRoles) // Work with actual copy of roles.
            {
                // Ignore roles, that is unwatched or user can have this role.
                if (expectedRoles.Any(o => o.Id == role.Id) || !watchedRoles.Contains(role.Id))
                    continue;

                // User cannot have this role.
                await user.RemoveRoleAsync(role);
            }

            // In next step add roles, that user may have.
            var missingRoles = expectedRoles.Where(o => !user.RoleIds.Contains(o.Id)).ToList();
            if (missingRoles.Count > 0)
                await user.AddRolesAsync(missingRoles);
        }

        private List<IRole> CalculateExpectedUserRoles(UserDetail userEntity, IGuild homeGuild)
        {
            var expectedRoleIds = new List<ulong>();

            if (userEntity.LeavedAt != null)
            {
                // User left association.

                if (userEntity.UserState != UserState.BasicMember)
                {
                    // User left association, but was chairman, vicechairman, verified, honorary, ...
                    expectedRoleIds.AddRange(Configuration.RoleGroups.ExMemberLeader);
                }
                else if (userEntity.UserState != UserState.ActiveCollaborator)
                {
                    // Member was active collaborator.
                    expectedRoleIds.AddRange(Configuration.RoleGroups.ExMemberCollaborator);
                }
                else
                {
                    // User left association and was basic member.
                    expectedRoleIds.AddRange(Configuration.RoleGroups.ExMemberBasic);
                }
            }
            else
            {
                // User is member in association.

                if (userEntity.TestPeriodEnd != null && userEntity.TestPeriodEnd <= DateTime.Now)
                {
                    // User have testing period.
                    expectedRoleIds.AddRange(Configuration.RoleGroups.TestPeriod);
                }
                else
                {
                    // User not have testing period.
                    if (userEntity.UserState == UserState.ActiveCollaborator)
                    {
                        // active collaborators have special roles.
                        expectedRoleIds.AddRange(Configuration.RoleGroups.ActiveCollaborators);
                    }
                    else
                    {
                        expectedRoleIds.AddRange(Configuration.RoleGroups.EveryoneValid); // Roles for everyone.

                        if (userEntity.UserState == UserState.Chairman) // Chairman
                            expectedRoleIds.AddRange(Configuration.RoleGroups.Chairman);
                        else if (userEntity.UserState == UserState.ViceChairman)
                            expectedRoleIds.AddRange(Configuration.RoleGroups.ViceChairman);
                    }

                    foreach (var teamMember in userEntity.Teams)
                    {
                        if (teamMember.IsLeader)
                            expectedRoleIds.AddRange(Configuration.RoleGroups.TeamLeaders);

                        if (teamMember.TeamDiscordId == null)
                            continue;

                        var teamRoleId = Convert.ToUInt64(teamMember.TeamDiscordId);
                        if (homeGuild.Roles.Any(o => o.Id == teamRoleId))
                            expectedRoleIds.Add(teamRoleId);
                    }
                }
            }

            return expectedRoleIds
                .Distinct()
                .Select(o => homeGuild.GetRole(o))
                .Where(o => o != null)
                .ToList();
        }
    }
}
