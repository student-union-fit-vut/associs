﻿using AssocIS.Data.Models.Teams;
using AssocIS.Data.Models.Users;
using Discord;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External.Discord
{
    public partial class DiscordExternalService
    {
        public override bool IsTeamsProcessingEnabled() => Configuration.Enabled && Configuration.Features.Teams;

        protected override async Task ProcessTeamCreatedAsync(Team team, bool createRole)
        {
            var homeGuild = await GetHomeGuildAsync();
            var role = await GetRoleAsync(homeGuild, team, createRole);
            if (role == null) return;

            await SyncTeamLeadersAsync(team.Leader, homeGuild);
        }

        protected override async Task ProcessTeamUpdatedAsync(Team before, Team after, bool createRole)
        {
            var homeGuild = await GetHomeGuildAsync();
            var role = await GetRoleAsync(homeGuild, after, createRole);
            if (role == null) return;

            await role.ModifyAsync(o => o.Name = after.Name);
            if (before.Leader?.Id != after.Leader?.Id)
            {
                await SyncTeamLeadersAsync(before.Leader, homeGuild);
                await SyncTeamLeadersAsync(after.Leader, homeGuild);
            }
        }

        protected override async Task ProcessTeamRemovedAsync(Team team)
        {
            var homeGuild = await GetHomeGuildAsync();
            var role = await GetRoleAsync(homeGuild, team, false);
            if (role == null) return;

            await SyncTeamLeadersAsync(team.Leader, homeGuild);
            await role.DeleteAsync();
        }

        private async Task<IRole> GetRoleAsync(IGuild homeGuild, Team team, bool createIfNotExists)
        {
            var role = string.IsNullOrEmpty(team.DiscordRoleId) ? null : homeGuild.GetRole(Convert.ToUInt64(team.DiscordRoleId));

            if (role == null && createIfNotExists)
            {
                role = await homeGuild.CreateRoleAsync(team.Name, null, null, false, false, null);

                var teamEntity = await Repository.Team.FindTeamByIdAsync(team.Id);
                teamEntity.DiscordRoleId = role.Id.ToString();
                await Repository.CommitAsync();
            }

            return role;
        }

        private async Task SyncTeamLeadersAsync(UserInfo leader, IGuild homeGuild)
        {
            if (string.IsNullOrEmpty(leader?.DiscordId)) return;

            var discordUser = await homeGuild.GetUserAsync(Convert.ToUInt64(leader.DiscordId));
            if (discordUser != null)
            {
                var userEntity = await Repository.User.FindUserByIdAsync(leader.Id);
                await SynchronizeUserRolesAsync(new(userEntity, 0), discordUser, homeGuild);
            }
        }
    }
}
