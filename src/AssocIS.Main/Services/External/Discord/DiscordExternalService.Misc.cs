﻿using Discord;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External.Discord
{
    public partial class DiscordExternalService
    {
        public async Task<Dictionary<ulong, string>> GetHomeGuildRolesAsync()
        {
            if (!Configuration.Enabled) return new Dictionary<ulong, string>();
            var homeGuild = await GetHomeGuildAsync();

            return homeGuild.Roles.Where(o => !o.IsManaged && homeGuild.EveryoneRole.Id != o.Id)
                .OrderByDescending(o => o.Position)
                .ToDictionary(o => o.Id, o => o.Name);
        }

        public async Task<Dictionary<ulong, string>> GetMembersOfHomeGuildAsync()
        {
            if (!Configuration.Enabled) return new Dictionary<ulong, string>();
            var homeGuild = await GetHomeGuildAsync();
            var usersList = (await homeGuild.GetUsersAsync()).ToList();

            return usersList.Where(o => !o.IsBot && !o.IsWebhook).ToDictionary(
                o => o.Id,
                o => !string.IsNullOrEmpty(o.Nickname) ? $"{o.Nickname} ({o.Username}#{o.Discriminator})" : $"{o.Username}#{o.Discriminator}"
            );
        }

        public async Task<Dictionary<ulong, string>> GetTextChannelsOfHomeGuildAsync()
        {
            if (!Configuration.Enabled) return new Dictionary<ulong, string>();
            var homeGuild = await GetHomeGuildAsync();
            var channels = await homeGuild.GetTextChannelsAsync();
            return channels.ToDictionary(o => o.Id, o => o.Name);
        }

        public async Task<IRole> CreateRoleAsync(string name)
        {
            if (!Configuration.Enabled) return null;
            var homeGuild = await GetHomeGuildAsync();
            return await homeGuild.CreateRoleAsync(name, null, null, false, false, null);
        }

        public async Task<IRole> GetRoleAsync(ulong id)
        {
            if (!Configuration.Enabled) return null;
            var homeGuild = await GetHomeGuildAsync();
            return homeGuild.GetRole(id);
        }
    }
}
