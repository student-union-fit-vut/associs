﻿using AssocIS.Database.Services;
using AssocIS.Main.Models.Configuration;
using AssocIS.Main.Models.Configuration.External.Discord;
using Discord;
using Discord.Rest;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External.Discord
{
    public partial class DiscordExternalService : ExternalService
    {
        private DiscordRestClient DiscordClient { get; }
        private DiscordConfiguration Configuration { get; }

        public DiscordExternalService(IConfiguration configuration, ILoggerFactory loggerFactory, IAssocISRepository repository,
            DiscordRestClient discordRestClient) : base(configuration, loggerFactory, repository)
        {
            Configuration = GetConfiguration<DiscordConfiguration>("Discord");
            DiscordClient = discordRestClient;

            DiscordClient.Log += OnLogAsync;
        }

        public Task InitAsync()
        {
            if (DiscordClient.LoginState == LoginState.LoggedIn) return Task.CompletedTask;
            return DiscordClient.LoginAsync(TokenType.Bot, Configuration.Token);
        }

        public async Task<DiscordRestClient> GetClientAsync()
        {
            await InitAsync();
            return DiscordClient;
        }

        public async Task<IGuild> GetHomeGuildAsync(bool withCounts = false) => await (await GetClientAsync()).GetGuildAsync(Configuration.HomeGuildId, withCounts);
        public async Task<ITextChannel> GetNotificationsChannelAsync() => await (await GetHomeGuildAsync()).GetTextChannelAsync(Configuration.NotificationsChannelId);
        public async Task<ICategoryChannel> GetTemporaryCategoryAsync() => (await (await GetHomeGuildAsync()).GetCategoriesAsync()).FirstOrDefault(o => o.Id == Configuration.NotificationsChannelId);

        private async Task<List<ulong>> GetWatchedRolesAsync()
        {
            var roles = new List<ulong>();
            roles.AddRange(Configuration.RoleGroups.All);

            var teamRoles = await Repository.Team.GetTeamsQuery(null, false)
                .Where(o => o.DiscordRoleId != null)
                .Select(o => o.DiscordRoleId)
                .Distinct()
                .ToListAsync();
            roles.AddRange(teamRoles.Select(o => Convert.ToUInt64(o)));
            return roles.Distinct().ToList();
        }
    }
}
