﻿using AssocIS.Data.Models.Events;
using AssocIS.Data.Models.Meetings;
using AssocIS.Data.Models.Teams;
using AssocIS.Data.Models.Users;
using AssocIS.Database.Enums;
using AssocIS.Database.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External
{
    public abstract class ExternalService
    {
        private IConfiguration Configuration { get; }
        protected IAssocISRepository Repository { get; set; }
        protected ILogger Logger { get; }

        protected ExternalService(IConfiguration configuration, ILoggerFactory loggerFactory,
            IAssocISRepository repository)
        {
            Configuration = configuration;
            Logger = loggerFactory.CreateLogger(GetType());
            Repository = repository;
        }

        protected TConfiguration GetConfiguration<TConfiguration>([NotNull] string sectionKey) where TConfiguration : class
        {
            var section = Configuration.GetSection(sectionKey);
            if (!section.Exists())
                throw new KeyNotFoundException($"Požadovaná konfigurace '{sectionKey}' neexistuje.");

            return section.Get<TConfiguration>();
        }

        #region Meeting

        public Task MeetingNotifiedAsync(Meeting meeting, string calendarEventId, Dictionary<MeetingPresenceType, List<UserInfo>> expectedMembers) =>
            ExecuteAsync(() => ProcessMeetingAsync(meeting, calendarEventId, expectedMembers), IsMeetingProcessingEnabled);

        protected virtual Task ProcessMeetingAsync(Meeting meeting, string calendarEventId,
            Dictionary<MeetingPresenceType, List<UserInfo>> expectedMembers) => Task.CompletedTask;
        protected virtual bool IsMeetingProcessingEnabled() => false;

        #endregion

        #region Members

        protected virtual bool IsMembersProcessingEnabled() => false;
        protected virtual bool IsMembersSynchronizationEnabled() => false;

        public Task MemberCreatedAsync(UserDetail user) =>
            ExecuteAsync(() => ProcessMemberCreatedAsync(user), IsMembersProcessingEnabled);

        public Task MemberUpdatedAsync(UserDetail user) =>
            ExecuteAsync(() => ProcessMemberUpdatedAsync(user), IsMembersProcessingEnabled);

        public Task SynchronizeMembersAsync(List<UserDetail> users)
            => ExecuteAsync(() => ProcessSynchronizeMembersAsync(users), IsMembersSynchronizationEnabled);

        protected virtual Task ProcessMemberCreatedAsync(UserDetail user) => Task.CompletedTask;
        protected virtual Task ProcessMemberUpdatedAsync(UserDetail user) => Task.CompletedTask;
        protected virtual Task ProcessSynchronizeMembersAsync(List<UserDetail> users) => Task.CompletedTask;

        #endregion

        #region Events

        protected virtual bool IsEventProcessingEnabled() => false;

        public Task EventCreatedAsync(Event @event, bool createCommunicationChannel)
            => ExecuteAsync(() => ProcessEventCreatedAsync(@event, createCommunicationChannel), IsEventProcessingEnabled);

        public Task EventUpdatedAsync(Event @event, bool createCommunicationChannel)
            => ExecuteAsync(() => ProcessEventUpdatedAsync(@event, createCommunicationChannel), IsEventProcessingEnabled);

        public Task EventRemovedAsync(Event @event)
            => ExecuteAsync(() => ProcessEventRemovedAsync(@event), IsEventProcessingEnabled);

        protected virtual Task ProcessEventCreatedAsync(Event @event, bool createCommunicationChannel) => Task.CompletedTask;
        protected virtual Task ProcessEventUpdatedAsync(Event @event, bool createCommunicationChannel) => Task.CompletedTask;
        protected virtual Task ProcessEventRemovedAsync(Event @event) => Task.CompletedTask;

        #endregion

        #region Teams

        public virtual bool IsTeamsProcessingEnabled() => false;

        public Task TeamCreatedAsync(Team team, bool createRole)
            => ExecuteAsync(() => ProcessTeamCreatedAsync(team, createRole), IsTeamsProcessingEnabled);

        public Task TeamUpdatedAsync(Team before, Team after, bool createRole)
            => ExecuteAsync(() => ProcessTeamUpdatedAsync(before, after, createRole), IsTeamsProcessingEnabled);

        public Task TeamRemovedAsync(Team team)
            => ExecuteAsync(() => ProcessTeamRemovedAsync(team), IsTeamsProcessingEnabled);

        protected virtual Task ProcessTeamCreatedAsync(Team team, bool createRole) => Task.CompletedTask;
        protected virtual Task ProcessTeamUpdatedAsync(Team before, Team after, bool createRole) => Task.CompletedTask;
        protected virtual Task ProcessTeamRemovedAsync(Team team) => Task.CompletedTask;

        #endregion

        #region EveryoneMessage

        protected virtual bool IsEveryoneMessageProcessingEnabled() => false;

        public virtual Task EveryoneMessageAsync(string message)
            => ExecuteAsync(() => ProcessEveryoneMessageAsync(message), IsEveryoneMessageProcessingEnabled);

        protected virtual Task ProcessEveryoneMessageAsync(string message) => Task.CompletedTask;

        #endregion

        #region Helpers

        private async Task ExecuteAsync(Func<Task> task, Func<bool> isEnabled, [CallerMemberName] string method = null)
        {
            try
            {
                if (!isEnabled()) return;
                await task();
            }
            catch (Exception ex)
            {
                LogException(ex, method);
            }
        }

        private void LogException(Exception ex, [CallerMemberName] string method = null)
        {
            Logger.LogError(ex, $"An error occured while processing the method ({method}) on the external service ({GetType().Name})");
        }

        #endregion
    }
}
