﻿using AssocIS.Main.Services.External.Discord;
using AssocIS.Main.Services.External.Google;
using Discord;
using Discord.Rest;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External
{
    public static class ExternalServiceExtensions
    {
        public static Task AggregatedExecutionAsync(this IEnumerable<ExternalService> services, Func<ExternalService, Task> method)
        {
            var toProcessServices = services.Select(o => method(o)).ToArray();
            return Task.WhenAll(toProcessServices);
        }

        public static IServiceCollection AddExternalServices(this IServiceCollection services)
        {
            // Dependencies
            services.AddSingleton<DiscordRestClient>(new DiscordRestClient(new DiscordRestConfig()
            {
                LogLevel = LogSeverity.Debug,
                RateLimitPrecision = RateLimitPrecision.Millisecond
            }));

            // Services
            services
                .AddScoped<ExternalService, GoogleExternalService>()
                .AddScoped<ExternalService, DiscordExternalService>();

            return services;
        }
    }
}
