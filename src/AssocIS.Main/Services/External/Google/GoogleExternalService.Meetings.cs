﻿using AssocIS.Data.Models.Meetings;
using AssocIS.Data.Models.Users;
using AssocIS.Database.Enums;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External.Google
{
    public partial class GoogleExternalService
    {
        protected override bool IsMeetingProcessingEnabled() => Configuration.Enabled && Configuration.Features.Meetings;

        protected override async Task ProcessMeetingAsync(Meeting meeting, string calendarEventId, Dictionary<MeetingPresenceType, List<UserInfo>> expectedMembers)
        {
            using var service = CreateCalendarService();

            bool isNew = string.IsNullOrEmpty(calendarEventId);
            var calendarEvent = isNew ? null : await service.Events.Get(Configuration.DefaultCalendarId, calendarEventId).ExecuteAsync();

            if (meeting.State == MeetingState.Summoned)
                await ProcessSummonedMeetingAsync(calendarEvent, isNew, meeting, service, expectedMembers);
            else if (meeting.State == MeetingState.Cancelled && calendarEvent != null)
                await service.Events.Delete(Configuration.DefaultCalendarId, calendarEventId).ExecuteAsync();
        }

        private async Task ProcessSummonedMeetingAsync(Event calendarEvent, bool isNew, Meeting meeting, CalendarService calendarService,
            Dictionary<MeetingPresenceType, List<UserInfo>> expectedMembers)
        {
            if (calendarEvent == null)
            {
                isNew = true;
                calendarEvent = new Event();
            }

            calendarEvent.Summary = meeting.Name;
            calendarEvent.Start = new EventDateTime() { DateTime = meeting.At };
            calendarEvent.End = new EventDateTime() { DateTime = meeting.ExpectedEnd };
            calendarEvent.Created = meeting.LogData.CreatedAt;
            calendarEvent.Location = meeting.Location;
            calendarEvent.Description = string.Join("\n", meeting.Agenda.ConvertAll(o => $"- {o}")) + "\n\n" + meeting.Message;
            calendarEvent.Attendees = new List<EventAttendee>();

            if (expectedMembers.Count > 0)
            {
                foreach (var pair in expectedMembers)
                {
                    foreach (var user in pair.Value)
                    {
                        calendarEvent.Attendees.Add(new EventAttendee()
                        {
                            Email = user.Email,
                            Optional = pair.Key != MeetingPresenceType.Expected,
                            DisplayName = $"{user.Name} {user.Surname} ({user.Username})"
                        });
                    }
                }
            }

            if (isNew)
            {
                var createdEvent = await calendarService.Events.Insert(calendarEvent, Configuration.DefaultCalendarId).ExecuteAsync();

                var meetingEntity = await Repository.Meeting.FindMeetingByIdAsync(meeting.Id);
                meetingEntity.CalendarEventId = createdEvent.Id;
                await Repository.CommitAsync();
            }
            else
            {
                await calendarService.Events.Patch(calendarEvent, Configuration.DefaultCalendarId, calendarEvent.Id).ExecuteAsync();
            }
        }
    }
}
