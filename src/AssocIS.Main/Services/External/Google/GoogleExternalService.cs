﻿using AssocIS.Database.Services;
using AssocIS.Main.Models.Configuration;
using AssocIS.Main.Models.Configuration.External.Google;
using Google.Apis.Admin.Directory.directory_v1;
using Google.Apis.Admin.Directory.directory_v1.Data;
using Google.Apis.Calendar.v3;
using Google.Apis.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.External.Google
{
    public partial class GoogleExternalService : ExternalService
    {
        private GoogleConfiguration Configuration { get; }
        private EmailConfiguration EmailConfiguration { get; }
        private EmailService EmailService { get; }

        public GoogleExternalService(IConfiguration configuration, ILoggerFactory loggerFactory,
            IAssocISRepository repository, EmailService emailService) : base(configuration, loggerFactory, repository)
        {
            Configuration = GetConfiguration<GoogleConfiguration>("Google");
            EmailConfiguration = GetConfiguration<EmailConfiguration>("Email");
            EmailService = emailService;
        }

        private BaseClientService.Initializer CreateInitializer(string scope) => CreateInitializer(new[] { scope });

        private BaseClientService.Initializer CreateInitializer(string[] scopes)
        {
            var credentials = Configuration.CreateCredential(scopes.ToList());
            return new BaseClientService.Initializer() { HttpClientInitializer = credentials };
        }

        private DirectoryService CreateDirectoryService()
        {
            var scopes = new[] {
                DirectoryService.ScopeConstants.AdminDirectoryUser,
                DirectoryService.ScopeConstants.AdminDirectoryGroup
            };

            return new DirectoryService(CreateInitializer(scopes));
        }

        private CalendarService CreateCalendarService() => new CalendarService(CreateInitializer(CalendarService.ScopeConstants.Calendar));

        public async Task<List<string>> GetWatchedGroupsAsync()
        {
            var mailingListsQuery = Repository.Team.GetMailingListsQuery(null)
                .Select(o => o.Email).AsNoTracking();
            var mailingLists = await mailingListsQuery.ToListAsync();
            mailingLists.AddRange(Configuration.Groups.All);

            return mailingLists.Distinct().ToList();
        }
    }
}
