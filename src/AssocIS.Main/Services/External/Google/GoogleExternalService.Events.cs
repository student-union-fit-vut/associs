﻿using AssocIS.Data.Models.Events;
using Google.Apis.Calendar.v3.Data;
using System.Threading.Tasks;
using GoogleCalendarData = Google.Apis.Calendar.v3.Data;

namespace AssocIS.Main.Services.External.Google
{
    public partial class GoogleExternalService
    {
        protected override bool IsEventProcessingEnabled() => Configuration.Enabled && Configuration.Features.Events;

        protected override async Task ProcessEventCreatedAsync(Data.Models.Events.Event @event, bool createCommunicationChannel)
        {
            if (!string.IsNullOrEmpty(@event.CalendarEventId)) return;
            using var service = CreateCalendarService();

            var calendarEvent = new GoogleCalendarData.Event()
            {
                Summary = @event.Name,
                Start = new EventDateTime() { DateTime = @event.From },
                End = new EventDateTime() { DateTime = @event.To },
                Created = @event.LogData.CreatedAt,
                Description = @event.Description + (string.IsNullOrEmpty(@event.Note) ? "\n\n" + @event.Note : "")
            };

            var creationRequest = service.Events.Insert(calendarEvent, Configuration.DefaultCalendarId);
            var createdEvent = await creationRequest.ExecuteAsync();

            var eventEntity = await Repository.Event.GetEventByIdAsync(@event.Id);
            eventEntity.CalendarEventId = createdEvent.Id;
            await Repository.CommitAsync();
        }

        protected override async Task ProcessEventUpdatedAsync(Data.Models.Events.Event @event, bool createCommunicationChannel)
        {
            if (string.IsNullOrEmpty(@event.CalendarEventId)) return;
            using var service = CreateCalendarService();

            var calendarEvent = await service.Events.Get(Configuration.DefaultCalendarId, @event.CalendarEventId).ExecuteAsync();
            if (calendarEvent == null)
            {
                @event.CalendarEventId = null;
                await ProcessEventCreatedAsync(@event, createCommunicationChannel);
                return;
            }

            calendarEvent.Summary = @event.Name;
            calendarEvent.Start = new EventDateTime() { DateTime = @event.From };
            calendarEvent.End = new EventDateTime() { DateTime = @event.To };
            calendarEvent.Created = @event.LogData.CreatedAt;
            calendarEvent.Description = @event.Description + (string.IsNullOrEmpty(@event.Note) ? "\n\n" + @event.Note : "");
            await service.Events.Patch(calendarEvent, Configuration.DefaultCalendarId, @event.CalendarEventId).ExecuteAsync();
        }

        protected override async Task ProcessEventRemovedAsync(Data.Models.Events.Event @event)
        {
            if (string.IsNullOrEmpty(@event.CalendarEventId)) return;
            using var service = CreateCalendarService();

            await service.Events.Delete(Configuration.DefaultCalendarId, @event.CalendarEventId).ExecuteAsync();
        }
    }
}
