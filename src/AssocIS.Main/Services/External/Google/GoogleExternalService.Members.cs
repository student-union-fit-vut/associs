﻿using AssocIS.Common.Helpers;
using AssocIS.Data.Models.Users;
using AssocIS.Data.Resources.Templates.EmailTemplates;
using AssocIS.Database.Enums;
using Google.Apis.Admin.Directory.directory_v1;
using Google.Apis.Admin.Directory.directory_v1.Data;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoogleDirectory = global::Google.Apis.Admin.Directory.directory_v1;

namespace AssocIS.Main.Services.External.Google
{
    public partial class GoogleExternalService
    {
        protected override bool IsMembersProcessingEnabled() => Configuration.Enabled && Configuration.Features.Members;
        protected override bool IsMembersSynchronizationEnabled() => Configuration.Enabled && Configuration.Features.MembersSynchronization;

        protected override async Task ProcessMemberCreatedAsync(UserDetail user)
        {
            if (!string.IsNullOrEmpty(user.GooglePrimaryEmail)) return;

            using var service = CreateDirectoryService();
            await CreateGoogleUserAsync(user, service);
        }

        private async Task CreateGoogleUserAsync(UserDetail user, DirectoryService service)
        {
            var googleUser = MapUserToGoogle(user);
            var request = service.Users.Insert(googleUser);
            await request.ExecuteAsync();

            var userEntity = await Repository.User.FindUserByIdAsync(user.Id);
            userEntity.GooglePrimaryEmail = googleUser.PrimaryEmail;
            await Repository.CommitAsync();

            user.GooglePrimaryEmail = googleUser.PrimaryEmail;

            await SetMemberGroupsAsync(googleUser, user, service);
            await SendMailWithPasswordAsync(user, googleUser.Password);
        }

        protected override async Task ProcessMemberUpdatedAsync(UserDetail user)
        {
            if (string.IsNullOrEmpty(user.GooglePrimaryEmail)) return;
            using var service = CreateDirectoryService();

            var googleUserRequest = service.Users.Get(user.GooglePrimaryEmail);
            var googleUser = await googleUserRequest.ExecuteAsync();
            if (googleUser == null) return;

            UpdateGoogleUser(googleUser, user);
            var updateRequest = service.Users.Update(googleUser, user.GooglePrimaryEmail);
            await updateRequest.ExecuteAsync();
            await SetMemberGroupsAsync(googleUser, user, service);
        }

        protected override async Task ProcessSynchronizeMembersAsync(List<UserDetail> users)
        {
            using var service = CreateDirectoryService();

            foreach (var user in users)
            {
                var googleUser = string.IsNullOrEmpty(user.GooglePrimaryEmail) ? null : await service.Users.Get(user.GooglePrimaryEmail).ExecuteAsync();

                if (googleUser == null)
                {
                    await CreateGoogleUserAsync(user, service);
                }
                else
                {
                    UpdateGoogleUser(googleUser, user);
                    var updateRequest = service.Users.Update(googleUser, user.GooglePrimaryEmail);
                    await updateRequest.ExecuteAsync();

                    await SetMemberGroupsAsync(googleUser, user, service);
                }
            }
        }

        private GoogleDirectory.Data.User MapUserToGoogle(UserDetail user)
        {
            var password = StringHelpers.CreateSafeString(16);

            return new GoogleDirectory.Data.User()
            {
                ChangePasswordAtNextLogin = true,
                CreationTime = DateTime.Now,
                Name = new UserName()
                {
                    FamilyName = user.Surname,
                    GivenName = user.Name
                },
                Notes = user.GlobalNote,
                OrgUnitPath = Configuration.OrgUnit,
                PrimaryEmail = $"{user.Username}@{Configuration.Domain}",
                RecoveryEmail = user.Email,
                RecoveryPhone = user.PhoneNumber,
                Password = password
            };
        }

        private void UpdateGoogleUser(GoogleDirectory.Data.User googleUser, UserDetail user)
        {
            googleUser.Name = new UserName()
            {
                FamilyName = user.Surname,
                GivenName = user.Name
            };
            googleUser.Notes = user.GlobalNote;
            googleUser.RecoveryEmail = user.Email;
            googleUser.RecoveryPhone = user.PhoneNumber;
            googleUser.Suspended = user.LeavedAt != null && user.LeavedAt < DateTime.Now;
        }

        private async Task SendMailWithPasswordAsync(UserDetail user, string password)
        {
            var message = new MimeMessage()
            {
                Subject = EmailTemplates.GoogleAccountCreatedSubject,
                Body = new TextPart("html") { Text = CreateGoogleAccountCreatedBody(user, password) }
            };

            message.From.Add(EmailConfiguration.MailboxAddress);
            message.To.Add(new MailboxAddress($"{user.Name} {user.Surname}", user.Email));
            await EmailService.SendAsync(message);
        }

        private string CreateGoogleAccountCreatedBody(UserDetail user, string password)
        {
            var template = EmailTemplates.CreatedGoogleAccount;

            foreach (var property in user.GetType().GetProperties())
            {
                var value = property.GetValue(user, null);

                if (value == null)
                {
                    template = template.Replace("{{Property." + property.Name + "}}", "");
                    continue;
                }

                template = value switch
                {
                    DateTime dateTime => template.Replace("{{Property." + property.Name + "}}", dateTime.ToString(EmailConfiguration.BodyDateTimeFormat)),
                    _ => template.Replace("{{Property." + property.Name + "}}", value.ToString()),
                };
            }

            template = template.Replace("{{Password}}", password);
            return template + EmailTemplates.GeneratedFooter;
        }

        private async Task SetMemberGroupsAsync(GoogleDirectory.Data.User googleUser, UserDetail user, DirectoryService service)
        {
            var expectedGroups = await CalculateExpectedMemberGroupsAsync(user);
            if (expectedGroups == null) return;

            var memberGroupsRequest = service.Groups.List();
            memberGroupsRequest.UserKey = googleUser.Id;
            var memberGroups = await memberGroupsRequest.ExecuteAsync();
            var watchedGroups = await GetWatchedGroupsAsync();

            if (memberGroups.GroupsValue != null)
            {
                foreach (var group in memberGroups.GroupsValue)
                {
                    // Ignore groups, that is unwatched or user can have this group.
                    if (expectedGroups.Contains(group.Email) || !watchedGroups.Contains(group.Email))
                        continue;

                    // Otherwise remove member from this group.
                    var removeMemberRequest = service.Members.Delete(group.Id, googleUser.Id);
                    //await removeMemberRequest.ExecuteAsync();
                }

                foreach (var group in expectedGroups.Where(o => !memberGroups.GroupsValue.Any(x => x.Email == o)))
                {
                    var addMemberRequest = service.Members.Insert(new Member() { Email = googleUser.PrimaryEmail }, group);
                    await addMemberRequest.ExecuteAsync();
                }
            }
            else
            {
                // Member not have any group. Add all
                foreach (var group in expectedGroups)
                {
                    var addMemberRequest = service.Members.Insert(new Member() { Email = googleUser.PrimaryEmail }, group);
                    await addMemberRequest.ExecuteAsync();
                }
            }
        }

        private async Task<List<string>> CalculateExpectedMemberGroupsAsync(UserDetail user)
        {
            if (user.LeavedAt != null) return null; // Process is ignored if member left association.

            var result = new List<string>();

            if (user.UserState == UserState.ActiveCollaborator)
            {
                // Active collaborators have cannot have groups such as chairman, ...
                result.Add(Configuration.Groups.ActiveCollaborators);
            }
            else
            {
                result.Add(Configuration.Groups.BasicMembers);

                if (user.UserState == UserState.Chairman)
                    result.Add(Configuration.Groups.Chairman);
                else if (user.UserState == UserState.ViceChairman)
                    result.Add(Configuration.Groups.ViceChairman);

                foreach (var teamMember in user.Teams)
                {
                    if (teamMember.IsLeader)
                        result.Add(Configuration.Groups.TeamLeaders);

                    var teamMailingLists = await Repository.Team.GetMailingListsQuery(teamMember.Id)
                        .Select(o => o.Email).AsNoTracking().ToListAsync();
                    if (teamMailingLists.Count > 0)
                        result.AddRange(teamMailingLists);
                }
            }

            return result.Distinct().ToList();
        }
    }
}
