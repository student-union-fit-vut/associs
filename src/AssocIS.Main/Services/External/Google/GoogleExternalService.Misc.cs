﻿using System.Net.Mail;

namespace AssocIS.Main.Services.External.Google
{
    public partial class GoogleExternalService
    {
        public MailAddress CreateMailFromAnotherAddress(string mail)
        {
            var original = new MailAddress(mail);
            return new MailAddress($"{original.User}@{Configuration.Domain}");
        }
    }
}
