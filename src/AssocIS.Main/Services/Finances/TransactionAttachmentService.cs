﻿using AssocIS.Data.Infrastructure;
using AssocIS.Database.Services;
using AssocIS.Main.Infrastructure.Auth;
using AssocIS.Main.Services.Auth;
using AssocIS.Common.Extensions;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Data.Models.Finance.Transactions;
using System;
using Microsoft.AspNetCore.StaticFiles;

namespace AssocIS.Main.Services.Finances
{
    [Service]
    public class TransactionAttachmentService
    {
        private IAssocISRepository Repository { get; }
        private FileExtensionContentTypeProvider FileExtensionContentTypeProvider { get; }

        public TransactionAttachmentService(IAssocISRepository repository, FileExtensionContentTypeProvider fileExtensionContentTypeProvider)
        {
            Repository = repository;
            FileExtensionContentTypeProvider = fileExtensionContentTypeProvider;
        }

        public async Task<Tuple<byte[], string>> GetAttachmentDataAsync(long transactionId, long attachmentId, ClaimsPrincipal loggedUser)
        {
            var creatorUserId = loggedUser.IsInPolicy(Policies.FinanceManagement) ? (long?)null : loggedUser.GetUserId();
            var attachment = await Repository.Finance.GetTransactionAttachmentByIdAsync(transactionId, attachmentId, creatorUserId, false, false);

            if (attachment == null)
                return null;

            var contentType = FileExtensionContentTypeProvider.TryGetContentType(attachment.Name, out string ct) ? ct : null;
            return Tuple.Create(attachment?.DecompressedData, contentType);
        }

        private async Task<TransactionAttachment> GetTransactionAttachmentDetailAsync(long transactionId, long attachmentId)
        {
            var entity = await Repository.Finance.GetTransactionAttachmentByIdAsync(transactionId, attachmentId, null, true, true);
            return entity == null ? null : new TransactionAttachment(entity);
        }

        public async Task<TransactionAttachment> CreateAsync(long transactionId, CreateTransactionAttachmentParams parameters, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var entity = await parameters.ToEntityAsync(transactionId, loggedUserId);

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            return await GetTransactionAttachmentDetailAsync(transactionId, entity.Id);
        }

        public async Task RemoveAsync(long transactionId, long attachmentId)
        {
            var entity = await Repository.Finance.GetTransactionAttachmentByIdAsync(transactionId, attachmentId, null, true, false);

            if (entity == null)
                return;

            Repository.Remove(entity);
            await Repository.CommitAsync();
        }
    }
}
