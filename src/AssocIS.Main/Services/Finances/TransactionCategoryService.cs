﻿using AssocIS.Data.Infrastructure;
using AssocIS.Data.Models.Finance.Categories;
using AssocIS.Database.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Services.Finances
{
    [Service(ServiceLifetime.Scoped)]
    public class TransactionCategoryService
    {
        private IAssocISRepository Repository { get; }

        public TransactionCategoryService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<TransactionCategory> CreateAsync(UpdateCategoryParams parameters)
        {
            var entity = parameters.ToEntity();

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            return new TransactionCategory(entity, 0);
        }

        public async Task<List<TransactionCategory>> GetCategoriesAsync()
        {
            var query = Repository.Finance.GetCategoriesQueryWithCount();
            var data = await query.ToListAsync();

            return data.ConvertAll(o => new TransactionCategory(o.Item1, o.Item2));
        }

        public async Task<TransactionCategory> UpdateCategoryAsync(long id, UpdateCategoryParams parameters)
        {
            var entity = await Repository.Finance.GetCategoryByIdAsync(id);

            if (entity == null)
                return null;

            var transactionCount = await Repository.Finance.GetTransactionCountInCategoryAsync(id);

            parameters.UpdateEntity(entity);
            await Repository.CommitAsync();

            return new TransactionCategory(entity, transactionCount);
        }

        public async Task<TransactionCategory> DeleteCategoryAsync(long id)
        {
            var category = await Repository.Finance.GetCategoryByIdAsync(id);

            if (category == null)
                return null;

            Repository.Remove(category);
            await Repository.CommitAsync();

            return new TransactionCategory();
        }

        public async Task<bool> CategoryNameExistsAsync(string name)
        {
            return await Repository.Finance.TransactionCategoryNameExistsAsync(name);
        }
    }
}
