﻿using AssocIS.Data.Models.Finance.Suppliers;
using AssocIS.Common.Extensions;
using AssocIS.Database.Services;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Data.Infrastructure;
using System.Collections.Generic;

namespace AssocIS.Main.Services.Finances
{
    [Service]
    public class SupplierService
    {
        private IAssocISRepository Repository { get; }

        public SupplierService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<List<SupplierListItem>> GetSuppliersListAsync(string nameQuery)
        {
            var query = Repository.Finance.GetSuppliersQuery(nameQuery).AsNoTracking();
            var data = await query.ToListAsync();
            var list = new List<SupplierListItem>();

            foreach(var supplier in data)
            {
                var canRemove = !await Repository.Finance.AnyTransactionOfSupplierAsync(supplier.Id);
                list.Add(new SupplierListItem(supplier, canRemove));
            }

            return list;
        }

        public async Task<Supplier> GetSupplierAsync(long id)
        {
            var entity = await Repository.Finance.GetSupplierByIdAsync(id);

            if (entity == null)
                return null;

            var canRemove = !await Repository.Finance.AnyTransactionOfSupplierAsync(id);
            return new Supplier(entity, canRemove);
        }

        public async Task<Supplier> CreateAsync(UpdateSupplierParams parameters, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var entity = parameters.ToEntity(loggedUserId);

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            return await GetSupplierAsync(entity.Id);
        }

        public async Task<Supplier> UpdateAsync(long id, UpdateSupplierParams parameters, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Finance.GetSupplierByIdAsync(id);

            if (entity == null)
                return null;

            var loggedUserId = loggedUser.GetUserId();
            parameters.UpdateEntity(entity, loggedUserId);

            await Repository.CommitAsync();
            return await GetSupplierAsync(id);
        }

        public async Task<bool> CanRemoveSupplierAsync(long id)
        {
            return !await Repository.Finance.AnyTransactionOfSupplierAsync(id);
        }

        public async Task RemoveSupplierAsync(long id)
        {
            if (await Repository.Finance.AnyTransactionOfSupplierAsync(id))
                return;

            var entity = await Repository.Finance.GetSupplierByIdAsync(id);

            if (entity == null)
                return;

            Repository.Remove(entity);
            await Repository.CommitAsync();
        }

        public async Task<bool> SupplierExistsAsync(long id)
        {
            return await Repository.Finance.SupplierExistsAsync(id);
        }
    }
}
