﻿using AssocIS.Data.Infrastructure;
using AssocIS.Data.Models.Finance.Accounts;
using AssocIS.Common.Extensions;
using AssocIS.Database.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography.Xml;

namespace AssocIS.Main.Services.Finances
{
    [Service]
    public class AccountService
    {
        private IAssocISRepository Repository { get; }

        public AccountService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<Account> CreateAsync(UpdateAccountParams parameters, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var entity = parameters.ToEntity(loggedUserId);

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            entity = await Repository.Finance.GetAccountByIdAsync(entity.Id);
            return new Account(entity, 0, 0, true);
        }

        public async Task<List<AccountListItem>> GetAccountsListAsync(SearchAccountParams parameters)
        {
            var query = Repository.Finance.GetAccountsQuery(parameters.NameQuery, parameters.AccountNumberQuery, parameters.BankCode);
            var data = await query.ToListAsync();
            var result = new List<AccountListItem>();

            foreach(var item in data)
            {
                var canRemove = !await Repository.Finance.AnyTransactionOnAccountAsync(item.Id);
                result.Add(new AccountListItem(item, canRemove));
            }

            return result;
        }

        public async Task<Account> GetAccountDetailAsync(long id)
        {
            var entity = await Repository.Finance.GetAccountByIdAsync(id);

            if (entity == null)
                return null;

            var state = await Repository.Finance.CalculateAccountStateAsync(id);
            var canRemove = !await Repository.Finance.AnyTransactionOnAccountAsync(id);

            return new Account(entity, state.Item1, state.Item2, canRemove);
        }

        public async Task<Account> UpdateAsync(long id, UpdateAccountParams parameters, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Finance.GetAccountByIdAsync(id);

            if (entity == null)
                return null;

            var loggedUserId = loggedUser.GetUserId();
            parameters.UpdateEntity(entity, loggedUserId);

            await Repository.CommitAsync();

            return await GetAccountDetailAsync(id);
        }

        public async Task RemoveAsync(long id)
        {
            if (await Repository.Finance.AnyTransactionOnAccountAsync(id))
                return;

            var entity = await Repository.Finance.GetAccountByIdAsync(id);

            if (entity == null)
                return;

            Repository.Remove(entity);
            await Repository.CommitAsync();
        }

        public async Task<bool> AccountExistsAsync(long id)
        {
            return await Repository.Finance.AccountExistsAsync(id);
        }
    }
}
