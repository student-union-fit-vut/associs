﻿using AssocIS.Data.Infrastructure;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Finance.Transactions;
using AssocIS.Common.Extensions;
using AssocIS.Database.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Database.Enums;
using AssocIS.Data.Models.Finance.Suppliers;
using AssocIS.Data.Models.Finance.Categories;
using AssocIS.Main.Infrastructure.Auth;
using AssocIS.Main.Services.Auth;
using System;

namespace AssocIS.Main.Services.Finances
{
    [Service]
    public class FinanceService
    {
        private IAssocISRepository Repository { get; set; }

        public FinanceService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<PaginatedData<TransactionListItem>> GetTransactionListAsync(SearchTransactionParams parameters, ClaimsPrincipal loggedUser)
        {
            var query = Repository.Finance.GetTransactionsQuery(parameters.ProcessedFrom, parameters.ProcessedTo, parameters.AmountFrom,
                parameters.AmountTo, parameters.TransactionTypes?.ToArray(), parameters.NameQuery, parameters.AccountIds?.ToArray(),
                parameters.SupplierIds?.ToArray(), parameters.CategoryIds?.ToArray(), parameters.OnlyWithAttachments);

            bool haveHigherPerms = loggedUser.IsInPolicy(Policies.FinanceManagement);
            if (!haveHigherPerms)
            {
                var loggedUserId = loggedUser.GetUserId();
                query = query.Where(o => o.CreatedById == loggedUserId);
            }

            return await PaginatedData<TransactionListItem>.CreateAsync(query.AsNoTracking(), parameters, entity =>
            {
                bool canManage = haveHigherPerms || entity.CreatedById == loggedUser.GetUserId();
                return new TransactionListItem(entity, canManage);
            });
        }

        public async Task<Transaction> GetTransactionDetailAsync(long id, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.IsInPolicy(Policies.FinanceManagement) ? (long?)null : loggedUser.GetUserId();
            var entity = await Repository.Finance.GetTransactionByIdAsync(id, loggedUserId);
            return entity == null ? null : new Transaction(entity, loggedUserId == null || entity.CreatedById == loggedUserId.Value);
        }

        public async Task<Transaction> CreateAsync(UpdateTransactionParams parameters, ClaimsPrincipal loggedUser)
        {
            var categories = await GetCategoriesFromIdsAsync(parameters.Categories);
            var loggedUserId = loggedUser.GetUserId();
            var entity = parameters.ToEntity(loggedUserId, categories);

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            return await GetTransactionDetailAsync(entity.Id, loggedUser);
        }

        public async Task<Transaction> UpdateAsync(long id, UpdateTransactionParams parameters, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var creatorUserId = loggedUser.IsInPolicy(Policies.FinanceManagement) ? (long?)null : loggedUserId;
            var entity = await Repository.Finance.GetTransactionByIdAsync(id, creatorUserId);

            if (entity == null)
                return null;

            var categories = await GetCategoriesFromIdsAsync(parameters.Categories);
            parameters.UpdateEntity(entity, loggedUserId, categories);
            await Repository.CommitAsync();

            return await GetTransactionDetailAsync(id, loggedUser);
        }

        public async Task<bool> TransactionExistsAsync(long id, ClaimsPrincipal loggedUser)
        {
            var query = Repository.Finance.GetTransactionsQuery();

            if (!loggedUser.IsInPolicy(Policies.FinanceManagement))
            {
                var loggedUserId = loggedUser.GetUserId();
                query = query.Where(o => o.CreatedById == loggedUserId);
            }

            return await query.AnyAsync(o => o.Id == id);
        }

        public async Task RemoveTransactionAsync(long id)
        {
            var entity = await Repository.Finance.GetTransactionByIdAsync(id);

            if (entity.Attachments.Count > 0)
                Repository.RemoveCollection(entity.Attachments);

            Repository.Remove(entity);
            await Repository.CommitAsync();
        }

        private async Task<List<Database.Entity.Finance.TransactionCategory>> GetCategoriesFromIdsAsync(List<long> ids)
        {
            return await Repository.Finance.GetCategoriesQuery()
                .Where(o => ids.Contains(o.Id))
                .ToListAsync();
        }

        public async Task<List<TransactionReportItem<TransactionType>>> GetReportByTransactionTypesAsync()
        {
            var query = await Repository.Finance.GetTransactionsQuery()
                .AsNoTracking()
                .GroupBy(o => o.Type)
                .Select(o => new TransactionReportItem<TransactionType>(o.Key, o.Count(), o.Sum(x => x.Amount)))
                .ToListAsync();

            return Enum.GetValues<TransactionType>().Select(o => query.Find(x => x.GroupValue == o) ?? new TransactionReportItem<TransactionType>(o, 0, 0.0M)).ToList();
        }

        public async Task<List<TransactionReportItem<SupplierListItem>>> GetReportBySupplierAsync()
        {
            var query = Repository.Finance.GetTransactionsQuery()
                .AsNoTracking()
                .GroupBy(o => o.SupplierId)
                .Select(o => new { SupplierId = o.Key, Count = o.Count(), Amount = o.Sum(x => x.Type == TransactionType.Incoming ? x.Amount : -x.Amount) });

            var data = await query.ToListAsync();
            var suppliers = await Repository.Finance.GetSuppliersQuery(null).ToListAsync();
            var result = new List<TransactionReportItem<SupplierListItem>>();

            var withoutSupplier = data.Find(o => o.SupplierId == null);
            if (withoutSupplier != null)
                result.Add(new TransactionReportItem<SupplierListItem>(null, withoutSupplier.Count, withoutSupplier.Amount));

            foreach (var supplier in suppliers)
            {
                var item = data.Find(o => o.SupplierId == supplier.Id);
                result.Add(new TransactionReportItem<SupplierListItem>(new SupplierListItem(supplier, false), item?.Count ?? 0, item?.Amount ?? 0.0M));
            }

            return result;
        }

        public async Task<List<TransactionReportItem<TransactionCategory>>> GetReportByTransactionCategoryAsync()
        {
            var categoriesQuery = Repository.Finance.GetCategoriesQuery()
                .AsNoTracking()
                .Select(o => new { Category = o, o.Transactions.Count, Sum = o.Transactions.Sum(x => x.Type == TransactionType.Incoming ? x.Amount : -x.Amount) });

            return (await categoriesQuery.ToListAsync()).ConvertAll(o => new TransactionReportItem<TransactionCategory>(new TransactionCategory(o.Category, o.Count), o.Count, o.Sum));
        }
    }
}
