﻿using AssocIS.Data.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;

namespace AssocIS.Main.Services.System
{
    [Service(ServiceLifetime.Singleton)]
    public class RequestsCounterService
    {
        public int Count { get; private set; }

        public void Increment()
        {
            int count = Count;
            Interlocked.Increment(ref count);
            Count = count;
        }
    }
}
