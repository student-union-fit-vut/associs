﻿using AssocIS.Data.Infrastructure;
using AssocIS.Data.Models.Accesses;
using AssocIS.Database.Services;
using AssocIS.Common.Extensions;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Data.Models.Common;

namespace AssocIS.Main.Services.Accesses
{
    [Service(ServiceLifetime.Transient)]
    public class AccessService
    {
        private IAssocISRepository Repository { get; }

        public AccessService(IAssocISRepository repository)
        {
            Repository = repository;
        }

        public async Task<AccessDefinition> CreateAccessDefinitionAsync(UpdateAccessDefinitionParams parameters, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var entity = parameters.ToEntity(loggedUserId);

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            return await GetAccessDefinitionDetailAsync(entity.Id);
        }

        public async Task<AccessDefinition> GetAccessDefinitionDetailAsync(long id)
        {
            var entity = await Repository.Access.GetAccessDefinitionByIdAsync(id);
            return entity == null ? null : new AccessDefinition(entity);
        }

        public async Task<PaginatedData<AccessDefinitionListItem>> GetAccessDefinitionListAsync(AccessDefinitionSearchParams parameters)
        {
            var query = Repository.Access.GetAccessDefinitionsQuery(parameters.Type, parameters.CreatedById, parameters.DescriptionContains);
            return await PaginatedData<AccessDefinitionListItem>.CreateAsync(query, parameters, entity => new AccessDefinitionListItem(entity));
        }

        public async Task<PaginatedData<AccessMemberListItem>> GetAccessMembersAsync(long id, PaginationParams pagination)
        {
            if (!await AccessDefinitionExistsAsync(id))
                return null;

            var query = Repository.Access.GetAccessMembersQuery(id);
            return await PaginatedData<AccessMemberListItem>.CreateAsync(query, pagination, entity => new AccessMemberListItem(entity));
        }

        public async Task<AccessMember> GetAccessMemberDetailAsync(long accessId, long userId)
        {
            var entity = await Repository.Access.GetAccessMemberByIdAsync(accessId, userId);
            return entity == null ? null : new AccessMember(entity);
        }

        public async Task<AccessMember> SetAccessMemberAsync(long accessId, SetAccessMemberParams parameters, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var entity = await Repository.Access.GetAccessMemberByIdAsync(accessId, parameters.UserId);

            if (entity == null)
            {
                entity = parameters.ToEntity(accessId, loggedUserId);
                await Repository.AddAsync(entity);
            }
            else
            {
                parameters.UpdateEntity(entity, loggedUserId);
            }

            await Repository.CommitAsync();
            return await GetAccessMemberDetailAsync(accessId, parameters.UserId);
        }

        public async Task<bool> AccessDefinitionExistsAsync(long id)
        {
            return await Repository.Access.AccessDefinitionExistsAsync(id);
        }

        public async Task<AccessDefinition> UpdateAccessDefinitionAsync(long id, UpdateAccessDefinitionParams parameters, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Access.GetAccessDefinitionByIdAsync(id);

            if (entity == null)
                return null;

            var loggedUserId = loggedUser.GetUserId();
            parameters.UpdateEntity(entity, loggedUserId);
            await Repository.CommitAsync();

            return await GetAccessDefinitionDetailAsync(id);
        }

        public async Task RemoveAccessDefinitionAsync(long id)
        {
            var entity = await Repository.Access.GetAccessDefinitionByIdAsync(id, true);
            if (entity == null) return;

            if (entity.Members.Count > 0)
                Repository.RemoveCollection(entity.Members);

            Repository.Remove(entity);
            await Repository.CommitAsync();
        }
    }
}
