﻿using AssocIS.Database.Enums;
using AssocIS.Database.Services;
using AssocIS.Main.Models.Configuration;
using AssocIS.Data.Extensions.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using AssocIS.Data.Infrastructure;
using AssocIS.Data.Models.Meetings;
using AssocIS.Data.Models.Users;
using AssocIS.Data.Helpers;
using AssocIS.Data.Resources.Templates.EmailTemplates;
using AssocIS.Main.Services.External;

namespace AssocIS.Main.Services.Meetings
{
    [Service(ServiceLifetime.Scoped)]
    public class MeetingNotificationService
    {
        private IAssocISRepository Repository { get; }
        private EmailConfiguration EmailConfiguration { get; }
        private EmailService EmailService { get; }
        private IEnumerable<ExternalService> ExternalServices { get; }

        public MeetingNotificationService(IAssocISRepository repository, IOptionsSnapshot<EmailConfiguration> options, EmailService emailService,
            IEnumerable<ExternalService> externalServices)
        {
            Repository = repository;
            EmailConfiguration = options.Value;
            EmailService = emailService;
            ExternalServices = externalServices;
        }

        public async Task NotifyAsync(Database.Entity.Meeting meeting, MeetingState fromState, MeetingState toState)
        {
            if (!CanNotifyMeetingChange(fromState, toState))
                return;

            var expectedUsersQuery = Repository.Meeting.GetExpectedUsersQuery(meeting);
            var expectedUsers = await expectedUsersQuery.ToListAsync();
            var groupedExpectedUsers = new Dictionary<MeetingPresenceType, List<UserInfo>>();

            foreach (var user in expectedUsers)
            {
                var userPresence = meeting.Presences.FirstOrDefault(o => o.UserId == user.Id);
                var presence = MeetingPresenceHelper.CalculatePresence(meeting, userPresence, user);

                if (!groupedExpectedUsers.ContainsKey(presence))
                    groupedExpectedUsers.Add(presence, new List<UserInfo>() { new UserInfo(user) });
                else
                    groupedExpectedUsers[presence].Add(new UserInfo(user));
            }

            await ExternalServices.AggregatedExecutionAsync(
                service => service.MeetingNotifiedAsync(new(meeting, null), meeting.CalendarEventId, groupedExpectedUsers)
            );
            await NotifyToEmailAsync(meeting, expectedUsers);

            meeting.LastNotifiedAt = DateTime.Now;
            await Repository.CommitAsync();
        }

        private static bool CanNotifyMeetingChange(MeetingState fromState, MeetingState toState)
        {
            if (fromState == toState)
                return false;

            var allowedCombinations = new Func<bool>[]
            {
                () => fromState == MeetingState.Drafted && toState == MeetingState.Summoned, // Drafted => Summoned
                () => fromState == MeetingState.Summoned && toState == MeetingState.Cancelled, // Summoned => Cancelled
                () => fromState == MeetingState.Cancelled && toState == MeetingState.Summoned // Cancelled => Summoned
            };

            return allowedCombinations.Any(s => s());
        }

        private async Task NotifyToEmailAsync(Database.Entity.Meeting meeting, List<Database.Entity.User> expectedUsers)
        {
            var messages = expectedUsers.ConvertAll(user => BuildMessage(meeting, user));

            if (messages == null || messages.Count == 0)
                return; // We haven't messages to send.

            await EmailService.SendBatchAsync(messages);
        }

        private MimeMessage BuildMessage(Database.Entity.Meeting meeting, Database.Entity.User user)
        {
            var message = new MimeMessage()
            {
                Subject = BuildSubject(meeting),
                Body = new TextPart("html") { Text = BuildBody(meeting) }
            };

            message.From.Add(EmailConfiguration.MailboxAddress);
            message.To.Add(new MailboxAddress(user.FormatName(), user.Email));

            return message;
        }

        private string BuildSubject(Database.Entity.Meeting meeting)
        {
            return meeting.State switch
            {
                MeetingState.Summoned => string.Format(EmailTemplates.MeetingSummonedSubject, meeting.At.ToString(EmailConfiguration.SubjectDateTimeFormat), meeting.Name),
                MeetingState.Cancelled => string.Format(EmailTemplates.MeetingCancelledSubject, meeting.Name, meeting.At.ToString(EmailConfiguration.SubjectDateTimeFormat)),
                _ => throw new NotSupportedException("Unsupported meeting state for subject."),
            };
        }

        private string BuildBody(Database.Entity.Meeting meeting)
        {
            var builder = new StringBuilder();

            string template = null;
            switch (meeting.State)
            {
                case MeetingState.Cancelled:
                    template = EmailTemplates.MeetingCancelled;
                    break;
                case MeetingState.Summoned:
                    template = EmailTemplates.MeetingSummoned;
                    break;
            }

            if (template == null)
                throw new NotSupportedException("Unsupported meeting state for body.");

            foreach (var property in meeting.GetType().GetProperties())
            {
                var value = property.GetValue(meeting, null);

                if (value == null)
                {
                    template = template.Replace("{{Property." + property.Name + "}}", "");
                    continue;
                }

                template = value switch
                {
                    DateTime dateTime => template.Replace("{{Property." + property.Name + "}}", dateTime.ToString(EmailConfiguration.BodyDateTimeFormat)),
                    _ => template.Replace("{{Property." + property.Name + "}}", value.ToString()),
                };
            }

            template = template.Replace("{{CreatedBy.FormatedName}}", meeting.CreatedBy.FormatName());
            template = template.Replace("{{CreatedBy.Email}}", meeting.CreatedBy.Email);
            template = template.Replace("{{FormatedAgenda}}", $"<ul>{string.Concat(meeting.Agenda.Select(o => $"<li>{o.Content}</li>"))}</ul>");

            return builder
                .Append(template)
                .Append(EmailTemplates.GeneratedFooter)
                .ToString();
        }
    }
}
