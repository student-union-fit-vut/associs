﻿using AssocIS.Data.Models.Meetings;
using AssocIS.Database.Services;
using System.Security.Claims;
using System.Threading.Tasks;
using AssocIS.Common.Extensions;
using AssocIS.Database.Enums;
using System;
using AssocIS.Data.Models.Common;
using System.Linq;
using AssocIS.Data.Models.Users;
using AssocIS.Data.Helpers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using AssocIS.Main.Models.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using AssocIS.Data.Infrastructure;

namespace AssocIS.Main.Services.Meetings
{
    [Service(ServiceLifetime.Scoped)]
    public class MeetingService
    {
        private IAssocISRepository Repository { get; }
        private MeetingNotificationService MeetingNotificationService { get; }
        private MeetingConfiguration MeetingConfiguration { get; }

        public MeetingService(IAssocISRepository repository, MeetingNotificationService meetingNotificationService, IOptionsSnapshot<MeetingConfiguration> options)
        {
            Repository = repository;
            MeetingNotificationService = meetingNotificationService;
            MeetingConfiguration = options.Value;
        }

        public async Task<Meeting> CreateMeetingAsync(UpdateMeetingParams meetingParams, ClaimsPrincipal loggedUser)
        {
            var loggedUserId = loggedUser.GetUserId();
            var entity = meetingParams.ToEntity(loggedUserId);

            await Repository.AddAsync(entity);
            await Repository.CommitAsync();

            if (entity.State == MeetingState.Summoned)
                return await NotifyMeetingAsync(entity.Id, loggedUser); // Notify and get meeting info

            return await GetMeetingAsync(entity.Id, loggedUser);
        }

        public async Task<Meeting> UpdateMeetingAsync(long id, UpdateMeetingParams meetingParams, ClaimsPrincipal loggedUser)
        {
            var entity = await Repository.Meeting.FindMeetingByIdAsync(id);

            if (entity == null)
                return null;

            var loggedUserId = loggedUser.GetUserId();
            var loggedUserEntity = await Repository.User.FindUserByIdAsync(loggedUserId);

            var oldState = entity.State;
            Repository.RemoveCollection(entity.Agenda);
            meetingParams.UpdateEntity(entity, loggedUserId);
            await Repository.CommitAsync();

            await MeetingNotificationService.NotifyAsync(entity, oldState, meetingParams.MeetingState);
            return new Meeting(entity, loggedUserEntity);
        }

        public async Task<PaginatedData<MeetingListItem>> GetMeetingsListAsync(MeetingSearchParams searchParams, ClaimsPrincipal loggedUser)
        {
            var query = Repository.Meeting.GetMeetingsQuery(searchParams.NameQuery, searchParams.From, searchParams.To, searchParams.AttendanceTypes,
                searchParams.MeetingStates, searchParams.AgendaQuery, searchParams.ReportQuery, searchParams.AuthorId, searchParams.MessageQuery);

            var loggedUserId = loggedUser.GetUserId();
            var loggedUserEntity = await Repository.User.FindUserByIdAsync(loggedUserId);

            return await PaginatedData<MeetingListItem>.CreateAsync(query, searchParams, entity => new MeetingListItem(entity, loggedUserEntity));
        }

        public async Task<Meeting> GetMeetingAsync(long id, ClaimsPrincipal loggedUser)
        {
            var meeting = await Repository.Meeting.FindMeetingByIdAsync(id);

            var loggedUserId = loggedUser.GetUserId();
            var loggedUserEntity = await Repository.User.FindUserByIdAsync(loggedUserId);
            return meeting == null ? null : new Meeting(meeting, loggedUserEntity);
        }

        public async Task<Meeting> NotifyMeetingAsync(long id, ClaimsPrincipal loggedUser)
        {
            var meeting = await Repository.Meeting.FindMeetingByIdAsync(id);

            if (meeting == null)
                return null;

            if (meeting.State != MeetingState.Summoned)
                throw new InvalidOperationException(meeting.Name); // Only summoned state can notify.

            await MeetingNotificationService.NotifyAsync(meeting, MeetingState.Drafted, MeetingState.Summoned);
            await Repository.CommitAsync();

            var loggedUserId = loggedUser.GetUserId();
            var loggedUserEntity = await Repository.User.FindUserByIdAsync(loggedUserId);
            return new Meeting(meeting, loggedUserEntity);
        }

        public async Task<Meeting> ApologizeFromMeetingAsync(long id, ClaimsPrincipal loggedUser)
        {
            var meeting = await Repository.Meeting.FindMeetingByIdAsync(id);

            if (meeting == null)
                return null;

            if (meeting.At <= DateTime.Now) // Meeting running.
                throw new InvalidOperationException(meeting.Name);

            var loggedUserId = loggedUser.GetUserId();
            var loggedUserEntity = await Repository.User.FindUserByIdAsync(loggedUserId);
            var presence = meeting.Presences.FirstOrDefault(o => o.UserId == loggedUserId);

            if (presence == null)
            {
                presence = new Database.Entity.MeetingPresence()
                {
                    PresenceType = MeetingPresenceType.Apologized,
                    UserId = loggedUserId
                };

                meeting.Presences.Add(presence);
            }
            else
            {
                if (presence.PresenceType == MeetingPresenceType.Apologized)
                    return new Meeting(meeting, loggedUserEntity); // Skip save process. User is apologized.

                presence.PresenceType = MeetingPresenceType.Apologized;
            }

            await Repository.CommitAsync();
            return new Meeting(meeting, loggedUserEntity);
        }

        public async Task<List<MeetingPresenceListItem>> GetMeetingPresenceAsync(long meetingId)
        {
            var meeting = await Repository.Meeting.FindMeetingByIdAsync(meetingId);

            if (meeting == null)
                return null;

            var states = Enum.GetValues<UserState>().Where(o => o != UserState.ActiveCollaborator).ToList();
            var usersQuery = Repository.User.GetUsersQuery(null, true, false, true, states);
            var users = await usersQuery.ToListAsync();

            return users.ConvertAll(o =>
            {
                var presence = meeting.Presences.FirstOrDefault(x => x.UserId == o.Id);

                return new MeetingPresenceListItem()
                {
                    Presence = MeetingPresenceHelper.CalculatePresence(meeting, presence, o),
                    User = new User(o)
                };
            });
        }

        public async Task<List<UserPresenceStatus>> GetPresenceReportAsync()
        {
            var states = Enum.GetValues<UserState>().Where(o => o != UserState.ActiveCollaborator).ToList();
            var users = await Repository.User.GetUsersQuery(null, true, false, true, states).AsNoTracking().ToListAsync();
            var meetings = await Repository.Meeting.GetReportableMeetingsQuery().AsNoTracking().ToListAsync();

            var result = users.Select(o =>
            {
                var reportItem = new UserPresenceStatus(o);

                foreach (var meeting in meetings)
                {
                    var userPresence = meeting.Presences.FirstOrDefault(x => x.UserId == o.Id);
                    var calculatedPresence = MeetingPresenceHelper.CalculatePresence(meeting, userPresence, o);

                    reportItem.AddPresence(calculatedPresence);
                }

                reportItem.IsWarn = reportItem.AbsentCount >= MeetingConfiguration.WarningAbsentCount;
                return reportItem;
            });

            return result.ToList();
        }

        public async Task<Meeting> SetMeetingPresence(long id, List<SetMeetingPresenceParams> presenceParams, ClaimsPrincipal loggedUser)
        {
            var meeting = await Repository.Meeting.FindMeetingByIdAsync(id);

            if (meeting == null)
                return null;

            foreach (var param in presenceParams)
            {
                var presence = meeting.Presences.FirstOrDefault(o => o.UserId == param.UserId);

                if (presence == null)
                {
                    presence = param.ToEntity();
                    meeting.Presences.Add(presence);
                }
                else
                {
                    presence.PresenceType = param.PresenceType;
                }
            }

            await Repository.CommitAsync();

            var loggedUserId = loggedUser.GetUserId();
            var loggedUserEntity = await Repository.User.FindUserByIdAsync(loggedUserId);
            return new Meeting(meeting, loggedUserEntity);
        }

        public async Task PairMeetingWithCalendarEvent(long id, string calendarEventId)
        {
            var meeting = await Repository.Meeting.FindMeetingByIdAsync(id);

            if (meeting == null)
                return;

            meeting.CalendarEventId = calendarEventId;
            await Repository.CommitAsync();
        }
    }
}
