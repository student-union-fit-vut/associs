﻿using AssocIS.Data.Infrastructure;
using AssocIS.Main.Models.Configuration;
using MailKit.Net.Smtp;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Services
{
    [Service(ServiceLifetime.Scoped)]
    public class EmailService
    {
        private EmailConfiguration EmailConfiguration { get; }
        private ILogger<EmailService> Logger { get; }

        public EmailService(IOptionsSnapshot<EmailConfiguration> options, ILogger<EmailService> logger)
        {
            EmailConfiguration = options.Value;
            Logger = logger;
        }

        public async Task SendAsync(MimeMessage message)
        {
            using var client = await CreateClientAsync();

            await client.SendAsync(message);
            await client.DisconnectAsync(true);
        }

        public async Task SendBatchAsync(IEnumerable<MimeMessage> messages)
        {
            using var client = await CreateClientAsync();

            foreach (var message in messages)
            {
                await SendEmailAsync(client, message);
            }

            await client.DisconnectAsync(true);
        }

        private async Task<SmtpClient> CreateClientAsync()
        {
            var smtp = new SmtpClient();

            await smtp.ConnectAsync(EmailConfiguration.Smtp.Address, EmailConfiguration.Smtp.Port);
            await smtp.AuthenticateAsync(EmailConfiguration.Smtp.Username, EmailConfiguration.Smtp.Password);

            return smtp;
        }

        private async Task SendEmailAsync(SmtpClient client, MimeMessage message)
        {
            var addressList = message.To.Select(o => o.ToString()).ToList();
            var senderList = message.From.Select(o => o.ToString()).ToList();

            Logger.LogInformation($"Sending email to {string.Join(", ", addressList)} from {string.Join(", ", senderList)} with subject {message.Subject}");
            await client.SendAsync(message);
        }
    }
}
