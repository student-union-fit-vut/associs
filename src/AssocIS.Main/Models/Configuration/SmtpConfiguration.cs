﻿namespace AssocIS.Main.Models.Configuration
{
    public class SmtpConfiguration
    {
        public string Address { get; set; }
        public ushort Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
