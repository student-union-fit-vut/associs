﻿using System;

namespace AssocIS.Main.Models.Configuration
{
    public class ResetPasswordConfiguration
    {
        public int MinimalLength { get; set; }
        public int MaximalLength { get; set; }

        public int GetRandBetweenValue()
        {
            return new Random().Next(MinimalLength, MaximalLength);
        }
    }
}
