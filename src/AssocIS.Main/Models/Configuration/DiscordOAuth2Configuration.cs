﻿namespace AssocIS.Main.Models.Configuration
{
    public class DiscordOAuth2Configuration : OAuth2ServiceConfiguration
    {
        public override string AuthorizeEndpoint => "https://discord.com/api/oauth2/authorize";
        public override string TokenEndpoint => "https://discord.com/api/oauth2/token";
        public override string UserInformationEndpoint => "https://discord.com/api/users/@me";
    }
}
