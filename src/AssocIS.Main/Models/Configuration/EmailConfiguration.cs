﻿using MimeKit;

namespace AssocIS.Main.Models.Configuration
{
    public class EmailConfiguration
    {
        public string SenderName { get; set; }
        public string SenderAddress { get; set; }
        public SmtpConfiguration Smtp { get; set; }
        public string SubjectDateTimeFormat { get; set; }
        public string BodyDateTimeFormat { get; set; }

        public EmailConfiguration()
        {
            Smtp = new SmtpConfiguration();
        }

        public MailboxAddress MailboxAddress => new MailboxAddress(SenderName, SenderAddress);
    }
}
