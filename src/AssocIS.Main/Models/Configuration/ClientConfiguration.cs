﻿using System.Collections.Generic;

namespace AssocIS.Main.Models.Configuration
{
    public class ClientConfiguration
    {
        public string ClientId { get; set; }
        public Dictionary<string, bool> Modules { get; set; }

        public ClientConfiguration()
        {
            Modules = new Dictionary<string, bool>();
        }
    }
}
