﻿using AssocIS.Main.Models.Configuration.External.Google;

namespace AssocIS.Main.Models.Configuration
{
    public class OAuth2Configuration
    {
        public DiscordOAuth2Configuration Discord { get; set; }
        public GoogleOAuth2Configuration Google { get; set; }
    }
}
