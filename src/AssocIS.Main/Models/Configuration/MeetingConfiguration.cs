﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Models.Configuration
{
    public class MeetingConfiguration
    {
        public int WarningAbsentCount { get; set; }
    }
}
