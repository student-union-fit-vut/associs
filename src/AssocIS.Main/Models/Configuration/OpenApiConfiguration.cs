﻿using System.Collections.Generic;

namespace AssocIS.Main.Models.Configuration
{
    public class OpenApiConfiguration
    {
        public string[] Servers { get; set; }
    }
}
