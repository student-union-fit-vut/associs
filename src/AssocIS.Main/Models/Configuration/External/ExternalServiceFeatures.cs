﻿namespace AssocIS.Main.Models.Configuration.External
{
    public class ExternalServiceFeatures
    {
        public bool Meetings { get; set; }
        public bool Teams { get; set; }
        public bool Members { get; set; }
        public bool Events { get; set; }
        public bool MembersSynchronization { get; set; }
        public bool Everyone { get; set; }
    }
}
