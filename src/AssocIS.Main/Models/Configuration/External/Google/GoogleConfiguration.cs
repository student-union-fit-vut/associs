﻿using Google.Apis.Auth.OAuth2;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AssocIS.Main.Models.Configuration.External.Google
{
    public class GoogleConfiguration
    {
        public string Domain { get; set; }
        public bool Enabled { get; set; }
        public ServiceAccountConfiguration ServiceAccountConfig { get; set; }
        public string DefaultCalendarId { get; set; }
        public string OrgUnit { get; set; }
        public ExternalServiceFeatures Features { get; set; }
        public string ServiceAccountUser { get; set; }
        public GroupsConfiguration Groups { get; set; }

        public GoogleCredential CreateCredential(List<string> scopes)
        {
            var json = JsonConvert.SerializeObject(ServiceAccountConfig);
            return GoogleCredential.FromJson(json).CreateScoped(scopes).CreateWithUser(ServiceAccountUser);
        }
    }
}
