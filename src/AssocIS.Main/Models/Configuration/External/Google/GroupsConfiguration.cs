﻿using System.Collections.Generic;
using System.Linq;

namespace AssocIS.Main.Models.Configuration.External.Google
{
    public class GroupsConfiguration
    {
        public string ActiveCollaborators { get; set; }
        public string Leaders { get; set; }
        public string ViceChairman { get; set; }
        public string Chairman { get; set; }
        public string TeamLeaders { get; set; }
        public string BasicMembers { get; set; }

        public IEnumerable<string> All => (new[] { ActiveCollaborators, Leaders, ViceChairman, Chairman, TeamLeaders, BasicMembers }).Distinct();
    }
}
