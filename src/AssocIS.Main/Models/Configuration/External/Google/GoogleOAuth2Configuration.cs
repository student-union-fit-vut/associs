﻿using Google.Apis.Auth.OAuth2;

namespace AssocIS.Main.Models.Configuration.External.Google
{
    public class GoogleOAuth2Configuration : OAuth2ServiceConfiguration
    {
        public ClientSecrets CreateSecrets()
        {
            return new ClientSecrets()
            {
                ClientSecret = ClientSecret,
                ClientId = ClientId
            };
        }
    }
}
