﻿using Discord;

namespace AssocIS.Main.Models.Configuration.External.Discord
{
    public class DiscordConfiguration
    {
        public string Token { get; set; }
        public ulong HomeGuildId { get; set; }
        public bool Enabled { get; set; }
        public ulong NotificationsChannelId { get; set; }
        public ulong TemporaryChannelsCategoryId { get; set; }
        public RoleGroupConfiguration RoleGroups { get; set; }
        public ExternalServiceFeatures Features { get; set; }
        public string DateTimeFormat { get; set; }
    }
}
