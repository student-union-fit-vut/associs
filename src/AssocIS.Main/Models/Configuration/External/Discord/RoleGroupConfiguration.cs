﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AssocIS.Main.Models.Configuration.External.Discord
{
    public class RoleGroupConfiguration
    {
        public ulong[] TeamLeaders { get; set; }
        public ulong[] Chairman { get; set; }
        public ulong[] ViceChairman { get; set; }
        public ulong[] EveryoneValid { get; set; }
        public ulong[] TestPeriod { get; set; }
        public ulong[] ExMemberLeader { get; set; }
        public ulong[] ExMemberBasic { get; set; }
        public ulong[] ActiveCollaborators { get; set; }
        public ulong[] ExMemberCollaborator { get; set; }

        public RoleGroupConfiguration()
        {
            TeamLeaders = Enumerable.Empty<ulong>().ToArray();
            Chairman = Enumerable.Empty<ulong>().ToArray();
            ViceChairman = Enumerable.Empty<ulong>().ToArray();
            EveryoneValid = Enumerable.Empty<ulong>().ToArray();
            TestPeriod = Enumerable.Empty<ulong>().ToArray();
            ExMemberLeader = Enumerable.Empty<ulong>().ToArray();
            ExMemberBasic = Enumerable.Empty<ulong>().ToArray();
            ActiveCollaborators = Enumerable.Empty<ulong>().ToArray();
            ExMemberCollaborator = Enumerable.Empty<ulong>().ToArray();
        }

        public IEnumerable<ulong> All => new[] { 
            TeamLeaders, Chairman, ViceChairman, EveryoneValid, EveryoneValid, TestPeriod, ExMemberLeader, ExMemberBasic ,
            ActiveCollaborators, ExMemberCollaborator
        }.SelectMany(o => o).Distinct();

        public IEnumerable<IRole> AsRoles(Func<RoleGroupConfiguration, ulong[]> groupSelector, IGuild guild)
        {
            return groupSelector(this).Distinct().Select(o => guild.GetRole(o)).Where(o => o != null);
        }
    }
}
