﻿using AssocIS.Common.Extensions;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace AssocIS.Main.Models.Configuration
{
    public class JwtConfiguration
    {
        private string _issuer;
        private string _audience;
        private string _secret;

        public string Issuer
        {
            get => _issuer.TryFillEnvironment();
            set => _issuer = value;
        }

        public string Audience
        {
            get => _audience.TryFillEnvironment();
            set => _audience = value;
        }

        public string Secret
        {
            get => _secret.TryFillEnvironment();
            set => _secret = value;
        }

        public int HoursExpiration { get; set; }

        public SymmetricSecurityKey GetSecret()
        {
            return string.IsNullOrEmpty(Secret) ? null : new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Secret));
        }
    }
}
