﻿using System.Linq;

namespace AssocIS.Main.Models.Configuration
{
    public class OAuth2ServiceConfiguration
    {
        public bool Disabled { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string RedirectUrl { get; set; }
        public string[] Scopes { get; set; }

        public virtual string AuthorizeEndpoint { get; } = "";
        public virtual string TokenEndpoint { get; } = "";
        public virtual string UserInformationEndpoint { get; } = "";

        public OAuth2ServiceConfiguration()
        {
            Scopes = Enumerable.Empty<string>().ToArray();
        }
    }
}
