﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Main.Models.Auth
{
    /// <summary>
    /// Authentification proces result codes.
    /// </summary>
    public enum AuthResultCodes
    {
        /// <summary>
        /// Success.
        /// </summary>
        OK,

        /// <summary>
        /// User not exists or invalid password.
        /// </summary>
        UserNotFoundOrInvalidPassword,

        /// <summary>
        /// Refresh token not exists or is expired.
        /// </summary>
        RefreshTokenNotFound,

        /// <summary>
        /// User is not exists or not paired with external account.
        /// </summary>
        UserNotFoundOrNotPaired
    }
}
