﻿using AssocIS.Data.Models.Auth;
using AssocIS.Data.Resources.Auth;

namespace AssocIS.Main.Models.Auth
{
    /// <summary>
    /// Authentification result.
    /// </summary>
    public class AuthResult
    {
        /// <summary>
        /// Result code of authentification process.
        /// </summary>
        public AuthResultCodes Code { get; set; }

        /// <summary>
        /// JWT token if process is success.
        /// </summary>
        public JwtToken Token { get; set; }

        public AuthResult(AuthResultCodes errorCode)
        {
            Code = errorCode;
        }

        public AuthResult(JwtToken token) : this(AuthResultCodes.OK)
        {
            Token = token;
        }

        public string GetCodeResourceName()
        {
            return Code switch
            {
                AuthResultCodes.UserNotFoundOrInvalidPassword => nameof(AuthResources.UnknownUserOrInvalidPassword),
                AuthResultCodes.RefreshTokenNotFound => nameof(AuthResources.RefreshTokenNotFound),
                AuthResultCodes.UserNotFoundOrNotPaired => nameof(AuthResources.UserNotFoundOrNotPaired),
                _ => null
            };
        }
    }
}
