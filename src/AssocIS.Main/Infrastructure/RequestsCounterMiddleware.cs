﻿using AssocIS.Main.Services.System;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace AssocIS.Main.Infrastructure
{
    public class RequestsCounterMiddleware
    {
        private RequestsCounterService Service { get; }
        private RequestDelegate Next { get; }

        public RequestsCounterMiddleware(RequestsCounterService service, RequestDelegate next)
        {
            Service = service;
            Next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await Next(context);
            }
            finally
            {
                Service.Increment();
            }
        }
    }
}
