﻿using AssocIS.Main.Services.Auth;
using System.Linq;
using System.Security.Claims;

namespace AssocIS.Main.Infrastructure.Auth
{
    public static class ClaimsPrincipalExtensions
    {
        public static bool IsInPolicy(this ClaimsPrincipal claimsPrincipal, string policyName)
        {
            if (!Policies.PolicyToRolesMapping.ContainsKey(policyName))
                return false;

            var mapping = Policies.PolicyToRolesMapping[policyName];
            return mapping.Any(o => claimsPrincipal.IsInRole(o));
        }
    }
}
