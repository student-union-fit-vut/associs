﻿using NJsonSchema;
using NSwag;
using NSwag.Generation.Processors;
using NSwag.Generation.Processors.Contexts;

namespace AssocIS.Main.Infrastructure.OpenApi
{
    public class CultureOperationProcessor : IOperationProcessor
    {
        public bool Process(OperationProcessorContext context)
        {
            context.OperationDescription.Operation.Parameters.Add(new OpenApiParameter()
            {
                Name = "Accept-Language",
                Kind = OpenApiParameterKind.Header,
                Type = JsonObjectType.String,
                IsRequired = false
            });

            return true;
        }
    }
}
