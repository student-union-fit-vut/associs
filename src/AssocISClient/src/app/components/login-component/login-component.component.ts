import { HttpStatusCodes } from './../../core/enums/http-status-codes';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoginParams } from './../../core/models/auth/login-params';
import { Nullable } from './../../core/models/index';
import { OAuth2Links } from './../../core/models/auth/oauth2/oauth2-links';
import { OAuth2Service } from './../../core/services/oauth2.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { EMPTY, throwError, Observable } from 'rxjs';
import { AppService } from 'src/app/core/services/app.service';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-login-component',
    templateUrl: './login-component.component.html',
    styleUrls: ['./login-component.component.scss']
})
export class LoginComponentComponent implements OnInit {
    public oAuth2Links: Nullable<OAuth2Links>;
    public form: FormGroup;
    loginErrorMessage: string | undefined;

    constructor(
        private oauthService: OAuth2Service,
        private formBuilder: FormBuilder,
        private router: Router,
        private authService: AuthService,
        public appService: AppService,
        private titleService: Title
    ) { }

    get username(): AbstractControl { return this.form.get('username'); }
    get password(): AbstractControl { return this.form.get('password'); }

    ngOnInit(): void {
        this.oauthService.getOAuth2Links().subscribe(links => this.oAuth2Links = links);
        this.titleService.setTitle('AssocIS | Login');

        this.form = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        const queryParams = new URLSearchParams(location.search);
        if (queryParams.has('sessionId')) {
            this.oauthService.loginViaOAuth2(queryParams.get('sessionId')).pipe(catchError((err: HttpErrorResponse) => {
                if (err.status === HttpStatusCodes.NotFound) {
                    this.router.navigate(['/login']);
                    return EMPTY;
                }

                return throwError(err);
            })).subscribe(_ => this.router.navigate(['/', 'app']));
        }
    }

    formSubmit(): void {
        const loginParams = new LoginParams(
            this.username.value as string,
            this.password.value as string,
            this.appService.clientConfiguration.clientId
        );

        this.authService.login(loginParams).pipe(
            catchError((err: HttpErrorResponse) => this.catchUnsuccessLogin(err))
        ).subscribe(_ => this.router.navigate(['/', 'app']));
    }

    catchUnsuccessLogin(err: HttpErrorResponse): Observable<never> {
        if (err.status === HttpStatusCodes.Forbidden) {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
            this.loginErrorMessage = err.error.message;
            return EMPTY;
        }

        return throwError(err);
    }
}
