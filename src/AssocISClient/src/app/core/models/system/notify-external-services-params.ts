import { EveryoneNotificationType } from './../../enums/everyone-notification-type';

export class NotifyExternalServicesParams {
    constructor(
        public clientType: EveryoneNotificationType,
        public content: string
    ) { }

    static create(data: any): NotifyExternalServicesParams {
        if (!data) { return null; }

        return new NotifyExternalServicesParams(
            data.clientType,
            data.content
        );
    }
}
