import { DateTime } from './../datetime';

export class DiagnosticData {
    public startAt: DateTime;
    public uptime: string;
    public cpuTime: string;
    public memoryUsed: number;
    public requestsCount: number;

    static create(data: any): DiagnosticData {
        if (!data) { return null; }
        const item = new DiagnosticData();

        item.cpuTime = data.cpuTime;
        item.memoryUsed = data.memoryUsed;
        item.requestsCount = data.requestsCount;
        item.startAt = DateTime.fromISOString(data.startAt);
        item.uptime = data.uptime;
        return item;
    }
}
