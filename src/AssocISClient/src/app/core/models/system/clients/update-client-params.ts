import { ClientType } from './../../../enums/client-type';

export class UpdateClientParams {
    constructor(
        public name: string,
        public description: string,
        public clientType: ClientType
    ) { }

    static create(data: any): UpdateClientParams {
        if (!data) { return null; }

        return new UpdateClientParams(data.name, data.description, data.clientType);
    }
}
