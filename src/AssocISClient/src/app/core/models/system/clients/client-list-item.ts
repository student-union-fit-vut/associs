import { Support } from './../../../lib/support';
import { ClientType, ClientTypeTexts } from './../../../enums/client-type';

export class ClientListItem {
    public id: number;
    public clientId: string;
    public name: string;
    public clientType: ClientType;
    public isUnused: boolean;

    get formatedClientType(): string {
        return ClientTypeTexts[Support.getEnumKeyByValue(ClientType, this.clientType) as string] as string;
    }

    static create(data: any): ClientListItem {
        if (!data) { return null; }

        const item = new ClientListItem();
        item.clientId = data.clientId;
        item.clientType = data.clientType;
        item.id = data.id;
        item.name = data.name;
        item.isUnused = data.isUnused ?? false;
        return item;
    }
}
