import { QueryString } from './../../common';

export class ClientsSearchParams {
    constructor(
        public nameQuery?: string
    ) { }

    static get empty(): ClientsSearchParams {
        return ClientsSearchParams.create({});
    }

    static create(data: any): ClientsSearchParams {
        if (!data) { return null; }

        return new ClientsSearchParams(data.nameQuery);
    }

    asQueryParams(): QueryString[] {
        return [
            this.nameQuery ? new QueryString('nameQuery', this.nameQuery) : null
        ].filter(o => o);
    }
}
