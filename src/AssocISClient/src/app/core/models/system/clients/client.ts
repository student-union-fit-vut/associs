import { DateTime } from './../../datetime';
import { ClientListItem } from './client-list-item';

export class Client extends ClientListItem {
    public description: string;
    public createdAt: DateTime;
    public lastAccess: DateTime;

    static create(data: any): Client {
        if (!data) { return null; }

        const client = new Client();
        Object.assign(client, super.create(data));
        client.createdAt = DateTime.fromISOString(data.createdAt);
        client.description = data.description;
        client.lastAccess = data.lastAccess ? DateTime.fromISOString(data.lastAccess) : null;
        return client;
    }
}
