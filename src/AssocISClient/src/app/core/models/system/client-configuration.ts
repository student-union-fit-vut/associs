export class ClientConfiguration {
    public clientId: string;
    public version: string;
    public modules: { [key: string]: boolean };
    public defaultLanguage: string;
    public discordEnabled: boolean;
    public googleEnabled: boolean;

    static create(data: any): ClientConfiguration {
        if (!data) { return null; }
        const item = new ClientConfiguration();

        item.clientId = data.clientId;
        item.defaultLanguage = data.defaultLanguage;
        item.modules = data.modules;
        item.version = data.version;
        item.discordEnabled = data.discordEnabled;
        item.googleEnabled = data.googleEnabled;
        return item;
    }
}
