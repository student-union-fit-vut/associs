import * as Moment from 'moment';

export type DateTimeCompareResult = 'now' | 'before' | 'after';

export class DateTime {
    constructor(public moment: Moment.Moment) { }

    static get now(): DateTime {
        return new DateTime(Moment());
    }

    get time(): number {
        return this.moment.unix();
    }

    static fromISOString(datetime: string): DateTime {
        const moment = Moment(datetime);

        if (!moment.isValid()) {
            throw new Error('Datetime is not valid ISO string.');
        }

        return new DateTime(moment);
    }

    compare(dateTime: DateTime): DateTimeCompareResult {
        if (this.moment.isSame(dateTime.moment)) { return 'now'; }
        return this.moment.isBefore(dateTime.moment) ? 'before' : 'after';
    }

    compareWithNow(): DateTimeCompareResult {
        return this.compare(DateTime.now);
    }

    toLocaleString(): string {
        return this.moment.locale('cs').format('L LTS');
    }

    addMonths(months: number): DateTime {
        this.moment.add(months, 'months');
        return this;
    }

    toISOString(keepOffset: boolean = true): string {
        return this.moment.toISOString(keepOffset);
    }

    toLocaleISOString(keepOffset: boolean = true): string {
        return this.moment.locale('cs').toISOString(keepOffset);
    }

    toUtcISOString(keepOffset: boolean = true): string {
        return this.moment.locale('cs').utc().toISOString(keepOffset);
    }
}
