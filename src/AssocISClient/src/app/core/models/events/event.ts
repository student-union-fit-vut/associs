import { LogData } from './../common';
import { EventListItem } from './event-list-item';

export class Event extends EventListItem {
    public description: string;
    public note: string;
    public discordChannelId: string;
    public calendarEventId: string;
    public logData: LogData;

    get haveCalendar(): boolean {
        return !!this.calendarEventId;
    }

    static create(data: any): Event | null {
        if (!data) { return null; }
        const event = new Event();
        const baseClass = super.create(data);

        Object.assign(event, baseClass);
        event.description = data.description;
        event.discordChannelId = data.discordChannelId;
        event.expectedCount = data.expectedCount;
        event.logData = LogData.create(data.logData);
        event.note = data.note;
        event.calendarEventId = data.calendarEventId;

        return event;
    }
}
