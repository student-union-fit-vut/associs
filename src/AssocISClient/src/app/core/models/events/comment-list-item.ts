import { DateTime } from './../datetime';
import { User } from '../users';

export class CommentListItem {
    public id: number;
    public createdBy: User;
    public createdAt: DateTime;
    public modifiedAt: DateTime;
    public text: string;

    static create(data: any): CommentListItem | null {
        if (!data) { return null; }

        const item = new CommentListItem();
        item.id = data.id;
        item.createdAt = DateTime.fromISOString(data.createdAt);
        item.createdBy = User.create(data.createdBy);
        item.modifiedAt = data.modifiedAt ? DateTime.fromISOString(data.modifiedAt) : null;
        item.text = data.text;

        return item;
    }
}
