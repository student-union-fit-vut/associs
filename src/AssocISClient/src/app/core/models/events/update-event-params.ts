export class UpdateEventParams {
    constructor(
        public name: string,
        public from: string,
        public to: string,
        public description: string,
        public note: string,
        public expectedCount: number,
        public realCount: number,
        public discordChannelId: string,
        public createDiscordChannel: boolean
    ) { }

    static create(data: any): UpdateEventParams {
        if (!data) { return null; }

        return new UpdateEventParams(data.name, data.from, data.to, data.description, data.note, data.expectedCount, data.realCount,
            data.discordChannelId, data.createDiscordChannel);
    }
}
