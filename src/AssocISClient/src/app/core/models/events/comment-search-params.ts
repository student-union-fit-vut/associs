import { QueryString } from '../common';

export class CommentSearchParams {
    constructor(
        public textQuery?: string,
        public createdById?: number
    ) { }

    static create(data: any): CommentSearchParams {
        if (!data) { return null; }

        return new CommentSearchParams(data.textQuery, data.createdById);
    }

    asQueryParams(): QueryString[] {
        return [
            this.textQuery ? new QueryString('textQuery', this.textQuery) : null,
            this.createdById ? new QueryString('createdById', this.createdById) : null
        ].filter(o => o);
    }
}
