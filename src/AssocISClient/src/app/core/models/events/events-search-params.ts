import { QueryString } from '../../models/common';

export class EventsSearchParams {
    constructor(
        public nameQuery?: string,
        public excludeFuture: boolean = false,
        public excludeOngoing: boolean = false,
        public excludePast: boolean = false
    ) { }

    static get empty(): EventsSearchParams {
        return this.create({});
    }

    static create(data: any): EventsSearchParams {
        if (!data) { return null; }

        return new EventsSearchParams(data.nameQuery, data.excludeFuture, data.excludeOngoing, data.excludePast);
    }

    asQueryParams(): QueryString[] {
        return [
            this.nameQuery ? new QueryString('nameQuery', this.nameQuery) : null,
            new QueryString('excludeFuture', this.excludeFuture),
            new QueryString('excludeOngoing', this.excludeOngoing),
            new QueryString('excludePast', this.excludePast)
        ].filter(o => o);
    }
}
