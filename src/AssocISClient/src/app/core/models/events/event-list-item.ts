import { DateTime } from './../datetime';

export class EventListItem {
    public id: number;
    public name: string;
    public from: DateTime;
    public to: DateTime;
    public expectedCount: number;
    public realCount: number;
    public canManage: boolean;

    static create(data: any): EventListItem | null {
        if (!data) { return null; }

        const item = new EventListItem();
        item.id = data.id;
        item.name = data.name;
        item.from = DateTime.fromISOString(data.from);
        item.to = DateTime.fromISOString(data.to);
        item.expectedCount = data.expectedCount;
        item.realCount = data.realCount;
        item.canManage = data.canManage;

        return item;
    }
}
