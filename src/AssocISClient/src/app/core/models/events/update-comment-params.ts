export class UpdateCommentParams {
    constructor(
        public text: string
    ) { }
}
