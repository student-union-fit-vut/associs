export class UpdateMeetingParams {
    constructor(
        public name: string,
        public at: string,
        public expectedEnd: string,
        public agenda: string[],
        public report: string,
        public attendanceType: number,
        public meetingState: number,
        public location: string,
        public message: string
    ) { }
}
