import { MeetingPresenceType } from './../../enums/meeting-presence-type';

export class SetPresenceParams {
    constructor(
        public userId: number,
        public presenceType: MeetingPresenceType
    ) { }
}
