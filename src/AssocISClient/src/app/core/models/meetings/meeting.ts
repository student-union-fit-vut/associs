import { LogData } from './../common';
import { DateTime } from './../datetime';
import { MeetingListItem } from './meeting-list-item';

export class Meeting extends MeetingListItem {
    public lastNotifiedAt: DateTime;
    public agenda: string[];
    public report: string;
    public logData: LogData;
    public location: string;
    public message: string;

    static create(data: any): Meeting {
        if (!data) { return null; }
        const baseClass = super.create(data);
        const meeting = new Meeting();

        Object.assign(meeting, baseClass);
        meeting.agenda = data.agenda;
        meeting.lastNotifiedAt = data.lastNotifiedAt ? DateTime.fromISOString(data.lastNotifiedAt) : null;
        meeting.location = data.location;
        meeting.logData = LogData.create(data.logData);
        meeting.message = data.message;
        meeting.report = data.report;
        return meeting;
    }
}
