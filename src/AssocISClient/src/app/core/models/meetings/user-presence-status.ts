import { User } from './../users/user';

export class UserPresenceStatus {
    public user: User;
    public unknownCount: number;
    public absentCount: number;
    public presentCount: number;
    public expectedCount: number;
    public apologizedCount: number;
    public isWarn: boolean;

    static create(data: any): UserPresenceStatus {
        if (!data) { return null; }

        const item = new UserPresenceStatus();

        item.user = User.create(data.user);
        item.unknownCount = data.unknownCount;
        item.absentCount = data.absentCount;
        item.apologizedCount = data.apologizedCount;
        item.expectedCount = data.expectedCount;
        item.isWarn = data.isWarn;
        item.presentCount = data.presentCount;
        return item;
    }
}
