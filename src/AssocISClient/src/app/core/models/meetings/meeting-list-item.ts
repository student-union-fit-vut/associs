import { Support } from 'src/app/core/lib/support';
import { MeetingPresenceType, MeetingPresenceTypeTexts } from './../../enums/meeting-presence-type';
import { MeetingState } from './../../enums/meeting-state';
import { MeetingAttendanceType, MeetingAttendanceTypeTexts } from './../../enums/meeting-attendance-type';
import { DateTime } from './../datetime';

export class MeetingListItem {
    public id: number;
    public name: string;
    public at: DateTime;
    public expectedEnd: DateTime;
    public attendanceType: MeetingAttendanceType = MeetingAttendanceType.Optional;
    public state: MeetingState = MeetingState.Drafted;
    public currentUserPresence: MeetingPresenceType;

    get formatedAttendance(): string {
        return MeetingAttendanceTypeTexts[Support.getEnumKeyByValue(MeetingAttendanceType, this.attendanceType) as string] as string;
    }

    get formatedPresence(): string {
        return MeetingPresenceTypeTexts[Support.getEnumKeyByValue(MeetingPresenceType, this.currentUserPresence) as string] as string;
    }

    get isDrafted(): boolean { return this.state === MeetingState.Drafted; }
    get isSummoned(): boolean { return this.state === MeetingState.Summoned; }
    get isCancelled(): boolean { return this.state === MeetingState.Cancelled; }

    get isExpected(): boolean { return this.currentUserPresence === MeetingPresenceType.Expected; }

    get isPresenceOk(): boolean | null {
        if (!this.isSummoned) { return null; }
        return this.currentUserPresence !== MeetingPresenceType.Absent;
    }

    get canApologize(): boolean {
        return this.isSummoned && this.currentUserPresence === MeetingPresenceType.Expected;
    }

    static create(data: any): MeetingListItem | null {
        if (!data) { return null; }

        const item = new MeetingListItem();

        item.id = data.id;
        item.at = DateTime.fromISOString(data.at);
        item.attendanceType = data.attendanceType;
        item.currentUserPresence = data.currentUserPresence;
        item.expectedEnd = DateTime.fromISOString(data.expectedEnd);
        item.name = data.name;
        item.state = data.state;
        return item;
    }
}
