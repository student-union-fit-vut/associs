import { QueryString } from './../common';
import { DateTime } from './../datetime';

export class MeetingSearchParams {
    constructor(
        public nameQuery?: string,
        public from?: DateTime,
        public to?: DateTime,
        public attendanceTypes?: number[],
        public meetingStates?: number[],
        public agendaQuery?: string,
        public reportQuery?: string,
        public messageQuery?: string,
        public authorId?: number
    ) { }

    static create(data: any): MeetingSearchParams {
        if (!data) { return null; }

        return new MeetingSearchParams(
            data.nameQuery,
            data.from ? DateTime.fromISOString(data.from) : null,
            data.to ? DateTime.fromISOString(data.to) : null,
            data.attendanceTypes,
            data.meetingStates,
            data.agendaQuery,
            data.messageQuery,
            data.authorId
        );
    }

    asQueryParams(): QueryString[] {
        const result = [
            this.nameQuery ? new QueryString('nameQuery', this.nameQuery) : null,
            this.from ? new QueryString('from', this.from.toUtcISOString()) : null,
            this.to ? new QueryString('to', this.to.toUtcISOString()) : null,
            this.agendaQuery ? new QueryString('agendaQuery', this.agendaQuery) : null,
            this.messageQuery ? new QueryString('messageQuery', this.messageQuery) : null,
            this.reportQuery ? new QueryString('reportQuery', this.reportQuery) : null,
            this.authorId ? new QueryString('authorId', this.authorId) : null
        ];

        if (this.attendanceTypes) {
            result.push(...this.attendanceTypes.map(o => new QueryString('attendanceTypes', o)));
        }

        if (this.meetingStates) {
            result.push(...this.meetingStates.map(o => new QueryString('meetingStates', o)));
        }

        return result.filter(o => o);
    }

    toInterface(): any {
        return {
            nameQuery: this.nameQuery,
            from: this.from?.toUtcISOString(),
            to: this.to?.toUtcISOString(),
            attendanceTypes: this.attendanceTypes,
            meetingStates: this.meetingStates,
            agendaQuery: this.agendaQuery,
            messageQuery: this.messageQuery,
            authorId: this.authorId
        };
    }
}
