import { Support } from '../../lib/support';
import { MeetingPresenceType, MeetingPresenceTypeTexts } from '../../enums/meeting-presence-type';
import { User } from '../users';

export class MeetingPresenceListItem {
    public user: User;
    public presence: MeetingPresenceType;

    get formatedPresence(): string {
        return MeetingPresenceTypeTexts[Support.getEnumKeyByValue(MeetingPresenceType, this.presence) as string] as string;
    }

    static create(data: any): MeetingPresenceListItem {
        if (!data) { return null; }

        const item = new MeetingPresenceListItem();

        item.user = User.create(data.user);
        item.presence = data.presence;
        return item;
    }
}
