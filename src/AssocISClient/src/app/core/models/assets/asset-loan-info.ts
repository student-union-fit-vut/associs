import { User } from '../users/user';
import { DateTime } from './../datetime';
import { Asset } from './asset';

export class AssetLoanInfo {
    public id: number;
    public asset: Asset;
    public loanedAt: DateTime;
    public returnedAt: DateTime;
    public daysLoaned: number;
    public user: User;

    static create(data: any): AssetLoanInfo {
        if (!data) { return null; }
        const item = new AssetLoanInfo();

        item.id = data.id;
        item.asset = Asset.create(data.asset);
        item.daysLoaned = data.daysLoaned;
        item.loanedAt = DateTime.fromISOString(data.loanedAt);
        item.returnedAt = data.returnedAt ? DateTime.fromISOString(data.returnedAt) : null;
        item.user = User.create(data.user);

        return item;
    }
}
