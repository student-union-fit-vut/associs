export class UpdateAssetCategoryParams {
    constructor(
        public name: string,
        public nameChanged: boolean,
        public icon: string
    ) { }
}
