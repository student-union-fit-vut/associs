export class InventoryAsset {
    constructor(
        public assetId: number,
        public count: number,
        public note: string
    ) { }
}
