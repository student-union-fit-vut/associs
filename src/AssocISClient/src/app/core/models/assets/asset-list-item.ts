import { Asset } from './asset';

export class AssetListItem extends Asset {
    public availableCount: number;

    get isAvailable(): boolean {
        return this.availableCount > 0;
    }

    static create(data: any): AssetListItem {
        if (!data) { return null; }

        const item = new AssetListItem();
        const base = super.create(data);

        Object.assign(item, base);
        item.availableCount = data.availableCount;

        return item;
    }
}
