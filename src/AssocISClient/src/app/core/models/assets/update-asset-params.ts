import { AssetFlags } from './../../enums/asset-flags';
export class UpdateAssetParams {
    constructor(
        public name: string,
        public description: string,
        public location: string,
        public categories: number[],
        public loansDisabled: boolean,
        public isRemoved: boolean
    ) { }

    static create(data: any): UpdateAssetParams {
        if (!data) { return null; }

        const loansDisabled = (data.flags & AssetFlags.LoansDisabled) !== 0;
        const isRemoved = (data.flags & AssetFlags.Removed) !== 0;

        return new UpdateAssetParams(data.name, data.description, data.location, data.categories,
            loansDisabled, isRemoved);
    }
}
