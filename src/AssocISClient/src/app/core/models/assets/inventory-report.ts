import { DateTime } from './../datetime';
import { User } from './../users/user';

export class InventoryReport {
    public id: number;
    public user: User;
    public at: DateTime;
    public note: string;
    public count: number;

    static create(data: any): InventoryReport {
        if (!data) { return null; }
        const report = new InventoryReport();

        report.id = data.id;
        report.user = User.create(data.user);
        report.at = DateTime.fromISOString(data.at);
        report.note = data.note;
        report.count = data.count;

        return report;
    }
}
