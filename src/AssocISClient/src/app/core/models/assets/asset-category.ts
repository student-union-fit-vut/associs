export class AssetCategory {
    public id: number;
    public name: string;
    public icon: string;

    static create(data: any): AssetCategory | null {
        if (!data) { return null; }
        const category = new AssetCategory();

        category.id = data.id;
        category.name = data.name;
        category.icon = data.icon;

        return category;
    }
}
