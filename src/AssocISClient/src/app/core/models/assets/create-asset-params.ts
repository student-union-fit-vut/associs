import { UpdateAssetParams } from './update-asset-params';

export class CreateAssetParams extends UpdateAssetParams {
    constructor(
        public name: string,
        public description: string,
        public location: string,
        public categories: number[],
        public loansDisabled: boolean,
        public isRemoved: boolean,
        public count: number
    ) {
        super(name, description, location, categories, loansDisabled, isRemoved);
    }

    static create(data: any): CreateAssetParams {
        if (!data) { return null; }

        const base = super.create(data);
        return new CreateAssetParams(base.name, base.description, base.location, base.categories,
            base.loansDisabled, base.isRemoved, data.count);
    }
}
