import { LogData } from '../common';
import { AssetListItem } from './asset-list-item';

export class AssetDetail extends AssetListItem {
    public description: string;
    public location: string;
    public totalCount: number;
    public flags: number;
    public logData: LogData;

    static create(data: any): AssetDetail {
        if (!data) { return null; }

        const base = super.create(data);
        const asset = new AssetDetail();

        Object.assign(asset, base);
        asset.description = data.description;
        asset.flags = data.flags;
        asset.location = data.location;
        asset.logData = LogData.create(data.logData);
        asset.totalCount = data.totalCount;

        return asset;
    }
}
