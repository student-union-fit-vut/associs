import { AssetCategory } from './asset-category';

export class Asset {
    public id: number;
    public name: string;
    public categories: AssetCategory[];

    get anyCategory(): boolean {
        return this.categories.some(o => o);
    }

    static create(data: any): Asset {
        if (!data) { return null; }
        const asset = new Asset();

        asset.id = data.id;
        asset.name = data.name;
        asset.categories = data.categories ? data.categories.map((o: any) => AssetCategory.create(o)) : null;

        return asset;
    }

    getTopCategories(count: number): AssetCategory[] {
        return this.categories.filter((_, i) => (i + 1) <= count);
    }
}
