import { QueryString } from '../common';

export class InventorySearchParams {
    constructor(
        public userId?: number
    ) { }

    static get empty(): InventorySearchParams { return InventorySearchParams.create({}); }

    static create(data: any): InventorySearchParams {
        if (!data) { return null; }

        return new InventorySearchParams(data.userId);
    }

    asQueryParams(): QueryString[] {
        return [
            this.userId ? new QueryString('userId', this.userId) : null
        ].filter(o => o);
    }
}
