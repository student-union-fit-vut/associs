import { QueryString } from './../common';

export class AssetSearchParams {
    constructor(
        public nameQuery?: string,
        public isAvailable?: boolean,
        public categories?: number[],
        public includeRemoved?: boolean
    ) { }

    static get empty(): AssetSearchParams { return AssetSearchParams.create({}); }

    static create(data: any): AssetSearchParams {
        if (!data) { return null; }

        return new AssetSearchParams(data.nameQuery, data.isAvailable, data.categories, data.includeRemoved);
    }

    asQueryParams(): QueryString[] {
        const items = [
            this.nameQuery ? new QueryString('nameQuery', this.nameQuery) : null,
            this.isAvailable != null ? new QueryString('isAvailable', this.isAvailable) : null,
            this.includeRemoved ? new QueryString('includeRemoved', this.includeRemoved) : null
        ];

        if (this.categories) {
            items.push(...this.categories.map(o => new QueryString('categories', o)));
        }

        return items.filter(o => o);
    }
}
