import { QueryString } from './../common';
export class LoanSearchParams {
    constructor(
        public onlyActive: boolean
    ) { }

    static get empty(): LoanSearchParams { return LoanSearchParams.create({}); }

    static create(data: any): LoanSearchParams {
        if (!data) { return null; }
        return new LoanSearchParams(data.onlyActive || false);
    }

    asQueryParams(): QueryString[] {
        return [
            new QueryString('onlyActive', this.onlyActive)
        ];
    }
}
