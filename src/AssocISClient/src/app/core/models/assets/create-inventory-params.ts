import { InventoryAsset } from './inventory-asset';

export class CreateInventoryParams {
    constructor(
        public userId: number,
        public at: string,
        public assets: InventoryAsset[]
    ) { }
}
