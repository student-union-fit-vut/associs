export class LoginParams {
    constructor(
        public username: string,
        public password: string,
        public clientId: string
    ) { }
}
