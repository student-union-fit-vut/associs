import { Permissions } from './../../enums/permissions';
import { Nullable } from './../index';
import { DateTime } from './../datetime';
import * as JwtDecode from 'jwt-decode';

export class JwtToken {
    public accessToken: Nullable<string>;
    public type: Nullable<string>;
    public expiresAt: Nullable<string>;
    public createdAt: Nullable<string>;
    public refreshToken: Nullable<string>;

    get expirationDatetime(): DateTime {
        return DateTime.fromISOString(this.expiresAt);
    }

    static fromInterface(data: any): JwtToken {
        if (!data) { return null; }
        const result = new JwtToken();

        result.accessToken = data.accessToken;
        result.createdAt = data.createdAt;
        result.expiresAt = data.expiresAt;
        result.refreshToken = data.refreshToken;
        result.type = data.type;
        return result;
    }
}

export class JwtTokenContent {
    public name: string;
    public surname: string;
    public email: string;
    public givenName: string;
    public id: number;
    public roles: string[];
    public issuer: string;
    public audience: string;

    constructor(accessToken: string) {
        const jwt = JwtDecode.default(accessToken) as any;

        this.name = jwt.name;
        this.surname = jwt.family_name;
        this.email = jwt.email;
        this.givenName = jwt.given_name;
        this.id = parseInt(jwt.nameid, 10);
        this.roles = Array.isArray(jwt.role) ? jwt.role : [jwt.role];
        this.issuer = jwt.iss;
        this.audience = jwt.aud;
    }

    get completeName(): string { return `${this.name} ${this.surname}`; }

    hasPermission(permission: Permissions): boolean {
        return permission === Permissions.Everyone || this.roles.some(o => o === Permissions.Chairperson || o === permission);
    }
}
