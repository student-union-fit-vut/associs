import { Nullable } from './../../index';

export class OAuth2Links {
    public discord: Nullable<string>;
    public google: Nullable<string>;

    static create(data: any): OAuth2Links | null {
        if (!data) { return null; }
        const links = new OAuth2Links();

        links.discord = data.discord;
        links.google = data.google;

        return links;
    }
}
