export class SupplierListItem {
    public id: number;
    public name: string;
    public canRemove: boolean;

    static create(data: any): SupplierListItem {
        if (!data) { return null; }

        const item = new SupplierListItem();
        item.id = data.id;
        item.name = data.name;
        item.canRemove = data.canRemove;
        return item;
    }
}
