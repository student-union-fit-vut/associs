import { QueryString } from './../../common';

export class SupplierSearchParams {
    constructor(
        public nameQuery?: string
    ) { }

    static get empty(): SupplierSearchParams {
        return SupplierSearchParams.create({});
    }

    static create(data: any): SupplierSearchParams | null {
        if (!data) { return null; }

        return new SupplierSearchParams(data.nameQuery);
    }

    asQueryParams(): QueryString[] {
        return [
            this.nameQuery ? new QueryString('nameQuery', this.nameQuery) : null
        ].filter(o => o);
    }
}
