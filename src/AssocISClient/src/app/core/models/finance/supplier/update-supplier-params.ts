export class UpdateSupplierParams {
    constructor(
        public name: string,
        public webLink: string,
        public contact: string,
        public note: string
    ) { }

    static create(data: any): UpdateSupplierParams {
        if (!data) { return null; }

        return new UpdateSupplierParams(data.name, data.webLink, data.contact, data.note);
    }
}
