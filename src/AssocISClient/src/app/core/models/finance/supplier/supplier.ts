import { LogData } from './../../common';
import { SupplierListItem } from './supplier-list-item';

export class Supplier extends SupplierListItem {
    public contact: string;
    public webLink: string;
    public note: string;
    public logData: LogData;

    static create(data: any): Supplier {
        if (!data) { return null; }
        const supplier = new Supplier();
        const baseClass = super.create(data);

        Object.assign(supplier, baseClass);
        supplier.contact = data.contact;
        supplier.logData = LogData.create(data.logData);
        supplier.note = data.note;
        supplier.webLink = data.webLink;
        return supplier;
    }
}
