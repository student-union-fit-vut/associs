export class UpdateCategoryParams {
    constructor(
        public name: string,
        public nameChanged: boolean,
        public icon: string
    ) { }
}
