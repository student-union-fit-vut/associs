export class TransactionCategory {
    public id: number;
    public name: string;
    public icon: string;
    public transactionCount: number;
    public originalName: string;

    get nameChanged(): boolean { return this.name !== this.originalName; }

    static create(data: any): TransactionCategory | null {
        if (!data) { return null; }

        const item = new TransactionCategory();

        item.id = data.id;
        item.icon = data.icon;
        item.name = data.name;
        item.transactionCount = data.transactionCount;
        item.originalName = data.name;
        return item;
    }
}
