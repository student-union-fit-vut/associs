import { Support } from './../../../lib/support';
import { LogData } from './../../common';
import { TransactionAttachmentType, TransactionAttachmentTypeTexts } from './../../../enums/transaction-attachment-type';

export class Attachment {
    public id: number;
    public name: string;
    public type: TransactionAttachmentType;
    public logData: LogData;

    get formatedType(): string {
        return TransactionAttachmentTypeTexts[Support.getEnumKeyByValue(TransactionAttachmentType, this.type) as string] as string;
    }

    static create(data: any): Attachment {
        if (!data) { return null; }

        const attachment = new Attachment();
        attachment.id = data.id;
        attachment.name = data.name;
        attachment.type = data.type;
        attachment.logData = LogData.create(data.logData);
        return attachment;
    }
}
