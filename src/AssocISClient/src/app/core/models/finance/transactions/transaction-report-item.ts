export class TransactionReportItem<TReport> {
    public groupValue: TReport;
    public count: number;
    public amount: number;

    // eslint-disable-next-line no-shadow
    static create<TReport>(data: any, groupValueMapper: (_: any) => TReport): TransactionReportItem<TReport> {
        if (!data) { return null; }

        const item = new TransactionReportItem<TReport>();

        item.amount = data.amount;
        item.count = data.count;
        item.groupValue = groupValueMapper(data.groupValue);
        return item;
    }
}

export type ReportTypes = 'type' | 'supplier' | 'category';
