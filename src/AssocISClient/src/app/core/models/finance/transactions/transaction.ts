import { LogData } from './../../common';
import { Attachment } from './attachment';
import { TransactionListItem } from './transaction-list-item';

export class Transaction extends TransactionListItem {
    public note: string;
    public attachments: Attachment[];
    public logData: LogData;

    static create(data: any): Transaction {
        if (!data) { return null; }
        const baseClass = super.create(data);
        const transaction = new Transaction();

        Object.assign(transaction, baseClass);
        transaction.attachments = data.attachments.map((o: Attachment) => Attachment.create(o));
        transaction.logData = LogData.create(data.logData);
        transaction.name = data.name;
        transaction.note = data.note;
        return transaction;
    }
}
