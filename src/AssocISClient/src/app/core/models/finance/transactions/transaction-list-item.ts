import { TransactionType } from './../../../enums/transaction-type';
import { TransactionCategory } from './../category/transaction-category';
import { DateTime } from './../../datetime';
import { SupplierListItem } from './../supplier/supplier-list-item';
import { AccountListItem } from './../account/account-list-item';

export class TransactionListItem {
    public id: number;
    public name: string;
    public account: AccountListItem;
    public supplier: SupplierListItem;
    public amount: number;
    public type: number;
    public processedAt: DateTime;
    public categories: TransactionCategory[];
    public canManage: boolean;

    get isIncoming(): boolean { return this.type === TransactionType.Incoming; }
    get isOutgoing(): boolean { return this.type === TransactionType.Outgoing; }

    static create(data: any): TransactionListItem | null {
        if (!data) { return null; }

        const item = new TransactionListItem();
        item.id = data.id;
        item.name = data.name;
        item.account = AccountListItem.create(data.account);
        item.supplier = SupplierListItem.create(data.supplier);
        item.amount = data.amount;
        item.type = data.type;
        item.processedAt = DateTime.fromISOString(data.processedAt);
        item.categories = data.categories?.map((o: any) => TransactionCategory.create(o));
        item.canManage = data.canManage;
        return item;
    }

    getTopCategories(count: number): TransactionCategory[] {
        return this.categories.filter((_, i) => (i + 1) <= count);
    }
}
