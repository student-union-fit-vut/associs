import { TransactionType } from './../../../enums/transaction-type';

export class UpdateTransactionParams {
    constructor(
        public processedAt: string,
        public amount: number,
        public type: TransactionType,
        public name: string,
        public note: string,
        public accountId: number,
        public supplierId: number,
        public categories: number[]
    ) { }
}
