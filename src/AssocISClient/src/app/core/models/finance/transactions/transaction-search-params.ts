import { TransactionType } from 'src/app/core/enums';
import { QueryString } from '../../common';
import { DateTime } from './../../datetime';

export class TransactionSearchParams {
    constructor(
        public processedFrom?: DateTime,
        public processedTo?: DateTime,
        public amountFrom?: number,
        public amountTo?: number,
        public transactionTypes?: TransactionType[],
        public nameQuery?: string,
        public accounts?: number[],
        public suppliers?: number[],
        public categories?: number[],
        public onlyWithAttachments?: boolean
    ) { }

    static create(data: any): TransactionSearchParams {
        if (!data) { return null; }

        return new TransactionSearchParams(
            data.processedFrom ? DateTime.fromISOString(data.processedFrom) : null,
            data.processedTo ? DateTime.fromISOString(data.processedTo) : null,
            data.amountFrom,
            data.amountTo,
            data.transactionTypes,
            data.nameQuery,
            data.accounts,
            data.suppliers,
            data.categories,
            data.onlyWithAttachments
        );
    }

    asQueryParams(): QueryString[] {
        const params = [
            this.processedFrom ? new QueryString('processedFrom', this.processedFrom.toUtcISOString()) : null,
            this.processedTo ? new QueryString('processedTo', this.processedTo.toUtcISOString()) : null,
            this.amountFrom ? new QueryString('amountFrom', this.amountFrom) : null,
            this.amountTo ? new QueryString('amountTo', this.amountTo) : null,
            this.nameQuery ? new QueryString('nameQuery', this.nameQuery) : null,
            new QueryString('onlyWithAttachments', this.onlyWithAttachments)
        ];

        if (this.transactionTypes) {
            params.push(...this.transactionTypes.map(o => new QueryString('transactionTypes', o)));
        }

        if (this.accounts) {
            params.push(...this.accounts.map(o => new QueryString('accountIds', o)));
        }

        if (this.suppliers) {
            params.push(...this.suppliers.map(o => new QueryString('supplierIds', o)));
        }

        if (this.categories) {
            params.push(...this.categories.map(o => new QueryString('categoryIds', o)));
        }

        return params.filter(o => o);
    }

    toInterface(): any {
        return {
            processedFrom: this.processedFrom?.toISOString(),
            processedTo: this.processedTo?.toISOString(),
            amountFrom: this.amountFrom,
            amountTo: this.amountTo,
            transactionTypes: this.transactionTypes,
            nameQuery: this.nameQuery,
            accounts: this.accounts,
            suppliers: this.suppliers,
            categories: this.categories,
            onlyWithAttachments: this.onlyWithAttachments
        };
    }
}
