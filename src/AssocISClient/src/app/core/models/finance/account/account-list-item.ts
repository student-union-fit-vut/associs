export class AccountListItem {
    public id: number;
    public name: string;
    public accountNumber: string;
    public bankCode: string;
    public canRemove: boolean;

    static create(data: any): AccountListItem {
        if (!data) { return null; }

        const item = new AccountListItem();

        item.id = data.id;
        item.name = data.name;
        item.canRemove = data.canRemove;
        item.bankCode = data.bankCode;
        item.accountNumber = data.accountNumber;

        return item;
    }
}
