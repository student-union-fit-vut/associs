export class UpdateAccountParams {
    constructor(
        public name: string,
        public note: string,
        public accountNumber: string,
        public bankCode: string
    ) { }

    static create(data: any): UpdateAccountParams {
        if (!data) { return null; }

        return new UpdateAccountParams(data.name, data.note, data.accountNumber, data.bankCode);
    }
}
