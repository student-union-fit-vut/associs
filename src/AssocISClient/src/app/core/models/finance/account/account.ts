import { LogData } from './../../common';
import { AccountListItem } from './account-list-item';

export class Account extends AccountListItem {
    public transactionCount: number;
    public accountState: number;
    public logData: LogData;
    public note: string;

    static create(data: any): Account {
        if (!data) { return null; }
        const baseClass = super.create(data);
        const account = new Account();

        Object.assign(account, baseClass);
        account.accountState = data.accountState;
        account.logData = LogData.create(data.logData);
        account.note = data.note;
        account.transactionCount = data.transactionCount;

        return account;
    }
}
