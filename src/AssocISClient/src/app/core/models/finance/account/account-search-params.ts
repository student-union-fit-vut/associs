import { QueryString } from './../../common';

export class AccountSearchParams {
    constructor(
        public nameQuery?: string,
        public bankCode?: string,
        public accountNumberQuery?: string
    ) { }

    static get empty(): AccountSearchParams {
        return AccountSearchParams.create({});
    }

    static create(data: any): AccountSearchParams {
        if (!data) { return null; }

        return new AccountSearchParams(
            data.nameQuery,
            data.bankCode,
            data.accountNumberQuery
        );
    }

    asQueryParams(): QueryString[] {
        return [
            this.nameQuery ? new QueryString('nameQuery', this.nameQuery) : null,
            this.bankCode ? new QueryString('bankCode', this.bankCode) : null,
            this.accountNumberQuery ? new QueryString('accountNumberQuery', this.accountNumberQuery) : null
        ].filter(o => o);
    }

    toInterface(): any {
        return {
            nameQuery: this.nameQuery,
            bankCode: this.bankCode,
            accountNumberQuery: this.accountNumberQuery
        };
    }
}
