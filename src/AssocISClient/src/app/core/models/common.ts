import { User } from './users/user';
import { DateTime } from './datetime';

export type Nullable<T> = T | null | undefined;

export class QueryString {
    constructor(
        public key: string,
        public value: string | number | boolean | null | undefined
    ) { }

    toString(): string {
        if (this.value == null || this.value === undefined) { return ''; }
        return `${encodeURIComponent(this.key)}=${encodeURIComponent(this.value.toString())}`;
    }
}

export class LogData {
    public createdAt: DateTime;
    public createdBy: User;
    public lastModifiedAt: DateTime;
    public lastModifiedBy: User;

    static create(data: any): LogData {
        const logData = new LogData();

        logData.createdAt = DateTime.fromISOString(data.createdAt);
        logData.createdBy = User.create(data.createdBy);
        logData.lastModifiedAt = data.lastModifiedAt ? DateTime.fromISOString(data.lastModifiedAt) : null;
        logData.lastModifiedBy = data.lastModifiedBy ? User.create(data.lastModifiedBy) : null;
        return logData;
    }
}

export interface KeyValuePair<TKey, TValue> {
    key: TKey;
    value: TValue;
}

export type Dictionary<TKey, TValue> = KeyValuePair<TKey, TValue>[];
export type Tuple<T1, T2, T3, T4, T5, T6> = [T1, T2, T3, T4, T5, T6];
export type OnChangeType = (value: any) => void;
export type OnTouchedType = () => void;
