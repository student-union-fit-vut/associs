import { UserState, UserStateTexts } from '../../enums';
import { Support } from '../../lib/support';

export class User {
    public id: number;
    public username: string;
    public name: string;
    public surname: string;
    public anyLoggedIn: boolean;
    public email: string;
    public isAnonymized: boolean;
    public userState: number;

    get fullName(): string { return `${this.name} ${this.surname}`; }

    get anyWarning(): boolean {
        return !this.anyLoggedIn;
    }

    get formattedState(): string {
        return UserStateTexts[Support.getEnumKeyByValue(UserState, this.userState) as string] as string;
    }

    static create(data: any): User | null {
        if (!data) { return null; }
        const user = new User();

        user.id = data.id;
        user.name = data.name;
        user.username = data.username;
        user.surname = data.surname;
        user.anyLoggedIn = data.anyLoggedIn;
        user.email = data.email;
        user.isAnonymized = data.isAnonymized;
        user.userState = data.userState;
        return user;
    }
}

