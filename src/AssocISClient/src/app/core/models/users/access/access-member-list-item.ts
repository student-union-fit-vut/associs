import { User } from './../user';

export class AccessMemberListItem {
    public user: User;
    public allowed: boolean;
    public note: string;

    static create(data: any): AccessMemberListItem {
        if (!data) { return null; }
        const item = new AccessMemberListItem();

        item.user = User.create(data.user);
        item.allowed = data.allowed;
        item.note = data.note;
        return item;
    }
}
