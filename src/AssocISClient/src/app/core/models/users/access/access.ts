import { LogData } from './../../common';
import { AccessListItem } from './access-list-item';

export class Access extends AccessListItem {
    public logData: LogData;

    static create(data: any): Access {
        if (!data) { return null; }
        const baseClass = super.create(data);
        const item = new Access();

        Object.assign(item, baseClass);
        item.logData = LogData.create(data.logData);
        return item;
    }
}
