export class SetAccessMemberParams {
    constructor(
        public userId: number,
        public allowed: boolean,
        public note: string
    ) { }
}
