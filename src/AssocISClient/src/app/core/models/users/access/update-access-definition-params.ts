import { AccessType } from './../../../enums/access-type';

export class UpdateAccessDefinitionParams {
    constructor(
        public type: AccessType,
        public description: string
    ) { }
}
