import { Support } from 'src/app/core/lib/support';
import { AccessType, AccessTypeTexts } from './../../../enums/access-type';

export class AccessListItem {
    public id: number;
    public type: AccessType;
    public description: string;

    get typeName(): string {
        return AccessTypeTexts[Support.getEnumKeyByValue(AccessType, this.type) as string] as string;
    }

    static create(data: any): AccessListItem {
        if (!data) { return null; }
        const item = new AccessListItem();

        item.id = data.id;
        item.type = data.type;
        item.description = data.description;
        return item;
    }
}
