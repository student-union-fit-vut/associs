import { LogData } from './../../common';
import { AccessMemberListItem } from './access-member-list-item';

export class AccessMember extends AccessMemberListItem {
    public logData: LogData;

    static create(data: any): AccessMember {
        if (!data) { return null; }
        const baseClass = super.create(data);
        const item = new AccessMember();

        Object.assign(item, baseClass);
        item.logData = LogData.create(data.logData);
        return item;
    }
}
