import { QueryString } from '../../common';
import { AccessType } from './../../../enums/access-type';

export class AccessDefinitionSearchParams {
    constructor(
        public type?: AccessType,
        public descriptionContains?: string,
        public createdById?: number
    ) { }

    static create(data: any): AccessDefinitionSearchParams {
        if (!data) { return null; }

        return new AccessDefinitionSearchParams(
            data.type ? parseInt(data.userId, 10) as AccessType : null,
            data.descriptionContains,
            data.createdById ? parseInt(data.createdById, 10) : null
        );
    }

    asQueryParams(): QueryString[] {
        return [
            this.type ? new QueryString('type', this.type) : null,
            this.descriptionContains ? new QueryString('descriptionContains', this.descriptionContains) : null,
            this.createdById ? new QueryString('createdById', this.createdById) : null
        ].filter(o => o);
    }

    toInterface(): any {
        return {
            type: this.type,
            descriptionContains: this.descriptionContains,
            createdById: this.createdById
        };
    }
}
