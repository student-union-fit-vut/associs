import { QueryString } from '../../common';
import { DateTime } from './../../datetime';

export class InterviewSearchParams {
    constructor(
        public userId?: number,
        public leaderUserId?: number,
        public fromDate?: DateTime,
        public toDate?: DateTime
    ) { }

    static create(data: any): InterviewSearchParams {
        if (!data) { return null; }

        return new InterviewSearchParams(
            data.userId ? parseInt(data.userId, 10) : null,
            data.leaderUserId ? parseInt(data.leaderUserId, 10) : null,
            data.fromDate ? DateTime.fromISOString(data.fromDate) : null,
            data.toDate ? DateTime.fromISOString(data.toDate) : null
        );
    }

    asQueryParams(): QueryString[] {
        return [
            this.userId ? new QueryString('userId', this.userId) : null,
            this.leaderUserId ? new QueryString('leaderUserId', this.leaderUserId) : null,
            this.fromDate ? new QueryString('from', this.fromDate.toUtcISOString()) : null,
            this.toDate ? new QueryString('to', this.toDate.toUtcISOString()) : null
        ].filter(o => o);
    }

    toInterface(): any {
        return {
            userId: this.userId,
            leaderUserId: this.leaderUserId,
            fromDate: this.fromDate?.toISOString(),
            toDate: this.toDate?.toISOString()
        };
    }
}
