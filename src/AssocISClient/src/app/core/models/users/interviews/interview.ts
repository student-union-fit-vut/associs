import { InterviewListItem } from './interview-list-item';

export class Interview extends InterviewListItem {
    public note: string;

    static create(data: any): Interview {
        if (!data) { return null; }
        const item = new Interview();

        Object.assign(item, super.create(data));
        item.note = data.note;
        return item;
    }
}
