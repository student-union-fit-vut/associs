import { DateTime } from './../../datetime';

export class UpdateInterviewParams {
    constructor(
        public userId: number,
        public leaderId: number,
        public at: DateTime,
        public note: string
    ) { }

    toInterface(): any {
        return {
            userId: this.userId,
            leaderId: this.leaderId,
            at: this.at.toUtcISOString(),
            note: this.note
        };
    }
}
