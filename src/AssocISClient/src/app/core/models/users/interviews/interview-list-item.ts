import { DateTime } from './../../datetime';
import { User } from './../user';

export class InterviewListItem {
    public id: number;
    public user: User = null;
    public leader: User = null;
    public at: DateTime;

    static create(data: any): InterviewListItem {
        if (!data) { return null; }

        const item = new InterviewListItem();

        item.at = data.at ? DateTime.fromISOString(data.at) : null;
        item.id = data.id;
        item.leader = User.create(data.leader);
        item.user = User.create(data.user);
        return item;
    }
}
