import { User } from './user';

export class UserInfo extends User {
    public discordId: string;

    static create(data: any): UserInfo | null {
        if (!data) { return null; }
        const userInfo = new UserInfo();

        Object.assign(userInfo, super.create(data));
        userInfo.discordId = data.discordId;
        return userInfo;
    }
}

