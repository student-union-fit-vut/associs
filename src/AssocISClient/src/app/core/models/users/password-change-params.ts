export class PasswordChangeParams {
    constructor(
        public oldPassword: string,
        public newPassword: string,
        public confirmPassword: string
    ) { }
}
