import { UserState } from '../../enums';
import { QueryString } from './../common';

export class UserFilter {
    public nameQuery: string;
    public ignoreLeftUsers = false;
    public ignoreTestPeriodUsers = false;
    public ignoreAnonymized = true;
    public states: UserState[] = [];

    static fromInterface(data: any): UserFilter {
        if (!data) { return null; }
        const result = new UserFilter();

        result.ignoreLeftUsers = data.ignoreLeftUsers;
        result.ignoreTestPeriodUsers = data.ignoreTestPeriodUsers;
        result.nameQuery = data.nameQuery;
        result.ignoreAnonymized = data.ignoreAnonymized;
        result.states = data.states;
        return result;
    }

    asQueryParams(): QueryString[] {
        return [
            new QueryString('nameQuery', this.nameQuery),
            new QueryString('ignoreLeftUsers', this.ignoreLeftUsers),
            new QueryString('ignoreTestPeriodUsers', this.ignoreTestPeriodUsers),
            new QueryString('ignoreAnonymized', this.ignoreAnonymized),
            ...(this.states ? this.states.map(o => new QueryString('states', o)) : [])
        ];
    }

    toInterface(): any {
        return {
            nameQuery: this.nameQuery,
            ignoreLeftUsers: this.ignoreLeftUsers,
            ignoreTestPeriodUsers: this.ignoreTestPeriodUsers,
            ignoreAnonymized: this.ignoreAnonymized,
            states: this.states
        };
    }
}
