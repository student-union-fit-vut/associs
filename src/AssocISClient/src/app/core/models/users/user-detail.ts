import { UserInfo } from './user-info';
import { LogData } from './../common';
import { DateTime } from './../datetime';
import { TeamMembership } from '../teams/team-membership';

export class UserDetail extends UserInfo {
    public phoneNumber: string;
    public joinedAt: DateTime;
    public leavedAt: DateTime;
    public studyProgram: string;
    public haveProfilePicture: boolean;
    public testPeriodEnd: DateTime;
    public globalNote: string;
    public userNote: string;
    public logData: LogData;
    public havePassword: boolean;
    public permissions: number;
    public teams: TeamMembership[];
    public googlePrimaryEmail: string;
    public lastLogin: DateTime;

    get canAnonymize(): boolean {
        return !this.isAnonymized && this.leavedAt != null;
    }

    static create(data: any): UserDetail {
        const user = new UserDetail();

        Object.assign(user, UserInfo.create(data));
        user.globalNote = data.globalNote;
        user.googlePrimaryEmail = data.googlePrimaryEmail;
        user.havePassword = data.havePassword;
        user.haveProfilePicture = data.haveProfilePicture;
        user.joinedAt = DateTime.fromISOString(data.joinedAt);
        user.leavedAt = data.leavedAt ? DateTime.fromISOString(data.leavedAt) : null;
        user.logData = LogData.create(data.logData);
        user.permissions = data.permissions;
        user.phoneNumber = data.phoneNumber;
        user.studyProgram = data.studyProgram;
        user.surname = data.surname;
        user.teams = data.teams.map((o: any) => TeamMembership.create(o));
        user.testPeriodEnd = data.testPeriodEnd ? DateTime.fromISOString(data.testPeriodEnd) : null;
        user.userNote = data.userNote;
        user.lastLogin = data.lastLogin ? DateTime.fromISOString(data.lastLogin) : null;
        return user;
    }
}
