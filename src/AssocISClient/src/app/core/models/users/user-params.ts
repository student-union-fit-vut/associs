export class UserParams {
    constructor(
        public username: string,
        public usernameChanged: boolean,
        public name: string,
        public surname: string,
        public email: string,
        public phoneNumber: string,
        public joinedAt: string,
        public studyProgram: string,
        public testPeriodEnd: string,
        public userState: number,
        public leavedAt: string,
        public permissions: number,
        public globalNote: string,
        public discordId: string,
        public googlePrimaryEmail: string,
        public createGSuiteFromEmail: boolean
    ) { }
}
