import { UserDetail } from './user-detail';

export class ResetPasswordResult {
    public user: UserDetail;
    public hashedPassword: string;

    get rawPassword(): string {
        return atob(this.hashedPassword);
    }

    static create(data: any): ResetPasswordResult {
        const result = new ResetPasswordResult();

        result.hashedPassword = data.hashedPassword;
        result.user = UserDetail.create(data.user);
        return result;
    }
}
