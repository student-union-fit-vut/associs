export class UpdateTeamParams {
    constructor(
        public name: string,
        public nameChanged: boolean,
        public description: string,
        public discordRoleId: string,
        public createDiscordRole: boolean,
        public leaderId: number,
        public googleGroups: string[]
    ) { }
}
