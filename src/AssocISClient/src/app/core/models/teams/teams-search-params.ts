import { QueryString } from '../common';

export class TeamsSearchParams {
    constructor(
        public nameQuery?: string,
        public onlyEmptyTeams?: boolean
    ) { }

    static create(data: any): TeamsSearchParams {
        if (!data) { return null; }

        return new TeamsSearchParams(
            data.nameQuery,
            data.onlyEmptyTeams
        );
    }

    asQueryParams(): QueryString[] {
        return [
            this.nameQuery ? new QueryString('nameQuery', this.nameQuery) : null,
            new QueryString('onlyEmptyTeams', this.onlyEmptyTeams)
        ].filter(o => o);
    }

    toInterface(): any {
        return {
            nameQuery: this.nameQuery,
            onlyEmptyTeams: this.onlyEmptyTeams
        };
    }
}
