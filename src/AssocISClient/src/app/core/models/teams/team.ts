import { DateTime } from './../datetime';
import { TeamListItem } from './team-list-item';

export class Team extends TeamListItem {
    public description: string;
    public createdAt: DateTime;
    public discordRoleId: string;
    public googleGroups: string[];

    static create(data: any): Team {
        if (!data) { return null; }
        const baseClass = super.create(data);
        const team = new Team();

        Object.assign(team, baseClass);
        team.createdAt = DateTime.fromISOString(data.createdAt);
        team.description = data.description;
        team.discordRoleId = data.discordRoleId;
        team.googleGroups = data.googleGroups.map((o: string) => o);

        return team;
    }
}
