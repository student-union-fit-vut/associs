import { UserInfo } from '../users';

export class TeamListItem {
    public id: number;
    public name: string;
    public leader: UserInfo;
    public membersCount: number;
    public canRemove: boolean;

    static create(data: any): TeamListItem {
        if (!data) { return null; }
        const item = new TeamListItem();

        item.id = data.id;
        item.leader = UserInfo.create(data.leader);
        item.membersCount = data.membersCount;
        item.name = data.name;
        item.canRemove = data.canRemove;
        return item;
    }
}
