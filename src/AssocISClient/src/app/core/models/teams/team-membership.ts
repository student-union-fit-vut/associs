export class TeamMembership {
    public id: number;
    public name: string;
    public isLeader: boolean;
    public teamDiscordId: string;

    static create(data: any): TeamMembership {
        if (!data) { return null; }
        const membership = new TeamMembership();

        membership.name = data.name;
        membership.isLeader = data.isLeader;
        membership.id = data.id;
        membership.teamDiscordId = data.teamDiscordId;
        return membership;
    }
}
