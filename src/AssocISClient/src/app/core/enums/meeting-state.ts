export enum MeetingState {
    Drafted = 0,
    Summoned = 1,
    Cancelled = 2
}

export enum MeetingStateTexts {
    Drafted = 'Vytvořeno',
    Summoned = 'Svoláno',
    Cancelled = 'Zrušeno'
}
