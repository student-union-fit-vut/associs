export enum AssetFlags {
    LoansDisabled = 1,
    Removed = 2
}

export enum AssetFlagsTexts {
    LoansDisabled = 'Výpůjčky deaktivovány',
    Removed = 'Smazáno'
}
