export enum UserState {
    BasicMember = 0,
    HonoraryMember = 1,
    Chairman = 2,
    ViceChairman = 3,
    VerifiedMember = 4,
    ActiveCollaborator = 5
}

export enum UserStateTexts {
    BasicMember = 'Řádný člen',
    HonoraryMember = 'Čestný člen',
    Chairman = 'Předseda',
    ViceChairman = 'Místopředseda',
    VerifiedMember = 'Ověřený člen',
    ActiveCollaborator = 'Aktivní spolupracovník'
}
