export enum TransactionType {
    Incoming = 0,
    Outgoing = 1
}

export enum TransactionTypeTexts {
    Incoming = 'Příchozí transakce',
    Outgoing = 'Odchozí transakce'
}
