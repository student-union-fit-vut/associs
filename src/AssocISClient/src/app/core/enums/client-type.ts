export enum ClientType {
    ThirdPartyApplication = 0,
    WebClient = 1
}

export enum ClientTypeTexts {
    ThirdPartyApplication = 'Aplikace třetích stran',
    WebClient = 'Webový klient'
}
