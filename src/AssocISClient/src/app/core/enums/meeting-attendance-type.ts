export enum MeetingAttendanceType {
    Required = 0,
    Optional = 1,
    RequiredForExpected = 2
}

export enum MeetingAttendanceTypeTexts {
    Required = 'Účast povinná',
    Optional = 'Účast nepovinná',
    RequiredForExpected = 'Účast povinná pro očekávané'
}
