export enum TransactionAttachmentType {
    Invoice
}

export enum TransactionAttachmentTypeTexts {
    Invoice = 'Daňový doklad/faktura'
}
