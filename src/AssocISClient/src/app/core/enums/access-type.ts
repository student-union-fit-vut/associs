export enum AccessType {
    Area,
    Facebook,
    Google
}

export enum AccessTypeTexts {
    Area = 'Prostory',
    Facebook = 'Facebook',
    Google = 'Google'
}
