export enum Permissions {
    TeamLeader = 'team_leader',
    Chairperson = 'chairperson',
    ViceChairperson = 'vice_chairperson',
    UserManager = 'user',
    TodoListsManager = 'todo',
    EventManager = 'event',
    MeetingManager = 'meeting',
    PropertyManager = 'property',
    FinanceManager = 'finance',
    WorkItemsManager = 'work-items',
    JobsManager = 'jobs',
    AppManager = 'app',
    Everyone = '*',
    Disabled = '-',
    BasicMember = 'basic'
}

export enum PermissionsMask {
    None = 0,
    UserManager = 1,
    // TodoListsManager = 2,
    EventManager = 4,
    MeetingManager = 8,
    PropertyManager = 16,
    FinanceManager = 32,
    // WorkItemsManager = 64,
    // JobsManager = 128,
    AppManager = 256
}

export enum PermissionsMaskTexts {
    UserManager = 'Správce členů',
    TodoListsManager = 'Správce TODO listů',
    EventManager = 'Správce akcí',
    MeetingManager = 'Správce schůzí',
    PropertyManager = 'Správce majetku',
    FinanceManager = 'Správce financí',
    WorkItemsManager = 'Správce úkolů',
    JobsManager = 'Správce volných pozic',
    AppManager = 'Správce aplikace'
}
