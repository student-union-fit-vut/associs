export enum MeetingPresenceType {
    Unknown = 0,
    Absent = 1,
    Present = 2,
    Expected = 3,
    Apologized = 4,
    Optional = 5
}

export enum MeetingPresenceTypeTexts {
    Unknown = '-',
    Absent = 'Nepřítomen',
    Present = 'Přítomen',
    Expected = 'Očekáván',
    Apologized = 'Omluven',
    Optional = 'Neočekáván'
}
