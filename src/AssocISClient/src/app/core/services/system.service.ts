import { PaginatedData } from './../../shared/components/data-list/models';
import { catchError, map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import { Client, ClientListItem, ClientsSearchParams, DiagnosticData, NotifyExternalServicesParams, UpdateClientParams } from '../models';
import { PaginationParams } from 'src/app/shared/components/data-list/models';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class SystemService {
    constructor(
        private base: BaseService
    ) { }

    notifyExternalServices(params: NotifyExternalServicesParams): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/system/notifications/external`;

        return this.base.http.post(url, params, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getClientsList(params: ClientsSearchParams, pagination: PaginationParams): Observable<PaginatedData<ClientListItem>> {
        const queryParams = [
            ...params.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');
        const url = `${environment.apiUrl}/api/v1/auth/clients?${queryParams}`;

        return this.base.http.get<PaginatedData<ClientListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => ClientListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    regenerateClientId(id: number): Observable<Client> {
        const url = `${environment.apiUrl}/api/v1/auth/clients/${id}/regenerate`;

        return this.base.http.put<Client>(url, {}, { headers: this.base.httpHeaders }).pipe(
            map(data => Client.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeClient(id: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/auth/clients/${id}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getClient(id: number): Observable<Client> {
        const url = `${environment.apiUrl}/api/v1/auth/clients/${id}`;

        return this.base.http.get<Client>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Client.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateClient(id: number, params: UpdateClientParams): Observable<Client> {
        const url = `${environment.apiUrl}/api/v1/auth/clients/${id}`;

        return this.base.http.put<Client>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Client.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createClient(params: UpdateClientParams): Observable<Client> {
        const url = `${environment.apiUrl}/api/v1/auth/clients`;

        return this.base.http.post<Client>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Client.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getDiagnosticData(): Observable<DiagnosticData> {
        const url = `${environment.apiUrl}/api/v1/system/diag`;

        return this.base.http.get<DiagnosticData>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => DiagnosticData.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
