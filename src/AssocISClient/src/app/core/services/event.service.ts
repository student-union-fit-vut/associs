import { CommentSearchParams } from './../models/events/comment-search-params';
import { UpdateCommentParams } from './../models/events/update-comment-params';
import { map, catchError } from 'rxjs/operators';
import { EventListItem } from './../models/events/event-list-item';
import { PaginatedData } from './../../shared/components/data-list/models';
import { environment } from './../../../environments/environment';
import { PaginationParams } from '../../shared/components/data-list';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import { CommentListItem, Event, EventsSearchParams, UpdateEventParams } from '../models';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class EventService {
    constructor(
        private base: BaseService
    ) { }

    getEventsList(params: EventsSearchParams, pagination: PaginationParams): Observable<PaginatedData<EventListItem>> {
        const queryParams = [
            ...params.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');

        const url = `${environment.apiUrl}/api/v1/event?${queryParams}`;

        return this.base.http.get<PaginatedData<EventListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => EventListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createEvent(params: UpdateEventParams): Observable<Event> {
        const url = `${environment.apiUrl}/api/v1/event`;

        return this.base.http.post<Event>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Event.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getEvent(id: number): Observable<Event> {
        const url = `${environment.apiUrl}/api/v1/event/${id}`;

        return this.base.http.get<Event>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Event.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateEvent(id: number, params: UpdateEventParams): Observable<Event> {
        const url = `${environment.apiUrl}/api/v1/event/${id}`;

        return this.base.http.put<Event>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Event.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeEvent(id: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/event/${id}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getComments(eventId: number, filter: CommentSearchParams, pagination: PaginationParams): Observable<PaginatedData<CommentListItem>> {
        const queryParams = [
            ...filter.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');
        const url = `${environment.apiUrl}/api/v1/event/${eventId}/comments?${queryParams}`;

        return this.base.http.get<PaginatedData<CommentListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => CommentListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createComment(eventId: number, params: UpdateCommentParams): Observable<CommentListItem> {
        const url = `${environment.apiUrl}/api/v1/event/${eventId}/comment`;

        return this.base.http.post<CommentListItem>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => CommentListItem.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateComment(eventId: number, commentId: number, params: UpdateCommentParams): Observable<CommentListItem> {
        const url = `${environment.apiUrl}/api/v1/event/${eventId}/comment/${commentId}`;

        return this.base.http.put<CommentListItem>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => CommentListItem.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeComment(eventId: number, commentId: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/event/${eventId}/comment/${commentId}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
