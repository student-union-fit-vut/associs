import { QueryString } from 'src/app/core/models';
import { Dictionary } from './../models/common';
import { catchError, map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { Observable, of } from 'rxjs';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import { AppService } from './app.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class DataService {
    constructor(
        private base: BaseService,
        private appService: AppService
    ) { }

    getUserSelectList(): Observable<Dictionary<number, string>> {
        const url = `${environment.apiUrl}/api/v1/data/users`;

        return this.base.http.get<Dictionary<number, string>>(url, { headers: this.base.httpHeaders }).pipe(
            map(o => Object.keys(o).map(x => ({ key: parseInt(x, 10), value: o[x] as string }))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getAssetsSelectList(): Observable<Dictionary<number, string>> {
        const url = `${environment.apiUrl}/api/v1/data/assets`;

        return this.base.http.get<Dictionary<number, string>>(url, { headers: this.base.httpHeaders }).pipe(
            map(o => Object.keys(o).map(x => ({ key: parseInt(x, 10), value: o[x] as string }))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getDiscordUsersSelectList(): Observable<Dictionary<string, string>> {
        if (!this.appService.clientConfiguration.discordEnabled) { return of(null as Dictionary<string, string>); }
        const url = `${environment.apiUrl}/api/v1/data/users/discord`;

        return this.base.http.get<Dictionary<string, string>>(url, { headers: this.base.httpHeaders }).pipe(
            map(o => Object.keys(o).map(x => ({ key: x, value: o[x] as string }))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getRolesSelectList(): Observable<Dictionary<string, string>> {
        if (!this.appService.clientConfiguration.discordEnabled) { return of(null as Dictionary<string, string>); }
        const url = `${environment.apiUrl}/api/v1/data/roles/discord`;

        return this.base.http.get<Dictionary<string, string>>(url, { headers: this.base.httpHeaders }).pipe(
            map(o => Object.keys(o).map(x => ({ key: x, value: o[x] as string }))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getTextChannelsInDiscordHomeGuildAsync(): Observable<Dictionary<string, string>> {
        if (!this.appService.clientConfiguration.discordEnabled) { return of(null as Dictionary<string, string>); }
        const url = `${environment.apiUrl}/api/v1/data/channels/discord`;

        return this.base.http.get<Dictionary<string, string>>(url, { headers: this.base.httpHeaders }).pipe(
            map(o => Object.keys(o).map(x => ({ key: x, value: o[x] as string }))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getTeamsSelectList(ignoredUsers: number[] | null = null): Observable<Dictionary<number, string>> {
        const query = ignoredUsers ? ignoredUsers.map(o => new QueryString('ignoredUsers', o.toString())) : null;
        const url = `${environment.apiUrl}/api/v1/data/teams?${query?.map(o => o.toString()).join('&')}`;

        return this.base.http.get<Dictionary<number, string>>(url, { headers: this.base.httpHeaders }).pipe(
            map(o => Object.keys(o).map(x => ({ key: parseInt(x, 10), value: o[x] as string }))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
