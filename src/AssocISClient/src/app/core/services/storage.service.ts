import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StorageService {
    read<TItem>(key: string): TItem | null {
        const data = localStorage.getItem(key);
        return data ? JSON.parse(data) as TItem : null;
    }

    remove(key: string): void {
        localStorage.removeItem(key);
    }

    save<TItem>(key: string, item: TItem): void {
        localStorage.setItem(key, JSON.stringify(item));
    }
}
