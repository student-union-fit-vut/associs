import { catchError, map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import { QueryString } from '../models';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ValidationsService {
    constructor(
        private base: BaseService
    ) { }

    usernameExists(username: string): Observable<boolean> {
        const usernameParam = new QueryString('username', username);
        const url = `${environment.apiUrl}/api/v1/validations/username/exists?${usernameParam.toString()}`;

        return this.base.http.get<boolean>(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    validatePhoneNumber(phoneNumber: string): Observable<boolean> {
        const param = new QueryString('phoneNumber', phoneNumber);
        const url = `${environment.apiUrl}/api/v1/validations/phoneNumber?${param.toString()}`;

        return this.base.http.get<boolean>(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    userIdExists(id: number): Observable<boolean> {
        const url = `${environment.apiUrl}/api/v1/validations/user/${id}/exists`;

        return this.base.http.get<boolean>(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    teamNameExists(name: string): Observable<boolean> {
        const url = `${environment.apiUrl}/api/v1/validations/team/exists?name=${name}`;

        return this.base.http.get<boolean>(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    assetCategoryNameExists(): AsyncValidatorFn {
        return (control: AbstractControl) => {
            const value = control.value as string;
            const url = `${environment.apiUrl}/api/v1/validations/asset/category/exists?name=${value}`;

            return this.base.http.get<boolean>(url, { headers: this.base.httpHeaders }).pipe(
                map((result: boolean) => result ? { exists: true } : null)
            );
        };
    }

    transactionCategoryNameExists(): AsyncValidatorFn {
        return (control: AbstractControl) => {
            const value = control.value as string;
            const url = `${environment.apiUrl}/api/v1/validations/finance/category/exists?name=${value}`;

            return this.base.http.get<boolean>(url, { headers: this.base.httpHeaders }).pipe(
                map((result: boolean) => result ? { exists: true } : null)
            );
        };
    }
}
