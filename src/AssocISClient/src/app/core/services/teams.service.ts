import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { PaginatedData, PaginationParams } from './../../shared/components/data-list/models';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import { Team, TeamListItem, TeamsSearchParams, UpdateTeamParams, User } from '../models';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class TeamsService {
    constructor(
        private base: BaseService
    ) { }

    getTeamsList(filter: TeamsSearchParams, pagination: PaginationParams): Observable<PaginatedData<TeamListItem>> {
        const queryParams = [
            ...filter.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');

        const url = `${environment.apiUrl}/api/v1/team?${queryParams}`;

        return this.base.http.get<PaginatedData<TeamListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => TeamListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeTeam(id: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/team/${id}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getTeamDetail(id: number): Observable<Team> {
        const url = `${environment.apiUrl}/api/v1/team/${id}`;

        return this.base.http.get<Team>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Team.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createTeam(params: UpdateTeamParams): Observable<Team> {
        const url = `${environment.apiUrl}/api/v1/team`;

        return this.base.http.post<Team>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Team.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateTeam(id: number, params: UpdateTeamParams): Observable<Team> {
        const url = `${environment.apiUrl}/api/v1/team/${id}`;

        return this.base.http.put<Team>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Team.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getTeamMembersList(teamId: number, pagination: PaginationParams): Observable<PaginatedData<User>> {
        const url = `${environment.apiUrl}/api/v1/team/${teamId}/members?${pagination.toQueryString()}`;

        return this.base.http.get<PaginatedData<User>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => User.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    addTeamMember(teamId: number, userId: number): Observable<Team> {
        const url = `${environment.apiUrl}/api/v1/team/${teamId}/members/${userId}`;

        return this.base.http.put<Team>(url, {}, { headers: this.base.httpHeaders }).pipe(
            map(data => Team.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeTeamMember(teamId: number, userId: number): Observable<Team> {
        const url = `${environment.apiUrl}/api/v1/team/${teamId}/members/${userId}`;

        return this.base.http.delete<Team>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Team.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
