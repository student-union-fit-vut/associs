import { HttpErrorResponse } from '@angular/common/http';
import { AccessMemberListItem } from './../models/users/access/access-member-list-item';
import { UpdateAccessDefinitionParams } from './../models/users/access/update-access-definition-params';
import { catchError, map } from 'rxjs/operators';
import { AccessListItem } from './../models/users/access/access-list-item';
import { PaginatedData } from './../../shared/components/data-list/models';
import { environment } from './../../../environments/environment';
import { PaginationParams } from 'src/app/shared/components/data-list/models';
import { AccessDefinitionSearchParams } from './../models/users/access/access-definition-search-params';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import { Access, AccessMember, SetAccessMemberParams } from '../models';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AccessService {
    constructor(
        private base: BaseService
    ) { }

    getAccessList(filter: AccessDefinitionSearchParams, pagination: PaginationParams): Observable<PaginatedData<AccessListItem>> {
        const params = [
            ...filter.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');

        const url = `${environment.apiUrl}/api/v1/access?${params}`;

        return this.base.http.get<PaginatedData<AccessListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => AccessListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeAccessDefinition(id: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/access/${id}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getAccessDefinition(id: number): Observable<Access> {
        const url = `${environment.apiUrl}/api/v1/access/${id}`;

        return this.base.http.get<Access>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Access.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createAccessDefinition(params: UpdateAccessDefinitionParams): Observable<Access> {
        const url = `${environment.apiUrl}/api/v1/access`;

        return this.base.http.post<Access>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Access.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateAccessDefinition(id: number, params: UpdateAccessDefinitionParams): Observable<Access> {
        const url = `${environment.apiUrl}/api/v1/access/${id}`;

        return this.base.http.put<Access>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Access.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getAccessMembers(accessId: number, pagination: PaginationParams): Observable<PaginatedData<AccessMemberListItem>> {
        const paginationQuery = pagination.asQueryParams().map(o => o.toString()).join('&');
        const url = `${environment.apiUrl}/api/v1/access/${accessId}/members?${paginationQuery}`;

        return this.base.http.get<PaginatedData<AccessMemberListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => AccessMemberListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    setAccessMember(accessId: number, params: SetAccessMemberParams): Observable<AccessMember> {
        const url = `${environment.apiUrl}/api/v1/access/${accessId}/members`;

        return this.base.http.post<AccessMember>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => AccessMember.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getAccessMember(accessId: number, userId: number): Observable<AccessMember> {
        const url = `${environment.apiUrl}/api/v1/access/${accessId}/members/${userId}`;

        return this.base.http.get<AccessMember>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => AccessMember.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
