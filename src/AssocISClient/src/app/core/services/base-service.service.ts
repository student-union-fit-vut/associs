import { StorageService } from './storage.service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, throwError, Observable } from 'rxjs';
import { JwtToken } from '../models';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { HttpStatusCodes } from '../enums';
import { Support } from '../lib/support';
import { ValidationErrorsModalComponent } from 'src/app/shared/components/modal';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class BaseService {
    constructor(
        private storage: StorageService,
        private modalService: ModalService,
        private router: Router,
        public http: HttpClient
    ) { }

    get httpHeaders(): HttpHeaders | { [header: string]: string | string[] } {
        const token = JwtToken.fromInterface(this.storage.read<JwtToken>('auth'));
        // eslint-disable-next-line @typescript-eslint/naming-convention
        return { Authorization: `${token.type} ${token.accessToken}` };
    }

    catchError(err: HttpErrorResponse, suppressModal: boolean = false): Observable<never> {
        /* eslint-disable @typescript-eslint/no-unsafe-member-access,
            @typescript-eslint/no-unsafe-argument, @typescript-eslint/restrict-template-expressions,
            @typescript-eslint/no-unsafe-call, @typescript-eslint/restrict-plus-operands
        */
        if (err.status === HttpStatusCodes.BadRequest && err.error?.errors) {
            const modal = this.modalService.showCustomModal<ValidationErrorsModalComponent>(ValidationErrorsModalComponent, 'lg');
            modal.componentInstance.errors = Support.flattern(Object.values(err.error.errors)) as string[];
            return EMPTY;
        } else if (err.status === HttpStatusCodes.Unauthorized) {
            this.storage.remove('auth');
            this.router.navigate(['/', 'login']);
            return EMPTY;
        } else if (err.status !== HttpStatusCodes.OK) {
            let message = 'Při vykonání požadavku došlo k neočekávané chybě.<br>';

            if (err.status > 0) {
                if (err.error.message) {
                    message += `<p>${err.error.message}</p>`;
                } else if (err.error.errors) {
                    message += `<ul class="mt-3">${err.error.errors.map(o => '<li>' + o + '</li>').join('')}</ul>`;
                }
            } else {
                message += `<p>${err.message}</p>`;
            }

            if (!suppressModal) {
                this.modalService.showNotification('Chyba požadavku', message, 'lg');
            }
        }

        return throwError(err);
    }
}
