import { HttpErrorResponse } from '@angular/common/http';
import { MeetingState } from './../enums/meeting-state';
import { map, catchError } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { PaginationParams, PaginatedData } from './../../shared/components/data-list/models';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import {
    Meeting, MeetingListItem, MeetingPresenceListItem, MeetingSearchParams,
    SetPresenceParams, UpdateMeetingParams, UserPresenceStatus
} from '../models';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MeetingService {
    constructor(
        private base: BaseService
    ) { }

    getMeetingList(filter: MeetingSearchParams, pagination: PaginationParams): Observable<PaginatedData<MeetingListItem>> {
        const params = [
            ...filter.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');

        const url = `${environment.apiUrl}/api/v1/meeting?${params}`;

        return this.base.http.get<PaginatedData<MeetingListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => MeetingListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    notifyMeeting(id: number): Observable<Meeting> {
        const url = `${environment.apiUrl}/api/v1/meeting/${id}/notify`;

        return this.base.http.post<Meeting>(url, null, { headers: this.base.httpHeaders }).pipe(
            map(data => Meeting.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    apologizeFromMeeting(id: number): Observable<Meeting> {
        const url = `${environment.apiUrl}/api/v1/meeting/${id}/apologize`;

        return this.base.http.put<Meeting>(url, null, { headers: this.base.httpHeaders }).pipe(
            map(data => Meeting.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getMeeting(id: number): Observable<Meeting> {
        const url = `${environment.apiUrl}/api/v1/meeting/${id}`;

        return this.base.http.get<Meeting>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Meeting.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createMeeting(params: UpdateMeetingParams): Observable<Meeting> {
        const url = `${environment.apiUrl}/api/v1/meeting`;

        return this.base.http.post<Meeting>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Meeting.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateMeeting(id: number, params: UpdateMeetingParams): Observable<Meeting> {
        const url = `${environment.apiUrl}/api/v1/meeting/${id}`;

        return this.base.http.put<Meeting>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Meeting.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    willNotifyMeeting(oldState: MeetingState, newState: MeetingState): boolean {
        if (oldState === MeetingState.Drafted && newState === MeetingState.Summoned) { return true; }
        if (oldState === MeetingState.Summoned && newState === MeetingState.Cancelled) { return true; }
        if (oldState === MeetingState.Cancelled && newState === MeetingState.Cancelled) { return true; }

        return false;
    }

    getMeetingPresenceAsync(id: number): Observable<MeetingPresenceListItem[]> {
        const url = `${environment.apiUrl}/api/v1/meeting/${id}/presence`;

        return this.base.http.get<MeetingPresenceListItem[]>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => data.map(o => MeetingPresenceListItem.create(o))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    setMeetingPresence(meetingId: number, params: SetPresenceParams[]): Observable<Meeting> {
        const url = `${environment.apiUrl}/api/v1/meeting/${meetingId}/presence`;

        return this.base.http.put<Meeting>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Meeting.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getMeetingReport(): Observable<UserPresenceStatus[]> {
        const url = `${environment.apiUrl}/api/v1/meeting/report/presence`;

        return this.base.http.get<UserPresenceStatus[]>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => data.map(o => UserPresenceStatus.create(o))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
