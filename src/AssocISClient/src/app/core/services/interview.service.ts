import { Interview } from './../models/users/interviews/interview';
import { catchError, map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { PaginatedData, PaginationParams } from './../../shared/components/data-list/models';
import { Observable } from 'rxjs';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import { InterviewListItem, InterviewSearchParams, UpdateInterviewParams } from '../models';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class InterviewService {
    constructor(
        private base: BaseService
    ) { }

    getInterviewList(filter: InterviewSearchParams, pagination: PaginationParams): Observable<PaginatedData<InterviewListItem>> {
        const params = [
            ...filter.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');

        const url = `${environment.apiUrl}/api/v1/interview?${params}`;

        return this.base.http.get<PaginatedData<InterviewListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(o => PaginatedData.create<InterviewListItem>(o, entity => InterviewListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getInterview(id: number): Observable<Interview> {
        const url = `${environment.apiUrl}/api/v1/interview/${id}`;

        return this.base.http.get<Interview>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Interview.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeInterview(id: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/interview/${id}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createInterview(params: UpdateInterviewParams): Observable<Interview> {
        const url = `${environment.apiUrl}/api/v1/interview`;

        return this.base.http.post<Interview>(url, params.toInterface(), { headers: this.base.httpHeaders }).pipe(
            map(data => Interview.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateInterview(id: number, params: UpdateInterviewParams): Observable<Interview> {
        const url = `${environment.apiUrl}/api/v1/interview/${id}`;

        return this.base.http.put<Interview>(url, params.toInterface(), { headers: this.base.httpHeaders }).pipe(
            map(data => Interview.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
