import { JwtToken } from './../models/auth/jwt-token';
import { StorageService } from './storage.service';
import { environment } from './../../../environments/environment';
import { OAuth2Links } from './../models/auth/oauth2/oauth2-links';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class OAuth2Service {
    constructor(
        private base: BaseService,
        private storage: StorageService
    ) { }

    getOAuth2Links(): Observable<OAuth2Links> {
        const url = `${environment.apiUrl}/api/v1/auth/oauth2/links?clientRedirectUrl=${location.href}`;

        return this.base.http.get<OAuth2Links>(url).pipe(
            map((o: OAuth2Links) => OAuth2Links.create(o)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    loginViaOAuth2(sessionId: string): Observable<JwtToken> {
        const url = `${environment.apiUrl}/api/v1/auth/oauth2/token?sessionId=${sessionId}`;

        return this.base.http.get(url).pipe(
            map(o => JwtToken.fromInterface(o)),
            tap(token => this.storage.save('auth', token)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
