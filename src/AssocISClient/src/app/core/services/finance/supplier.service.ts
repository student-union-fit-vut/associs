import { UpdateSupplierParams } from './../../models/finance/supplier/update-supplier-params';
import { BaseService } from './../base-service.service';
import { Injectable } from '@angular/core';
import { Supplier, SupplierListItem, SupplierSearchParams } from '../../models';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class SupplierService {
    constructor(
        private base: BaseService
    ) { }

    getSupplierList(params: SupplierSearchParams): Observable<SupplierListItem[]> {
        const queryParams = params.asQueryParams().map(o => o.toString()).join('&');
        const url = `${environment.apiUrl}/api/v1/finance/supplier?${queryParams}`;

        return this.base.http.get<SupplierListItem[]>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => data.map(o => SupplierListItem.create(o))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeSupplier(id: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/finance/supplier/${id}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getSupplier(id: number): Observable<Supplier> {
        const url = `${environment.apiUrl}/api/v1/finance/supplier/${id}`;

        return this.base.http.get<Supplier>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Supplier.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createSuplier(params: UpdateSupplierParams): Observable<Supplier> {
        const url = `${environment.apiUrl}/api/v1/finance/supplier`;

        return this.base.http.post<Supplier>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Supplier.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateSuplier(id: number, params: UpdateSupplierParams): Observable<Supplier> {
        const url = `${environment.apiUrl}/api/v1/finance/supplier/${id}`;

        return this.base.http.put<Supplier>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Supplier.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
