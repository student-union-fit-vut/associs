import { UpdateCategoryParams } from './../../models/finance/category/update-category-params';
import { map, catchError } from 'rxjs/operators';
import { environment } from './../../../../environments/environment';
import { BaseService } from './../base-service.service';
import { Injectable } from '@angular/core';
import { TransactionCategory } from '../../models';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class TransactionCategoryService {
    constructor(
        private base: BaseService
    ) { }

    getCategories(): Observable<TransactionCategory[]> {
        const url = `${environment.apiUrl}/api/v1/finance/category`;

        return this.base.http.get<TransactionCategory[]>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => data.map(o => TransactionCategory.create(o))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createCategory(params: UpdateCategoryParams): Observable<TransactionCategory> {
        const url = `${environment.apiUrl}/api/v1/finance/category`;

        return this.base.http.post<TransactionCategory>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => TransactionCategory.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateCategory(id: number, params: UpdateCategoryParams): Observable<TransactionCategory> {
        const url = `${environment.apiUrl}/api/v1/finance/category/${id}`;

        return this.base.http.put<TransactionCategory>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => TransactionCategory.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    deleteCategory(id: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/finance/category/${id}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
