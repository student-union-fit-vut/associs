import { UpdateAccountParams } from './../../models/finance/account/update-account-params';
import { catchError, map } from 'rxjs/operators';
import { environment } from './../../../../environments/environment';
import { AccountSearchParams } from './../../models/finance/account/account-search-params';
import { BaseService } from './../base-service.service';
import { Injectable } from '@angular/core';
import { Account, AccountListItem } from '../../models';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AccountService {
    constructor(
        public base: BaseService
    ) { }

    getAccountList(filter: AccountSearchParams): Observable<AccountListItem[]> {
        const queryParams = filter.asQueryParams().map(o => o.toString()).join('&');
        const url = `${environment.apiUrl}/api/v1/finance/account?${queryParams}`;

        return this.base.http.get<AccountListItem[]>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => data.map(o => AccountListItem.create(o))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getAccount(id: number): Observable<Account> {
        const url = `${environment.apiUrl}/api/v1/finance/account/${id}`;

        return this.base.http.get<Account>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Account.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createAccount(params: UpdateAccountParams): Observable<Account> {
        const url = `${environment.apiUrl}/api/v1/finance/account`;

        return this.base.http.post<Account>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Account.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateAccount(id: number, params: UpdateAccountParams): Observable<Account> {
        const url = `${environment.apiUrl}/api/v1/finance/account/${id}`;

        return this.base.http.put<Account>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Account.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeAccount(id: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/finance/account/${id}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
