import { TransactionAttachmentType } from './../../enums/transaction-attachment-type';
import { UpdateTransactionParams } from './../../models/finance/transactions/update-transaction-params';
import { catchError, map } from 'rxjs/operators';
import { PaginationParams, PaginatedData } from './../../../shared/components/data-list/models';
import { BaseService } from '../base-service.service';
import { Injectable } from '@angular/core';
import { QueryString, Transaction, TransactionListItem, TransactionSearchParams } from '../../models';
import { environment } from 'src/environments/environment';
import { Attachment } from '../../models/finance/transactions/attachment';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class TransactionService {
    constructor(
        public base: BaseService
    ) { }

    getTransactionList(params: TransactionSearchParams, pagination: PaginationParams): Observable<PaginatedData<TransactionListItem>> {
        const queryParams = [
            ...params.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');

        const url = `${environment.apiUrl}/api/v1/finance?${queryParams}`;

        return this.base.http.get<PaginatedData<TransactionListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => TransactionListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeTransaction(id: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/finance/${id}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getTransaction(id: number): Observable<Transaction> {
        const url = `${environment.apiUrl}/api/v1/finance/${id}`;

        return this.base.http.get<Transaction>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => Transaction.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createTransaction(params: UpdateTransactionParams): Observable<Transaction> {
        const url = `${environment.apiUrl}/api/v1/finance`;

        return this.base.http.post<Transaction>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Transaction.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateTransaction(id: number, params: UpdateTransactionParams): Observable<Transaction> {
        const url = `${environment.apiUrl}/api/v1/finance/${id}`;

        return this.base.http.put<Transaction>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => Transaction.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    uploadAttachment(transactionId: number, type: TransactionAttachmentType, file: File): Observable<Attachment> {
        const transactionType = new QueryString('type', type);
        const url = `${environment.apiUrl}/api/v1/finance/${transactionId}/attachment?${transactionType.toString()}`;

        const formData = new FormData();
        formData.append('file', file);

        return this.base.http.post<Attachment>(url, formData, { headers: this.base.httpHeaders }).pipe(
            map(data => Attachment.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getAttachment(transactionId: number, attachmentId: number): Observable<ArrayBuffer> {
        const url = `${environment.apiUrl}/api/v1/finance/${transactionId}/attachment/${attachmentId}`;

        return this.base.http.get(url, { responseType: 'arraybuffer', headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeAttachment(transactionId: number, attachmentId: number): Observable<unknown> {
        const url = `${environment.apiUrl}/api/v1/finance/${transactionId}/attachment/${attachmentId}`;

        return this.base.http.delete(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
