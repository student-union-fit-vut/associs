import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ReportTypes, TransactionReportItem } from './../../models/finance/transactions/transaction-report-item';
import { BaseService } from './../base-service.service';
import { Injectable } from '@angular/core';
import { SupplierListItem, TransactionCategory } from '../../models';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class FinanceReportService {
    constructor(
        private base: BaseService
    ) { }

    getReport<TReport>(reportType: ReportTypes): Observable<TransactionReportItem<TReport>[]> {
        const mappingFunction = this.getReportMappingMethod(reportType);
        const url = `${environment.apiUrl}/api/v1/finance/report/${reportType}`;

        return this.base.http.get<TransactionReportItem<TReport>[]>(url, { headers: this.base.httpHeaders }).pipe(
            // eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-argument
            map(data => data.map(o => TransactionReportItem.create<TReport>(o, entity => mappingFunction(entity)))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    private getReportMappingMethod(reportType: ReportTypes): (_: ReportTypes) => TransactionCategory | SupplierListItem | any | null {
        /* eslint-disable @typescript-eslint/indent, @typescript-eslint/unbound-method, @typescript-eslint/no-unsafe-return */
        switch (reportType) {
            case 'category': return TransactionCategory.create;
            case 'supplier': return SupplierListItem.create;
            case 'type': return (data: any) => data;
            default: return null;
        }
        /* eslint-enable */
    }
}
