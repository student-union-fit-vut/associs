import { HttpErrorResponse } from '@angular/common/http';
import { JwtTokenContent } from './../models/auth/jwt-token';
import { catchError, map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { LoginParams } from './../models/auth/login-params';
import { BaseService } from './base-service.service';
import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { JwtToken } from '../models';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(
        private storage: StorageService,
        private base: BaseService
    ) { }

    get authToken(): JwtToken { return JwtToken.fromInterface(this.storage.read<JwtToken>('auth')); }
    get loggedUser(): JwtTokenContent { return new JwtTokenContent(this.authToken?.accessToken); }

    isUserLogged(): boolean {
        const token = this.authToken;
        return token && token.expirationDatetime.compareWithNow() === 'after';
    }

    logout(): void {
        if (!this.authToken) { return; }
        this.base.http.delete(`${environment.apiUrl}/api/v1/auth/refresh`, { headers: this.base.httpHeaders }).subscribe();
        this.storage.remove('auth');
    }

    login(loginParams: LoginParams): Observable<void> {
        const url = `${environment.apiUrl}/api/v1/auth/login`;

        return new Observable<void>(observer => {
            this.base.http.post<JwtToken>(url, loginParams).pipe(
                map(token => JwtToken.fromInterface(token)),
                catchError((err: HttpErrorResponse) => this.base.catchError(err, true))
            ).subscribe(token => {
                this.storage.save('auth', token);
                observer.next();
            }, err => observer.error(err));
        });
    }
}
