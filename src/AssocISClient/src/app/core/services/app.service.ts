import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ClientConfiguration } from '../models';

@Injectable({ providedIn: 'root' })
export class AppService {
    clientConfiguration: ClientConfiguration;
    private profilePicChangeSubject = new Subject();
    // eslint-disable-next-line @typescript-eslint/member-ordering
    profilePictureChange = this.profilePicChangeSubject.asObservable();

    notifyProfilePictureChange(): void {
        this.profilePicChangeSubject.next();
    }

    initClientConfiguration(http: HttpClient): Promise<ClientConfiguration> {
        const url = `${environment.apiUrl}/api/v1/system/client`;

        return http.get<ClientConfiguration>(url).pipe(
            map(data => ClientConfiguration.create(data)),
            tap(data => this.clientConfiguration = data),
            tap(_ => console.log(`${new Date().toISOString()} Client configuration initialized.`))
        ).toPromise();
    }
}
