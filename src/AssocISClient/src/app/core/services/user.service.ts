import { PasswordChangeParams } from './../models/users/password-change-params';
import { PaginationParams, PaginatedData } from './../../shared/components/data-list/models';
import { UserFilter } from './../models/users/user-filter';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import { ResetPasswordResult, User, UserDetail, UserNoteParams, UserParams } from '../models';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(
        private base: BaseService
    ) { }

    getProfilePicture(userId: number): Observable<any> {
        const url = `${environment.apiUrl}/api/v1/user/${userId}/image`;

        return this.base.http.get(url, { headers: this.base.httpHeaders, responseType: 'arraybuffer' }).pipe(
            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getUserList(filter: UserFilter, pagination: PaginationParams): Observable<PaginatedData<User>> {
        const query = [
            ...filter.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');

        const url = `${environment.apiUrl}/api/v1/user?${query}`;

        return this.base.http.get<PaginatedData<User>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create<User>(data, entity => User.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    resetPassword(id: number): Observable<ResetPasswordResult> {
        const url = `${environment.apiUrl}/api/v1/user/${id}/password/reset`;

        return this.base.http.put<ResetPasswordResult>(url, null, { headers: this.base.httpHeaders }).pipe(
            map(data => ResetPasswordResult.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getUser(id: number): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user/${id}`;

        return this.base.http.get<UserDetail>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => UserDetail.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateProfilePicture(image: File): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user/@me/image`;

        const formData = new FormData();
        formData.append('file', image);

        return this.base.http.post<UserDetail>(url, formData, { headers: this.base.httpHeaders }).pipe(
            map(data => UserDetail.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    clearProfilePicture(): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user/@me/image`;

        return this.base.http.delete<UserDetail>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => UserDetail.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createUser(data: UserParams): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user`;

        return this.base.http.post<UserDetail>(url, data, { headers: this.base.httpHeaders }).pipe(
            map((result: UserDetail) => UserDetail.create(result)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateUser(id: number, data: UserParams): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user/${id}`;

        return this.base.http.put<UserDetail>(url, data, { headers: this.base.httpHeaders }).pipe(
            map((result: UserDetail) => UserDetail.create(result)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateLoggedUser(data: UserParams): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user/@me`;

        return this.base.http.put<UserDetail>(url, data, { headers: this.base.httpHeaders }).pipe(
            map((result: UserDetail) => UserDetail.create(result)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    setMyNote(id: number, data: UserNoteParams): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user/${id}/note`;

        return this.base.http.post<UserDetail>(url, data, { headers: this.base.httpHeaders }).pipe(
            map((result: UserDetail) => UserDetail.create(result)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    deletePassword(id: number): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user/${id}/password`;

        return this.base.http.delete<UserDetail>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => UserDetail.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    changePassword(params: PasswordChangeParams): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user/@me/password`;

        return this.base.http.put<UserDetail>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => UserDetail.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getProfilePictureUrl(user: User): string {
        return `${environment.apiUrl}/api/v1/user/${user.id}/image`;
    }

    anonymizeUser(id: number): Observable<UserDetail> {
        const url = `${environment.apiUrl}/api/v1/user/${id}/anonymize`;

        return this.base.http.delete<UserDetail>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => UserDetail.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
