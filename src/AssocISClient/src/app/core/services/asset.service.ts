import { QueryString } from './../models/common';
import { AssetLoanInfo } from './../models/assets/asset-loan-info';
import { UpdateAssetParams } from './../models/assets/update-asset-params';
import { map, catchError } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { PaginatedData, PaginationParams } from './../../shared/components/data-list/models';
import { BaseService } from './base-service.service';
import { Injectable } from '@angular/core';
import {
    AssetCategory,
    AssetDetail, AssetListItem, AssetSearchParams, CreateAssetParams, CreateInventoryParams,
    InventoryReport, LoanSearchParams, UpdateAssetCategoryParams
} from '../models';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class AssetService {
    constructor(
        private base: BaseService
    ) { }

    getAssetsList(params: AssetSearchParams, pagination: PaginationParams): Observable<PaginatedData<AssetListItem>> {
        const queryParams = [
            ...params.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');
        const url = `${environment.apiUrl}/api/v1/asset?${queryParams}`;

        return this.base.http.get<PaginatedData<AssetListItem>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => AssetListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createAsset(params: CreateAssetParams): Observable<AssetDetail> {
        const url = `${environment.apiUrl}/api/v1/asset`;

        return this.base.http.post<AssetDetail>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => AssetDetail.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getAsset(id: number): Observable<AssetDetail> {
        const url = `${environment.apiUrl}/api/v1/asset/${id}`;

        return this.base.http.get<AssetDetail>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => AssetDetail.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateAsset(id: number, params: UpdateAssetParams): Observable<AssetDetail> {
        const url = `${environment.apiUrl}/api/v1/asset/${id}`;

        return this.base.http.put<AssetDetail>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => AssetDetail.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    loanAsset(id: number): Observable<AssetLoanInfo> {
        const url = `${environment.apiUrl}/api/v1/asset/${id}/loan`;

        return this.base.http.post<AssetLoanInfo>(url, {}, { headers: this.base.httpHeaders }).pipe(
            map(data => AssetLoanInfo.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getLoansOfAsset(assetId: number, params: LoanSearchParams, pagination: PaginationParams): Observable<PaginatedData<AssetLoanInfo>> {
        const queryParams = [
            ...params.asQueryParams(),
            ...pagination.asQueryParams()
        ].map(o => o.toString()).join('&');
        const url = `${environment.apiUrl}/api/v1/asset/${assetId}/loan?${queryParams}`;

        return this.base.http.get<PaginatedData<AssetLoanInfo>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => AssetLoanInfo.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    returnLoan(assetId: number, loanId: number): Observable<AssetLoanInfo> {
        const url = `${environment.apiUrl}/api/v1/asset/${assetId}/loan/${loanId}/return`;

        return this.base.http.put<AssetLoanInfo>(url, {}, { headers: this.base.httpHeaders }).pipe(
            map(data => AssetLoanInfo.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createInventory(params: CreateInventoryParams): Observable<AssetListItem[]> {
        const url = `${environment.apiUrl}/api/v1/asset/inventory`;

        return this.base.http.post<AssetListItem[]>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => data.map(entity => AssetListItem.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getInventoryReportsOfAsset(assetId: number, userId: number, pagination: PaginationParams): Observable<PaginatedData<InventoryReport>> {
        const queryParams = [
            userId ? new QueryString('userId', userId) : null,
            ...pagination.asQueryParams()
        ].filter(o => o).map(o => o.toString()).join('&');

        const url = `${environment.apiUrl}/api/v1/asset/${assetId}/inventory?${queryParams}`;

        return this.base.http.get<PaginatedData<InventoryReport>>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => PaginatedData.create(data, entity => InventoryReport.create(entity))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    getCategories(): Observable<AssetCategory[]> {
        const url = `${environment.apiUrl}/api/v1/asset/category`;

        return this.base.http.get<AssetCategory[]>(url, { headers: this.base.httpHeaders }).pipe(
            map(data => data.map(o => AssetCategory.create(o))),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    createCategory(params: UpdateAssetCategoryParams): Observable<AssetCategory> {
        const url = `${environment.apiUrl}/api/v1/asset/category`;

        return this.base.http.post<AssetCategory>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => AssetCategory.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    updateCategory(id: number, params: UpdateAssetCategoryParams): Observable<AssetCategory> {
        const url = `${environment.apiUrl}/api/v1/asset/category/${id}`;

        return this.base.http.put<AssetCategory>(url, params, { headers: this.base.httpHeaders }).pipe(
            map(data => AssetCategory.create(data)),
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }

    removeCategory(id: number): Observable<AssetCategory> {
        const url = `${environment.apiUrl}/api/v1/asset/category/${id}`;

        return this.base.http.delete<AssetCategory>(url, { headers: this.base.httpHeaders }).pipe(
            catchError((err: HttpErrorResponse) => this.base.catchError(err))
        );
    }
}
