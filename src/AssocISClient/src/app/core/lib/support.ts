import { SelectItems } from './../../shared/components/select/select-item';

export class Support {
    static getEnumKeyByValue(type: any, value: any): any {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-unsafe-member-access
        return Object.keys(type).find(o => type[o] === value);
    }

    static convertImageToBase64(data: any): string {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        const bytes = new Uint8Array(data);
        return window.btoa(bytes.reduce((prev, curr) => prev + String.fromCharCode(curr), ''));
    }

    static downloadBinaryData(data: any, filename: string): void {
        const anchor = document.createElement('a');
        anchor.style.display = 'none';

        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        const blob = new Blob([data], { type: 'octet/stream' });
        const url = URL.createObjectURL(blob);

        anchor.href = url;
        anchor.download = filename;
        anchor.click();
        URL.revokeObjectURL(url);
    }

    static flattern<T>(arr: Array<T>): any[] {
        const result = [];

        for (const item of arr) {
            if (Array.isArray(item)) {
                // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
                result.push(...this.flattern(item));
            } else {
                result.push(item);
            }
        }

        return result;
    }
}

export class SelectHelpers {
    static getSelectItemsFromEnum(enumeration: any, translationEnumeration: any): SelectItems {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        return Object.keys(enumeration)
            .filter(o => !isNaN(parseInt(o, 10)))
            .map(o => parseInt(o, 10))
            // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
            .map(o => ({ key: o, value: translationEnumeration[Support.getEnumKeyByValue(enumeration, o) as string] as string }))
            .filter(o => o.value && o.value.length > 0);
    }
}
