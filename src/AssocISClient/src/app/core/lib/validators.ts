import { DateTime } from './../models/datetime';
import { AbstractControl, ValidatorFn } from '@angular/forms';

export class Validations {
    static checkPasswords(passwordName: string, confirmPasswordName: string, switchEquality: boolean = false): ValidatorFn {
        return (control: AbstractControl) => {
            const password = control.get(passwordName).value as string;
            const confirmPassword = control.get(confirmPasswordName).value as string;

            if (switchEquality ? password === confirmPassword : password !== confirmPassword) {
                control.get(confirmPasswordName).setErrors({ equals: true });
            }

            return null;
        };
    }

    static checkUrl(): ValidatorFn {
        return (control: AbstractControl) => {
            if (!control.value) { return null; }
            const elem = document.createElement('input');

            elem.type = 'url';
            elem.value = control.value as string;

            return elem.checkValidity() ? null : { url: true };
        };
    }

    static checkDates(dateFromControlId: string, dateToControlId: string): ValidatorFn {
        return (control: AbstractControl) => {
            try {
                const dateFromControl = control.get(dateFromControlId);
                const dateToControl = control.get(dateToControlId);

                const dateFromValue = dateFromControl.value as string;
                const dateToValue = dateToControl.value as string;
                if (!dateFromValue || !dateToValue) { return null; }

                const dateFrom = DateTime.fromISOString(dateFromValue);
                const dateTo = DateTime.fromISOString(dateToValue);

                const compareResult = dateFrom.compare(dateTo);
                if (compareResult !== 'before') {
                    dateFromControl.setErrors({ dateInterval: true });
                    dateToControl.setErrors({ dateInterval: true });
                    return { dateInterval: true };
                }

                return null;
            } catch (e) {
                return null;
            }
        };
    }
}

export class ValidationHelper {
    static isInvalid(form: AbstractControl, controlId: string, errorId: string = null): boolean {
        const control = form.get(controlId);
        if (!control.touched) { return false; }

        return errorId ? control.hasError(errorId) : control.invalid;
    }
}
