import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthGuardService implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService
    ) { }


    canActivate(_: ActivatedRouteSnapshot, __: RouterStateSnapshot)
        // eslint-disable-next-line @typescript-eslint/type-annotation-spacing
        : boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        if (!this.authService.isUserLogged()) {
            this.authService.logout();
            this.router.navigate(['/', 'login']);
            return false;
        }

        return true;
    }
}
