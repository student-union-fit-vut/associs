import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { APP_BASE_HREF, CommonModule, PlatformLocation } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponentComponent } from './components/login-component/login-component.component';
import { AppService } from './core/services/app.service';

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        LoginComponentComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    providers: [
        {
            provide: APP_BASE_HREF,
            useFactory: (platformLocation: PlatformLocation) => platformLocation.getBaseHrefFromDOM(),
            deps: [PlatformLocation]
        },
        {
            provide: APP_INITIALIZER,
            useFactory: (appService: AppService, http: HttpClient) => () => appService.initClientConfiguration(http),
            deps: [AppService, HttpClient],
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
