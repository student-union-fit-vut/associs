import { OnChangeType, OnTouchedType } from './../../../core/models/common';
import { NG_VALUE_ACCESSOR, FormGroup, FormBuilder, ControlValueAccessor } from '@angular/forms';
import { Component, OnInit, forwardRef, Input } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';
import { SelectItems } from '../select/select-item';
import { noop } from 'rxjs';
import { AppService } from 'src/app/core/services/app.service';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'user-selector',
    templateUrl: './user-selector.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => UserSelectorComponent),
            multi: true
        }
    ]
})
export class UserSelectorComponent implements OnInit, ControlValueAccessor {
    @Input() useDiscord = false;
    @Input() multiselect = false;
    @Input() searchable = false;

    items: SelectItems;
    form: FormGroup;

    private onChange: OnChangeType = noop;

    constructor(
        private dataService: DataService,
        private fb: FormBuilder,
        private appService: AppService
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            selector: []
        });

        if (!this.appService.clientConfiguration.discordEnabled && this.useDiscord) {
            throw new Error('Discord is not available.');
        }

        if (this.useDiscord) {
            this.dataService.getDiscordUsersSelectList().subscribe(users => this.items = users.map(o => o));
        } else {
            this.dataService.getUserSelectList().subscribe(users => this.items = users.map(o => o));
        }

        this.form.get('selector').valueChanges.subscribe(o => this.onChange(o));
    }

    writeValue(obj: any): void {
        this.form.get('selector').setValue(obj);
    }

    registerOnChange(fn: OnChangeType): void {
        this.onChange = fn;
    }

    registerOnTouched(_: OnTouchedType): void {
        noop();
    }

    setDisabledState?(isDisabled: boolean): void {
        if (isDisabled) {
            this.form.get('selector').disable();
        } else {
            this.form.get('selector').enable();
        }
    }

}
