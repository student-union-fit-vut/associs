import { filter } from 'rxjs/operators';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';

export class ModalData<TComponent> {
    constructor(public modal: NgbModalRef) { }

    get onClose(): Observable<any> { return this.modal.closed; }

    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    get onAccept(): Observable<any> { return this.onClose.pipe(filter(o => o)); }
    get onDecline(): Observable<any> { return this.onClose.pipe(filter(o => o === false)); }

    get componentInstance(): TComponent {
        return this.modal.componentInstance as TComponent;
    }
}
