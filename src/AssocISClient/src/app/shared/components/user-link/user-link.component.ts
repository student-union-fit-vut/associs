import { Component, Input } from '@angular/core';
import { User } from 'src/app/core/models';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'user-link',
    templateUrl: './user-link.component.html',
})
export class UserLinkComponent {
    @Input() user: User;
}
