import { Component, Input } from '@angular/core';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'common-dashboard',
    templateUrl: './common-dashboard.component.html'
})
export class CommonDashboardComponent {
    @Input() padding = true;
}
