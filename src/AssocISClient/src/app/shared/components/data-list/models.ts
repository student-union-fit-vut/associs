import { QueryString } from 'src/app/core/models';

export const pageSizes = [5, 10, 25, 50, 100, 200];
export const defaultPageSize = 25;

export class PaginatedData<T> {
    public data: T[];
    public page: number;
    public totalItemsCount: number;
    public canNext: boolean;
    public canPrev: boolean;

    // eslint-disable-next-line no-shadow
    static create<T>(data: any, dataMapper: (obj: any) => T): PaginatedData<T> {
        const result = new PaginatedData<T>();

        /* eslint-disable */
        result.data = data.data.map((o: any) => dataMapper(o));
        result.page = data.page;
        result.totalItemsCount = data.totalItemsCount;
        result.canNext = data.canNext;
        result.canPrev = data.canPrev;
        /* eslint-enable */

        return result;
    }
}

export class PaginationParams {
    constructor(
        public limit: number,
        public page: number
    ) { }

    asQueryParams(): QueryString[] {
        return [
            new QueryString('limit', this.limit),
            new QueryString('page', this.page)
        ];
    }

    toQueryString(): string {
        return this.asQueryParams().map(o => o.toString()).join('&');
    }
}
