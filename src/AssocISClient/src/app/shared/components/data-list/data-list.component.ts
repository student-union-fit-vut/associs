import { defaultPageSize, pageSizes, PaginatedData, PaginationParams } from './models';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'data-list',
    templateUrl: './data-list.component.html'
})
export class DataListComponent implements OnInit {
    @Output() readData = new EventEmitter<any>();

    isDataLoaded = false;
    totalItemsCount = 0;
    items: any[] = [];
    pageSize: number;
    currentPage = 1;
    form: FormGroup;

    constructor(
        private fb: FormBuilder
    ) { }

    get limit(): AbstractControl { return this.form.get('limit'); }
    get pageSizes(): number[] { return pageSizes; }

    ngOnInit(): void {
        this.form = this.fb.group({
            limit: [defaultPageSize, Validators.compose([
                Validators.min(pageSizes.reduce((prev, curr) => Math.min(prev, curr), Number.MAX_VALUE)),
                Validators.max(pageSizes.reduce((prev, curr) => Math.max(prev, curr), Number.MIN_VALUE))
            ])]
        });

        this.form.valueChanges.subscribe(() => this.onChange());
        this.onChange();
    }

    onChange(): void {
        const limit = parseInt(this.limit.value as string, 10);
        this.pageSize = limit;
        this.isDataLoaded = false;

        const paginationParams = new PaginationParams(limit, this.currentPage);
        this.readData.emit(paginationParams);
    }

    setData(result: PaginatedData<any>): void {
        this.items = result.data;
        this.currentPage = result.page;
        this.totalItemsCount = result.totalItemsCount === 0 ? 1 : result.totalItemsCount;

        this.isDataLoaded = true;
    }

    pageChange(_: number): void {
        this.onChange();
    }
}
