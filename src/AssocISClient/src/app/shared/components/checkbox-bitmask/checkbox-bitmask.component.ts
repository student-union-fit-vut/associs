import { noop } from 'rxjs';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Dictionary, OnChangeType, OnTouchedType } from './../../../core/models/common';
import { Component, Input, OnInit, forwardRef } from '@angular/core';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'checkbox-bitmask',
    templateUrl: './checkbox-bitmask.component.html',
    styleUrls: ['./checkbox-bitmask.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => CheckboxBitmaskComponent),
            multi: true
        }
    ]
})
export class CheckboxBitmaskComponent implements OnInit, ControlValueAccessor {
    @Input() options: Dictionary<number, string>;

    bitmask = 0;
    disabled: boolean;
    gridSize: number;

    private onChange: OnChangeType = noop;
    private onTouched: OnTouchedType = noop;

    ngOnInit(): void {
        this.gridSize = Math.max(Math.min(this.options?.length ?? 3, 3), 1);
    }

    writeValue(obj: number): void {
        this.bitmask = obj;
    }

    registerOnChange(fn: OnChangeType): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: OnTouchedType): void {
        this.onTouched = fn;
    }

    setDisabledState?(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    /* eslint-disable no-bitwise */
    onValueChange(maskValue: number, isChecked: boolean): void {
        if (isChecked) {
            this.bitmask |= maskValue;
        } else {
            this.bitmask &= ~maskValue;
        }

        this.onTouched();
        this.onChange(this.bitmask);
    }

    isChecked(mask: number): boolean {
        return (this.bitmask & mask) !== 0;
    }
}
