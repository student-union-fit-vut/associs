import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Component, forwardRef, Input } from '@angular/core';
import { noop } from 'rxjs';
import { SelectItems } from '../select/select-item';
import { OnChangeType, OnTouchedType } from 'src/app/core/models';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'multiselect',
    templateUrl: './multiselect.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MultiselectComponent),
            multi: true
        }
    ]
})
export class MultiselectComponent implements ControlValueAccessor {
    @Input() items: SelectItems = [];

    selectedItems = [];
    disabled = false;

    private onChange: OnChangeType = noop;

    writeValue(obj: SelectItems): void {
        this.selectedItems = obj;
    }

    registerOnChange(fn: OnChangeType): void {
        this.onChange = fn;
    }

    registerOnTouched(_: OnTouchedType): void {
        noop();
    }

    setDisabledState?(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onValueChange(): void {
        this.onChange(this.selectedItems);
    }
}
