import { KeyValuePair } from './../../../core/models/common';

export interface SelectItem extends KeyValuePair<any, any> {
    icon?: string;
}

export type SelectItems = SelectItem[];
