import { noop } from 'rxjs';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { SelectItem } from './select-item';
import { Component, Input, forwardRef } from '@angular/core';
import { OnChangeType, OnTouchedType } from 'src/app/core/models';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'selector',
    templateUrl: './select.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SelectComponent),
            multi: true
        }
    ]
})
export class SelectComponent implements ControlValueAccessor {
    @Input() items: SelectItem[];
    @Input() searchable = false;

    selectedItem: SelectItem;
    disabled = false;

    private onChange: OnChangeType = noop;
    private onTouched: OnTouchedType = noop;

    writeValue(obj: SelectItem): void {
        this.selectedItem = obj;
    }

    registerOnChange(fn: OnChangeType): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: OnTouchedType): void {
        this.onTouched = fn;
    }

    setDisabledState?(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onValueChange(): void {
        this.onChange(this.selectedItem);
    }

    onBlur(): void {
        this.onTouched();
    }
}
