import { LogData } from './../../../core/models/common';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-log-data',
    templateUrl: './log-data.component.html'
})
export class LogDataComponent {
    @Input() logData: LogData;
    @Input() padding = true;
    @Input() noExternalContent = false;
}
