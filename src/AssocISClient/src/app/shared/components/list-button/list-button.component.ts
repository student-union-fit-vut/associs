import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'list-button',
    templateUrl: './list-button.component.html'
})
export class ListButtonComponent {
    @Input() link?: string;
    @Input() title?: string;
    @Input() iconGroup = 'fas';
    @Input() icon?: string;
    @Input() classList: string[] = [];

    @Output() clicked = new EventEmitter<unknown>();
}
