import { OnChangeType, OnTouchedType } from './../../../core/models/common';
import { Component, forwardRef, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { noop } from 'rxjs';

@Component({
    selector: 'app-checkbox',
    templateUrl: './checkbox.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => CheckboxComponent),
            multi: true
        }
    ]
})
export class CheckboxComponent implements OnInit, ControlValueAccessor {
    // eslint-disable-next-line @angular-eslint/no-output-rename, @angular-eslint/no-output-on-prefix
    @Output('onChange') onChangeEvent = new EventEmitter<boolean>();

    @Input() id: string;
    @Input() label: string;
    @Input() checked = false;
    @Input() disabled = false;
    form: FormGroup;

    private onChange: OnChangeType = noop;

    constructor(private fb: FormBuilder) { }

    get checkbox(): AbstractControl { return this.form.get('checkbox'); }

    ngOnInit(): void {
        this.form = this.fb.group({
            checkbox: [{ value: this.checked, disabled: this.disabled }]
        });

        this.checkbox.valueChanges.subscribe((value: boolean) => {
            this.onChange(value);
            this.onChangeEvent.emit(value);
            this.checked = value;
        });
    }

    writeValue(obj: boolean): void {
        this.checkbox.setValue(obj);
        this.checked = obj;
    }

    registerOnChange(fn: OnChangeType): void {
        this.onChange = fn;
    }

    registerOnTouched(_: OnTouchedType): void { noop(); }

    setDisabledState?(isDisabled: boolean): void {
        if (isDisabled) {
            this.checkbox.disable();
        } else {
            this.checkbox.enable();
        }

        this.disabled = isDisabled;
    }
}
