import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent {
    @Input() title: string;
    @Input() icon: string;
    @Input() isHideable = true;
    @Input() radius = true;

    public hiddenBody: boolean;

    toggleBodyVisible(): void {
        this.hiddenBody = !this.hiddenBody;
    }
}
