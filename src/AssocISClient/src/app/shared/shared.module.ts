import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardComponent } from './components/card/card.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { LoadingComponent } from './components/loading/loading.component';
import { DataListComponent } from './components/data-list/data-list.component';
import { NgbModalModule, NgbNavModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './components/modal/modal.component';
import { CheckboxBitmaskComponent } from './components/checkbox-bitmask/checkbox-bitmask.component';
import { LogDataComponent } from './components/log-data/log-data.component';
import { MultiselectComponent } from './components/multiselect/multiselect.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { UserLinkComponent } from './components/user-link/user-link.component';
import { SelectComponent } from './components/select/select.component';
import { ChartsModule } from 'ng2-charts';
import { ValidationErrorsModalComponent } from './components/modal/validation-errors-modal/validation-errors-modal.component';
import { ListButtonComponent } from './components/list-button/list-button.component';
import { UserSelectorComponent } from './components/user-selector/user-selector.component';
import { CommonDashboardComponent } from './components/common-dashboard/common-dashboard.component';

@NgModule({
    declarations: [
        CardComponent,
        CheckboxComponent,
        LoadingComponent,
        DataListComponent,
        ModalComponent,
        CheckboxBitmaskComponent,
        LogDataComponent,
        MultiselectComponent,
        UserLinkComponent,
        SelectComponent,
        ValidationErrorsModalComponent,
        ListButtonComponent,
        UserSelectorComponent,
        CommonDashboardComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        FormsModule,
        NgbPaginationModule,
        NgbModalModule,
        NgbNavModule,
        NgSelectModule,
        RouterModule,
        ChartsModule
    ],
    exports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        FormsModule,
        CardComponent,
        CheckboxComponent,
        LoadingComponent,
        DataListComponent,
        ModalComponent,
        NgbModalModule,
        CheckboxBitmaskComponent,
        NgbNavModule,
        LogDataComponent,
        MultiselectComponent,
        UserLinkComponent,
        SelectComponent,
        ChartsModule,
        ValidationErrorsModalComponent,
        ListButtonComponent,
        UserSelectorComponent,
        CommonDashboardComponent
    ]
})
export class SharedModule { }
