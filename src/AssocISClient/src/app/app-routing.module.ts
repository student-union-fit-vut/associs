import { AuthGuardService } from './core/guards/auth-guard.service';
import { LoginComponentComponent } from './components/login-component/login-component.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'app',
        pathMatch: 'full'
    },
    {
        path: 'app',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./application/application.module').then(mod => mod.ApplicationModule)
    },
    { path: 'login', component: LoginComponentComponent },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
