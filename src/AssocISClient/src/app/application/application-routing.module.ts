import { SectionNotFoundComponent } from './components/section-not-found/section-not-found.component';
import { AppScreenComponent } from './app-screen/app-screen.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: AppScreenComponent,
        children: [
            {
                path: '',
                redirectTo: 'users',
                pathMatch: 'full'
            },
            {
                path: 'users',
                loadChildren: () => import('./users/users.module').then(mod => mod.UsersModule),
                data: { breadcrumb: 'Členové' }
            },
            {
                path: 'teams',
                loadChildren: () => import('./teams/teams.module').then(mod => mod.TeamsModule),
                data: { breadcrumb: 'Týmy' }
            },
            {
                path: 'meetings',
                loadChildren: () => import('./meetings/meetings.module').then(mod => mod.MeetingsModule),
                data: { breadcrumb: 'Schůze' }
            },
            {
                path: 'finance',
                loadChildren: () => import('./finance/finance.module').then(mod => mod.FinanceModule),
                data: { breadcrumb: 'Finance' }
            },
            {
                path: 'events',
                loadChildren: () => import('./events/events.module').then(mod => mod.EventsModule),
                data: { breadcrumb: 'Akce' }
            },
            {
                path: 'assets',
                loadChildren: () => import('./assets/assets.module').then(mod => mod.AssetsModule),
                data: { breadcrumb: 'Majetek' }
            },
            {
                path: 'system',
                loadChildren: () => import('./system/system.module').then(mod => mod.SystemModule),
                data: { breadcrumb: 'Systém' }
            },
            {
                path: '**',
                component: SectionNotFoundComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ApplicationRoutingModule { }
