import { ApplicationRoutingModule } from './application-routing.module';
import { SharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { AppScreenComponent } from './app-screen/app-screen.component';
import { MenuComponent } from './components/menu/menu.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SectionNotFoundComponent } from './components/section-not-found/section-not-found.component';

@NgModule({
    declarations: [
        AppScreenComponent,
        MenuComponent,
        HeaderComponent,
        SidebarComponent,
        SectionNotFoundComponent
    ],
    imports: [
        SharedModule,
        ApplicationRoutingModule
    ]
})
export class ApplicationModule { }
