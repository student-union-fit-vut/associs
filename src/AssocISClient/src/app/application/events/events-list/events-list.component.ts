import { EventService } from './../../../core/services/event.service';
import { PaginationParams } from 'src/app/shared/components/data-list/models';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { EventsSearchParams } from './../../../core/models/events/events-search-params';
import { Component, ViewChild } from '@angular/core';

@Component({
    selector: 'app-events-list',
    templateUrl: './events-list.component.html'
})
export class EventsListComponent {
    @ViewChild('list', { static: false }) list: DataListComponent;

    private filter: EventsSearchParams;

    constructor(
        private modalService: ModalService,
        private eventService: EventService
    ) { }

    filterChanged(filter: EventsSearchParams): void {
        this.filter = filter;
        if (this.list) { this.list.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.eventService.getEventsList(this.filter, pagination).subscribe(data => this.list.setData(data));
    }

    removeEvent(id: number): void {
        this.modalService.showQuestion('Smazání akce', 'Opravdu si přejete smazat akci?', 'lg').onAccept.subscribe(() => {
            this.eventService.removeEvent(id).subscribe(() => {
                this.modalService.showNotification('Smazání akce', 'Akce byla úspěšně smazána.')
                    .onClose.subscribe(_ => this.list.onChange());
            });
        });
    }

}
