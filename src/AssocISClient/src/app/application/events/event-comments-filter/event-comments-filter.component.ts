import { debounceTime } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CommentSearchParams } from 'src/app/core/models';
import { DataService } from 'src/app/core/services/data.service';
import { SelectItems } from 'src/app/shared/components/select/select-item';

@Component({
    selector: 'app-event-comments-filter',
    templateUrl: './event-comments-filter.component.html'
})
export class EventCommentsFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<CommentSearchParams>();

    users: SelectItems = [];
    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private dataService: DataService
    ) { }

    ngOnInit(): void {
        this.dataService.getUserSelectList().subscribe(users => {
            this.users = users;
            this.users.unshift({ key: null, value: 'Nevybráno' });
        });

        this.form = this.fb.group({
            textQuery: [null],
            createdById: [null]
        });

        this.form.valueChanges.pipe(debounceTime(200)).subscribe(_ => this.submitFilter());
        this.submitFilter();
    }

    resetFilter(): boolean {
        this.form.patchValue({
            textQuery: null,
            createdById: null
        });

        return false;
    }

    private submitFilter(): void {
        const params = CommentSearchParams.create(this.form.value);
        this.filterChanged.emit(params);
    }

}
