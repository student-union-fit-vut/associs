import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './../../../core/services/auth.service';
import { DataListComponent } from './../../../shared/components/data-list/data-list.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommentListItem, CommentSearchParams, Event, UpdateCommentParams, User } from 'src/app/core/models';
import { EventService } from 'src/app/core/services/event.service';
import { PaginationParams } from 'src/app/shared/components/data-list';
import { UserService } from 'src/app/core/services/user.service';
import { Permissions } from 'src/app/core/enums';
import { ModalService } from 'src/app/shared/components/modal/modal.service';

@Component({
    selector: 'app-event-comments',
    templateUrl: './event-comments.component.html',
    styleUrls: ['./event-comments.component.scss']
})
export class EventCommentsComponent implements OnInit, OnDestroy {
    @ViewChild('comments', { static: false }) comments: DataListComponent;

    event: Event;
    form: FormGroup;
    timeout: any;
    editedCommentId: number;
    loaded = false;

    private filter: CommentSearchParams;

    constructor(
        private eventService: EventService,
        private route: ActivatedRoute,
        private authService: AuthService,
        private fb: FormBuilder,
        private userService: UserService,
        private modalService: ModalService
    ) { }

    get loggedUserId(): number { return this.authService.loggedUser.id; }

    ngOnInit(): void {
        const id = parseInt(this.route.snapshot.params.id, 10);

        this.eventService.getEvent(id).subscribe(event => {
            this.event = event;
            if (this.comments) { this.comments.onChange(); }
        });

        this.form = this.fb.group({ message: ['', Validators.required] });
        this.startRefresh();
    }

    startRefresh(): void {
        this.timeout = setInterval(() => this.comments.onChange(), 5000);
    }

    ngOnDestroy(): void {
        clearInterval(this.timeout);
    }

    filterChanged(filter: CommentSearchParams): void {
        this.filter = filter;
        if (this.comments) { this.comments.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        if (!this.event) { return; }

        this.eventService.getComments(this.event.id, this.filter, pagination).subscribe(comments => {
            this.comments.setData(comments);

            if (!this.loaded) {
                setTimeout(() => {
                    const chatWindow = document.getElementsByClassName('direct-chat-messages')[0];
                    chatWindow.scrollTop = chatWindow.scrollHeight;
                }, 200);

                this.loaded = true;
            }
        });
    }

    sendComment(): void {
        const params = new UpdateCommentParams(this.form.value.message);

        if (this.editedCommentId) {
            this.eventService.updateComment(this.event.id, this.editedCommentId, params).subscribe(_ => {
                this.cancelEditation();
            });
        } else {
            this.eventService.createComment(this.event.id, params).subscribe(_ => {
                this.comments.onChange();
                this.form.patchValue({ message: null });
            });
        }
    }

    getProfilePictureUrl(user: User): string {
        return this.userService.getProfilePictureUrl(user);
    }

    removeComment(comment: CommentListItem): void {
        clearInterval(this.timeout);
        this.modalService.showQuestion('Smazání komentáře', 'Opravdu si přeješ smazat komentář?', 'lg').onAccept.subscribe(() => {
            this.eventService.removeComment(this.event.id, comment.id).subscribe(_ => {
                this.comments.onChange();
                this.startRefresh();
            });
        });
    }

    canRemoveComment(comment: CommentListItem): boolean {
        return this.authService.loggedUser.hasPermission(Permissions.EventManager) || this.loggedUserId === comment.createdBy.id;
    }

    editComment(comment: CommentListItem): void {
        clearInterval(this.timeout);
        this.editedCommentId = comment.id;
        this.form.get('message').setValue(comment.text);
    }

    cancelEditation(): boolean {
        this.editedCommentId = null;
        this.form.get('message').setValue(null);
        this.comments.onChange();
        this.startRefresh();

        return false;
    }
}
