import { SharedModule } from '../../shared/shared.module';
import { EventsRoutingModule } from './events-routing.module';
import { NgModule } from '@angular/core';
import { EventsDashboardComponent } from './events-dashboard/events-dashboard.component';
import { EventsListComponent } from './events-list/events-list.component';
import { EventsFilterComponent } from './events-filter/events-filter.component';
import { EventsEditComponent } from './events-edit/events-edit.component';
import { EventCommentsComponent } from './event-comments/event-comments.component';
import { EventCommentsFilterComponent } from './event-comments-filter/event-comments-filter.component';

@NgModule({
    declarations: [
        EventsDashboardComponent,
        EventsListComponent,
        EventsFilterComponent,
        EventsEditComponent,
        EventCommentsComponent,
        EventCommentsFilterComponent
    ],
    imports: [
        SharedModule,
        EventsRoutingModule
    ]
})
export class EventsModule { }
