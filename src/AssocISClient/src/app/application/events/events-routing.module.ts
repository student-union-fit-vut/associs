import { EventCommentsComponent } from './event-comments/event-comments.component';
import { EventsEditComponent } from './events-edit/events-edit.component';
import { EventsDashboardComponent } from './events-dashboard/events-dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', component: EventsDashboardComponent, data: { breadcrumb: 'Seznam' } },
    { path: 'add', component: EventsEditComponent, data: { breadcrumb: 'Vytvoření', isAdd: true } },
    { path: ':id', component: EventsEditComponent, data: { breadcrumb: 'Editace', isAdd: false } },
    { path: ':id/comments', component: EventCommentsComponent, data: { breadcrumb: 'Komentáře' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EventsRoutingModule { }
