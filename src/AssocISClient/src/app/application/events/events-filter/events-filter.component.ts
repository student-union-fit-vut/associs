import { StorageService } from 'src/app/core/services/storage.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EventsSearchParams } from 'src/app/core/models';

@Component({
    selector: 'app-events-filter',
    templateUrl: './events-filter.component.html'
})
export class EventsFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<EventsSearchParams>();

    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private storage: StorageService
    ) { }

    ngOnInit(): void {
        const filter = EventsSearchParams.create(this.storage.read<EventsSearchParams>('EventsSearchParams') || {});
        this.initForm(filter);
        this.submitFilter();
    }

    resetFilter(): boolean {
        const filter = EventsSearchParams.empty;

        this.form.patchValue({
            nameQuery: filter.nameQuery,
            excludeFuture: filter.excludeFuture,
            excludeOngoing: filter.excludeOngoing,
            excludePast: filter.excludePast
        });

        this.submitFilter();
        return false;
    }

    submitFilter(): void {
        const params = EventsSearchParams.create(this.form.value);

        this.filterChanged.emit(params);
        this.storage.save('EventsSearchParams', params);
    }

    private initForm(filter: EventsSearchParams): void {
        this.form = this.fb.group({
            nameQuery: [filter.nameQuery],
            excludeFuture: [filter.excludeFuture],
            excludeOngoing: [filter.excludeOngoing],
            excludePast: [filter.excludePast]
        });
    }
}
