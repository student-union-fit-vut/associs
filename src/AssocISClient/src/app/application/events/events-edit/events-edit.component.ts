import { SelectItems } from './../../../shared/components/select/select-item';
import { Validations } from './../../../core/lib/validators';
import { UpdateEventParams } from './../../../core/models/events/update-event-params';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { Event } from './../../../core/models/events/event';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventService } from 'src/app/core/services/event.service';
import { ValidationHelper } from 'src/app/core/lib/validators';
import { DataService } from 'src/app/core/services/data.service';
import { AppService } from 'src/app/core/services/app.service';

@Component({
    selector: 'app-events-edit',
    templateUrl: './events-edit.component.html'
})
export class EventsEditComponent implements OnInit {
    event: Event;
    isCreate: boolean;
    form: FormGroup;
    channels: SelectItems;

    constructor(
        private eventService: EventService,
        private modalService: ModalService,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private data: DataService,
        private appService: AppService
    ) { }

    get canManage(): boolean {
        return this.isCreate || this.event.canManage;
    }

    ngOnInit(): void {
        this.isCreate = this.route.routeConfig?.data?.isAdd ?? false;

        if (this.appService.clientConfiguration.discordEnabled) {
            this.data.getTextChannelsInDiscordHomeGuildAsync().subscribe(channels => {
                this.channels = channels;
                this.channels.unshift({ key: null, value: 'Nevybráno' });
            });
        }

        if (this.isCreate) {
            this.event = new Event();
            this.initForm();
        } else {
            const id = parseInt(this.route.snapshot.params.id, 10);

            this.eventService.getEvent(id).subscribe(event => {
                this.event = event;
                this.initForm();
            });
        }
    }

    submitForm(): void {
        const params = UpdateEventParams.create(this.form.value);

        if (this.isCreate) {
            this.eventService.createEvent(params).subscribe(event => {
                this.modalService.showNotification('Vytvoření akce', 'Akce byla úspěšně vytvořena.', 'lg').onClose.subscribe(() => {
                    this.router.navigate([`/app/events/${event.id}`]);
                });
            });
        } else {
            this.modalService.showQuestion('Editace akce', 'Opravdu si přejete upravit akci?', 'lg').onAccept.subscribe(() => {
                this.eventService.updateEvent(this.event.id, params).subscribe(event => {
                    this.modalService.showNotification('Editace akce', 'Akce byla úspěšně upravena.', 'lg').onClose.subscribe(() => {
                        this.event = event;

                        this.form.patchValue({
                            name: event.name,
                            from: event.from?.toLocaleISOString()?.slice(0, 16),
                            to: event.to?.toLocaleISOString()?.slice(0, 16),
                            description: event.description,
                            note: event.note,
                            expectedCount: event.expectedCount,
                            realCount: event.realCount,
                            discordChannelId: event.discordChannelId,
                            createDiscordChannel: false
                        });
                    });
                });
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    private initForm(): void {
        this.form = this.fb.group({
            name: [
                { value: this.event.name, disabled: !this.canManage },
                Validators.compose([Validators.required, Validators.maxLength(100)])
            ],
            from: [{ value: this.event.from?.toLocaleISOString()?.slice(0, 16), disabled: !this.canManage }, Validators.required],
            to: [{ value: this.event.to?.toLocaleISOString()?.slice(0, 16), disabled: !this.canManage }],
            description: [{ value: this.event.description, disabled: !this.canManage }],
            note: [{ value: this.event.note, disabled: !this.canManage }],
            expectedCount: [{ value: this.event.expectedCount, disabled: !this.canManage }],
            realCount: [{ value: this.event.realCount, disabled: !this.canManage }],
            discordChannelId: [{ value: this.event.discordChannelId, disabled: !this.canManage }, Validators.maxLength(20)],
            createDiscordChannel: [{ value: false, disabled: !this.canManage }]
        }, {
            validators: Validators.compose([
                Validations.checkDates('from', 'to')
            ])
        });

        if (this.appService.clientConfiguration.discordEnabled) {
            this.form.get('discordChannelId').valueChanges.subscribe(id => {
                if (!id || id.length === 0) {
                    this.form.get('createDiscordChannel').enable({ emitEvent: false });
                } else {
                    this.form.get('createDiscordChannel').disable({ emitEvent: false });
                }
            });

            this.form.get('createDiscordChannel').valueChanges.subscribe(create => {
                const roleIdFormField = this.form.get('discordChannelId');

                if (create) {
                    roleIdFormField.disable({ emitEvent: false });
                } else {
                    roleIdFormField.enable({ emitEvent: false });
                }
            });
        }
    }
}
