import { AccountEditComponent } from './account-edit/account-edit.component';
import { AccountsDashboardComponent } from './accounts-dashboard/accounts-dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    { path: '', component: AccountsDashboardComponent, data: { breadcrumb: 'Seznam' } },
    { path: 'add', component: AccountEditComponent, data: { breadcrumb: 'Vytvoření', isAdd: true } },
    { path: ':id', component: AccountEditComponent, data: { breadcrumb: 'Editace', isAdd: false } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AccountsRoutingModule { }
