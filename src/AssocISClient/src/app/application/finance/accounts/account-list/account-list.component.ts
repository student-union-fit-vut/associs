import { AccountListItem } from './../../../../core/models/finance/account/account-list-item';
import { AccountService } from './../../../../core/services/finance/account.service';
import { AccountSearchParams } from './../../../../core/models/finance/account/account-search-params';
import { Component } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Permissions } from 'src/app/core/enums';
import { ModalService } from 'src/app/shared/components/modal/modal.service';

@Component({
    selector: 'app-account-list',
    templateUrl: './account-list.component.html'
})
export class AccountListComponent {
    accounts: AccountListItem[];

    private filter: AccountSearchParams;

    constructor(
        private accountService: AccountService,
        private authService: AuthService,
        private modalService: ModalService
    ) { }

    get canModify(): boolean {
        return this.authService.loggedUser.hasPermission(Permissions.FinanceManager);
    }

    filterChanged(filter: AccountSearchParams): void {
        this.filter = filter;
        this.readData();
    }

    readData(): void {
        this.accounts = null;
        this.accountService.getAccountList(this.filter).subscribe(data => this.accounts = data);
    }

    removeAccount(id: number): void {
        this.modalService.showQuestion('Smazání účtu', 'Opravu si přejete tento účet smazat?', 'lg').onAccept.subscribe(() => {
            this.accountService.removeAccount(id).subscribe(_ => {
                this.modalService.showNotification('Smazání účtu', 'Účet byl úspěšně smazán.', 'lg')
                    .onClose.subscribe(() => this.readData());
            });
        });
    }
}
