import { ValidationHelper } from './../../../../core/lib/validators';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from './../../../../core/services/finance/account.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Account, UpdateAccountParams } from 'src/app/core/models';
import { ModalService } from 'src/app/shared/components/modal/modal.service';

@Component({
    selector: 'app-account-edit',
    templateUrl: './account-edit.component.html'
})
export class AccountEditComponent implements OnInit {
    account: Account;
    form: FormGroup;
    isCreate: boolean;

    constructor(
        private accountService: AccountService,
        private modalService: ModalService,
        private fb: FormBuilder,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.isCreate = this.route.routeConfig?.data?.isAdd ?? false;

        if (this.isCreate) {
            this.account = new Account();
            this.initForm();
        } else {
            const id = parseInt(this.route.snapshot.params.id, 10);
            this.accountService.getAccount(id).subscribe(account => {
                this.account = account;
                this.initForm();
            });
        }
    }

    submitForm(): void {
        const params = UpdateAccountParams.create(this.form.value);

        if (this.isCreate) {
            this.modalService.showQuestion('Vytvoření účtu', 'Opravdu si přejete vytvořit účet?').onAccept.subscribe(() => {
                this.accountService.createAccount(params).subscribe(account => {
                    this.modalService.showNotification('Vytvoření účtu', 'Účet byl úspěšně vytvořen.', 'lg').onClose.subscribe(_ => {
                        this.patchAccount(account);
                    });
                });
            });
        } else {
            this.modalService.showQuestion('Úprava účtu', 'Opravdu si přejete upravit tento účet?').onAccept.subscribe(() => {
                this.accountService.updateAccount(this.account.id, params).subscribe(account => {
                    this.modalService.showNotification('Úprava účtu', 'Účet byl úspěšně upraven.').onClose.subscribe(() => {
                        this.patchAccount(account);
                    });
                });
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    private initForm(): void {
        this.form = this.fb.group({
            name: [this.account.name, Validators.required],
            note: [this.account.note],
            accountNumber: [this.account.accountNumber, Validators.maxLength(50)],
            bankCode: [this.account.bankCode, Validators.maxLength(10)]
        });
    }

    private patchAccount(account: Account): void {
        this.account = account;

        this.form.patchValue({
            name: account.name,
            note: account.note,
            accountNumber: account.accountNumber,
            bankCode: account.bankCode
        });
    }
}
