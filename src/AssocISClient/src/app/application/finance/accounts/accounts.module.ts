import { AccountsRoutingModule } from './accounts-routing.module';
import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { AccountsDashboardComponent } from './accounts-dashboard/accounts-dashboard.component';
import { AccountFilterComponent } from './account-filter/account-filter.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AccountEditComponent } from './account-edit/account-edit.component';

@NgModule({
    declarations: [
        AccountsDashboardComponent,
        AccountFilterComponent,
        AccountListComponent,
        AccountEditComponent
    ],
    imports: [
        SharedModule,
        AccountsRoutingModule
    ]
})
export class AccountsModule { }
