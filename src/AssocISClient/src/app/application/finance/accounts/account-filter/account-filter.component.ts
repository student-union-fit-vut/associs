import { StorageService } from './../../../../core/services/storage.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AccountSearchParams } from 'src/app/core/models';

@Component({
    selector: 'app-account-filter',
    templateUrl: './account-filter.component.html'
})
export class AccountFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<AccountSearchParams>();

    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private storage: StorageService
    ) { }

    get defaultFilter(): AccountSearchParams { return new AccountSearchParams(); }

    ngOnInit(): void {
        const filter = AccountSearchParams.create(this.storage.read<AccountSearchParams>('AccountSearchParams') || {});

        this.initForm(filter);
        this.submitForm();
    }

    resetFilter(): boolean {
        const filter = this.defaultFilter;

        this.form.patchValue({
            nameQuery: filter.nameQuery,
            bankCode: filter.bankCode,
            accountNumberQuery: filter.accountNumberQuery
        });

        this.submitForm();
        return false;
    }

    submitForm(): void {
        const params = AccountSearchParams.create(this.form.value);

        this.filterChanged.emit(params);
        this.storage.save('AccountSearchParams', params.toInterface());
    }

    private initForm(filter: AccountSearchParams): void {
        this.form = this.fb.group({
            nameQuery: [filter.nameQuery],
            bankCode: [filter.bankCode],
            accountNumberQuery: [filter.accountNumberQuery]
        });
    }
}
