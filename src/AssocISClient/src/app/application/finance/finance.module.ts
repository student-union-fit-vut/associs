import { FinanceRoutingModule } from './finance-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [],
    imports: [
        SharedModule,
        FinanceRoutingModule
    ]
})
export class FinanceModule { }
