import { CategoriesRoutingModule } from './categories-routing.module';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
    declarations: [DashboardComponent],
    imports: [
        SharedModule,
        CategoriesRoutingModule
    ]
})
export class CategoriesModule { }
