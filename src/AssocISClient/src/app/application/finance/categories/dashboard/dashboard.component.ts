import { ValidationHelper } from './../../../../core/lib/validators';
import { UpdateCategoryParams } from './../../../../core/models/finance/category/update-category-params';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { availableIcons } from './../../../../core/lib/data';
import { TransactionCategoryService } from './../../../../core/services/finance';
import { TransactionCategory } from './../../../../core/models';
import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'src/app/shared/components/select/select-item';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { ValidationsService } from 'src/app/core/services/validations.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { Permissions } from 'src/app/core/enums';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    categories: TransactionCategory[];
    icons: SelectItem[];
    form: FormGroup;
    selectedCategory: TransactionCategory;

    constructor(
        private transactionCategoryService: TransactionCategoryService,
        private fb: FormBuilder,
        private modalService: ModalService,
        private validationsService: ValidationsService,
        private authService: AuthService
    ) { }

    get canModifyCategories(): boolean { return this.authService.loggedUser.hasPermission(Permissions.FinanceManager); }

    ngOnInit(): void {
        this.readData();
        this.icons = availableIcons.map(o => ({ key: o.icon, icon: o.icon, value: o.text }));
        this.icons.unshift({ key: null, icon: null, value: 'Nevybráno' });

        this.form = this.fb.group({
            name: [
                null,
                Validators.compose([Validators.maxLength(100), Validators.required]),
                [this.validationsService.transactionCategoryNameExists()]
            ],
            icon: [null]
        });
    }

    readData(): void {
        this.transactionCategoryService.getCategories().subscribe(categories => this.categories = categories);
    }

    selectCategory(category: TransactionCategory): void {
        this.selectedCategory = category;

        this.form.patchValue({
            name: category.name,
            icon: category.icon
        });
    }

    addCategory(): boolean {
        const params = new UpdateCategoryParams(this.form.value.name, true, this.form.value.icon);

        this.transactionCategoryService.createCategory(params).subscribe(_ => {
            this.modalService.showNotification('Vytvoření kategorie', 'Kategorie byla úspěšně vytvořena.', 'lg').onClose.subscribe(__ => {
                this.readData();
            });
        });

        return false;
    }

    updateCategory(): boolean {
        const params = new UpdateCategoryParams(
            this.form.value.name,
            this.form.value.name !== this.selectedCategory.name,
            this.form.value.icon
        );

        this.modalService.showQuestion('Úprava kategorie', 'Opravdu si přeješ upravit kategorii?', 'lg').onAccept.subscribe(_ => {
            this.transactionCategoryService.updateCategory(this.selectedCategory.id, params).subscribe(__ => {
                this.modalService.showNotification('Úprava kategorie', 'Kategorie byla úspěšně upravena.', 'lg').onClose.subscribe(() => {
                    this.readData();
                });
            });
        });

        return false;
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    removeCategory(category: TransactionCategory): void {
        this.modalService.showQuestion('Smazání kategorie', 'Opravdu si přeješ smazat kategorii?', 'lg').onAccept.subscribe(_ => {
            this.transactionCategoryService.deleteCategory(category.id).subscribe(__ => {
                this.modalService.showNotification('Smazání kategorie', 'Kategorie byla úspěšně smazána.', 'lg').onClose.subscribe(() => {
                    this.readData();
                });
            });
        });
    }
}
