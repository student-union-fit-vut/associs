import { SupplierService } from './../../../../core/services/finance/supplier.service';
import { Component } from '@angular/core';
import { SupplierListItem, SupplierSearchParams } from 'src/app/core/models';
import { AuthService } from 'src/app/core/services/auth.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { Permissions } from 'src/app/core/enums';

@Component({
    selector: 'app-supplier-list',
    templateUrl: './supplier-list.component.html'
})
export class SupplierListComponent {
    suppliers: SupplierListItem[];
    private filter: SupplierSearchParams;

    constructor(
        private authService: AuthService,
        private supplierService: SupplierService,
        private modalService: ModalService
    ) { }

    get canModify(): boolean {
        return this.authService.loggedUser.hasPermission(Permissions.FinanceManager);
    }

    filterChanged(filter: SupplierSearchParams): void {
        this.filter = filter;
        this.readData();
    }

    readData(): void {
        this.suppliers = null;
        this.supplierService.getSupplierList(this.filter).subscribe(suppliers => this.suppliers = suppliers);
    }

    removeSupplier(item: SupplierListItem): void {
        this.modalService.showQuestion('Smazání dodavatele', 'Opravdu si přejete smazat dodavatele?', 'lg').onAccept.subscribe(() => {
            this.supplierService.removeSupplier(item.id).subscribe(() => {
                this.modalService.showNotification('Smazání dodavatele', 'Dodavatel byl úspěšně smazán.', 'lg').onClose.subscribe(() => {
                    this.readData();
                });
            });
        });
    }
}
