import { SupplierEditComponent } from './supplier-edit/supplier-edit.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SupplierDashboardComponent } from './supplier-dashboard/supplier-dashboard.component';

const routes: Routes = [
    { path: '', component: SupplierDashboardComponent, data: { breadcrumb: 'Seznam' } },
    { path: 'add', component: SupplierEditComponent, data: { breadcrumb: 'Vytvoření', isAdd: true } },
    { path: ':id', component: SupplierEditComponent, data: { breadcrumb: 'Editace', isAdd: false } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SuppliersRoutingModule { }
