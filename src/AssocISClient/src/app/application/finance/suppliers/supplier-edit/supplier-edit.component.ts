import { ValidationHelper, Validations } from './../../../../core/lib/validators';
import { ActivatedRoute } from '@angular/router';
import { ModalService } from './../../../../shared/components/modal/modal.service';
import { SupplierService } from './../../../../core/services/finance';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Supplier, UpdateSupplierParams } from 'src/app/core/models';

@Component({
    selector: 'app-supplier-edit',
    templateUrl: './supplier-edit.component.html'
})
export class SupplierEditComponent implements OnInit {
    supplier: Supplier;
    form: FormGroup;
    isCreate: boolean;

    constructor(
        private supplierService: SupplierService,
        private modalService: ModalService,
        private fb: FormBuilder,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.isCreate = this.route.routeConfig?.data?.isAdd ?? false;

        if (this.isCreate) {
            this.supplier = new Supplier();
            this.initForm();
        } else {
            const id = parseInt(this.route.snapshot.params.id, 10);

            this.supplierService.getSupplier(id).subscribe(supplier => {
                this.supplier = supplier;
                this.initForm();
            });
        }
    }

    submitForm(): void {
        const params = UpdateSupplierParams.create(this.form.value);

        if (this.isCreate) {
            this.modalService.showQuestion('Vytvoření dodavatele', 'Opravdu si přejete vytvořit dodavatele?', 'lg').onAccept
                .subscribe(() => {
                    this.supplierService.createSuplier(params).subscribe(supplier => {
                        this.modalService.showNotification('Vytvoření dodavatele', 'Dodavatel byl úspěšně vytvořen', 'lg').onClose
                            .subscribe(() => this.patchSupplier(supplier));
                    });
                });
        } else {
            this.modalService.showQuestion('Úprava dodavatele', 'Opravdu si přejete upravit tohoto dodavatele?', 'lg').onAccept
                .subscribe(() => {
                    this.supplierService.updateSuplier(this.supplier.id, params).subscribe(supplier => {
                        this.modalService.showNotification('Úprava dodavatele', 'Úprava dodavatele byla úspěšná.', 'lg').onClose
                            .subscribe(() => this.patchSupplier(supplier));
                    });
                });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    private initForm(): void {
        this.form = this.fb.group({
            name: [this.supplier.name, Validators.required],
            contact: [this.supplier.contact],
            webLink: [this.supplier.webLink, Validations.checkUrl()],
            note: [this.supplier.note]
        });
    }

    private patchSupplier(supplier: Supplier): void {
        this.supplier = supplier;

        this.form.patchValue({
            name: supplier.name,
            contact: supplier.contact,
            webLink: supplier.webLink,
            note: supplier.note
        });
    }
}
