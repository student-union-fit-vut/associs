import { SuppliersRoutingModule } from './suppliers-routing.module';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { SupplierDashboardComponent } from './supplier-dashboard/supplier-dashboard.component';
import { SupplierFilterComponent } from './supplier-filter/supplier-filter.component';
import { SupplierListComponent } from './supplier-list/supplier-list.component';
import { SupplierEditComponent } from './supplier-edit/supplier-edit.component';

@NgModule({
    declarations: [SupplierDashboardComponent, SupplierFilterComponent, SupplierListComponent, SupplierEditComponent],
    imports: [
        SharedModule,
        SuppliersRoutingModule
    ]
})
export class SuppliersModule { }
