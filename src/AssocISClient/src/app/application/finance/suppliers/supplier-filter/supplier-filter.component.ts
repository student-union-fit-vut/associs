import { FormGroup, FormBuilder } from '@angular/forms';
import { SupplierSearchParams } from './../../../../core/models';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
    selector: 'app-supplier-filter',
    templateUrl: './supplier-filter.component.html'
})
export class SupplierFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<SupplierSearchParams>();
    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private storage: StorageService
    ) { }

    get defaultFilter(): SupplierSearchParams { return new SupplierSearchParams(); }

    ngOnInit(): void {
        const filter = SupplierSearchParams.create(this.storage.read<SupplierSearchParams>('SupplierSearchParams') || {});

        this.initFilter(filter);
        this.submitForm();
    }

    resetForm(): boolean {
        const filter = this.defaultFilter;

        this.form.patchValue({
            nameQuery: filter.nameQuery
        });

        this.submitForm();
        return false;
    }

    submitForm(): void {
        const filter = SupplierSearchParams.create(this.form.value);

        this.filterChanged.emit(filter);
        this.storage.save('SupplierSearchParams', filter);
    }

    private initFilter(filter: SupplierSearchParams): void {
        this.form = this.fb.group({
            nameQuery: [filter.nameQuery]
        });
    }
}
