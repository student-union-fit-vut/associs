import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
        path: 'transactions',
        loadChildren: () => import('./transactions/transactions.module').then(mod => mod.TransactionsModule),
        data: { breadcrumb: 'Transakce' }
    },
    {
        path: 'accounts',
        loadChildren: () => import('./accounts/accounts.module').then(mod => mod.AccountsModule),
        data: { breadcrumb: 'Účty' }
    },
    {
        path: 'suppliers',
        loadChildren: () => import('./suppliers/suppliers.module').then(mod => mod.SuppliersModule),
        data: { breadcrumb: 'Dodavatelé' }
    },
    {
        path: 'category',
        loadChildren: () => import('./categories/categories.module').then(mod => mod.CategoriesModule),
        data: { breadcrumb: 'Kategorie' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FinanceRoutingModule { }
