import { TransactionsRoutingModule } from './transactions-routing.module';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { TransactionDashboardComponent } from './transaction-dashboard/transaction-dashboard.component';
import { TransactionFilterComponent } from './transaction-filter/transaction-filter.component';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { TransactionEditComponent } from './transaction-edit/transaction-edit.component';
import { TransactionReportsComponent } from './reports/transaction-reports/transaction-reports.component';
import { ReportByTypeComponent } from './reports/report-by-type/report-by-type.component';
import { ReportBySupplierComponent } from './reports/report-by-supplier/report-by-supplier.component';
import { ReportByCategoryComponent } from './reports/report-by-category/report-by-category.component';
import { TranactionAttachmentsComponent } from './tranaction-attachments/tranaction-attachments.component';

@NgModule({
    declarations: [
        TransactionDashboardComponent,
        TransactionFilterComponent,
        TransactionListComponent,
        TransactionEditComponent,
        TransactionReportsComponent,
        ReportByTypeComponent,
        ReportBySupplierComponent,
        ReportByCategoryComponent,
        TranactionAttachmentsComponent
    ],
    imports: [
        SharedModule,
        TransactionsRoutingModule
    ]
})
export class TransactionsModule { }
