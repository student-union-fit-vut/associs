import { ModalService } from './../../../../shared/components/modal/modal.service';
import { TransactionService } from '../../../../core/services/finance';
import { Support } from './../../../../core/lib/support';
import { TransactionAttachmentType, TransactionAttachmentTypeTexts } from './../../../../core/enums/transaction-attachment-type';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Transaction } from './../../../../core/models/finance/transactions/transaction';
import { Component, Input, OnInit } from '@angular/core';
import { SelectItem } from '../../../../shared/components/select/select-item';
import { Attachment } from '../../../../core/models/finance/transactions/attachment';

@Component({
    selector: 'app-tranaction-attachments',
    templateUrl: './tranaction-attachments.component.html',
    styleUrls: ['./tranaction-attachments.component.scss']
})
export class TranactionAttachmentsComponent implements OnInit {
    @Input() transaction: Transaction;

    form: FormGroup;
    attachmentTypes: SelectItem[] = [];

    constructor(
        private fb: FormBuilder,
        private transactionService: TransactionService,
        private modalService: ModalService
    ) { }

    ngOnInit(): void {
        this.attachmentTypes = Object.keys(TransactionAttachmentType)
            .filter(o => !isNaN(parseInt(o, 10)))
            .map(o => parseInt(o, 10))
            .map(o => ({ key: o, value: TransactionAttachmentTypeTexts[Support.getEnumKeyByValue(TransactionAttachmentType, o)] }));

        this.form = this.fb.group({
            type: [TransactionAttachmentType.Invoice, Validators.required]
        });
    }

    removeAttachment(id: number): void {
        this.modalService.showQuestion('Smazání přílohy', 'Opravdu si přejete smazat přílohu?', 'lg').onAccept.subscribe(() => {
            this.transactionService.removeAttachment(this.transaction.id, id).subscribe(() => {
                this.transaction.attachments = this.transaction.attachments.filter(o => o.id !== id);
            });
        });
    }

    getData(attachment: Attachment): void {
        this.transactionService.getAttachment(this.transaction.id, attachment.id).subscribe(data => {
            Support.downloadBinaryData(data, attachment.name);
        });
    }

    submitUpload(): void {
        const fileElem = document.getElementById('attachmentFile') as HTMLInputElement;
        if (fileElem.files.length === 0) { return; }

        const file = fileElem.files[0];
        const type = this.form.get('type').value;

        this.transactionService.uploadAttachment(this.transaction.id, type, file).subscribe(attachment => {
            this.transaction.attachments.push(attachment);
        });
    }
}
