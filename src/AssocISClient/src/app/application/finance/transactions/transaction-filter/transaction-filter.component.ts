import { Validations, ValidationHelper } from './../../../../core/lib/validators';
import { Support } from './../../../../core/lib/support';
import { TransactionTypeTexts } from './../../../../core/enums/transaction-type';
import { Dictionary } from './../../../../core/models/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TransactionCategoryService, AccountService } from './../../../../core/services/finance';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AccountSearchParams, SupplierSearchParams, TransactionSearchParams } from 'src/app/core/models';
import { SupplierService } from 'src/app/core/services/finance';
import { TransactionType } from 'src/app/core/enums';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
    selector: 'app-transaction-filter',
    templateUrl: './transaction-filter.component.html'
})
export class TransactionFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<TransactionSearchParams>();

    form: FormGroup;

    accounts: Dictionary<number, string> = [];
    suppliers: Dictionary<number, string> = [];
    categories: Dictionary<number, string> = [];
    transactionTypes: Dictionary<number, string> = [];

    constructor(
        private supplierService: SupplierService,
        private accountService: AccountService,
        private categoryService: TransactionCategoryService,
        private fb: FormBuilder,
        private storage: StorageService
    ) { }

    get defaultFilter(): TransactionSearchParams { return new TransactionSearchParams(); }

    ngOnInit(): void {
        this.supplierService.getSupplierList(SupplierSearchParams.empty)
            .subscribe(suppliers => this.suppliers = suppliers.map(o => ({ key: o.id, value: o.name })));
        this.accountService.getAccountList(AccountSearchParams.empty)
            .subscribe(accounts => this.accounts = accounts.map(o => ({ key: o.id, value: o.name })));
        this.categoryService.getCategories().subscribe(categories => this.categories = categories.map(o => ({ key: o.id, value: o.name })));

        this.transactionTypes = Object.keys(TransactionType)
            .filter(o => !isNaN(parseInt(o, 10)))
            .map(o => parseInt(o, 10))
            .map(o => ({ key: o, value: TransactionTypeTexts[Support.getEnumKeyByValue(TransactionType, o)] }));

        const filter = TransactionSearchParams.create(this.storage.read<TransactionSearchParams>('TransactionSearchParams') || {});
        this.initForm(filter);
        this.submitForm();
    }

    resetFilter(): boolean {
        const filter = this.defaultFilter;

        this.form.patchValue({
            processedFrom: filter.processedFrom?.toLocaleISOString()?.slice(0, 16),
            processedTo: filter.processedTo?.toLocaleISOString()?.slice(0, 16),
            amountFrom: filter.amountFrom,
            amountTo: filter.amountTo,
            transactionTypes: filter.transactionTypes,
            nameQuery: filter.nameQuery,
            accounts: filter.accounts,
            suppliers: filter.suppliers,
            categories: filter.categories,
            onlyWithAttachments: filter.onlyWithAttachments
        });

        this.submitForm();
        return false;
    }

    submitForm(): void {
        const filter = TransactionSearchParams.create(this.form.value);

        this.filterChanged.emit(filter);
        this.storage.save('TransactionSearchParams', filter);
    }

    categoryClicked(categoryId: number): void {
        this.form.get('categories').patchValue([categoryId]);
        this.submitForm();
    }

    isInvalid(controlId: string, errorId?: string): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    private initForm(filter: TransactionSearchParams): void {
        this.form = this.fb.group({
            processedFrom: [filter.processedFrom?.toLocaleISOString()?.slice(0, 16)],
            processedTo: [filter.processedTo?.toLocaleISOString()?.slice(0, 16)],
            amountFrom: [filter.amountFrom],
            amountTo: [filter.amountTo],
            transactionTypes: [filter.transactionTypes],
            nameQuery: [filter.nameQuery],
            accounts: [filter.accounts],
            suppliers: [filter.suppliers],
            categories: [filter.categories],
            onlyWithAttachments: [filter.onlyWithAttachments]
        }, {
            validators: Validators.compose([
                Validations.checkDates('processedFrom', 'processedTo')
            ])
        });
    }
}
