import { ChartOptions } from 'chart.js';
import { SingleDataSet, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { Component, OnInit } from '@angular/core';
import { FinanceReportService } from 'src/app/core/services/finance';
import { SupplierListItem } from 'src/app/core/models';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
    selector: 'app-report-by-supplier',
    templateUrl: './report-by-supplier.component.html'
})
export class ReportBySupplierComponent implements OnInit {
    labels: string[];
    isLoaded = false;
    amountData: SingleDataSet;
    countData: SingleDataSet;

    options: ChartOptions = {
        tooltips: {
            enabled: true
        },
        plugins: {
            datalabels: {
                formatter: (value, ctx) => {
                    const label = ctx.chart.data.labels[ctx.dataIndex];
                    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                    return `${label}\n${value}`;
                },
            },
        }
    };
    plugins: any = [pluginDataLabels];

    constructor(
        private reportService: FinanceReportService
    ) { }

    ngOnInit(): void {
        this.reportService.getReport<SupplierListItem>('supplier').subscribe(data => {
            this.labels = data.map(o => (o.groupValue?.name) ?? 'Neznámý dodavatel');
            this.amountData = data.map(o => o.amount);
            this.countData = data.map(o => o.count);
            this.isLoaded = true;

            monkeyPatchChartJsTooltip();
            monkeyPatchChartJsLegend();
        });
    }

}
