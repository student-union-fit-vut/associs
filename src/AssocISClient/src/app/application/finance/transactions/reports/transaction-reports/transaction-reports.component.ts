import { Component } from '@angular/core';

@Component({
    selector: 'app-transaction-reports',
    templateUrl: './transaction-reports.component.html'
})
export class TransactionReportsComponent {
    activeTabId: string;
}
