import { TransactionTypeTexts } from './../../../../../core/enums/transaction-type';
import { Component, OnInit } from '@angular/core';
import { FinanceReportService } from 'src/app/core/services/finance';
import { SingleDataSet, monkeyPatchChartJsTooltip, monkeyPatchChartJsLegend } from 'ng2-charts';
import { ChartOptions } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
    selector: 'app-report-by-type',
    templateUrl: './report-by-type.component.html'
})
export class ReportByTypeComponent implements OnInit {
    labels: string[];
    isLoaded = false;
    amountData: SingleDataSet;
    countData: SingleDataSet;

    options: ChartOptions = {
        tooltips: {
            enabled: true
        },
        plugins: {
            datalabels: {
                formatter: (value, ctx) => {
                    const label = ctx.chart.data.labels[ctx.dataIndex];
                    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                    return `${label}\n${value}`;
                },
            },
        }
    };
    plugins: any = [pluginDataLabels];

    constructor(
        private reportService: FinanceReportService
    ) { }

    ngOnInit(): void {
        this.labels = Object.values(TransactionTypeTexts);

        this.reportService.getReport<number>('type').subscribe(data => {
            this.amountData = data.map(o => o.amount);
            this.countData = data.map(o => o.count);
            this.isLoaded = true;

            monkeyPatchChartJsTooltip();
            monkeyPatchChartJsLegend();
        });
    }

}
