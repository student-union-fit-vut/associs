import { ChartOptions } from 'chart.js';
import { SingleDataSet, monkeyPatchChartJsTooltip, monkeyPatchChartJsLegend } from 'ng2-charts';
import { Component, OnInit } from '@angular/core';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { FinanceReportService } from 'src/app/core/services/finance';
import { TransactionCategory } from 'src/app/core/models';

@Component({
    selector: 'app-report-by-category',
    templateUrl: './report-by-category.component.html'
})
export class ReportByCategoryComponent implements OnInit {
    labels: string[];
    isLoaded = false;
    amountData: SingleDataSet;
    countData: SingleDataSet;

    options: ChartOptions = {
        tooltips: {
            enabled: true
        },
        plugins: {
            datalabels: {
                formatter: (value, ctx) => {
                    const label: any = ctx.chart.data.labels[ctx.dataIndex];
                    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                    return `${label}\n${value}`;
                },
            },
        }
    };
    plugins: any = [pluginDataLabels];

    constructor(private reportService: FinanceReportService) { }

    ngOnInit(): void {
        this.reportService.getReport<TransactionCategory>('category').subscribe(data => {
            this.labels = data.map(o => (o.groupValue?.name) ?? 'Neznámá kategorie');
            this.amountData = data.map(o => o.amount);
            this.countData = data.map(o => o.count);
            this.isLoaded = true;

            monkeyPatchChartJsTooltip();
            monkeyPatchChartJsLegend();
        });
    }

}
