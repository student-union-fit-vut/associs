import { TransactionEditComponent } from './transaction-edit/transaction-edit.component';
import { TransactionDashboardComponent } from './transaction-dashboard/transaction-dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { TransactionReportsComponent } from './reports/transaction-reports/transaction-reports.component';

const routes: Routes = [
    { path: '', component: TransactionDashboardComponent, data: { breadcrumb: 'Seznam' } },
    { path: 'add', component: TransactionEditComponent, data: { breadcrumb: 'Vytvoření', isAdd: true } },
    { path: 'reports', component: TransactionReportsComponent, data: { breadcrumb: 'Reporty' } },
    { path: ':id', component: TransactionEditComponent, data: { breadcrumb: 'Editace', isAdd: false } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TransactionsRoutingModule { }
