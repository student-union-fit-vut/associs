import { PaginationParams } from './../../../../shared/components/data-list/models';
import { ModalService } from './../../../../shared/components/modal/modal.service';
import { TransactionService } from './../../../../core/services/finance/transaction.service';
import { TransactionSearchParams } from 'src/app/core/models';
import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';

@Component({
    selector: 'app-transaction-list',
    templateUrl: './transaction-list.component.html'
})
export class TransactionListComponent {
    @Output() categoryItemClick = new EventEmitter<number>();
    @ViewChild('list', { static: false }) list: DataListComponent;

    private filter: TransactionSearchParams;

    constructor(
        private transactionService: TransactionService,
        private modalService: ModalService
    ) { }

    filterChanged(filter: TransactionSearchParams): void {
        this.filter = filter;
        if (this.list) { this.list.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.transactionService.getTransactionList(this.filter, pagination).subscribe(data => this.list.setData(data));
    }

    removeTransaction(id: number): void {
        this.modalService.showQuestion('Smazat transakci',
            'Opravdu si přejete tuto transakci smazat?<br><b>Tato akce je nevratná!</b>', 'lg')
            .onAccept.subscribe(() => this.transactionService.removeTransaction(id).subscribe(() => {
                this.modalService.showNotification('Smazat transakci', 'Transakce byla úspěšně smazána.', 'lg')
                    .onClose.subscribe(() => this.list.onChange());
            }));
    }

    onCategoryClick(categoryId: number): void {
        this.categoryItemClick.emit(categoryId);
    }
}
