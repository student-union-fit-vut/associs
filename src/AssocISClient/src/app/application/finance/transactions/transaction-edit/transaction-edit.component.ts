import { ValidationHelper } from './../../../../core/lib/validators';
import { UpdateTransactionParams } from './../../../../core/models/finance/transactions/update-transaction-params';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalService } from './../../../../shared/components/modal/modal.service';
import { Dictionary } from './../../../../core/models/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AccountSearchParams, SupplierSearchParams, Transaction } from 'src/app/core/models';
import { AccountService, SupplierService, TransactionCategoryService, TransactionService } from 'src/app/core/services/finance';
import { TransactionType, TransactionTypeTexts } from 'src/app/core/enums';
import { Support } from 'src/app/core/lib/support';
import { SelectItem } from 'src/app/shared/components/select/select-item';

@Component({
    selector: 'app-transaction-edit',
    templateUrl: './transaction-edit.component.html'
})
export class TransactionEditComponent implements OnInit {
    transaction: Transaction;
    isCreate: boolean;
    form: FormGroup;
    activeTabId: string;

    accounts: Dictionary<number, string> = [];
    suppliers: Dictionary<number, string> = [];
    categories: Dictionary<number, string> = [];
    transactionTypes: SelectItem[] = [];

    constructor(
        private transactionService: TransactionService,
        private accountService: AccountService,
        private supplierService: SupplierService,
        private categoryService: TransactionCategoryService,
        private modalService: ModalService,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.supplierService.getSupplierList(SupplierSearchParams.empty)
            .subscribe(suppliers => this.suppliers = suppliers.map(o => ({ key: o.id, value: o.name })));
        this.accountService.getAccountList(AccountSearchParams.empty)
            .subscribe(accounts => this.accounts = accounts.map(o => ({ key: o.id, value: o.name })));
        this.categoryService.getCategories().subscribe(categories => this.categories = categories.map(o => ({ key: o.id, value: o.name })));

        this.transactionTypes = Object.keys(TransactionType)
            .filter(o => !isNaN(parseInt(o, 10)))
            .map(o => parseInt(o, 10))
            .map(o => ({ key: o, value: TransactionTypeTexts[Support.getEnumKeyByValue(TransactionType, o)] }));

        this.isCreate = this.route.routeConfig?.data?.isAdd ?? false;

        if (this.isCreate) {
            this.transaction = new Transaction();
            this.initForm();
        } else {
            const id = parseInt(this.route.snapshot.params.id, 10);

            this.transactionService.getTransaction(id).subscribe(transaction => {
                this.transaction = transaction;
                this.initForm();
            });
        }
    }

    submitForm(): void {
        if (!this.form.touched) { return; }

        const params = new UpdateTransactionParams(
            this.form.value.processedAt,
            this.form.value.amount,
            this.form.value.type,
            this.form.value.name,
            this.form.value.note,
            this.form.value.account,
            this.form.value.supplier,
            this.form.value.categories
        );

        if (this.isCreate) {
            this.modalService.showQuestion('Vytvoření transakce', 'Opravdu si přejete vytvořit transakci?', 'lg').onAccept.subscribe(() => {
                this.transactionService.createTransaction(params).subscribe(transaction => {
                    this.router.navigate([`/app/finance/transactions/${transaction.id}`]);
                });
            });
        } else {
            this.modalService.showQuestion('Úprava tranakce', 'Opravdu si přejete upravit tranakci?', 'lg').onAccept.subscribe(() => {
                this.transactionService.updateTransaction(this.transaction.id, params).subscribe(_ => {
                    this.modalService.showNotification('Úprava transakce', 'Transakce byla úspěšně upravena', 'lg');
                });
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    private initForm(): void {
        this.form = this.fb.group({
            processedAt: [this.transaction.processedAt?.toLocaleISOString()?.slice(0, 16), Validators.required],
            amount: [this.transaction.amount, Validators.required],
            type: [this.transaction.type, Validators.required],
            name: [this.transaction.name, Validators.required],
            note: [this.transaction.note],
            account: [this.transaction.account?.id, Validators.required],
            supplier: [this.transaction.supplier?.id],
            categories: [this.transaction.categories?.map(o => o.id)]
        });
    }
}
