import { AssetDetail } from './../../../core/models/assets/asset-detail';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-asset-loan-dashboard',
    templateUrl: './asset-loan-dashboard.component.html'
})
export class AssetLoanDashboardComponent {
    @Input() asset: AssetDetail;
}
