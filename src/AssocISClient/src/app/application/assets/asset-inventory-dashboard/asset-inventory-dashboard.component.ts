import { AssetDetail } from './../../../core/models/assets/asset-detail';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-asset-inventory-dashboard',
    templateUrl: './asset-inventory-dashboard.component.html'
})
export class AssetInventoryDashboardComponent {
    @Input() asset: AssetDetail;
}
