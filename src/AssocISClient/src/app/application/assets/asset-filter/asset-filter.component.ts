import { SelectItems } from './../../../shared/components/select/select-item';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AssetSearchParams } from 'src/app/core/models';
import { StorageService } from 'src/app/core/services/storage.service';
import { AssetService } from 'src/app/core/services/asset.service';

@Component({
    selector: 'app-asset-filter',
    templateUrl: './asset-filter.component.html'
})
export class AssetFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<AssetSearchParams>();

    form: FormGroup;
    categories: SelectItems = [];
    availability: SelectItems = [
        { key: null, value: 'Nerozhoduje' },
        { key: true, value: 'Dostupné' },
        { key: false, value: 'Nedostupné' }
    ];

    constructor(
        private fb: FormBuilder,
        private storage: StorageService,
        private assetService: AssetService
    ) { }

    ngOnInit(): void {
        this.assetService.getCategories()
            .subscribe(categories => this.categories = categories.map(o => ({ key: o.id, value: o.name, icon: o.icon })));
        const filter = AssetSearchParams.create(this.storage.read<AssetSearchParams>('AssetSearchParams') || {});

        this.initFilter(filter);
        this.submitFilter();
    }

    resetFilter(): boolean {
        const filter = AssetSearchParams.empty;

        this.form.patchValue({
            nameQuery: filter.nameQuery,
            isAvailable: filter.isAvailable,
            categories: filter.categories,
            includeRemoved: filter.includeRemoved
        });

        this.submitFilter();
        return false;
    }

    submitFilter(): void {
        const params = AssetSearchParams.create(this.form.value);

        this.filterChanged.emit(params);
        this.storage.save('AssetSearchParams', params);
    }

    categoryClick(categoryId: number): void {
        this.form.get('categories').patchValue([categoryId]);
        this.submitFilter();
    }

    private initFilter(filter: AssetSearchParams): void {
        this.form = this.fb.group({
            nameQuery: [filter.nameQuery],
            isAvailable: [filter.isAvailable],
            categories: [filter.categories],
            includeRemoved: [filter.includeRemoved]
        });
    }
}
