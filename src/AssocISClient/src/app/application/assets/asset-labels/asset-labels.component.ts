import { Component, Input, OnInit } from '@angular/core';
import { AssetDetail } from 'src/app/core/models';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as html2canvas from 'html2canvas';
import { saveAs } from 'file-saver';

@Component({
    selector: 'app-asset-labels',
    templateUrl: './asset-labels.component.html',
    styleUrls: ['./asset-labels.component.scss']
})
export class AssetLabelsComponent implements OnInit {
    @Input() asset: AssetDetail;

    form: FormGroup;
    onlyQr = false;

    constructor(private fb: FormBuilder) { }

    // URL in QR code
    get qrUrl(): string {
        return location.href;
    }

    get categoryNames(): string {
        return this.asset.categories.map(o => o.name).join(', ');
    }

    ngOnInit(): void {
        this.form = this.fb.group({ onlyQr: [false] });
        this.form.valueChanges.subscribe(value => this.onlyQr = value.onlyQr as boolean);
    }

    screenshot(): void {
        html2canvas.default(document.getElementById('screenshotable')).then(canvas => {
            const filename = `Asset_${this.asset.id}.png`;
            canvas.toBlob(blob => {
                saveAs(blob, filename);
            }, 'image/png');
        });
    }
}
