import { DateTime } from './../../../core/models/datetime';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { SelectItems } from 'src/app/shared/components/select/select-item';
import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';
import { ValidationHelper } from 'src/app/core/lib/validators';
import { CreateInventoryParams, InventoryAsset } from 'src/app/core/models';
import { AssetService } from 'src/app/core/services/asset.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';

@Component({
    selector: 'app-asset-inventory-edit',
    templateUrl: './asset-inventory-edit.component.html'
})
export class AssetInventoryEditComponent implements OnInit {
    users: SelectItems = [];
    assets: SelectItems = [];

    form: FormGroup;

    constructor(
        private dataService: DataService,
        private fb: FormBuilder,
        private assetService: AssetService,
        private modalService: ModalService
    ) { }

    get formAssets(): FormArray { return this.form.get('assets') as FormArray; }

    ngOnInit(): void {
        this.dataService.getUserSelectList().subscribe(users => this.users = users);
        this.dataService.getAssetsSelectList().subscribe(a => this.assets = a);

        const assets = [this.createAssetRow()];
        this.form = this.fb.group({
            user: [null, Validators.required],
            at: [null],
            assets: this.fb.array(assets, Validators.required)
        });
    }

    createAssetRow(): FormGroup {
        return this.fb.group({
            asset: [null, Validators.required],
            count: [0, Validators.compose([Validators.required, Validators.min(0)])],
            note: ['']
        });
    }

    addAssetRow(): boolean {
        this.formAssets.push(this.createAssetRow());
        return false;
    }

    removeAssetRow(index: number): boolean {
        this.formAssets.removeAt(index);
        return false;
    }

    submitForm(): void {
        const params = new CreateInventoryParams(
            this.form.value.user,
            this.form.value.at || DateTime.now.toUtcISOString(),
            this.formAssets.value.map(o => new InventoryAsset(o.asset, o.count, o.note))
        );

        this.assetService.createInventory(params).subscribe(_ => {
            this.modalService.showNotification('Provedení inventury', 'Inventura byla úspěšně zaevidována.')
                .onClose.subscribe(() => this.clearInventory());
        });
    }

    isInvalid(controlId: string, errorId: string = null, form: AbstractControl = null): boolean {
        return ValidationHelper.isInvalid(form || this.form, controlId, errorId);
    }

    clearInventory(): void {
        this.form.patchValue({
            user: null,
            at: null
        });

        this.formAssets.clear();
        this.addAssetRow();

        this.form.markAsUntouched();
        for (const control of this.formAssets.controls) {
            control.markAsUntouched();
        }
    }

}
