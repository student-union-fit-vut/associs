import { AssetFlags, AssetFlagsTexts } from './../../../core/enums/asset-flags';
import { SelectHelpers } from './../../../core/lib/support';
import { Dictionary } from './../../../core/models/common';
import { ValidationHelper } from './../../../core/lib/validators';
import { UpdateAssetParams } from './../../../core/models/assets/update-asset-params';
import { AuthService } from './../../../core/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectItems } from './../../../shared/components/select/select-item';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetDetail } from './../../../core/models/assets/asset-detail';
import { Component, OnInit } from '@angular/core';
import { AssetService } from 'src/app/core/services/asset.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { Permissions } from 'src/app/core/enums';
import { CreateAssetParams } from 'src/app/core/models';

@Component({
    selector: 'app-asset-edit',
    templateUrl: './asset-edit.component.html'
})
export class AssetEditComponent implements OnInit {
    asset: AssetDetail;
    isCreate: boolean;
    form: FormGroup;
    activeTabId: string;
    categories: SelectItems = [];
    assetFlags: Dictionary<number, string> = [];

    constructor(
        private assetService: AssetService,
        private modalService: ModalService,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private router: Router,
        private authService: AuthService
    ) { }

    get canModify(): boolean {
        return this.authService.loggedUser.hasPermission(Permissions.PropertyManager);
    }

    ngOnInit(): void {
        this.assetService.getCategories()
            .subscribe(categories => this.categories = categories.map(o => ({ icon: o.icon, key: o.id, value: o.name })));
        this.isCreate = this.route.routeConfig?.data?.isAdd ?? false;
        this.assetFlags = SelectHelpers.getSelectItemsFromEnum(AssetFlags, AssetFlagsTexts);

        if (this.isCreate) {
            this.asset = new AssetDetail();
            this.initForm();
        } else {
            const id = parseInt(this.route.snapshot.params.id, 10);

            this.assetService.getAsset(id).subscribe(asset => {
                this.asset = asset;
                this.initForm();
            });
        }
    }

    submitForm(): void {
        if (this.isCreate) {
            const params = CreateAssetParams.create(this.form.value);

            this.assetService.createAsset(params).subscribe(asset => {
                this.modalService.showNotification('Vytvoření položky', 'Položka byla úspěšně vytvořena.').onClose.subscribe(() => {
                    this.router.navigate([`/app/assets/${asset.id}`]);
                });
            });
        } else {
            const params = UpdateAssetParams.create(this.form.value);

            this.modalService.showQuestion('Úprava položky', 'Opravdu si přejete upravit tuhle položku?').onAccept.subscribe(() => {
                this.assetService.updateAsset(this.asset.id, params).subscribe(asset => {
                    this.modalService.showNotification('Úprava položky', 'Položka byla úspěšně upravena').onClose.subscribe(() => {
                        this.asset = asset;
                    });
                });
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    private initForm(): void {
        this.form = this.fb.group({
            name: [
                { value: this.asset.name, disabled: !this.canModify },
                Validators.compose([Validators.required, Validators.maxLength(100)])
            ],
            description: [{ value: this.asset.description, disabled: !this.canModify }],
            location: [{ value: this.asset.location, disabled: !this.canModify }],
            categories: [{ value: (this.asset.categories || []).map(o => o.id), disabled: !this.canModify }],
            flags: [{ value: this.asset.flags, disabled: !this.canModify }],
            count: [{ value: this.asset.totalCount, disabled: !this.isCreate || !this.canModify }]
        });
    }
}
