import { SelectItems } from './../../../shared/components/select/select-item';
import { FormGroup, FormBuilder } from '@angular/forms';
import { InventorySearchParams } from './../../../core/models';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
    selector: 'app-asset-inventory-filter',
    templateUrl: './asset-inventory-filter.component.html'
})
export class AssetInventoryFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<InventorySearchParams>();
    form: FormGroup;
    users: SelectItems = [];

    constructor(
        private fb: FormBuilder,
        private dataService: DataService,
        private storage: StorageService
    ) { }

    ngOnInit(): void {
        this.dataService.getUserSelectList().subscribe(users => {
            this.users = users;
            this.users.unshift({ key: null, value: 'Nevybráno' });
        });

        const filter = InventorySearchParams.create(this.storage.read<InventorySearchParams>('InventorySearchParams') || {});

        this.initFilter(filter);
        this.submitFilter();
    }

    submitFilter(): void {
        const filter = InventorySearchParams.create(this.form.value);

        this.filterChanged.emit(filter);
        this.storage.save('InventorySearchParams', filter);
    }

    resetFilter(): boolean {
        const filter = InventorySearchParams.empty;

        this.form.patchValue({
            userId: filter.userId
        });

        this.submitFilter();
        return false;
    }

    private initFilter(filter: InventorySearchParams): void {
        this.form = this.fb.group({
            userId: [filter.userId]
        });
    }
}
