import { AssetService } from './../../../core/services/asset.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Permissions } from 'src/app/core/enums';
import { availableIcons } from 'src/app/core/lib/data';
import { ValidationHelper } from 'src/app/core/lib/validators';
import { AssetCategory } from 'src/app/core/models';
import { UpdateCategoryParams } from 'src/app/core/models/finance/category/update-category-params';
import { AuthService } from 'src/app/core/services/auth.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { SelectItems } from 'src/app/shared/components/select/select-item';
import { ValidationsService } from 'src/app/core/services/validations.service';

@Component({
    selector: 'app-asset-categories',
    templateUrl: './asset-categories.component.html'
})
export class AssetCategoriesComponent implements OnInit {
    categories: AssetCategory[];
    icons: SelectItems;
    form: FormGroup;
    selectedCategory: AssetCategory;

    constructor(
        private authService: AuthService,
        private fb: FormBuilder,
        private modalService: ModalService,
        private assetService: AssetService,
        private validationsService: ValidationsService
    ) { }

    get canManage(): boolean {
        return this.authService.loggedUser.hasPermission(Permissions.PropertyManager);
    }

    ngOnInit(): void {
        this.readData();
        this.icons = availableIcons.map(o => ({ key: o.icon, icon: o.icon, value: o.text }));
        this.icons.unshift({ key: null, icon: null, value: 'Nevybráno' });

        this.form = this.fb.group({
            name: [
                null,
                Validators.compose([Validators.required, Validators.maxLength(100)]),
                [this.validationsService.assetCategoryNameExists()]
            ],
            icon: [null]
        });
    }

    readData(): void {
        this.assetService.getCategories().subscribe(categories => this.categories = categories);
    }

    selectCategory(category: AssetCategory): void {
        this.selectedCategory = category;

        this.form.patchValue({
            name: category.name,
            icon: category.icon
        });
    }

    addCategory(): boolean {
        const params = new UpdateCategoryParams(this.form.value.name, true, this.form.value.icon);

        this.assetService.createCategory(params).subscribe(_ => {
            this.modalService.showNotification('Vytvoření kategorie', 'Kategorie byla úspěšně vytvořena.', 'lg').onClose.subscribe(() => {
                this.readData();
            });
        });

        return false;
    }

    updateCategory(): boolean {
        const params = new UpdateCategoryParams(
            this.form.value.name,
            this.form.value.name !== this.selectedCategory.name,
            this.form.value.icon
        );

        this.modalService.showQuestion('Úprava kategorie', 'Opravdu si přeješ upravit kategorii?', 'lg').onAccept.subscribe(_ => {
            this.assetService.updateCategory(this.selectedCategory.id, params).subscribe(category => {
                this.modalService.showNotification('Úprava kategorie', 'Kategorie byla úspěšně upravena.', 'lg').onClose.subscribe(__ => {
                    this.readData();

                    this.selectedCategory = category;
                });
            });
        });

        return false;
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    removeCategory(category: AssetCategory): void {
        this.modalService.showQuestion('Smazání kategorie', 'Opravdu si přeješ smazat kategorii?', 'lg').onAccept.subscribe(_ => {
            this.assetService.removeCategory(category.id).subscribe(__ => {
                this.modalService.showNotification('Smazání kategorie', 'Kategorie byla úspěšně smazána.', 'lg').onClose.subscribe(() => {
                    this.readData();
                });
            });
        });
    }
}
