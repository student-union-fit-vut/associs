import { AssetRoutingModule } from './asset-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { AssetCategoriesComponent } from './asset-categories/asset-categories.component';
import { AssetDashboardComponent } from './asset-dashboard/asset-dashboard.component';
import { AssetFilterComponent } from './asset-filter/asset-filter.component';
import { AssetListComponent } from './asset-list/asset-list.component';
import { AssetEditComponent } from './asset-edit/asset-edit.component';
import { AssetLoanDashboardComponent } from './asset-loan-dashboard/asset-loan-dashboard.component';
import { AssetLoanFilterComponent } from './asset-loan-filter/asset-loan-filter.component';
import { AssetLoanListComponent } from './asset-loan-list/asset-loan-list.component';
import { AssetInventoryDashboardComponent } from './asset-inventory-dashboard/asset-inventory-dashboard.component';
import { AssetInventoryFilterComponent } from './asset-inventory-filter/asset-inventory-filter.component';
import { AssetInventoryListComponent } from './asset-inventory-list/asset-inventory-list.component';
import { AssetInventoryEditComponent } from './asset-inventory-edit/asset-inventory-edit.component';
import { AssetLabelsComponent } from './asset-labels/asset-labels.component';
import { QrCodeModule } from 'ng-qrcode';

@NgModule({
    declarations: [
        AssetCategoriesComponent,
        AssetDashboardComponent,
        AssetFilterComponent,
        AssetListComponent,
        AssetEditComponent,
        AssetLoanDashboardComponent,
        AssetLoanFilterComponent,
        AssetLoanListComponent,
        AssetInventoryDashboardComponent,
        AssetInventoryFilterComponent,
        AssetInventoryListComponent,
        AssetInventoryEditComponent,
        AssetLabelsComponent
    ],
    imports: [
        SharedModule,
        AssetRoutingModule,
        QrCodeModule
    ]
})
export class AssetsModule { }
