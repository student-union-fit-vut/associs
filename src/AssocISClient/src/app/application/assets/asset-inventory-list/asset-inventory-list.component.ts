import { InventorySearchParams } from './../../../core/models/assets/inventory-search-params';
import { AssetDetail } from './../../../core/models/assets/asset-detail';
import { Component, Input, ViewChild } from '@angular/core';
import { AssetService } from 'src/app/core/services/asset.service';
import { DataListComponent, PaginationParams } from 'src/app/shared/components/data-list';
import { InventoryReport } from 'src/app/core/models';
import { ModalService } from 'src/app/shared/components/modal/modal.service';

@Component({
    selector: 'app-asset-inventory-list',
    templateUrl: './asset-inventory-list.component.html'
})
export class AssetInventoryListComponent {
    @ViewChild('list', { static: false }) list: DataListComponent;
    @Input() asset: AssetDetail;

    private filter: InventorySearchParams;

    constructor(
        private assetService: AssetService,
        private modalService: ModalService
    ) { }

    filterChanged(filter: InventorySearchParams): void {
        this.filter = filter;
        if (this.list) { this.list.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.assetService.getInventoryReportsOfAsset(this.asset.id, this.filter.userId, pagination)
            .subscribe(reports => this.list.setData(reports));
    }

    showNote(item: InventoryReport): void {
        this.modalService.showNotification('Report inventury - Poznámka', item.note, 'lg');
    }
}
