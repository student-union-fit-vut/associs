import { AssetListItem } from './../../../core/models/assets/asset-list-item';
import { PaginationParams } from './../../../shared/components/data-list/models';
import { DataListComponent } from './../../../shared/components/data-list/data-list.component';
import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { AssetSearchParams } from 'src/app/core/models';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { AssetService } from 'src/app/core/services/asset.service';

@Component({
    selector: 'app-asset-list',
    templateUrl: './asset-list.component.html'
})
export class AssetListComponent {
    @ViewChild('list', { static: false }) list: DataListComponent;
    @Output() categoryClick = new EventEmitter<number>();

    private filter: AssetSearchParams;

    constructor(
        private modalService: ModalService,
        private assetService: AssetService
    ) { }

    filterChanged(filter: AssetSearchParams): void {
        this.filter = filter;
        if (this.list) { this.list.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.assetService.getAssetsList(this.filter, pagination).subscribe(assets => {
            this.list.setData(assets);
        });
    }

    onCategoryClick(categoryId: number): void {
        this.categoryClick.emit(categoryId);
    }

    loanItem(item: AssetListItem): void {
        this.assetService.loanAsset(item.id).subscribe(() => {
            this.modalService.showNotification('Půjčka', 'Úspěšně vypůjčeno', 'sm').onClose.subscribe(() => this.list.onChange());
        });
    }
}
