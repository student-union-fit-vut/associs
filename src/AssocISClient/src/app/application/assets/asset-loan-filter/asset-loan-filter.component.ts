import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { LoanSearchParams } from 'src/app/core/models';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
    selector: 'app-asset-loan-filter',
    templateUrl: './asset-loan-filter.component.html'
})
export class AssetLoanFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<LoanSearchParams>();
    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private storage: StorageService
    ) { }

    ngOnInit(): void {
        const filter = LoanSearchParams.create(this.storage.read<LoanSearchParams>('LoanSearchParams') || {});

        this.initFilter(filter);
        this.submitFilter();
    }

    resetFilter(): boolean {
        const filter = LoanSearchParams.empty;

        this.form.patchValue({
            onlyActive: filter.onlyActive
        });

        this.submitFilter();
        return false;
    }

    submitFilter(): void {
        const filter = LoanSearchParams.create(this.form.value);

        this.filterChanged.emit(filter);
        this.storage.save('LoanSearchParams', filter);
    }

    private initFilter(filter: LoanSearchParams): void {
        this.form = this.fb.group({
            onlyActive: [filter.onlyActive]
        });
    }
}
