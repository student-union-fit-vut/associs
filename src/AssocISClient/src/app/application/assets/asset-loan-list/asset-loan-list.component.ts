import { AssetLoanInfo } from './../../../core/models/assets/asset-loan-info';
import { AssetDetail } from './../../../core/models/assets/asset-detail';
import { PaginationParams } from 'src/app/shared/components/data-list/models';
import { LoanSearchParams } from './../../../core/models/assets/loan-search-params';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';
import { Component, Input, ViewChild } from '@angular/core';
import { AssetService } from 'src/app/core/services/asset.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';

@Component({
    selector: 'app-asset-loan-list',
    templateUrl: './asset-loan-list.component.html'
})
export class AssetLoanListComponent {
    @ViewChild('list', { static: false }) list: DataListComponent;
    @Input() asset: AssetDetail;

    private filter: LoanSearchParams;

    constructor(
        private assetService: AssetService,
        private modalService: ModalService
    ) { }

    filterChanged(filter: LoanSearchParams): void {
        this.filter = filter;
        if (this.list) { this.list.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.assetService.getLoansOfAsset(this.asset.id, this.filter, pagination).subscribe(loans => this.list.setData(loans));
    }

    returnLoan(loan: AssetLoanInfo): void {
        this.assetService.returnLoan(this.asset.id, loan.id).subscribe(() => {
            this.modalService.showNotification('Vrácení výpůjčky', 'Výpůjčka byla úspěšně vrácena.').onClose.subscribe(() => {
                this.list.onChange();
            });
        });
    }
}
