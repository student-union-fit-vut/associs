import { AssetEditComponent } from './asset-edit/asset-edit.component';
import { AssetCategoriesComponent } from './asset-categories/asset-categories.component';
import { AssetDashboardComponent } from './asset-dashboard/asset-dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetInventoryEditComponent } from './asset-inventory-edit/asset-inventory-edit.component';

const routes: Routes = [
    { path: '', component: AssetDashboardComponent, data: { breadcrumb: 'Seznam' } },
    { path: 'category', component: AssetCategoriesComponent, data: { breadcrumb: 'Kategorie' } },
    { path: 'inventory', component: AssetInventoryEditComponent, data: { breadcrumb: 'Inventura' } },
    { path: 'add', component: AssetEditComponent, data: { breadcrumb: 'Vytvoření', isAdd: true } },
    { path: ':id', component: AssetEditComponent, data: { breadcrumb: 'Editace', isAdd: false } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AssetRoutingModule { }
