import { MeetingService } from './../../../core/services/meeting.service';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';
import { Component, ViewChild } from '@angular/core';
import { MeetingListItem, MeetingSearchParams } from 'src/app/core/models';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { PaginationParams } from 'src/app/shared/components/data-list/models';

@Component({
    selector: 'app-meetings-list',
    templateUrl: './meetings-list.component.html'
})
export class MeetingsListComponent {
    @ViewChild('list', { static: false }) list: DataListComponent;

    private filter: MeetingSearchParams;

    constructor(
        private meetingService: MeetingService,
        private modalService: ModalService
    ) { }

    onFilterChanged(filter: MeetingSearchParams): void {
        this.filter = filter;
        if (this.list) { this.list.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.meetingService.getMeetingList(this.filter, pagination).subscribe(data => this.list.setData(data));
    }

    notifyMeeting(item: MeetingListItem): void {
        this.modalService.showQuestion('Svolání schůze', 'Opravdu si přejete opětovně svolat schůzi?', 'lg').onAccept.subscribe(() => {
            this.meetingService.notifyMeeting(item.id).subscribe(() => {
                this.modalService.showNotification('Svolání schůze', 'Schůze byla opětovně svolána.', 'lg');
            });
        });
    }

    apologizeFromMeeting(item: MeetingListItem): void {
        this.modalService.showQuestion('Omluvení se ze schůze', 'Opravdu si přejete omluvit se ze schůze?', 'lg').onAccept.subscribe(() => {
            this.meetingService.apologizeFromMeeting(item.id).subscribe(() => {
                this.modalService.showNotification('Omluvení se ze schůze', 'Byl jsi ze schůze úspěšně omluven.', 'lg')
                    .onClose.subscribe(() => this.list.onChange());
            });
        });
    }
}
