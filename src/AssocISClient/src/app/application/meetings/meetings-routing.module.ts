import { MeetingReportComponent } from './meeting-report/meeting-report.component';
import { MeetingsEditComponent } from './meetings-edit/meetings-edit.component';
import { MeetingsDashboardComponent } from './meetings-dashboard/meetings-dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    { path: '', component: MeetingsDashboardComponent, data: { breadcrumb: 'Seznam schůzí' } },
    { path: 'add', component: MeetingsEditComponent, data: { breadcrumb: 'Vytvoření schůze', isAdd: true } },
    { path: 'report', component: MeetingReportComponent, data: { breadcrumb: 'Report' } },
    { path: ':id', component: MeetingsEditComponent, data: { breadcrumb: 'Editace schůze', idAdd: false } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MeetingsRoutingModule { }
