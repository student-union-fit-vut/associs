import { ValidationHelper, Validations } from './../../../core/lib/validators';
import { SelectHelpers } from './../../../core/lib/support';
import { SelectItems } from './../../../shared/components/select/select-item';
import { MeetingStateTexts } from './../../../core/enums/meeting-state';
import { filter } from 'rxjs/operators';
import { DataService } from './../../../core/services/data.service';
import { StorageService } from './../../../core/services/storage.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MeetingSearchParams } from 'src/app/core/models';
import { ValidationsService } from 'src/app/core/services/validations.service';
import { MeetingAttendanceType, MeetingAttendanceTypeTexts, MeetingState } from 'src/app/core/enums';

@Component({
    selector: 'app-meetings-filter',
    templateUrl: './meetings-filter.component.html'
})
export class MeetingsFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<MeetingSearchParams>();

    form: FormGroup;

    users: SelectItems = [];
    meetingAttendanceTypes: SelectItems = [];
    meetingStates: SelectItems = [];

    constructor(
        private fb: FormBuilder,
        private storage: StorageService,
        private dataService: DataService,
        private validation: ValidationsService
    ) { }

    get defaultFilter(): MeetingSearchParams { return new MeetingSearchParams(); }

    ngOnInit(): void {
        this.dataService.getUserSelectList().subscribe(users => {
            this.users = users;
            this.users.unshift({ key: null, value: 'Nevybráno' });
        });
        this.meetingAttendanceTypes = SelectHelpers.getSelectItemsFromEnum(MeetingAttendanceType, MeetingAttendanceTypeTexts);
        this.meetingStates = SelectHelpers.getSelectItemsFromEnum(MeetingState, MeetingStateTexts);

        const searchParams = MeetingSearchParams.create(this.storage.read<MeetingSearchParams>('MeetingSearchParams') || {});
        this.initForm(searchParams);
        this.submitFilter();
    }

    resetFilter(): boolean {
        const searchFilter = this.defaultFilter;

        this.form.patchValue({
            nameQuery: searchFilter.nameQuery,
            from: null,
            to: null,
            attendanceTypes: searchFilter.attendanceTypes,
            meetingStates: searchFilter.meetingStates,
            agendaQuery: searchFilter.agendaQuery,
            reportQuery: searchFilter.reportQuery,
            author: searchFilter.authorId,
            messageQuery: searchFilter.messageQuery
        });

        this.submitFilter();
        return false;
    }

    submitFilter(): void {
        const searchFilter = MeetingSearchParams.create(this.form.value);

        this.filterChanged.emit(searchFilter);
        this.storage.save('MeetingSearchParams', searchFilter.toInterface());
    }

    isInvalid(controlId: string, errorId?: string): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    private initForm(params: MeetingSearchParams): void {
        this.form = this.fb.group({
            nameQuery: [params.nameQuery],
            from: [params.from?.toLocaleISOString()?.slice(0, 16)],
            to: [params.to?.toLocaleISOString()?.slice(0, 16)],
            attendanceTypes: [params.attendanceTypes],
            meetingStates: [params.meetingStates],
            agendaQuery: [params.agendaQuery],
            reportQuery: [params.reportQuery],
            author: [params.authorId],
            messageQuery: [params.messageQuery]
        }, {
            validators: Validators.compose([
                Validations.checkDates('from', 'to')
            ])
        });

        this.form.get('author').valueChanges.pipe(filter(o => o && o !== 0)).subscribe(userId => {
            this.validation.userIdExists(userId).pipe(filter(o => !o)).subscribe(() => {
                this.form.get('author').setErrors({ exists: true });
            });
        });
    }
}
