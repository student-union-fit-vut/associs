import { Support } from 'src/app/core/lib/support';
import { Dictionary, MeetingPresenceListItem, SetPresenceParams } from 'src/app/core/models';
import { MeetingService } from './../../../core/services/meeting.service';
import { Component, Input, OnInit } from '@angular/core';
import { Meeting } from '../../../core/models';
import { MeetingPresenceType, MeetingPresenceTypeTexts } from 'src/app/core/enums';
import { ModalService } from 'src/app/shared/components/modal/modal.service';

@Component({
    selector: 'app-meeting-presence',
    templateUrl: './meeting-presence.component.html'
})
export class MeetingPresenceComponent implements OnInit {
    @Input() meeting: Meeting;

    presenceTypes: Dictionary<number, string> = [];
    presence: MeetingPresenceListItem[];

    constructor(
        private meetingService: MeetingService,
        private modalService: ModalService
    ) { }

    ngOnInit(): void {
        this.presenceTypes = Object.keys(MeetingPresenceType)
            .filter(o => !isNaN(parseInt(o, 10)))
            .map(o => parseInt(o, 10))
            .map(o => ({ key: o, value: MeetingPresenceTypeTexts[Support.getEnumKeyByValue(MeetingPresenceType, o)] }));

        this.meetingService.getMeetingPresenceAsync(this.meeting.id).subscribe(data => {
            this.presence = data;
        });
    }

    updatePresence(): void {
        const params = this.presence.map(o => new SetPresenceParams(o.user.id, o.presence));

        this.modalService.showQuestion('Úprava docházky', 'Opravdu si přejete upravit docházku?', 'lg').onAccept.subscribe(_ => {
            this.meetingService.setMeetingPresence(this.meeting.id, params).subscribe(__ => {
                this.modalService.showNotification('Úprava docházky', 'Docházka byla úspěšně upravena.', 'lg');
            });
        });
    }
}
