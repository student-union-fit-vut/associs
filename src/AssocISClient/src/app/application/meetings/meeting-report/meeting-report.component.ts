import { Component, OnInit } from '@angular/core';
import { UserPresenceStatus } from 'src/app/core/models';
import { MeetingService } from 'src/app/core/services/meeting.service';

@Component({
    selector: 'app-meeting-report',
    templateUrl: './meeting-report.component.html'
})
export class MeetingReportComponent implements OnInit {
    status: UserPresenceStatus[];

    constructor(
        private meetingService: MeetingService
    ) { }

    ngOnInit(): void {
        this.meetingService.getMeetingReport().subscribe(data => this.status = data);
    }
}
