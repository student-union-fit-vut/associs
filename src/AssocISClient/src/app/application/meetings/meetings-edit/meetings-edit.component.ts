import { SelectHelpers } from './../../../core/lib/support';
import { SelectItems } from './../../../shared/components/select/select-item';
import { ValidationHelper, Validations } from './../../../core/lib/validators';
import { UpdateMeetingParams } from './../../../core/models/meetings/update-meeting-params';
import { Permissions } from 'src/app/core/enums/permissions';
import { Router, ActivatedRoute } from '@angular/router';
import { MeetingService } from './../../../core/services/meeting.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Meeting } from 'src/app/core/models';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { MeetingAttendanceType, MeetingAttendanceTypeTexts, MeetingState, MeetingStateTexts } from 'src/app/core/enums';

@Component({
    selector: 'app-meetings-edit',
    templateUrl: './meetings-edit.component.html'
})
export class MeetingsEditComponent implements OnInit {
    @ViewChild('newAgendaInput') newAgendaInput: ElementRef<HTMLInputElement>;

    users: SelectItems = [];
    meeting: Meeting;
    form: FormGroup;
    isCreate: boolean;
    meetingStates: SelectItems = [];
    meetingAttendanceTypes: SelectItems = [];
    activeTabId: string;

    constructor(
        private fb: FormBuilder,
        private dataService: DataService,
        private meetingService: MeetingService,
        private router: Router,
        private route: ActivatedRoute,
        private modalService: ModalService,
        private authService: AuthService
    ) { }

    get haveHigherPerms(): boolean {
        return [Permissions.Chairperson, Permissions.MeetingManager, Permissions.TeamLeader]
            .some(o => this.authService.loggedUser.hasPermission(o));
    }

    get agenda(): FormArray {
        return this.form.get('agenda') as FormArray;
    }

    ngOnInit(): void {
        this.isCreate = this.route.routeConfig.data?.isAdd ?? false;

        this.dataService.getUserSelectList().subscribe(users => this.users = users);
        this.meetingAttendanceTypes = SelectHelpers.getSelectItemsFromEnum(MeetingAttendanceType, MeetingAttendanceTypeTexts);
        this.meetingStates = SelectHelpers.getSelectItemsFromEnum(MeetingState, MeetingStateTexts);

        if (this.isCreate) {
            this.meeting = new Meeting();
            this.initForm();
        } else {
            const id = parseInt(this.route.snapshot.params.id, 10);

            this.meetingService.getMeeting(id).subscribe(meeting => {
                this.meeting = meeting;
                this.initForm();
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    submitForm(): void {
        const params = new UpdateMeetingParams(
            this.form.value.name,
            this.form.value.at,
            this.form.value.expectedEnd,
            // eslint-disable-next-line @typescript-eslint/no-unsafe-return
            this.form.value.agenda.map((o: any) => o.text),
            this.form.value.report,
            this.form.value.attendanceType,
            this.form.value.state,
            this.form.value.location,
            this.form.value.message
        );

        if (this.isCreate) {
            let modalMessage = 'Opravdu si přejete vytvořit schůzi?\n';

            if (this.form.value.state === MeetingState.Summoned) {
                modalMessage += '<b>Vytvoření této schůze pošle všem očekávaným členům oznámení.</b>';
            }

            this.modalService.showQuestion('Vytvoření schůze', modalMessage, 'lg').onAccept.subscribe(() => {
                this.meetingService.createMeeting(params).subscribe(meeting => {
                    this.modalService.showNotification('Vytvoření schůze', 'Schůze byla úspěšně vytvořena.', 'lg').onClose.subscribe(() => {
                        this.router.navigate([`/app/meetings/${meeting.id}`]);
                    });
                });
            });
        } else {
            let modalMessage = 'Opravdu si přejete upravit tuhle schůzi?\n';

            if (this.meetingService.willNotifyMeeting(this.meeting.state, this.form.value.state)) {
                modalMessage += '<b>Úprava této schůze pošle všem očekávaným členům oznámení.</b>';
            }

            this.modalService.showQuestion('Úprava schůze', modalMessage, 'lg').onAccept.subscribe(() => {
                this.meetingService.updateMeeting(this.meeting.id, params).subscribe(meeting => {
                    this.modalService.showNotification('Úprava schůze', 'Schůze byla úspěšně upravena.', 'lg').onClose.subscribe(() => {
                        this.refreshMeetingAfterUpdate(meeting);
                    });
                });
            });
        }
    }

    addAgendaRow(text: string): boolean {
        const control = this.fb.group({ text }, { validators: Validators.required });
        this.agenda.push(control);
        this.newAgendaInput.nativeElement.value = '';

        return false;
    }

    removeAgendaRow(index: number): boolean {
        this.agenda.removeAt(index);

        return false;
    }

    createAgendaRows(agenda: string[]): FormGroup[] {
        return (agenda || []).map(o => this.fb.group({ text: [{ value: o, disabled: !this.haveHigherPerms }, Validators.required] }));
    }

    apologize(): void {
        this.modalService.showQuestion('Omluvení se ze schůze', 'Opravdu si přejete omluvit se ze schůze?', 'lg').onAccept.subscribe(() => {
            this.meetingService.apologizeFromMeeting(this.meeting.id).subscribe(meeting => {
                this.modalService.showNotification('Omluvení se ze schůze', 'Byl jsi ze schůze úspěšně omluven.', 'lg').onClose
                    .subscribe(() => this.refreshMeetingAfterUpdate(meeting));
            });
        });
    }

    private initForm(): void {
        const agenda = this.createAgendaRows(this.meeting.agenda);

        this.form = this.fb.group({
            name: [
                { value: this.meeting.name, disabled: !this.haveHigherPerms },
                Validators.compose([Validators.required, Validators.maxLength(100)])
            ],
            at: [{ value: this.meeting.at?.toLocaleISOString()?.slice(0, 16), disabled: !this.haveHigherPerms }, Validators.required],
            expectedEnd: [
                { value: this.meeting.expectedEnd?.toLocaleISOString()?.slice(0, 16), disabled: !this.haveHigherPerms },
                Validators.required
            ],
            agenda: this.fb.array(agenda, Validators.required),
            report: [{ value: this.meeting.report, disabled: !this.haveHigherPerms }],
            attendanceType: [{ value: this.meeting.attendanceType, disabled: !this.haveHigherPerms }, Validators.required],
            state: [{ value: this.meeting.state, disabled: !this.haveHigherPerms }, Validators.required],
            location: [{ value: this.meeting.location, disabled: !this.haveHigherPerms }],
            message: [{ value: this.meeting.message, disabled: !this.haveHigherPerms }]
        }, {
            validators: Validators.compose([
                Validations.checkDates('at', 'expectedEnd')
            ])
        });
    }

    private refreshMeetingAfterUpdate(newMeeting: Meeting): void {
        this.meeting = newMeeting;

        this.createAgendaRows(newMeeting.agenda);
        this.form.patchValue({
            name: newMeeting.name,
            at: newMeeting.at?.toLocaleISOString()?.slice(0, 16),
            expectedEnd: newMeeting.expectedEnd?.toLocaleISOString()?.slice(0, 16),
            agenda: newMeeting.agenda,
            report: this.meeting.report,
            attendanceType: this.meeting.attendanceType,
            state: this.meeting.state,
            location: this.meeting.location,
            message: this.meeting.message
        });
    }
}
