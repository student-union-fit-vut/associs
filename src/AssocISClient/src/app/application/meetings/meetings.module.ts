import { MeetingsRoutingModule } from './meetings-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { MeetingsDashboardComponent } from './meetings-dashboard/meetings-dashboard.component';
import { MeetingsListComponent } from './meetings-list/meetings-list.component';
import { MeetingsFilterComponent } from './meetings-filter/meetings-filter.component';
import { MeetingsEditComponent } from './meetings-edit/meetings-edit.component';
import { MeetingPresenceComponent } from './meeting-presence/meeting-presence.component';
import { MeetingReportComponent } from './meeting-report/meeting-report.component';

@NgModule({
    declarations: [
        MeetingsDashboardComponent,
        MeetingsListComponent,
        MeetingsFilterComponent,
        MeetingsEditComponent,
        MeetingPresenceComponent,
        MeetingReportComponent
    ],
    imports: [
        SharedModule,
        MeetingsRoutingModule
    ]
})
export class MeetingsModule { }
