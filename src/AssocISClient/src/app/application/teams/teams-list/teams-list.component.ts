import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { TeamsService } from './../../../core/services/teams.service';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';
import { Component, ViewChild } from '@angular/core';
import { TeamListItem, TeamsSearchParams } from 'src/app/core/models';
import { PaginationParams } from 'src/app/shared/components/data-list/models';

@Component({
    selector: 'app-teams-list',
    templateUrl: './teams-list.component.html'
})
export class TeamsListComponent {
    @ViewChild('list', { static: false }) list: DataListComponent;

    private params: TeamsSearchParams;

    constructor(
        private teamsService: TeamsService,
        private modalService: ModalService
    ) { }

    filterChanged(filter: TeamsSearchParams): void {
        this.params = filter;
        if (this.list) { this.list.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.teamsService.getTeamsList(this.params, pagination).subscribe(data => this.list.setData(data));
    }

    removeTeam(team: TeamListItem): void {
        this.modalService.showQuestion('Smazání týmu', 'Opravdu si přejete smazat tým?', 'lg').onAccept.subscribe(() => {
            this.teamsService.removeTeam(team.id).subscribe(() => {
                this.modalService.showNotification('Smazání týmu', `Tým "${team.name}" byl úspěšně smazán.`, 'lg').onClose.subscribe(() => {
                    this.list.onChange();
                });
            });
        });
    }
}
