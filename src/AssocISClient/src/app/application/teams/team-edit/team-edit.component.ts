import { SelectItems } from './../../../shared/components/select/select-item';
import { ValidationHelper } from './../../../core/lib/validators';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { filter, debounceTime } from 'rxjs/operators';
import { DataService } from './../../../core/services/data.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamsService } from './../../../core/services/teams.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Team, UpdateTeamParams } from 'src/app/core/models';
import { ValidationsService } from 'src/app/core/services/validations.service';
import { AppService } from 'src/app/core/services/app.service';

@Component({
    selector: 'app-team-edit',
    templateUrl: './team-edit.component.html'
})
export class TeamEditComponent implements OnInit {
    @ViewChild('newGoogleGroupInput') newGoogleGroupInput: ElementRef<HTMLInputElement>;

    team: Team;
    isCreate: boolean;
    form: FormGroup;
    users: SelectItems = [];
    activeTabId: string;
    roles: SelectItems;

    constructor(
        private teamsService: TeamsService,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private validation: ValidationsService,
        private data: DataService,
        private modalService: ModalService,
        private router: Router,
        private appService: AppService
    ) { }

    get googleGroups(): FormArray {
        return this.form.get('googleGroups') as FormArray;
    }

    ngOnInit(): void {
        this.isCreate = this.route.routeConfig.data?.isAdd ?? false;

        this.data.getUserSelectList().subscribe(users => {
            this.users = users;
            this.users.unshift({ key: null, value: 'Nevybráno' });
        });

        if (this.appService.clientConfiguration.discordEnabled) {
            this.data.getRolesSelectList().subscribe(roles => {
                this.roles = roles;
                this.roles.unshift({ key: null, value: 'Nevybráno' });
            });
        }

        if (this.isCreate) {
            this.team = new Team();
            this.initForm();
        } else {
            const teamId = parseInt(this.route.snapshot.params.id, 10);
            this.teamsService.getTeamDetail(teamId).subscribe(team => {
                this.team = team;
                this.initForm();
            });
        }
    }

    createGoogleGroupRows(groups: string[]): FormGroup[] {
        return (groups || []).map(g => this.fb.group({ groupMail: [g, Validators.email] }));
    }

    removeGoogleGroupRow(index: number): boolean {
        this.googleGroups.removeAt(index);
        return false;
    }

    addGoogleGroupRow(value: string): boolean {
        const control = this.fb.group({ groupMail: value });
        this.googleGroups.push(control);
        this.newGoogleGroupInput.nativeElement.value = '';

        return false;
    }

    submitForm(): void {
        const params = new UpdateTeamParams(
            this.form.value.name,
            this.form.value.name !== this.team.name,
            this.form.value.description,
            this.form.value.discordRoleId,
            this.form.value.createDiscordRole,
            this.form.value.leader,
            this.form.value.googleGroups.map((o: { groupMail: string }) => o.groupMail)
        );

        if (this.isCreate) {
            this.teamsService.createTeam(params).subscribe(team => {
                this.modalService.showNotification('Vytvoření týmu', `Tým "${team.name}" byl úspěšně vytvořen.`, 'lg')
                    .onClose.subscribe(() => this.router.navigate(['/app/teams']));
            });
        } else {
            this.teamsService.updateTeam(this.team.id, params).subscribe(team => {
                this.modalService.showNotification('Úprava týmu', `Tým "${team.name}" byl úspěšně upraven.`, 'lg').onClose.subscribe(() => {
                    this.router.navigate(['/app/teams']);
                });
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    private initForm(): void {
        const googleGroupRows = this.createGoogleGroupRows(this.team.googleGroups);

        this.form = this.fb.group({
            name: [this.team.name, Validators.compose([Validators.required, Validators.maxLength(100)])],
            description: [this.team.description],
            discordRoleId: [this.team.discordRoleId],
            createDiscordRole: [{ value: false, disabled: !!this.team.discordRoleId }],
            leader: [this.team.leader?.id],
            googleGroups: this.fb.array(googleGroupRows)
        });

        this.form.get('leader').valueChanges.pipe(filter(o => o && o !== 0)).subscribe(userId => {
            this.validation.userIdExists(userId).pipe(filter(o => !o)).subscribe(() => {
                this.form.get('leader').setErrors({ exists: true });
            });
        });

        this.form.get('name').valueChanges.pipe(
            filter(o => o && o.length > 0),
            debounceTime(200)
        ).subscribe(name => {
            this.validation.teamNameExists(name).pipe(filter(o => o)).subscribe(() => {
                this.form.get('name').setErrors({ unique: true });
            });
        });

        if (this.appService.clientConfiguration.discordEnabled) {
            this.form.get('discordRoleId').valueChanges.subscribe(id => {
                if (!id || id.length === 0) {
                    this.form.get('createDiscordRole').enable({ emitEvent: false });
                } else {
                    this.form.get('createDiscordRole').disable({ emitEvent: false });
                }
            });

            this.form.get('createDiscordRole').valueChanges.subscribe(create => {
                const roleIdFormField = this.form.get('discordRoleId');

                if (create) {
                    roleIdFormField.disable({ emitEvent: false });
                } else {
                    roleIdFormField.enable({ emitEvent: false });
                }
            });
        }
    }
}
