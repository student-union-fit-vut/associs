import { SelectItems } from './../../../shared/components/select/select-item';
import { filter } from 'rxjs/operators';
import { DataService } from './../../../core/services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Team } from 'src/app/core/models';
import { TeamsService } from 'src/app/core/services/teams.service';
import { ValidationsService } from 'src/app/core/services/validations.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { PaginationParams } from 'src/app/shared/components/data-list/models';
import { ValidationHelper } from 'src/app/core/lib/validators';

@Component({
    selector: 'app-team-members-list',
    templateUrl: './team-members-list.component.html'
})
export class TeamMembersListComponent implements OnInit {
    @Input() team: Team;

    @ViewChild('memberList', { static: false }) memberList: DataListComponent;

    createMemberForm: FormGroup;
    rawUsers: SelectItems = [];
    users: SelectItems = [];

    constructor(
        private teamsService: TeamsService,
        private fb: FormBuilder,
        private dataService: DataService,
        private validation: ValidationsService,
        private modalService: ModalService
    ) { }

    ngOnInit(): void {
        this.dataService.getUserSelectList().subscribe(users => {
            this.rawUsers = users;
            this.users = users;
        });

        this.createMemberForm = this.fb.group({
            user: [null, Validators.required]
        });

        this.createMemberForm.get('user').valueChanges.pipe(filter(o => o && o !== 0)).subscribe(id => {
            this.validation.userIdExists(id).pipe(filter(o => !o)).subscribe(() => {
                this.createMemberForm.get('user').setErrors({ exists: true });
            });
        });
    }

    readData(pagination: PaginationParams): void {
        this.teamsService.getTeamMembersList(this.team.id, pagination).subscribe(members => {
            this.memberList.setData(members);
            this.users = this.rawUsers.filter(o => !members.data.some(x => x.id === o.key));
        });
    }

    submitForm(): void {
        this.teamsService.addTeamMember(this.team.id, this.createMemberForm.value.user).subscribe(() => {
            const memberName: string = this.rawUsers.filter(o => o.key === this.createMemberForm.value.user)[0]?.value;
            this.modalService.showNotification('Přidání člena do týmu', `Člen "${memberName}" byl/a přidán do týmu.`, 'lg')
                .onClose.subscribe(() => this.memberList.onChange());
        });
    }

    removeMember(userId: number): void {
        this.modalService.showQuestion('Odebrání člena z týmu', 'Opravdu si přejete odebrat člena z týmu?', 'lg')
            .onAccept.subscribe(() => this.teamsService.removeTeamMember(this.team.id, userId).subscribe(() => {
                this.modalService.showNotification('Odebrání člena z týmu', 'Člen byl/a úspěšně odebrán z týmu.', 'lg')
                    .onClose.subscribe(() => this.memberList.onChange());
            }));
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.createMemberForm, controlId, errorId);
    }
}
