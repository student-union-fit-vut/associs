import { TeamsRoutingModule } from './teams-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { TeamsDashboardComponent } from './teams-dashboard/teams-dashboard.component';
import { TeamsListComponent } from './teams-list/teams-list.component';
import { TeamsFilterComponent } from './teams-filter/teams-filter.component';
import { TeamEditComponent } from './team-edit/team-edit.component';
import { TeamMembersListComponent } from './team-members-list/team-members-list.component';

@NgModule({
    declarations: [
        TeamsDashboardComponent,
        TeamsListComponent,
        TeamsFilterComponent,
        TeamEditComponent,
        TeamMembersListComponent
    ],
    imports: [
        SharedModule,
        TeamsRoutingModule
    ]
})
export class TeamsModule { }
