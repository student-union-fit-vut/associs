import { StorageService } from './../../../core/services/storage.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TeamsSearchParams } from 'src/app/core/models';

@Component({
    selector: 'app-teams-filter',
    templateUrl: './teams-filter.component.html'
})
export class TeamsFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<TeamsSearchParams>();

    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private storage: StorageService
    ) { }

    get defaultFilter(): TeamsSearchParams { return new TeamsSearchParams(); }

    ngOnInit(): void {
        const filter = TeamsSearchParams.create(this.storage.read<TeamsSearchParams>('TeamSearchParams') || {});

        this.initFilter(filter);
        this.submitFilter();
    }

    resetFilter(): boolean {
        const defaultFilter = this.defaultFilter;

        this.form.patchValue({
            nameQuery: defaultFilter.nameQuery,
            onlyEmptyTeams: defaultFilter.onlyEmptyTeams
        });

        this.submitFilter();
        return false;
    }

    submitFilter(): void {
        const filter = TeamsSearchParams.create(this.form.value);

        this.filterChanged.emit(filter);
        this.storage.save('TeamSearchParams', filter.toInterface());
    }

    private initFilter(filter: TeamsSearchParams): void {
        this.form = this.fb.group({
            nameQuery: [filter.nameQuery],
            onlyEmptyTeams: [filter.onlyEmptyTeams]
        });
    }
}
