import { TeamEditComponent } from './team-edit/team-edit.component';
import { TeamsDashboardComponent } from './teams-dashboard/teams-dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', component: TeamsDashboardComponent, data: { breadcrumb: 'Seznam týmů' } },
    { path: 'add', component: TeamEditComponent, data: { breadcrumb: 'Vytvoření týmu', isAdd: true } },
    { path: ':id', component: TeamEditComponent, data: { breadcrumb: 'Editace týmu', isAdd: false } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TeamsRoutingModule { }
