import { Component } from '@angular/core';
import { AppService } from 'src/app/core/services/app.service';

@Component({
    selector: 'app-app-screen',
    templateUrl: './app-screen.component.html'
})
export class AppScreenComponent {
    constructor(public appService: AppService) { }
}
