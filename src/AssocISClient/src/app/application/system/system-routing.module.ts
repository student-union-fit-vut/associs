import { ClientsEditComponent } from './clients-edit/clients-edit.component';
import { ClientsDashboardComponent } from './clients-dashboard/clients-dashboard.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DiagnosticsComponent } from './diagnostics/diagnostics.component';

const routes: Routes = [
    { path: 'notifications', component: NotificationsComponent, data: { breadcrumb: 'Notifikace' } },
    { path: 'clients', component: ClientsDashboardComponent, data: { breadcrumb: 'Klienti' } },
    { path: 'clients/add', component: ClientsEditComponent, data: { breadcrumb: 'Vytvoření klienta', isAdd: true } },
    { path: 'clients/:id', component: ClientsEditComponent, data: { breadcrumb: 'Modifikace klienta', isAdd: false } },
    { path: 'diagnostic', component: DiagnosticsComponent, data: { breadcrumb: 'Diagnostika' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SystemRoutingModule { }
