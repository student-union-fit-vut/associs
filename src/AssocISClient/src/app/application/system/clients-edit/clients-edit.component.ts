import { ValidationHelper } from './../../../core/lib/validators';
import { Support } from './../../../core/lib/support';
import { ClientType, ClientTypeTexts } from './../../../core/enums/client-type';
import { SelectItem } from './../../../shared/components/select/select-item';
import { ActivatedRoute, Router } from '@angular/router';
import { SystemService } from './../../../core/services/system.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Client } from './../../../core/models/system/clients/client';
import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { UpdateClientParams } from 'src/app/core/models';

@Component({
    selector: 'app-clients-edit',
    templateUrl: './clients-edit.component.html'
})
export class ClientsEditComponent implements OnInit {
    client: Client;
    form: FormGroup;
    isCreate: boolean;
    clientTypes: SelectItem[] = [];

    constructor(
        private systemService: SystemService,
        private modalService: ModalService,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.clientTypes = Object.keys(ClientType)
            .filter(o => !isNaN(parseInt(o, 10)))
            .map(o => parseInt(o, 10))
            .map(o => ({ key: o, value: ClientTypeTexts[Support.getEnumKeyByValue(ClientType, o)] }));

        this.isCreate = this.route.routeConfig?.data?.isAdd ?? false;

        if (this.isCreate) {
            this.client = new Client();
            this.initForm();
        } else {
            const id = parseInt(this.route.snapshot.params.id, 10);

            this.systemService.getClient(id).subscribe(client => {
                this.client = client;
                this.initForm();
            });
        }
    }

    submitForm(): void {
        const params = UpdateClientParams.create(this.form.value);

        if (this.isCreate) {
            this.systemService.createClient(params).subscribe(client => {
                this.modalService.showNotification('Vytvoření klienta', 'Klient byl úspěšně vytvořen.', 'lg').onClose.subscribe(() => {
                    this.router.navigate([`/app/system/clients/${client.id}`]);
                });
            });
        } else {
            this.modalService.showQuestion('Úprava klienta', 'Opravdu si přejete upravit klienta?', 'lg').onAccept.subscribe(() => {
                this.systemService.updateClient(this.client.id, params).subscribe(client => {
                    this.modalService.showNotification('Úprava klienta', 'Klient byl úspěšně upraven.', 'lg')
                        .onClose.subscribe(() => this.patchClient(client));
                });
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    private initForm(): void {
        this.form = this.fb.group({
            name: [this.client.name, Validators.compose([Validators.required, Validators.maxLength(100)])],
            description: [this.client.description],
            clientType: [this.client.clientType, Validators.required]
        });
    }

    private patchClient(client: Client): void {
        this.client = client;

        this.form.patchValue({
            name: client.name,
            description: client.description,
            clientType: client.clientType
        });
    }
}
