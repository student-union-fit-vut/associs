import { SystemService } from './../../../core/services/system.service';
import { PaginationParams } from './../../../shared/components/data-list/models';
import { DataListComponent } from './../../../shared/components/data-list/data-list.component';
import { Component, ViewChild } from '@angular/core';
import { ClientsSearchParams } from 'src/app/core/models';
import { ModalService } from 'src/app/shared/components/modal/modal.service';

@Component({
    selector: 'app-clients-list',
    templateUrl: './clients-list.component.html'
})
export class ClientsListComponent {
    @ViewChild('list', { static: false }) list: DataListComponent;

    private filter: ClientsSearchParams;

    constructor(
        private systemService: SystemService,
        private modalService: ModalService
    ) { }

    filterChanged(filter: ClientsSearchParams): void {
        this.filter = filter;
        if (this.list) { this.list.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.systemService.getClientsList(this.filter, pagination).subscribe(data => this.list.setData(data));
    }

    removeClient(id: number): void {
        this.modalService.showQuestion('Smazání klienta',
            'Opravdu si přejete smazat klienta?<br><b>Po smazání klient nebude schopen nikdy použít stejný identifikátor.</b>', 'lg')
            .onAccept.subscribe(() => {
                this.systemService.removeClient(id).subscribe(() => {
                    this.modalService.showNotification('Smazání klienta', 'Klient byl úspěšně smazán.', 'lg')
                        .onClose.subscribe(() => this.list.onChange());
                });
            });
    }

    regenerateClientId(id: number): void {
        this.modalService.showQuestion('Vygenerování nového identifikátoru', 'Opravdu si přejete vygenerovat nový identifikátor?', 'lg')
            .onAccept.subscribe(() => this.systemService.regenerateClientId(id).subscribe(client => {
                const text = `Nový přístupový identifikátor pro klienta ${client.name}:<h2 class="text-center mt-3 mb-3"><code>${client.clientId}</code></h2>`;
                this.modalService.showNotification('Vygenerování nového identifikátoru', text)
                    .onClose.subscribe(() => this.list.onChange());
            }));
    }

}
