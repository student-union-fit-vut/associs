import { StorageService } from './../../../core/services/storage.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ClientsSearchParams } from 'src/app/core/models';

@Component({
    selector: 'app-clients-filter',
    templateUrl: './clients-filter.component.html'
})
export class ClientsFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<ClientsSearchParams>();

    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private storage: StorageService
    ) { }

    ngOnInit(): void {
        const filter = ClientsSearchParams.create(this.storage.read<ClientsSearchParams>('ClientsSearchParams') || {});

        this.initForm(filter);
        this.submitFilter();
    }

    resetFilter(): boolean {
        const filter = ClientsSearchParams.empty;

        this.form.patchValue({
            nameQuery: filter.nameQuery
        });

        this.submitFilter();
        return false;
    }

    submitFilter(): void {
        const params = ClientsSearchParams.create(this.form.value);

        this.filterChanged.emit(params);
        this.storage.save('ClientsSearchParams', params);
    }

    private initForm(filter: ClientsSearchParams): void {
        this.form = this.fb.group({
            nameQuery: [filter.nameQuery]
        });
    }
}
