import { SystemRoutingModule } from './system-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { NotificationsComponent } from './notifications/notifications.component';
import { ClientsDashboardComponent } from './clients-dashboard/clients-dashboard.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsEditComponent } from './clients-edit/clients-edit.component';
import { ClientsFilterComponent } from './clients-filter/clients-filter.component';
import { DiagnosticsComponent } from './diagnostics/diagnostics.component';
import { NgxFilesizeModule } from 'ngx-filesize';

@NgModule({
    declarations: [
        NotificationsComponent,
        ClientsDashboardComponent,
        ClientsListComponent,
        ClientsEditComponent,
        ClientsFilterComponent,
        DiagnosticsComponent
    ],
    imports: [
        SharedModule,
        SystemRoutingModule,
        NgxFilesizeModule
    ]
})
export class SystemModule { }
