import { DiagnosticData } from './../../../core/models/system/diagnostic';
import { Component, OnInit } from '@angular/core';
import { SystemService } from 'src/app/core/services/system.service';

@Component({
    selector: 'app-diagnostics',
    templateUrl: './diagnostics.component.html'
})
export class DiagnosticsComponent implements OnInit {
    data!: DiagnosticData;

    constructor(
        private systemService: SystemService
    ) { }

    ngOnInit(): void {
        this.refreshData();
    }

    refreshData(): void {
        this.data = null;
        this.systemService.getDiagnosticData().subscribe(data => this.data = data);
    }

}
