import { ValidationHelper } from './../../../core/lib/validators';
import { ModalService } from './../../../shared/components/modal/modal.service';
import { SystemService } from './../../../core/services/system.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Support } from './../../../core/lib/support';
import { SelectItem } from './../../../shared/components/select/select-item';
import { Component, OnInit } from '@angular/core';
import { NotifyExternalServicesParams } from 'src/app/core/models';
import { EveryoneNotificationType, EveryoneNotificationTypeTexts } from 'src/app/core/enums';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html'
})
export class NotificationsComponent implements OnInit {
    clientTypes: SelectItem[] = [];
    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private systemService: SystemService,
        private modalService: ModalService
    ) { }

    ngOnInit(): void {
        this.clientTypes = Object.keys(EveryoneNotificationType)
            .filter(o => !isNaN(parseInt(o, 10)))
            .map(o => parseInt(o, 10))
            .map(o => ({ key: o, value: EveryoneNotificationTypeTexts[Support.getEnumKeyByValue(EveryoneNotificationType, o)] }));

        this.form = this.fb.group({
            clientType: [null, Validators.required],
            content: [null, Validators.required]
        });
    }

    submitForm(): void {
        const params = NotifyExternalServicesParams.create(this.form.value);

        this.modalService.showQuestion('Notifikace externích systémů', 'Opravdu si přejete odeslat upozornění?', 'lg')
            .onAccept.subscribe(() => this.systemService.notifyExternalServices(params).subscribe(() => {
                this.modalService.showNotification('Notifikace externích systémů', 'Notifikace byla úspěšně odeslána.', 'lg');
            }));
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }
}
