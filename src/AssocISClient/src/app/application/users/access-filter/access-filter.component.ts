import { SelectHelpers } from './../../../core/lib/support';
import { SelectItems } from './../../../shared/components/select/select-item';
import { AccessType, AccessTypeTexts } from './../../../core/enums';
import { DataService } from './../../../core/services/data.service';
import { StorageService } from './../../../core/services/storage.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AccessDefinitionSearchParams } from './../../../core/models/users/access/access-definition-search-params';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ValidationsService } from 'src/app/core/services/validations.service';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-access-filter',
    templateUrl: './access-filter.component.html'
})
export class AccessFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<AccessDefinitionSearchParams>();

    form: FormGroup;
    users: SelectItems = [];
    accessTypes: SelectItems = [];

    constructor(
        private fb: FormBuilder,
        private storage: StorageService,
        private dataService: DataService,
        private validation: ValidationsService
    ) { }

    get defaultFilter(): AccessDefinitionSearchParams { return new AccessDefinitionSearchParams(); }

    ngOnInit(): void {
        this.dataService.getUserSelectList().subscribe(users => {
            users.unshift({ key: 0, value: 'Nevybráno' });
            this.users = users;
        });

        this.accessTypes = SelectHelpers.getSelectItemsFromEnum(AccessType, AccessTypeTexts);
        this.accessTypes.unshift({ key: -1, value: 'Nevybráno' });

        this.initForm(
            AccessDefinitionSearchParams.create(
                this.storage.read<AccessDefinitionSearchParams>('AccessDefinitionSearchParams') || {})
        );
        this.submitFilter();
    }

    resetFilter(): boolean {
        const defaultFilter = this.defaultFilter;

        this.form.patchValue({
            type: defaultFilter.type,
            descriptionContains: defaultFilter.descriptionContains,
            createdBy: defaultFilter.createdById
        });

        this.submitFilter();
        return false;
    }

    submitFilter(): void {
        const newFilter = AccessDefinitionSearchParams.create(this.form.value);

        this.filterChanged.emit(newFilter);
        this.storage.save('AccessDefinitionSearchParams', newFilter);
    }

    private initForm(params: AccessDefinitionSearchParams): void {
        this.form = this.fb.group({
            type: [params.type || -1],
            descriptionContains: [params.descriptionContains],
            createdBy: [params.createdById || 0]
        });

        this.form.get('createdBy').valueChanges.pipe(filter(o => o && o !== 0)).subscribe(id => {
            this.validation.userIdExists(id).pipe(filter(o => !o)).subscribe(() => {
                this.form.get('createdBy').setErrors({ exists: true });
            });
        });
    }
}
