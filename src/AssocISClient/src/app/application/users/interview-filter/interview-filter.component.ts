import { ValidationHelper, Validations } from './../../../core/lib/validators';
import { SelectItems } from './../../../shared/components/select/select-item';
import { filter } from 'rxjs/operators';
import { DataService } from './../../../core/services/data.service';
import { StorageService } from './../../../core/services/storage.service';
import { InterviewSearchParams } from './../../../core/models/users/interviews/interview-search-params';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ValidationsService } from 'src/app/core/services/validations.service';

@Component({
    selector: 'app-interview-filter',
    templateUrl: './interview-filter.component.html'
})
export class InterviewFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<InterviewSearchParams>();

    form: FormGroup;
    users: SelectItems = [];

    constructor(
        private fb: FormBuilder,
        private storage: StorageService,
        private dataService: DataService,
        private validation: ValidationsService
    ) { }

    get defaultFilter(): InterviewSearchParams { return new InterviewSearchParams(); }

    ngOnInit(): void {
        this.dataService.getUserSelectList().subscribe(users => {
            users.unshift({ key: 0, value: 'Nevybráno' });
            this.users = users;
        });

        const searchFilter: InterviewSearchParams = InterviewSearchParams.create(
            this.storage.read<InterviewSearchParams>('InterviewSearchParams') || {});

        this.initForm(searchFilter);
        this.submitFilter();
    }

    initForm(params: InterviewSearchParams): void {
        this.form = this.fb.group({
            userId: [params.userId || 0],
            leaderUserId: [params.leaderUserId || 0],
            from: [params.fromDate?.toLocaleISOString(false)?.slice(0, 16)],
            to: [params.toDate?.toLocaleISOString(false)?.slice(0, 16)]
        }, {
            validators: Validators.compose([
                Validations.checkDates('from', 'to')
            ])
        });

        this.form.get('userId').valueChanges.pipe(filter(o => o && o !== 0)).subscribe(id => {
            this.validation.userIdExists(id).pipe(filter(o => !o)).subscribe(() => {
                this.form.get('userId').setErrors({ exists: true });
            });
        });

        this.form.get('leaderUserId').valueChanges.pipe(filter(o => o && o !== 0)).subscribe(id => {
            this.validation.userIdExists(id).pipe(filter(o => !o)).subscribe(() => {
                this.form.get('leaderUserId').setErrors({ exists: true });
            });
        });
    }

    resetFilter(): boolean {
        const searchFilter = this.defaultFilter;

        this.form.patchValue({
            userId: searchFilter.userId,
            leaderUserId: searchFilter.leaderUserId,
            from: null,
            to: null
        });

        this.submitFilter();
        return false;
    }

    submitFilter(): void {
        const searchFilter = InterviewSearchParams.create(this.form.value);

        this.filterChanged.emit(searchFilter);
        this.storage.save('InterviewSearchParams', searchFilter.toInterface());
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }
}
