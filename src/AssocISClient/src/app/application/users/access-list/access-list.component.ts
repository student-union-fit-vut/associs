import { AccessListItem } from './../../../core/models/users/access/access-list-item';
import { PaginationParams } from 'src/app/shared/components/data-list/models';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { AccessService } from './../../../core/services/access.service';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';
import { AccessDefinitionSearchParams } from './../../../core/models/users/access/access-definition-search-params';
import { Component, ViewChild } from '@angular/core';

@Component({
    selector: 'app-access-list',
    templateUrl: './access-list.component.html'
})
export class AccessListComponent {
    @ViewChild('datalist', { static: false }) datalist: DataListComponent;

    private filter: AccessDefinitionSearchParams;

    constructor(
        private accessService: AccessService,
        private modalService: ModalService
    ) { }

    filterChanged(filter: any): void {
        this.filter = filter;
        if (this.datalist) { this.datalist.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.accessService.getAccessList(this.filter, pagination).subscribe(data => this.datalist.setData(data));
    }

    removeAccess(item: AccessListItem): void {
        this.modalService.showQuestion('Odebrání definice přístupu', 'Opravdu si přejete odebrat definici přístupu?', 'lg')
            .onAccept.subscribe(() => this.accessService.removeAccessDefinition(item.id).subscribe(() => {
                this.modalService.showNotification('Odebrání definice přístupu', 'Definice přístupu byla úspěšně odebrána.', 'lg');
                this.datalist.onChange();
            }));
    }
}
