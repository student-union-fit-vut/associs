import { SelectItems } from './../../../shared/components/select/select-item';
import { ValidationHelper } from './../../../core/lib/validators';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { DateTime } from './../../../core/models/datetime';
import { ActivatedRoute, Router } from '@angular/router';
import { InterviewService } from './../../../core/services/interview.service';
import { DataService } from './../../../core/services/data.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Interview, UpdateInterviewParams } from 'src/app/core/models';
import { ValidationsService } from 'src/app/core/services/validations.service';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-interview-edit',
    templateUrl: './interview-edit.component.html'
})
export class InterviewEditComponent implements OnInit {
    users: SelectItems = [];
    interview: Interview;
    form: FormGroup;
    isCreate: boolean;

    constructor(
        private fb: FormBuilder,
        private dataService: DataService,
        private validation: ValidationsService,
        private interviewService: InterviewService,
        private router: Router,
        private route: ActivatedRoute,
        private modalService: ModalService
    ) { }

    ngOnInit(): void {
        this.isCreate = this.route.routeConfig.data?.isAdd ?? false;

        this.dataService.getUserSelectList().subscribe(users => {
            users.unshift({ key: 0, value: 'Nevybráno' });
            this.users = users;
        });

        if (this.isCreate) {
            this.interview = new Interview();
            this.initForm();
        } else {
            const id = parseInt(this.route.snapshot.params.id, 10);
            this.interviewService.getInterview(id).subscribe(interview => {
                this.interview = interview;
                this.initForm();
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    submitForm(): void {
        const params = new UpdateInterviewParams(
            this.form.value.user,
            this.form.value.leader,
            DateTime.fromISOString(this.form.value.at),
            this.form.value.note
        );

        if (this.isCreate) {
            this.interviewService.createInterview(params).subscribe(_ => {
                this.modalService.showNotification('Vytvoření pohovoru', 'Pohovor byl úspěšně vytvořen.').onClose.subscribe(() => {
                    this.router.navigate(['/', 'app', 'users', 'interviews']);
                });
            });
        } else {
            this.interviewService.updateInterview(this.interview.id, params).subscribe(() => {
                this.modalService.showNotification('Úprava pohovoru', 'Pohovor byl úspěšně upraven.').onClose.subscribe(() => {
                    this.router.navigate(['/', 'app', 'users', 'interviews']);
                });
            });
        }
    }

    private initForm(): void {
        this.form = this.fb.group({
            user: [this.interview.user?.id || 0, Validators.required],
            leader: [this.interview.leader?.id || 0],
            at: [this.interview.at?.toLocaleISOString(false)?.slice(0, 16), Validators.required],
            note: [this.interview.note]
        });

        this.form.get('user').valueChanges.pipe(filter(o => o && o !== 0)).subscribe(userId => {
            this.validation.userIdExists(userId).pipe(filter(o => !o)).subscribe(() => {
                this.form.get('user').setErrors({ exists: true });
            });
        });

        this.form.get('user').valueChanges.pipe(filter(o => !o || o === 0)).subscribe(() => {
            this.form.get('user').setErrors({ required: true });
        });

        this.form.get('leader').valueChanges.pipe(filter(o => o && o !== 0)).subscribe(leaderId => {
            this.validation.userIdExists(leaderId).pipe(filter(o => !o)).subscribe(() => {
                this.form.get('leader').setErrors({ exists: true });
            });
        });
    }
}
