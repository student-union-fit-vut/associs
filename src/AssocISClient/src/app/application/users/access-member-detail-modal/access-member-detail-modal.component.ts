import { AccessService } from 'src/app/core/services/access.service';
import { AccessMemberListItem } from './../../../core/models/users/access/access-member-list-item';
import { Access, AccessMember } from 'src/app/core/models';
import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-access-member-detail-modal',
    templateUrl: './access-member-detail-modal.component.html',
})
export class AccessMemberDetailModalComponent implements OnInit {
    @Input() access: Access;
    @Input() memberListItem: AccessMemberListItem;

    accessMember: AccessMember;

    constructor(
        private accessService: AccessService
    ) { }

    ngOnInit(): void {
        this.accessService.getAccessMember(this.access.id, this.memberListItem.user.id).subscribe(member => this.accessMember = member);
    }
}
