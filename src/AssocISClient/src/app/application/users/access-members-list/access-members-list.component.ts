import { SelectItems } from 'src/app/shared/components/select/select-item';
import { AccessMemberListItem } from './../../../core/models/users/access/access-member-list-item';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { SetAccessMemberParams } from 'src/app/core/models';
import { filter } from 'rxjs/operators';
import { DataService } from './../../../core/services/data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PaginationParams } from 'src/app/shared/components/data-list/models';
import { Access } from './../../../core/models/users/access/access';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';
import { AccessService } from 'src/app/core/services/access.service';
import { ValidationsService } from 'src/app/core/services/validations.service';
import { AccessMemberDetailModalComponent } from '../access-member-detail-modal/access-member-detail-modal.component';
import { ValidationHelper } from 'src/app/core/lib/validators';

@Component({
    selector: 'app-access-members-list',
    templateUrl: './access-members-list.component.html'
})
export class AccessMembersListComponent implements OnInit {
    @Input() access: Access;
    @ViewChild('memberList', { static: false }) memberList: DataListComponent;

    createMemberForm: FormGroup;
    users: SelectItems = [];

    constructor(
        private accessService: AccessService,
        private fb: FormBuilder,
        private dataService: DataService,
        private validation: ValidationsService,
        private modalService: ModalService
    ) { }

    ngOnInit(): void {
        this.dataService.getUserSelectList().subscribe(users => {
            this.users = users;
            this.users.unshift({ key: null, value: 'Nevybráno' });
        });

        this.createMemberForm = this.fb.group({
            user: [null, Validators.required],
            allowed: [false],
            note: ['']
        });

        this.createMemberForm.get('user').valueChanges.pipe(filter(o => o && o !== 0)).subscribe(id => {
            this.validation.userIdExists(id).pipe(filter(o => !o)).subscribe(() => {
                this.createMemberForm.get('user').setErrors({ exists: true });
            });
        });
    }

    readData(pagination: PaginationParams): void {
        this.accessService.getAccessMembers(this.access.id, pagination).subscribe(list => {
            this.memberList.setData(list);
            this.users = this.users.filter(o => !list.data.some(x => x.user.id === o.key));
        });
    }

    submitForm(): void {
        const params = new SetAccessMemberParams(
            this.createMemberForm.value.user,
            this.createMemberForm.value.allowed,
            this.createMemberForm.value.note
        );

        this.accessService.setAccessMember(this.access.id, params).subscribe(_ => {
            this.modalService.showNotification(
                'Vytvoření přístupu',
                'Přístup pro člena byl úspěšně vytvořen.', 'lg'
            ).onClose.subscribe(() => this.memberList.onChange());
        });
    }

    setAccess(item: AccessMemberListItem, allowed: boolean): void {
        const message = `Opravdu si přejete tomuto členovi ${(!allowed ? 'zakázat' : 'povolit')} přístup?`;

        this.modalService.showQuestion('Úprava přístupu', message).onAccept.subscribe(() => {
            const params = new SetAccessMemberParams(item.user.id, allowed, item.note);

            this.accessService.setAccessMember(this.access.id, params).subscribe(_ => {
                this.modalService.showNotification(
                    'Úprava přístupu',
                    'Přístup pro člena byl úspěšně upraven.',
                    'lg'
                ).onClose.subscribe(() => this.memberList.onChange());
            });
        });
    }

    openAccessMemberDetail(item: AccessMemberListItem): void {
        const modal = this.modalService.showCustomModal<AccessMemberDetailModalComponent>(AccessMemberDetailModalComponent);

        modal.componentInstance.access = this.access;
        modal.componentInstance.memberListItem = item;
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.createMemberForm, controlId, errorId);
    }
}
