import { SelectHelpers } from './../../../core/lib/support';
import { SelectItems } from './../../../shared/components/select/select-item';
import { ValidationHelper } from './../../../core/lib/validators';
import { UpdateAccessDefinitionParams } from './../../../core/models/users/access/update-access-definition-params';
import { AccessType, AccessTypeTexts } from './../../../core/enums/access-type';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Access } from 'src/app/core/models';
import { AccessService } from 'src/app/core/services/access.service';

@Component({
    selector: 'app-access-edit',
    templateUrl: './access-edit.component.html'
})
export class AccessEditComponent implements OnInit {
    access: Access;
    form: FormGroup;
    isCreate: boolean;
    activeTabId: string;

    accessTypes: SelectItems = [];

    constructor(
        private fb: FormBuilder,
        private accessService: AccessService,
        private router: Router,
        private route: ActivatedRoute,
        private modalService: ModalService
    ) { }

    ngOnInit(): void {
        this.isCreate = this.route.routeConfig.data?.isAdd ?? false;
        this.accessTypes = SelectHelpers.getSelectItemsFromEnum(AccessType, AccessTypeTexts);

        if (this.isCreate) {
            this.access = new Access();
            this.initForm();
        } else {
            const id = parseInt(this.route.snapshot.params.id, 10);
            this.accessService.getAccessDefinition(id).subscribe(access => {
                this.access = access;
                this.initForm();
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.form, controlId, errorId);
    }

    submitForm(): void {
        const params = new UpdateAccessDefinitionParams(
            this.form.value.type,
            this.form.value.description
        );

        if (this.isCreate) {
            this.accessService.createAccessDefinition(params).subscribe(_ => {
                this.modalService.showNotification('Vytvoření definice přístupu', 'Definice přístupu byla úspěšně vytvořena.', 'lg')
                    .onClose.subscribe(() => this.router.navigate(['/', 'app', 'users', 'access']));
            });
        } else {
            this.accessService.updateAccessDefinition(this.access.id, params).subscribe(() => {
                this.modalService.showNotification('Úprava definice přístupu', 'Definice přístupu byla úspěšně upravena.', 'lg')
                    .onClose.subscribe(() => this.router.navigate(['/', 'app', 'users', 'access']));
            });
        }

    }

    private initForm(): void {
        this.form = this.fb.group({
            type: [this.access.type, Validators.required],
            description: [this.access.description]
        });
    }
}
