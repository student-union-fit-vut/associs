import { SelectHelpers } from './../../../core/lib/support';
import { UserState, UserStateTexts } from './../../../core/enums/user-state';
import { SelectItems } from './../../../shared/components/select/select-item';
import { PasswordChangeParams } from './../../../core/models/users/password-change-params';
import { ValidationHelper, Validations } from './../../../core/lib/validators';
import { DateTime } from './../../../core/models/datetime';
import { AppService } from './../../../core/services/app.service';
import { PermissionsMask, PermissionsMaskTexts } from './../../../core/enums/permissions';
import { Dictionary } from './../../../core/models/common';
import { ValidationsService } from './../../../core/services/validations.service';
import { AuthService } from './../../../core/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserDetail } from './../../../core/models/users/user-detail';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { Permissions } from 'src/app/core/enums/permissions';
import { filter, debounceTime } from 'rxjs/operators';
import { Support } from 'src/app/core/lib/support';
import { TeamMembership, UserNoteParams, UserParams } from 'src/app/core/models';
import { DataService } from 'src/app/core/services/data.service';
import { TeamsService } from 'src/app/core/services/teams.service';

@Component({
    selector: 'app-edit',
    templateUrl: './edit-user.component.html',
    styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
    userDetail: UserDetail;
    userForm: FormGroup;
    myNoteForm: FormGroup;
    passwordForm: FormGroup;
    isCurrentUser = false;
    isCreate = false;
    permissionsMaskOptions: Dictionary<number, string>;
    profilePicture: string;
    activeTabId: string;
    addUserToTeamForm: FormGroup;

    userStates: SelectItems = [];
    availableTeams: SelectItems = [];

    constructor(
        private router: Router,
        private userService: UserService,
        private modalService: ModalService,
        private authService: AuthService,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private validations: ValidationsService,
        private appService: AppService,
        private data: DataService,
        private teamsService: TeamsService
    ) {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    get haveHigherPerms(): boolean {
        return this.authService.loggedUser.hasPermission(Permissions.UserManager);
    }

    get canModify(): boolean {
        return this.isCurrentUser || this.haveHigherPerms;
    }

    get haveLowerPerms(): boolean {
        return this.isCurrentUser && !this.haveHigherPerms;
    }

    get isCurrentUserTeamLeader(): boolean {
        return this.authService.loggedUser.hasPermission(Permissions.TeamLeader);
    }

    get isDiscordAvailable(): boolean {
        return this.appService.clientConfiguration.discordEnabled;
    }

    ngOnInit(): void {
        this.isCreate = this.route.routeConfig.data?.isAdd ?? false;

        this.permissionsMaskOptions = Object.keys(PermissionsMask).filter(o => isNaN(parseInt(o, 10))).map(o => ({
            key: PermissionsMask[o],
            value: PermissionsMaskTexts[o]
        })).filter(o => o.value);

        this.userStates = SelectHelpers.getSelectItemsFromEnum(UserState, UserStateTexts);

        if (this.isCreate) {
            this.userDetail = new UserDetail();
            this.initForm();
        } else {
            const userId = parseInt(this.route.snapshot.params.id, 10);
            this.isCurrentUser = this.authService.loggedUser.id === userId;

            this.userService.getUser(userId).subscribe(user => {
                this.userDetail = user;
                this.initForm();
            });
        }
    }

    isInvalid(controlId: string, errorId: string = null): boolean {
        return ValidationHelper.isInvalid(this.userForm, controlId, errorId);
    }

    changeProfilePicture(): void {
        const element = document.getElementById('profilePictureFileElement') as HTMLInputElement;

        element.click();
        element.onchange = _ => {
            if (element.files && element.files[0]) {
                this.userService.updateProfilePicture(element.files[0]).subscribe(() => {
                    this.loadProfilePicture();
                    this.appService.notifyProfilePictureChange();
                });
            }
        };
    }

    clearProfilePicture(): void {
        this.userService.clearProfilePicture().subscribe(() => {
            this.loadProfilePicture();
            this.appService.notifyProfilePictureChange();
        });
    }

    loadProfilePicture(): void {
        this.userService.getProfilePicture(this.isCreate ? -1 : this.userDetail.id)
            .subscribe(profilePic => this.profilePicture = Support.convertImageToBase64(profilePic));
    }

    submitForm(): void {
        const formData = this.userForm.value;
        const data = new UserParams(
            formData.username,
            formData.username !== this.userDetail.username,
            formData.name,
            formData.surname,
            formData.email,
            formData.phoneNumber,
            DateTime.fromISOString(formData.joinedAt).toUtcISOString(),
            formData.studyProgram,
            formData.testPeriodEnd ? DateTime.fromISOString(formData.testPeriodEnd).toUtcISOString() : null,
            parseInt(formData.userState, 10),
            formData.leavedAt ? DateTime.fromISOString(formData.leavedAt).toUtcISOString() : null,
            formData.permissions,
            formData.globalNote,
            formData.discordId,
            formData.googlePrimaryEmail,
            formData.createGSuiteFromEmail
        );

        if (this.isCreate) {
            this.userService.createUser(data).subscribe(user => {
                this.modalService.showNotification('Vytvoření člena', 'Člen byl úspěšně vytvořen.').onClose.subscribe(_ => {
                    this.router.navigate(['/', 'app', 'users', user.id]);
                });
            });
        } else {
            if (this.isCurrentUser) {
                this.userService.updateLoggedUser(data).subscribe(_ => {
                    this.modalService.showNotification(
                        'Změna přihlášeného člena',
                        'Tvůj účet byl úspěšně upraven.'
                    ).onClose.subscribe(__ => this.router.navigate(['/', 'app', 'users']));
                });
            } else {
                this.userService.updateUser(this.userDetail.id, data).subscribe(_ => {
                    this.modalService.showNotification('Změna člena', 'Člen byl úspěšně upraven.').onClose.subscribe(__ => {
                        this.router.navigate(['/', 'app', 'users']);
                    });
                });
            }
        }
    }

    teamClick(id: number): void {
        if (!this.isCurrentUserTeamLeader) { return; }
        this.router.navigate(['/', 'app', 'teams', id]);
    }

    submitMyNote(): void {
        const params = new UserNoteParams(this.myNoteForm.value.myNote);

        this.userService.setMyNote(this.userDetail.id, params).subscribe(_ => {
            this.modalService.showNotification('Změna osobní poznámky', 'Osobní poznámka byla úspěšně změněna.', 'lg');
        });
    }

    clearPassword(): void {
        this.modalService.showQuestion('Smazání hesla',
            'Opravdu si přejete smazat tomuto členovi uložené heslo?<br>Po smazání hesla se člen nedokáže heslem přihlásit a neobdrží vyšší oprávnění.', 'xl')
            .onAccept.subscribe(_ => {
                this.userService.deletePassword(this.userDetail.id).subscribe(user => {
                    this.modalService.showNotification('Smazání hesla', 'Heslo bylo úspěšně smazáno. Člen nyní má blokované všechny vyšší oprávnění.', 'lg');
                    this.userDetail.havePassword = user.havePassword;
                });
            });
    }

    resetPassword(): void {
        const text = 'Vázně si přejete provést reset hesla?<br><i>Reset hesla se nevztahuje na hesla v externích systémech (např.: Google, Discord, ...)</i>';

        this.modalService.showQuestion('Reset hesla', text, 'xl').onAccept.subscribe(() => {
            this.userService.resetPassword(this.userDetail.id).subscribe(result => {
                const resetText = `Nové heslo pro člena ${result.user.fullName} (${result.user.username}):` +
                    `<h2 class="text-center mt-3 mb-3"><code>${result.rawPassword}</code></h2>
                    <b>POZOR: Po uzavření tohoto okna již nepůjde heslo zpětně zjistit.</b>`;
                this.modalService.showNotification('Reset hesla', resetText, 'lg');
            });
        });
    }

    changePassword(): void {
        this.modalService.showQuestion('Změna hesla', 'Opravdu si přejete změnit heslo?').onAccept.subscribe(_ => {
            const params = new PasswordChangeParams(
                this.userDetail.havePassword ? this.passwordForm.value.oldPassword : '-',
                this.passwordForm.value.newPassword,
                this.passwordForm.value.confirmPassword
            );

            this.userService.changePassword(params).subscribe(user => {
                this.modalService.showNotification('Změna hesla', 'Heslo bylo úspěšně změněno.', 'lg');
                this.userDetail.havePassword = user.havePassword;

                this.passwordForm.patchValue({
                    oldPassword: '',
                    newPassword: '',
                    confirmPassword: ''
                });

                this.passwordForm.markAsUntouched();
            });
        });
    }

    anonymize(): void {
        this.modalService.showQuestion(
            'Anonymizace člena',
            `Opravdu si přejete anonymizovat člena ${this.userDetail.fullName} (${this.userDetail.username})?<br><b>Anonymizace člena je nevratná operace!</b>`
        ).onAccept.subscribe(_ => {
            this.userService.anonymizeUser(this.userDetail.id).subscribe(__ => {
                this.modalService.showNotification('Anonymizace člena', 'Člen byl úspěšně anonymizován.').onClose.subscribe(() => {
                    this.router.navigate(['/', 'app', 'users']);
                });
            });
        });
    }

    submitAddUserToTeam(): void {
        const teamId = this.addUserToTeamForm.get('team').value as number;

        this.teamsService.addTeamMember(teamId, this.userDetail.id).subscribe(team => {
            this.userDetail.teams.push({
                id: team.id,
                isLeader: false,
                name: team.name,
                teamDiscordId: team.discordRoleId
            });

            this.modalService.showNotification('Přidání člena do týmu.', `Člen byl úspěšně přidán do týmu "${team.name}"`);
            this.initAddUserToTeamForm();
        });
    }

    removeUserFromTeamClick(event: Event, membership: TeamMembership): void {
        event.stopPropagation();

        this.modalService.showQuestion('Odebrání člena z týmu', `Opravdu si přejete odebrat člena z týmu "${membership.name}"?`)
            .onAccept.subscribe(() => {
                this.teamsService.removeTeamMember(membership.id, this.userDetail.id).subscribe(_ => {
                    this.userDetail.teams = this.userDetail.teams.filter(o => o.id !== membership.id);
                    this.modalService.showNotification('Odebrání člena z týmu', `Člen byl úspěšně odebrán z týmu "${membership.name}"`);
                    this.initAddUserToTeamForm();
                });
            });
    }

    private initForm(): void {
        this.userForm = this.fb.group({
            name: [{ value: this.userDetail.name, disabled: !this.canModify }, Validators.required],
            surname: [{ value: this.userDetail.surname, disabled: !this.canModify }, Validators.required],
            username: [{ value: this.userDetail.username, disabled: !this.canModify }, Validators.required],
            email: [
                { value: this.userDetail.email, disabled: !this.canModify },
                Validators.compose([Validators.required, Validators.email])
            ],
            discordId: [{ value: this.userDetail.discordId, disabled: !this.canModify }],
            phoneNumber: [{ value: this.userDetail.phoneNumber, disabled: !this.canModify }, Validators.maxLength(20)],
            googlePrimaryEmail: [
                { value: this.userDetail.googlePrimaryEmail, disabled: !this.canModify && !this.isCreate },
                Validators.email
            ],
            createGSuiteFromEmail: [false],
            joinedAt: [
                {
                    value: this.userDetail.joinedAt?.toLocaleISOString(false).slice(0, 10),
                    disabled: !this.canModify || this.haveLowerPerms
                },
                Validators.required
            ],
            leavedAt: [
                { value: this.userDetail.leavedAt?.toLocaleISOString(false).slice(0, 10), disabled: !this.canModify || this.haveLowerPerms }
            ],
            testPeriodEnd: [
                {
                    value: this.userDetail.testPeriodEnd?.toLocaleISOString(false).slice(0, 10),
                    disabled: !this.canModify || this.haveLowerPerms
                }
            ],
            userState: [
                { value: this.userDetail.userState ?? UserState.BasicMember, disabled: !this.canModify || this.haveLowerPerms },
                Validators.required
            ],
            permissions: [{ value: this.userDetail.permissions, disabled: !this.canModify || this.haveLowerPerms }],
            studyProgram: [{ value: this.userDetail.studyProgram, disabled: !this.canModify }],
            globalNote: [{ value: this.userDetail.globalNote, disabled: !this.canModify }]
        }, {
            validators: Validators.compose([
                Validations.checkDates('joinedAt', 'leavedAt'),
                Validations.checkDates('joinedAt', 'testPeriodEnd')
            ])
        });

        this.userForm.get('username').valueChanges.pipe(
            filter(o => o.length > 0),
            debounceTime(200)
        ).subscribe(username => {
            this.validations.usernameExists(username).pipe(filter(o => o)).subscribe(() => {
                this.userForm.get('username').setErrors({ unique: true });
            });
        });

        this.userForm.get('phoneNumber').valueChanges.pipe(
            filter(o => o.length > 0),
            debounceTime(200)
        ).subscribe(phoneNumber => {
            this.validations.validatePhoneNumber(phoneNumber).pipe(filter(o => !o)).subscribe(() => {
                this.userForm.get('phoneNumber').setErrors({ format: true });
            });
        });

        this.loadProfilePicture();

        this.myNoteForm = this.fb.group({
            myNote: [this.userDetail.userNote]
        });

        this.passwordForm = this.fb.group({
            oldPassword: [''],
            newPassword: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        }, {
            validators: Validators.compose([
                Validations.checkPasswords('newPassword', 'confirmPassword'),
                Validations.checkPasswords('oldPassword', 'newPassword', true)
            ])
        });

        if (this.userDetail.havePassword) {
            this.passwordForm.get('oldPassword').setValidators(Validators.required);
        }

        this.passwordForm.valueChanges.subscribe(_ => {
            for (const control of Object.keys(this.passwordForm.controls)) {
                this.passwordForm.controls[control].updateValueAndValidity({ emitEvent: false });
            }
        });

        this.userForm.get('createGSuiteFromEmail').valueChanges.subscribe(checked => {
            console.log('x');
            const field = this.userForm.get('googlePrimaryEmail');
            if (checked) {
                field.setValue(null);
                field.disable();
            } else {
                field.enable();
            }
        });

        this.initAddUserToTeamForm();
    }

    private initAddUserToTeamForm(): void {
        this.data.getTeamsSelectList([this.userDetail.id]).subscribe(teams => {
            this.availableTeams = teams.map(o => o);

            this.addUserToTeamForm = this.fb.group({
                team: [null, Validators.required]
            });
        });
    }
}
