import { ModalService } from './../../../shared/components/modal/modal.service';
import { UserFilter } from './../../../core/models/users/user-filter';
import { Component, ViewChild } from '@angular/core';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';
import { UserService } from 'src/app/core/services/user.service';
import { PaginationParams } from 'src/app/shared/components/data-list/models';
import { User } from 'src/app/core/models';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
})
export class ListComponent {
    @ViewChild('datalist', { static: false }) datalist: DataListComponent;

    private filter: UserFilter;

    constructor(
        private userService: UserService,
        private modalService: ModalService
    ) { }

    filterChanged(filter: UserFilter): void {
        this.filter = filter;
        if (this.datalist) { this.datalist.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.userService.getUserList(this.filter, pagination).subscribe(data => this.datalist.setData(data));
    }

    openUserWarning(user: User): void {
        if (user.anyLoggedIn) { return; }
        this.modalService.showNotification('Varování', 'Tento uživatel se ještě nikdy nepřihlásil.');
    }

}
