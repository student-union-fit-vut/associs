import { AccessEditComponent } from './access-edit/access-edit.component';
import { AccessDashboardComponent } from './access-dashboard/access-dashboard.component';
import { InterviewEditComponent } from './interview-edit/interview-edit.component';
import { InterviewDashboardComponent } from './interview-dashboard/interview-dashboard.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ListDashboardComponent } from './list-dashboard/list-dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', component: ListDashboardComponent, data: { breadcrumb: 'Seznam členů' } },
    { path: 'add', component: EditUserComponent, data: { breadcrumb: 'Vytvoření člena', isAdd: true } },
    { path: 'interviews', component: InterviewDashboardComponent, data: { breadcrumb: 'Pohovory' } },
    { path: 'interviews/add', component: InterviewEditComponent, data: { breadcrumb: 'Vytvoření pohovoru', isAdd: true } },
    { path: 'interviews/:id', component: InterviewEditComponent, data: { breadcrumb: 'Úprava pohovoru', isAdd: false } },
    { path: 'access', component: AccessDashboardComponent, data: { breadcrumb: 'Přístupy' } },
    { path: 'access/add', component: AccessEditComponent, data: { breadcrumb: 'Vytvoření definice přístupu', isAdd: true } },
    { path: 'access/:id', component: AccessEditComponent, data: { breadcrumb: 'Editace definice přístupu', isAdd: false } },
    { path: ':id', component: EditUserComponent, data: { breadcrumb: 'Editace člena', isAdd: false } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule { }
