import { UsersRoutingModule } from './users-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { ListDashboardComponent } from './list-dashboard/list-dashboard.component';
import { ListFilterComponent } from './list-filter/list-filter.component';
import { ListComponent } from './list/list.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { InterviewListComponent } from './interview-list/interview-list.component';
import { InterviewDashboardComponent } from './interview-dashboard/interview-dashboard.component';
import { InterviewFilterComponent } from './interview-filter/interview-filter.component';
import { InterviewEditComponent } from './interview-edit/interview-edit.component';
import { AccessDashboardComponent } from './access-dashboard/access-dashboard.component';
import { AccessFilterComponent } from './access-filter/access-filter.component';
import { AccessListComponent } from './access-list/access-list.component';
import { AccessEditComponent } from './access-edit/access-edit.component';
import { AccessMembersListComponent } from './access-members-list/access-members-list.component';
import { AccessMemberDetailModalComponent } from './access-member-detail-modal/access-member-detail-modal.component';


@NgModule({
    declarations: [
        ListDashboardComponent,
        ListFilterComponent,
        ListComponent,
        EditUserComponent,
        InterviewListComponent,
        InterviewDashboardComponent,
        InterviewFilterComponent,
        InterviewEditComponent,
        AccessDashboardComponent,
        AccessFilterComponent,
        AccessListComponent,
        AccessEditComponent,
        AccessMembersListComponent,
        AccessMemberDetailModalComponent
    ],
    imports: [
        SharedModule,
        UsersRoutingModule
    ]
})
export class UsersModule { }
