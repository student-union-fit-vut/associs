import { StorageService } from './../../../core/services/storage.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserFilter } from 'src/app/core/models';
import { SelectItems } from 'src/app/shared/components/select/select-item';
import { UserState, UserStateTexts } from 'src/app/core/enums';
import { SelectHelpers } from 'src/app/core/lib/support';

@Component({
    selector: 'app-list-filter',
    templateUrl: './list-filter.component.html',
})
export class ListFilterComponent implements OnInit {
    @Output() filterChanged = new EventEmitter<UserFilter>();

    form: FormGroup;
    userStates: SelectItems = [];

    constructor(
        private fb: FormBuilder,
        private storage: StorageService
    ) { }

    get defaultFilter(): UserFilter { return new UserFilter(); }

    ngOnInit(): void {
        this.userStates = SelectHelpers.getSelectItemsFromEnum(UserState, UserStateTexts);
        const filter = UserFilter.fromInterface(this.storage.read<any>('UsersListFilter')) || new UserFilter();

        this.initFilter(filter);
        this.submitFilter();
    }

    initFilter(filter: UserFilter): void {
        this.form = this.fb.group({
            nameQuery: [filter.nameQuery],
            ignoreLeftUsers: [filter.ignoreLeftUsers],
            ignoreTestPeriodUsers: [filter.ignoreTestPeriodUsers],
            ignoreAnonymized: [filter.ignoreAnonymized],
            states: [filter.states]
        });
    }

    resetFilter(): boolean {
        const defaultFilter = this.defaultFilter;

        this.form.patchValue({
            nameQuery: defaultFilter.nameQuery,
            ignoreLeftUsers: defaultFilter.ignoreLeftUsers,
            ignoreTestPeriodUsers: defaultFilter.ignoreTestPeriodUsers,
            ignoreAnonymized: defaultFilter.ignoreAnonymized,
            states: defaultFilter.states
        });

        this.submitFilter();
        return false;
    }

    submitFilter(): void {
        const filter = UserFilter.fromInterface(this.form.value);

        this.filterChanged.emit(filter);
        this.storage.save('UsersListFilter', filter.toInterface());
    }
}
