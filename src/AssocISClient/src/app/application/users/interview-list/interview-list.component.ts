import { InterviewListItem } from './../../../core/models/users/interviews/interview-list-item';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { InterviewService } from './../../../core/services/interview.service';
import { DataListComponent } from 'src/app/shared/components/data-list/data-list.component';
import { InterviewSearchParams } from 'src/app/core/models';
import { PaginationParams } from 'src/app/shared/components/data-list/models';
import { Component, ViewChild } from '@angular/core';

@Component({
    selector: 'app-interview-list',
    templateUrl: './interview-list.component.html'
})
export class InterviewListComponent {
    @ViewChild('datalist', { static: false }) datalist: DataListComponent;

    private filter: InterviewSearchParams;

    constructor(
        private interviewService: InterviewService,
        private modalService: ModalService
    ) { }

    filterChanged(filter: InterviewSearchParams): void {
        this.filter = filter;
        if (this.datalist) { this.datalist.onChange(); }
    }

    readData(pagination: PaginationParams): void {
        this.interviewService.getInterviewList(this.filter, pagination).subscribe(data => this.datalist.setData(data));
    }

    removeInterview(item: InterviewListItem): void {
        this.modalService.showQuestion('Odebrání pohovoru', 'Opravdu si přejete smazat pohovor?', 'lg').onAccept.subscribe(() => {
            this.interviewService.removeInterview(item.id).subscribe(() => {
                this.modalService.showNotification('Odebrání pohovoru', 'Pohovor byl úspěšně odebrán.', 'lg');
                this.datalist.onChange();
            });
        });
    }
}
