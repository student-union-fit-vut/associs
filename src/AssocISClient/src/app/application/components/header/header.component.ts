import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { distinctUntilChanged, filter } from 'rxjs/operators';

interface IBreadcrumb {
    name: string;
    url: string;
}

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public breadcrumbs: IBreadcrumb[] = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private titleService: Title
    ) { }

    ngOnInit(): void {
        this.processBreadcrumbs();

        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
            distinctUntilChanged()
        ).subscribe(() => this.processBreadcrumbs());
    }

    processBreadcrumbs(): void {
        this.breadcrumbs = this.buildBreadcrumb(this.route.root);
        this.titleService.setTitle(`${this.breadcrumbs.map(o => o.name).join(' / ')} | AssocIS`);
    }

    private buildBreadcrumb(route: ActivatedRoute, url: string = '/app', breadcrumbs: IBreadcrumb[] = []): IBreadcrumb[] {
        // If no routeConfig is avalailable we are on the root path
        const label = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.breadcrumb : '';
        let path = route.routeConfig && route.routeConfig.data ? route.routeConfig.path : '';

        // If the route is dynamic route such as ':id', remove it
        const lastRoutePart = path.split('/').pop();
        const isDynamicRoute = lastRoutePart.startsWith(':');
        if (isDynamicRoute && !!route.snapshot) {
            const paramName = lastRoutePart.split(':')[1];
            path = path.replace(lastRoutePart, route.snapshot.params[paramName]);
        }

        // In the routeConfig the complete path is not available,
        // so we rebuild it each time
        const nextUrl = path ? `${url}/${path}` : url;

        const breadcrumb: IBreadcrumb = {
            name: label,
            url: nextUrl,
        };

        // Only adding route with non-empty label
        const newBreadcrumbs = breadcrumb.name ? [...breadcrumbs, breadcrumb] : [...breadcrumbs];
        if (route.firstChild) {
            // If we are not on our current path yet,
            // there will be more children to look after, to build our breadcumb
            return this.buildBreadcrumb(route.firstChild, nextUrl, newBreadcrumbs);
        }

        return newBreadcrumbs;
    }

}
