import { AppService } from './../../../core/services/app.service';
import { Support } from './../../../core/lib/support';
import { UserService } from './../../../core/services/user.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { ClientConfiguration, JwtTokenContent } from 'src/app/core/models';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    public profilePicture: any;

    constructor(
        private authService: AuthService,
        private userService: UserService,
        private appService: AppService
    ) { }


    get loggedUser(): JwtTokenContent { return this.authService.loggedUser; }
    get config(): ClientConfiguration { return this.appService.clientConfiguration; }

    ngOnInit(): void {
        this.reloadProfilePicture();
        this.appService.profilePictureChange.subscribe(_ => this.reloadProfilePicture());
    }

    reloadProfilePicture(): void {
        this.userService.getProfilePicture(this.loggedUser.id).subscribe(data => this.profilePicture = Support.convertImageToBase64(data));
    }
}
