import { JwtTokenContent } from './../../../core/models/auth/jwt-token';
import { Permissions } from './../../../core/enums/permissions';
import { MenuItem, MenuSubItem } from './menu-item';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/core/services/app.service';
import { ClientConfiguration } from 'src/app/core/models';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
    public items: MenuItem[] = [];

    constructor(
        private authService: AuthService,
        public router: Router,
        private appService: AppService
    ) { }

    get loggedUser(): JwtTokenContent { return this.authService.loggedUser; }
    get config(): ClientConfiguration { return this.appService.clientConfiguration; }

    ngOnInit(): void {
        this.items = [
            new MenuItem('Členové', 'fa-users', 'members', [
                new MenuSubItem('Seznam členů', 'fa-list', '/app/users', [Permissions.Everyone], this.config.modules.Members),
                new MenuSubItem('Nový člen', 'fa-plus', '/app/users/add', [Permissions.UserManager], this.config.modules.Members),
                new MenuSubItem('Pohovory', 'fa-comments', '/app/users/interviews', [Permissions.UserManager],
                    this.config.modules.Interviews),
                new MenuSubItem('Nový pohovor', 'fa-comment-dots', '/app/users/interviews/add', [Permissions.UserManager],
                    this.config.modules.Interviews),
                new MenuSubItem('Přístupy', 'fa-unlock-alt', '/app/users/access', [Permissions.UserManager], this.config.modules.Accesses),
                new MenuSubItem('Nový přístup', 'fa-plus', '/app/users/access/add', [Permissions.UserManager], this.config.modules.Accesses)
            ]),
            new MenuItem('Týmy', 'fa-user-friends', 'teams', [
                new MenuSubItem('Seznam', 'fa-list', '/app/teams', [Permissions.Everyone], this.config.modules.Teams),
                new MenuSubItem('Nový tým', 'fa-plus', '/app/teams/add', [Permissions.UserManager], this.config.modules.Teams)
            ]),
            new MenuItem('Schůze', 'fa-handshake', 'meetings', [
                new MenuSubItem('Seznam', 'fa-list', '/app/meetings', [Permissions.Everyone], this.config.modules.Meetings),
                new MenuSubItem('Nová schůze', 'fa-plus', '/app/meetings/add', [Permissions.MeetingManager, Permissions.TeamLeader],
                    this.config.modules.Meetings),
                new MenuSubItem('Report', 'fa-receipt', '/app/meetings/report', [Permissions.MeetingManager, Permissions.TeamLeader],
                    this.config.modules.Meetings)
            ]),
            new MenuItem('Finance', 'fa-money-bill-wave', 'finances', [
                new MenuSubItem('Transakce', 'fa-file-invoice', '/app/finance/transactions', [Permissions.BasicMember],
                    this.config.modules.Finance),
                new MenuSubItem('Nová transakce', 'fa-plus', '/app/finance/transactions/add', [Permissions.BasicMember],
                    this.config.modules.Finance),
                new MenuSubItem('Reporty', 'fa-receipt', '/app/finance/transactions/reports', [Permissions.FinanceManager],
                    this.config.modules.Finance),
                new MenuSubItem('Dodavatelé', 'fa-truck-moving', '/app/finance/suppliers', [Permissions.BasicMember],
                    this.config.modules.Finance),
                new MenuSubItem('Nový dodavatel', 'fa-plus', '/app/finance/suppliers/add', [Permissions.FinanceManager],
                    this.config.modules.Finance),
                new MenuSubItem('Účty', 'fa-cash-register', '/app/finance/accounts', [Permissions.BasicMember],
                    this.config.modules.Finance),
                new MenuSubItem('Nový účet', 'fa-plus', '/app/finance/accounts/add', [Permissions.FinanceManager],
                    this.config.modules.Finance),
                new MenuSubItem('Kategorie', 'fa-layer-group', '/app/finance/category', [Permissions.BasicMember],
                    this.config.modules.Finance)
            ]),
            new MenuItem('Akce', 'fa-calendar-alt', 'events', [
                new MenuSubItem('Seznam', 'fa-list', '/app/events', [Permissions.Everyone], this.config.modules.Events),
                new MenuSubItem('Nová akce', 'fa-plus', '/app/events/add', [Permissions.Everyone], this.config.modules.Events)
            ]),
            new MenuItem('Majetek', 'fa-dolly-flatbed', 'assets', [
                new MenuSubItem('Seznam', 'fa-list', '/app/assets', [Permissions.BasicMember], this.config.modules.Assets),
                new MenuSubItem('Nová položka', 'fa-plus', '/app/assets/add', [Permissions.PropertyManager], this.config.modules.Assets),
                new MenuSubItem('Inventura', 'fa-boxes', '/app/assets/inventory', [Permissions.PropertyManager],
                    this.config.modules.Assets),
                new MenuSubItem('Kategorie', 'fa-layer-group', '/app/assets/category', [Permissions.BasicMember], this.config.modules.Assets)
            ]),
            new MenuItem('Systém', 'fa-cogs', 'system', [
                new MenuSubItem('Diagnostika', 'fa-stethoscope', '/app/system/diagnostic', [Permissions.AppManager]),
                new MenuSubItem('Notifikace', 'fa-bullhorn', '/app/system/notifications', [Permissions.TeamLeader]),
                new MenuSubItem('Klienti', 'fa-users', '/app/system/clients', [Permissions.AppManager]),
                new MenuSubItem('Nový klient', 'fa-plus', '/app/system/clients/add', [Permissions.AppManager])
            ])
        ];

        this.items.forEach(o => o.isExpanded = o.items.some(x => x.isActive(this.router)));
    }

    logout(): void {
        this.authService.logout();
        this.router.navigate(['/', 'login']);
    }

    toggleOpenState(item: MenuItem, ev: MouseEvent): void {
        ev.preventDefault();
        ev.stopPropagation();

        if (this.items.some(o => o.id === item.id && o.isExpanded)) {
            return;
        }

        this.items
            .filter(o => o.id !== item.id)
            .forEach(o => o.isExpanded = false);

        item.toggle();
    }
}
