import { Router } from '@angular/router';
import { JwtTokenContent } from './../../../core/models/auth/jwt-token';
import { Permissions } from './../../../core/enums/permissions';

export class MenuSubItem {
    constructor(
        public title: string,
        public icon: string,
        public link: string,
        public permissions: Permissions[],
        public enabled: boolean = true
    ) { }

    hasPermission(token: JwtTokenContent): boolean {
        if (!this.enabled) { return false; }
        if (this.permissions.some(o => o === Permissions.Disabled)) { return false; }
        return this.permissions.some(o => token.hasPermission(o));
    }

    isActive(router: Router): boolean {
        return this.link === router.url;
    }
}

export class MenuItem {
    public isExpanded: boolean;

    constructor(
        public title: string,
        public icon: string,
        public id: string,
        public items: MenuSubItem[]
    ) { }

    hasSomePermission(token: JwtTokenContent): boolean {
        return this.items.some(o => o.hasPermission(token));
    }

    toggle(): void {
        this.isExpanded = !this.isExpanded;
    }
}
