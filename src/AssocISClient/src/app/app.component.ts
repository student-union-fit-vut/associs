import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    title = 'AssocISClient';

    ngOnInit(): void {
        // eslint-disable-next-line import/no-deprecated
        if (window.orientation !== undefined) {
            document.body.classList.add('mobile');
        }
    }
}
