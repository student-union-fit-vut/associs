﻿using AssocIS.Data.Enums;
using AssocIS.Data.Resources.Common;
using AssocIS.Database.Enums;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.System
{
    /// <summary>
    /// Parameters for everyone notification to external service.
    /// </summary>
    public class NotifyExternalServicesParams
    {
        /// <summary>
        /// Client type.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public EveryoneNotificationType Type { get; set; }

        /// <summary>
        /// Message content.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string Content { get; set; }
    }
}
