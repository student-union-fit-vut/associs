﻿using AssocIS.Data.Models.Common;

namespace AssocIS.Data.Models.Accesses
{
    /// <summary>
    /// Detailed information about access member.
    /// </summary>
    public class AccessMember : AccessMemberListItem
    {
        /// <summary>
        /// Data about creation and modification of member.
        /// </summary>
        public LogData LogData { get; set; }

        public AccessMember() { }

        public AccessMember(Database.Entity.AccessMember entity) : base(entity)
        {
            if (entity == null)
                return;

            LogData = new LogData(entity);
        }
    }
}
