﻿using AssocIS.Data.Models.Users;

namespace AssocIS.Data.Models.Accesses
{
    /// <summary>
    /// Simplified information about access members.
    /// </summary>
    public class AccessMemberListItem
    {
        /// <summary>
        /// User that have defined access.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Have allowed access?
        /// </summary>
        public bool Allowed { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }

        public AccessMemberListItem() { }

        public AccessMemberListItem(Database.Entity.AccessMember entity)
        {
            if (entity == null)
                return;

            User = new User(entity.User);
            Allowed = entity.Allowed;
            Note = entity.Note;
        }
    }
}
