﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.User;
using AssocIS.Database.Enums;

namespace AssocIS.Data.Models.Accesses
{
    /// <summary>
    /// Search parameters about access.
    /// </summary>
    public class AccessDefinitionSearchParams : PaginationParams
    {
        /// <summary>
        /// Access definition type (Area, Facebook, Google, ...)
        /// </summary>
        public AccessType? Type { get; set; }

        /// <summary>
        /// Description must contains.
        /// </summary>
        public string DescriptionContains { get; set; }

        /// <summary>
        /// Who created access definition.
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceType = typeof(UserResources), ErrorMessageResourceName = nameof(UserResources.UserNotFound))]
        public long? CreatedById { get; set; }
    }
}
