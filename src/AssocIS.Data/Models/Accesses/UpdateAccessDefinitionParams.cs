﻿using AssocIS.Data.Resources.Common;
using AssocIS.Database.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Accesses
{
    /// <summary>
    /// Data about create or update access definition.
    /// </summary>
    public class UpdateAccessDefinitionParams
    {
        /// <summary>
        /// Access type (Area, Facebook, Google)
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public AccessType Type { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        public Database.Entity.AccessDefinition ToEntity(long loggedUserId)
        {
            return new Database.Entity.AccessDefinition()
            {
                CreatedAt = DateTime.Now,
                CreatedById = loggedUserId,
                Description = Description,
                Type = Type
            };
        }

        public void UpdateEntity(Database.Entity.AccessDefinition entity, long loggedUserId)
        {
            entity.Type = Type;
            entity.Description = Description;
            entity.MarkAsModified(loggedUserId);
        }
    }
}
