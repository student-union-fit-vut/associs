﻿using AssocIS.Data.Models.Common;

namespace AssocIS.Data.Models.Accesses
{
    /// <summary>
    /// Detailed information about access definition.
    /// </summary>
    public class AccessDefinition : AccessDefinitionListItem
    {
        /// <summary>
        /// Information about creation and modification.
        /// </summary>
        public LogData LogData { get; set; }

        public AccessDefinition() { }

        public AccessDefinition(Database.Entity.AccessDefinition entity) : base(entity)
        {
            if (entity == null)
                return;

            LogData = new LogData(entity);
        }
    }
}
