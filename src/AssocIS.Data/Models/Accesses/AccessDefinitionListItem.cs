﻿using AssocIS.Database.Enums;

namespace AssocIS.Data.Models.Accesses
{
    /// <summary>
    /// Simplified data about access definition.
    /// </summary>
    public class AccessDefinitionListItem
    {
        /// <summary>
        /// Unique ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Type of access (Area, Facebook, Google)
        /// </summary>
        public AccessType Type { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }

        public AccessDefinitionListItem() { }

        public AccessDefinitionListItem(Database.Entity.AccessDefinition entity)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Type = entity.Type;
            Description = entity.Description;
        }
    }
}
