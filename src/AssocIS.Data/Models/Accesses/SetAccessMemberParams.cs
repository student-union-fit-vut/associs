﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.User;
using System;

namespace AssocIS.Data.Models.Accesses
{
    /// <summary>
    /// Parameters to create access for user.
    /// </summary>
    public class SetAccessMemberParams
    {
        /// <summary>
        /// ID of users that gets access. Only for create. In edit this property is ignored.
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceName = nameof(UserResources.UserNotFound), ErrorMessageResourceType = typeof(UserResources))]
        public long UserId { get; set; }

        /// <summary>
        /// Flag that specify allow or deny access.
        /// </summary>
        public bool Allowed { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }

        public Database.Entity.AccessMember ToEntity(long accessId, long loggedUserId)
        {
            return new Database.Entity.AccessMember()
            {
                AccessDefinitionId = accessId,
                Allowed = Allowed,
                CreatedAt = DateTime.Now,
                CreatedById = loggedUserId,
                Note = Note,
                UserId = UserId
            };
        }

        public void UpdateEntity(Database.Entity.AccessMember entity, long loggedUserId)
        {
            entity.Allowed = Allowed;
            entity.Note = Note;
            entity.MarkAsModified(loggedUserId);
        }
    }
}
