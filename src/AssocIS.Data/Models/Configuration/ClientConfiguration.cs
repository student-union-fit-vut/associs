﻿using System.Collections.Generic;

namespace AssocIS.Data.Models.Configuration
{
    /// <summary>
    /// Client side configuration.
    /// </summary>
    public class ClientConfiguration
    {
        /// <summary>
        /// Client Id
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Version
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Active and inactive modules.
        /// </summary>
        public Dictionary<string, bool> Modules { get; set; }

        /// <summary>
        /// Default localization.
        /// </summary>
        public string DefaultLanguage { get; set; }

        /// <summary>
        /// A flag that indicates that a discord is available.
        /// </summary>
        public bool DiscordEnabled { get; set; }

        /// <summary>
        /// A flag that indicates that google is available.
        /// </summary>
        public bool GoogleEnabled { get; set; }
    }
}
