﻿using AssocIS.Data.Models.Common;
using AssocIS.Database.Enums;
using System.Collections.Generic;

namespace AssocIS.Data.Models.Users
{
    /// <summary>
    /// Search request for user list.
    /// </summary>
    public class UserSearchParams : PaginationParams
    {
        /// <summary>
        /// Name, Surname or username query.
        /// </summary>
        public string NameQuery { get; set; }

        /// <summary>
        /// Ignore users who left association.
        /// </summary>
        public bool IgnoreLeftUsers { get; set; }

        /// <summary>
        /// Ignore users in testing period.
        /// </summary>
        public bool IgnoreTestPeriodUsers { get; set; }

        /// <summary>
        /// Ignore anonymized users.
        /// </summary>
        public bool IgnoreAnonymized { get; set; }

        /// <summary>
        /// Show members that have selected states. Empty list is all members.
        /// </summary>
        public List<UserState> States { get; set; }
    }
}
