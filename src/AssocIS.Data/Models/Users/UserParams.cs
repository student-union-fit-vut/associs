﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.User;
using AssocIS.Database.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Users
{
    /// <summary>
    /// Parameters for user creation.
    /// </summary>
    public class UserParams
    {
        /// <summary>
        /// Username
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [UniqueNameCheckValidation(typeof(Database.Entity.User), nameof(UsernameChanged), ErrorMessageResourceName = nameof(UserResources.UsernameExists), ErrorMessageResourceType = typeof(UserResources))]
        public string Username { get; set; }

        /// <summary>
        /// Helper field for detect that username have to validate.
        /// </summary>
        public bool UsernameChanged { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string Name { get; set; }

        /// <summary>
        /// Surname
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string Surname { get; set; }

        /// <summary>
        /// Email address.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [EmailAddress(ErrorMessageResourceName = nameof(CommonResources.InvalidEmail), ErrorMessageResourceType = typeof(CommonResources))]
        public string Email { get; set; }

        /// <summary>
        /// Phone number.
        /// </summary>
        [StringLength(20, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        [Phone(ErrorMessageResourceName = nameof(CommonResources.InvalidPhone), ErrorMessageResourceType = typeof(CommonResources))]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Date time of join to association. Date time have to be in UTC zone. Value is not changed if user updates self.
        /// </summary>
        public DateTime JoinedAt { get; set; }

        /// <summary>
        /// Study program if user is student.
        /// </summary>
        public string StudyProgram { get; set; }

        /// <summary>
        /// End of testing period of user. Date time have to be in UTC zone. Value is not changed if user updates self.
        /// </summary>
        public DateTime? TestPeriodEnd { get; set; }

        /// <summary>
        /// User state. (BasicMember, HonoraryMember, Chairman, ViceChairman). Value is not changed if user updates self.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public UserState UserState { get; set; }

        /// <summary>
        /// Date time when user left association. Date time have to be in UTC zone. Value is not changed if user updates self.
        /// </summary>
        public DateTime? LeavedAt { get; set; }

        /// <summary>
        /// <para>Bit mask of permissions.</para>
        /// <para>Value is not changed if user updates self.</para>
        /// <para>
        /// 0 = None
        /// 1 = UserManager
        /// 2 = TodoListsManager
        /// 4 = EventManager
        /// 8 = MeetingManager
        /// 16 = PropertyManager
        /// 32 = FinanceManager
        /// 64 = WorkItemsManager
        /// 128 = JobsManager
        /// 256 = AppManager
        /// </para>
        /// </summary>
        public long Permissions { get; set; }

        /// <summary>
        /// Everyone visible global note.
        /// </summary>
        public string GlobalNote { get; set; }

        /// <summary>
        /// Discord ID
        /// </summary>
        public string DiscordId { get; set; }

        /// <summary>
        /// GSuite email. If not contains null that bot creates new account at gsuite (if gsuite is supported).
        /// </summary>
        [EmailAddress(ErrorMessageResourceName = nameof(CommonResources.InvalidEmail), ErrorMessageResourceType = typeof(CommonResources))]
        public string GooglePrimaryEmail { get; set; }

        /// <summary>
        /// Flag that creates google email from users email.
        /// </summary>
        public bool CreateGSuiteFromEmail { get; set; }

        /// <summary>
        /// Method to create entity from request data.
        /// </summary>
        /// <param name="currentLoggedUserId">Logged user ID that creating accoung.</param>
        /// <param name="gSuiteMailGenerator">Method to create google primary email from user's email. Is called only if GooglePrimaryEmail is null and CreateGSuiteFromEmail is true.</param>
        /// <returns></returns>
        public Database.Entity.User ToEntity(long currentLoggedUserId, Func<string, string> gSuiteMailGenerator)
        {
            var entity = new Database.Entity.User()
            {
                CreatedAt = DateTime.Now,
                CreatedById = currentLoggedUserId,
                Email = Email,
                JoinedAt = JoinedAt,
                Name = Name,
                PhoneNumber = PhoneNumber,
                State = UserState,
                StudyProgram = StudyProgram,
                Surname = Surname,
                TestPeriodEnd = TestPeriodEnd,
                Username = Username,
                LeavedAt = LeavedAt,
                Permissions = Permissions,
                GlobalNote = GlobalNote,
                DiscordId = DiscordId
            };

            if (!string.IsNullOrEmpty(GooglePrimaryEmail))
                entity.GooglePrimaryEmail = GooglePrimaryEmail;
            else if (CreateGSuiteFromEmail)
                entity.GooglePrimaryEmail = gSuiteMailGenerator(Email);

            return entity;
        }

        public void UpdateEntity(Database.Entity.User entity, long currentLoggedUserId, bool isLowPermsSelfUpdate)
        {
            if (!isLowPermsSelfUpdate)
            {
                entity.JoinedAt = JoinedAt;
                entity.LeavedAt = LeavedAt;
                entity.State = UserState;
                entity.TestPeriodEnd = TestPeriodEnd;
                entity.Permissions = Permissions;
            }

            entity.Email = Email;
            entity.Name = Name;
            entity.PhoneNumber = PhoneNumber;
            entity.StudyProgram = StudyProgram;
            entity.Surname = Surname;
            entity.Username = Username;
            entity.GlobalNote = GlobalNote;
            entity.DiscordId = DiscordId;

            if (!string.IsNullOrEmpty(GooglePrimaryEmail))
                entity.GooglePrimaryEmail = GooglePrimaryEmail;

            entity.MarkAsModified(currentLoggedUserId);
        }
    }
}
