﻿namespace AssocIS.Data.Models.Users
{
    /// <summary>
    /// User object with some specific informations.
    /// </summary>
    public class UserInfo : User
    {
        /// <summary>
        /// Discord ID
        /// </summary>
        public string DiscordId { get; set; }

        public UserInfo() { }

        public UserInfo(Database.Entity.User entity) : base(entity)
        {
            if (entity == null)
                return;

            DiscordId = entity.DiscordId;
        }
    }
}
