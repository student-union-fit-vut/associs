﻿namespace AssocIS.Data.Models.Users
{
    /// <summary>
    /// Information about team for specific user.
    /// </summary>
    public class TeamMembership
    {
        /// <summary>
        /// Team ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Team name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Flag if user is leader of team.
        /// </summary>
        public bool IsLeader { get; set; }

        /// <summary>
        /// Discord ID of team.
        /// </summary>
        public string TeamDiscordId { get; set; }

        public TeamMembership(Database.Entity.TeamMember teamMember)
        {
            if (teamMember == null)
                return;

            Id = teamMember.TeamId;
            Name = teamMember.Team.Name;
            IsLeader = teamMember.IsLeader;
            TeamDiscordId = teamMember.Team.DiscordRoleId;
        }
    }
}
