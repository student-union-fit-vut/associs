﻿using AssocIS.Database.Enums;
using System;
using System.Linq.Expressions;

namespace AssocIS.Data.Models.Users
{
    /// <summary>
    /// Simplified information about user for list.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Unique ID.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Firstname.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Surname
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Flag that describe user was any logged in system.
        /// </summary>
        public bool AnyLoggedIn { get; set; }

        /// <summary>
        /// Email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Was user anonymized?
        /// </summary>
        public bool IsAnonymized { get; set; }

        /// <summary>
        /// Member status
        /// </summary>
        public UserState UserState { get; set; }

        public User() { }

        public User(Database.Entity.User user)
        {
            if (user == null)
                return;

            Id = user.Id;
            Username = user.Username;
            Name = user.Name;
            Surname = user.Surname;
            AnyLoggedIn = user.LastLogin != null;
            Email = user.Email;
            IsAnonymized = (user.Flags & (long)UserFlags.Anonymized) != 0;
            UserState = user.State;
        }
    }
}
