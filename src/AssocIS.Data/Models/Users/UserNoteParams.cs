﻿namespace AssocIS.Data.Models.Users
{
    /// <summary>
    /// Parameters for user note creation.
    /// </summary>
    public class UserNoteParams
    {
        /// <summary>
        /// Note content. If you want remove user note, set null.
        /// </summary>
        public string Content { get; set; }
    }
}
