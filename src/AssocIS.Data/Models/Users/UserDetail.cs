﻿using AssocIS.Data.Models.Common;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AssocIS.Data.Models.Users
{
    /// <summary>
    /// Detailed data of some user.
    /// </summary>
    public class UserDetail : UserInfo
    {
        /// <summary>
        /// Phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Date time of join to association. Date time is in UTC.
        /// </summary>
        public DateTime JoinedAt { get; set; }

        /// <summary>
        /// Date time of leave association. Date time is in UTC.
        /// If user left association, have lower permissions.
        /// </summary>
        public DateTime? LeavedAt { get; set; }

        /// <summary>
        /// Study program if user is student.
        /// </summary>
        public string StudyProgram { get; set; }

        /// <summary>
        /// Flag if user have set profile picture.
        /// </summary>
        public bool HaveProfilePicture { get; set; }

        /// <summary>
        /// Date time of end testing period. Date time is in UTC.
        /// </summary>
        public DateTime? TestPeriodEnd { get; set; }

        /// <summary>
        /// Everyone visible global note.
        /// </summary>
        public string GlobalNote { get; set; }

        /// <summary>
        /// Private note at user for currently logged user.
        /// </summary>
        public string UserNote { get; set; }

        /// <summary>
        /// Information about creation and modification of user.
        /// </summary>
        public LogData LogData { get; set; }

        /// <summary>
        /// Flag if user have set password. Without password have user lower permissions.
        /// </summary>
        public bool HavePassword { get; set; }

        /// <summary>
        /// <para>Bit mask of permissions.</para>
        /// <para>
        /// 0 = None
        /// 1 = UserManager
        /// 2 = TodoListsManager
        /// 4 = EventManager
        /// 8 = MeetingManager
        /// 16 = PropertyManager
        /// 32 = FinanceManager
        /// 64 = WorkItemsManager
        /// 128 = JobsManager
        /// 256 = AppManager
        /// </para>
        /// </summary>
        public long Permissions { get; set; }

        /// <summary>
        /// Teams.
        /// </summary>
        public List<TeamMembership> Teams { get; set; }

        /// <summary>
        /// Primary email in Google workspace.
        /// </summary>
        public string GooglePrimaryEmail { get; set; }

        /// <summary>
        /// UTC datetime of last login of user.
        /// </summary>
        public DateTime? LastLogin { get; set; }

        public UserDetail(Database.Entity.User entity, long currentLoggedUserId) : base(entity)
        {
            if (entity == null)
                return;

            PhoneNumber = entity.PhoneNumber;
            JoinedAt = entity.JoinedAt;
            LeavedAt = entity.LeavedAt;
            StudyProgram = entity.StudyProgram;
            HaveProfilePicture = entity.ProfileImageId != null;
            TestPeriodEnd = entity.TestPeriodEnd;
            GlobalNote = entity.GlobalNote;
            UserNote = entity.UserNotes.FirstOrDefault(o => o.AuthorId == currentLoggedUserId)?.Content;
            LogData = new LogData(entity);
            HavePassword = !string.IsNullOrEmpty(entity.Password);
            Permissions = entity.Permissions;
            Teams = entity.Teams.Select(o => new TeamMembership(o)).ToList();
            GooglePrimaryEmail = entity.GooglePrimaryEmail;
            LastLogin = entity.LastLogin == null ? null : entity.LastLogin.Value;
        }
    }
}
