﻿using System;
using System.Text;

namespace AssocIS.Data.Models.Users
{
    /// <summary>
    /// Result of password reset.
    /// </summary>
    public class ResetPasswordResult
    {
        /// <summary>
        /// Detailed information about user.
        /// </summary>
        public UserDetail User { get; set; }

        /// <summary>
        /// Base64 encoded raw password.
        /// </summary>
        public string HashedPassword { get; set; }

        public ResetPasswordResult() { }

        public ResetPasswordResult(UserDetail user, string rawPassword)
        {
            User = user;
            HashedPassword = Convert.ToBase64String(Encoding.UTF8.GetBytes(rawPassword));
        }
    }
}
