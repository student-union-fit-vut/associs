﻿using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.User;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Users
{
    /// <summary>
    /// Request for change password.
    /// </summary>
    public class PasswordChangeParams
    {
        /// <summary>
        /// Old password.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string OldPassword { get; set; }

        /// <summary>
        /// New password.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$", ErrorMessageResourceName = nameof(UserResources.WeakPassword), ErrorMessageResourceType = typeof(UserResources))]
        public string NewPassword { get; set; }

        /// <summary>
        /// Confirm new password.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [Compare("NewPassword", ErrorMessageResourceName = nameof(UserResources.SamePasswords), ErrorMessageResourceType = typeof(UserResources))]
        public string ConfirmPassword { get; set; }

        public bool CheckOldPassword(Database.Entity.User entity)
        {
            if (entity.Password == null)
                return true;

            return BCrypt.Net.BCrypt.Verify(OldPassword, entity.Password);
        }

        public void UpdateEntity(Database.Entity.User entity, long currentUserId)
        {
            entity.Password = BCrypt.Net.BCrypt.HashPassword(NewPassword);
            entity.MarkAsModified(currentUserId);
        }
    }
}
