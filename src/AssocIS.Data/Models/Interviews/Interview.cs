﻿namespace AssocIS.Data.Models.Interviews
{
    /// <summary>
    /// Detailed information about interview.
    /// </summary>
    public class Interview : InterviewListItem
    {
        /// <summary>
        /// Note from leader.
        /// </summary>
        public string Note { get; set; }

        public Interview() { }

        public Interview(Database.Entity.Interview entity) : base(entity)
        {
            if (entity == null)
                return;

            Note = entity.Note;
        }
    }
}
