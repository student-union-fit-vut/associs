﻿using AssocIS.Data.Models.Users;
using System;

namespace AssocIS.Data.Models.Interviews
{
    /// <summary>
    /// Simplified information about interview.
    /// </summary>
    public class InterviewListItem
    {
        /// <summary>
        /// Interview ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Member, that had interview.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Interview leader.
        /// </summary>
        public User Leader { get; set; }

        /// <summary>
        /// When was interview. Datetime is in UTC.
        /// </summary>
        public DateTime At { get; set; }

        public InterviewListItem() { }

        public InterviewListItem(Database.Entity.Interview entity)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            User = new User(entity.User);
            Leader = new User(entity.InterviewLeader);
            At = entity.At;
        }
    }
}
