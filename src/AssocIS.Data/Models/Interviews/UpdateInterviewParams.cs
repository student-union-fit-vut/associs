﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.User;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Interviews
{
    /// <summary>
    /// Request for create or update interview.
    /// </summary>
    public class UpdateInterviewParams
    {
        /// <summary>
        /// Id with destination user.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceName = nameof(UserResources.UserNotFound), ErrorMessageResourceType = typeof(UserResources))]
        public long UserId { get; set; }

        /// <summary>
        /// Leader's ID of interview. If is not filled that is used current logged user ID
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceName = nameof(UserResources.UserNotFound), ErrorMessageResourceType = typeof(UserResources))]
        public long? LeaderId { get; set; }

        /// <summary>
        /// Date time of interview. Date time is in UTC format.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public DateTime At { get; set; }

        /// <summary>
        /// Note to inverview.
        /// </summary>
        public string Note { get; set; }

        public Database.Entity.Interview ToEntity(long loggedUserId)
        {
            return new Database.Entity.Interview()
            {
                At = At,
                InterviewLeaderId = LeaderId ?? loggedUserId,
                Note = Note,
                UserId = UserId
            };
        }

        public void UpdateEntity(Database.Entity.Interview entity, long loggedUserId)
        {
            entity.At = At;
            entity.Note = Note;
            entity.InterviewLeaderId = LeaderId ?? loggedUserId;
        }
    }
}
