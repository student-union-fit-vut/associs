﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.User;
using System;

namespace AssocIS.Data.Models.Interviews
{
    /// <summary>
    /// Search parameters to interview.
    /// </summary>
    public class InterviewSearchParams : PaginationParams
    {
        /// <summary>
        /// Destination user id
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceName = nameof(UserResources.UserNotFound), ErrorMessageResourceType = typeof(UserResources))]
        public long? UserId { get; set; }

        /// <summary>
        /// Leader user id
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceName = nameof(UserResources.UserNotFound), ErrorMessageResourceType = typeof(UserResources))]
        public long? LeaderUserId { get; set; }

        /// <summary>
        /// Start of range when interview was. Datetime have to be in UTC.
        /// </summary>
        [DateLessThan("To", ErrorMessageResourceName = nameof(CommonResources.FromIsAfterEnd), ErrorMessageResourceType = typeof(CommonResources))]
        public DateTime? From { get; set; }

        /// <summary>
        /// End of range when interview was. Datetime have to be in UTC.
        /// </summary>
        public DateTime? To { get; set; }
    }
}
