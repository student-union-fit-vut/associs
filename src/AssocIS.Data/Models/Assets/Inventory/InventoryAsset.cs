﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Asset;
using AssocIS.Data.Resources.Common;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Assets.Inventory
{
    /// <summary>
    /// Items calculated in inventory.
    /// </summary>
    public class InventoryAsset
    {
        /// <summary>
        /// Asset ID
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [IdExistenceValidator(typeof(Database.Entity.Asset), ErrorMessageResourceName = nameof(AssetResources.AssetNotFound), ErrorMessageResourceType = typeof(AssetResources))]
        public long AssetId { get; set; }

        /// <summary>
        /// Count of items.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public int Count { get; set; }

        /// <summary>
        /// Note to inventory report item.
        /// </summary>
        public string Note { get; set; }
    }
}
