﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Asset;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.User;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AssocIS.Data.Models.Assets.Inventory
{
    /// <summary>
    /// Data for inventory reports for products.
    /// </summary>
    public class CreateInventoryParams : IValidatableObject
    {
        /// <summary>
        /// User ID that processed inventory.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceName = nameof(UserResources.UserNotFound), ErrorMessageResourceType = typeof(UserResources))]
        public long UserId { get; set; }

        /// <summary>
        /// When inventory was processed. Datetime have to be in UTC.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public DateTime At { get; set; }

        /// <summary>
        /// Assets processed in inventory.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [MinLength(1, ErrorMessageResourceName = nameof(AssetResources.InventoryAtLeastOneItem), ErrorMessageResourceType = typeof(AssetResources))]
        public InventoryAsset[] Assets { get; set; }

        public List<Database.Entity.InventoryReport> ToEntities()
        {
            return Assets.Select(o => new Database.Entity.InventoryReport()
            {
                AssetId = o.AssetId,
                At = At,
                Note = o.Note,
                UserId = UserId,
                Count = o.Count
            }).ToList();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var groupedData = Assets.GroupBy(o => o.AssetId).ToDictionary(o => o.Key, o => o.Count());

            if(groupedData.Any(o => o.Value > 1))
            {
                var localizer = validationContext.GetService<IStringLocalizer<AssetResources>>();
                yield return new ValidationResult(localizer[nameof(AssetResources.InventoryAssetItemDuplicity)]);
            }
        }
    }
}
