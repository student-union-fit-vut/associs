﻿using AssocIS.Data.Models.Users;
using System;

namespace AssocIS.Data.Models.Assets.Inventory
{
    /// <summary>
    /// Inventory report.
    /// </summary>
    public class InventoryReport
    {
        /// <summary>
        /// ID of inventory.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// User that processed inventory.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// When inventory was processed. Datetime is in UTC.
        /// </summary>
        public DateTime At { get; set; }

        /// <summary>
        /// Note of inventory.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Count of items, when inventory was processed.
        /// </summary>
        public int Count { get; set; }

        public InventoryReport() { }

        public InventoryReport(Database.Entity.InventoryReport entity)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            User = new User(entity.User);
            At = entity.At;
            Note = entity.Note;
            Count = entity.Count;
        }
    }
}
