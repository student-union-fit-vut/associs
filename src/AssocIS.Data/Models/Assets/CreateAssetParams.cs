﻿using System.Collections.Generic;

namespace AssocIS.Data.Models.Assets
{
    /// <summary>
    /// Data for create asset.
    /// </summary>
    public class CreateAssetParams : UpdateAssetParams
    {
        /// <summary>
        /// Count of items.
        /// </summary>
        public int Count { get; set; }

        public override Database.Entity.Asset ToEntity(long loggedUserId, List<Database.Entity.AssetCategory> categories)
        {
            var entity = base.ToEntity(loggedUserId, categories);
            entity.Count = Count;
            return entity;
        }
    }
}
