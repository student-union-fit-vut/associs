﻿using AssocIS.Data.Models.Users;
using System;

namespace AssocIS.Data.Models.Assets
{
    /// <summary>
    /// Information about loan.
    /// </summary>
    public class AssetLoanInfo
    {
        /// <summary>
        /// Loan ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Simplified information about asset.
        /// </summary>
        public Asset Asset { get; set; }

        /// <summary>
        /// Date and time, when asset was loaned. Date time is in UTC format.
        /// </summary>
        public DateTime LoanedAt { get; set; }

        /// <summary>
        /// Date and time, when asset was returned. Date time is in UTC format.
        /// </summary>
        public DateTime? ReturnedAt { get; set; }

        /// <summary>
        /// Calculated length of loan.
        /// </summary>
        public double DaysLoaned { get; set; }

        /// <summary>
        /// Simplified information about user, that loaned asset.
        /// </summary>
        public User User { get; set; }

        public AssetLoanInfo() { }

        public AssetLoanInfo(Database.Entity.AssetLoan entity)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Asset = new Asset(entity.Asset);
            LoanedAt = entity.LoanedAt;
            ReturnedAt = entity.ReturnedAt;
            DaysLoaned = ((entity.ReturnedAt ?? DateTime.Now) - entity.LoanedAt).TotalDays;
            User = new User(entity.User);
        }
    }
}
