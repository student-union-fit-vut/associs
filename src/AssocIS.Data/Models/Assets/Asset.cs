﻿using AssocIS.Data.Models.Assets.Category;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AssocIS.Data.Models.Assets
{
    /// <summary>
    /// Simplified data about assets.
    /// </summary>
    public class Asset
    {
        /// <summary>
        /// Unique ID of asset.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Name of asset.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Categories of asset.
        /// </summary>
        public List<AssetCategory> Categories { get; set; }

        public Asset() { }

        public Asset(Database.Entity.Asset entity)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Name = entity.Name;
            Categories = entity.Categories.Select(o => new AssetCategory(o)).ToList();
        }
    }
}
