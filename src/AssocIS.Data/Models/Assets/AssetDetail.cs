﻿using AssocIS.Data.Models.Common;
using AssocIS.Data.Models.Users;
using System;

namespace AssocIS.Data.Models.Assets
{
    /// <summary>
    /// Detailed information about asset.
    /// </summary>
    public class AssetDetail : AssetListInfo
    {
        /// <summary>
        /// Description about asset.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Location of asset.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Total count of items.
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// Flags of asset. <see cref="Database.Enums.AssetFlags"/>
        /// </summary>
        public long Flags { get; set; }

        /// <summary>
        /// Data about creation and modification of asset.
        /// </summary>
        public LogData LogData { get; set; }

        public AssetDetail() { }

        public AssetDetail(Database.Entity.Asset entity) : base(entity)
        {
            if (entity == null)
                return;

            Description = entity.Description;
            Location = entity.Location;
            TotalCount = entity.Count;
            Flags = entity.Flags;

            LogData = new LogData(entity);
        }
    }
}
