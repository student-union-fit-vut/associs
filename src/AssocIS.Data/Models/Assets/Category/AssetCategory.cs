﻿namespace AssocIS.Data.Models.Assets.Category
{
    /// <summary>
    /// List item of asset category.
    /// </summary>
    public class AssetCategory
    {
        /// <summary>
        /// Unique ID of category.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Name of category.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Link to icon.
        /// </summary>
        public string Icon { get; set; }

        public AssetCategory() { }

        public AssetCategory(Database.Entity.AssetCategory entity)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Name = entity.Name;
            Icon = entity.Icon;
        }
    }
}
