﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Asset;
using AssocIS.Data.Resources.Common;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Assets.Category
{
    /// <summary>
    /// Update or create asset category request data.
    /// </summary>
    public class UpdateCategoryParams
    {
        /// <summary>
        /// Name of category.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [StringLength(100, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        [UniqueNameCheckValidation(typeof(Database.Entity.AssetCategory), nameof(NameChanged), ErrorMessageResourceName = nameof(AssetResources.CategoryNameNotUnique), ErrorMessageResourceType = typeof(AssetResources))]
        public string Name { get; set; }

        /// <summary>
        /// Flag that describe name was changed.
        /// </summary>
        public bool NameChanged { get; set; }

        /// <summary>
        /// Link to icon of category.
        /// </summary>
        public string Icon { get; set; }

        public Database.Entity.AssetCategory ToEntity()
        {
            return new Database.Entity.AssetCategory()
            {
                Icon = Icon,
                Name = Name
            };
        }

        public void UpdateEntity(Database.Entity.AssetCategory entity)
        {
            entity.Icon = Icon;
            entity.Name = Name;
        }
    }
}
