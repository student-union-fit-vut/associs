﻿using AssocIS.Data.Models.Common;
using System.Collections.Generic;

namespace AssocIS.Data.Models.Assets
{
    /// <summary>
    /// Search params to find assets.
    /// </summary>
    public class AssetSearchParams : PaginationParams
    {
        /// <summary>
        /// Name of item contains query.
        /// </summary>
        public string NameQuery { get; set; }

        /// <summary>
        /// Is item available? (Yes/No/NoFilter)
        /// </summary>
        public bool? IsAvailable { get; set; }

        /// <summary>
        /// Categories.
        /// </summary>
        public List<long> Categories { get; set; }

        /// <summary>
        /// Include removed items in list.
        /// </summary>
        public bool IncludeRemoved { get; set; }
    }
}
