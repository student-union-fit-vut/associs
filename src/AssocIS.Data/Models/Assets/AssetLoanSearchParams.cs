﻿using AssocIS.Data.Models.Common;

namespace AssocIS.Data.Models.Assets
{
    /// <summary>
    /// Search parameters to filter loan list.
    /// </summary>
    public class AssetLoanSearchParams : PaginationParams
    {
        /// <summary>
        /// Filter to only active loans.
        /// </summary>
        public bool OnlyActive { get; set; }
    }
}
