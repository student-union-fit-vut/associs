﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Asset;
using AssocIS.Data.Resources.Common;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AssocIS.Data.Models.Assets
{
    /// <summary>
    /// Data for update asset.
    /// </summary>
    public class UpdateAssetParams
    {
        /// <summary>
        /// Name of asset.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [StringLength(100, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        public string Name { get; set; }

        /// <summary>
        /// Description about asset.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Where is asset stored (location).
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Categories of asset.
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.AssetCategory), ErrorMessageResourceName = nameof(AssetResources.AssetCategoryNotFound), ErrorMessageResourceType = typeof(AssetResources))]
        public List<long> Categories { get; set; }

        /// <summary>
        /// Loans are disabled for this item.
        /// </summary>
        public bool LoansDisabled { get; set; }

        /// <summary>
        /// Item is discarded and unavailable.
        /// </summary>
        public bool IsRemoved { get; set; }

        public virtual Database.Entity.Asset ToEntity(long loggedUserId, List<Database.Entity.AssetCategory> categories)
        {
            return new Database.Entity.Asset()
            {
                Categories = categories.ToHashSet(),
                CreatedAt = DateTime.Now,
                CreatedById = loggedUserId,
                Description = Description,
                Flags = GetFlags(),
                Location = Location,
                Name = Name
            };
        }

        public virtual void UpdateEntity(Database.Entity.Asset entity, long loggedUserId, List<Database.Entity.AssetCategory> categories)
        {
            entity.Categories = categories.ToHashSet();
            entity.Description = Description;
            entity.Flags = GetFlags();
            entity.Location = Location;
            entity.MarkAsModified(loggedUserId);
            entity.Name = Name;
        }

        private long GetFlags()
        {
            long flags = 0;

            if (LoansDisabled)
                flags |= (long)AssetFlags.LoansDisabled;

            if (IsRemoved)
                flags |= (long)AssetFlags.Removed;

            return flags;
        }
    }
}
