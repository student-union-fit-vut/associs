﻿using AssocIS.Data.Helpers;

namespace AssocIS.Data.Models.Assets
{
    /// <summary>
    /// List information about asset.
    /// </summary>
    public class AssetListInfo : Asset
    {
        /// <summary>
        /// Available count of assets to loan.
        /// </summary>
        public int AvailableCount { get; set; }

        public AssetListInfo() { }

        public AssetListInfo(Database.Entity.Asset entity) : base(entity)
        {
            if (entity == null)
                return;

            AvailableCount = AssetsHelper.CalculateAvailableCount(entity);
        }
    }
}
