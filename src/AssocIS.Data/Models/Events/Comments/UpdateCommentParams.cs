﻿using System;

namespace AssocIS.Data.Models.Events.Comments
{
    /// <summary>
    /// Parameters for create or modify comment.
    /// </summary>
    public class UpdateCommentParams
    {
        /// <summary>
        /// New text of comment.
        /// </summary>
        public string Text { get; set; }

        public Database.Entity.Events.EventComment ToEntity(long loggedUserId)
        {
            return new Database.Entity.Events.EventComment()
            {
                CreatedAt = DateTime.Now,
                CreatedById = loggedUserId,
                Text = Text
            };
        }

        public void UpdateEntity(Database.Entity.Events.EventComment entity, long loggedUserId)
        {
            entity.Text = Text;
            entity.MarkAsModified(loggedUserId);
        }
    }
}
