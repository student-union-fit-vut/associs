﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.User;

namespace AssocIS.Data.Models.Events.Comments
{
    /// <summary>
    /// Parameters for search in comments.
    /// </summary>
    public class SearchCommentParams : PaginationParams
    {
        /// <summary>
        /// Query search in comment.
        /// </summary>
        public string TextQuery { get; set; }

        /// <summary>
        /// Filter comments to comments from one author.
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceType = typeof(UserResources), ErrorMessageResourceName = nameof(UserResources.UserNotFound))]
        public long? CreatedById { get; set; }
    }
}
