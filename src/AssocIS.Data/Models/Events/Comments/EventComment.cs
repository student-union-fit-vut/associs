﻿using AssocIS.Data.Models.Users;
using System;

namespace AssocIS.Data.Models.Events.Comments
{
    /// <summary>
    /// Comment to event.
    /// </summary>
    public class EventComment
    {
        /// <summary>
        /// Unique ID of comment.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Who posted commentar.
        /// </summary>
        public User CreatedBy { get; set; }

        /// <summary>
        /// When commentar was posted.
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Datetime of last modification.
        /// </summary>
        public DateTime? ModifiedAt { get; set; }

        /// <summary>
        /// Comment content.
        /// </summary>
        public string Text { get; set; }

        public EventComment() { }

        public EventComment(Database.Entity.Events.EventComment entity)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            CreatedBy = new User(entity.CreatedBy);
            CreatedAt = entity.CreatedAt;
            ModifiedAt = entity.LastModifiedAt;
            Text = entity.Text;
        }
    }
}
