﻿using AssocIS.Data.Models.Common;

namespace AssocIS.Data.Models.Events
{
    /// <summary>
    /// Detailed information about event.
    /// </summary>
    public class Event : EventListItem
    {
        /// <summary>
        /// Description of event.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Note.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Discord channel ID.
        /// </summary>
        public string DiscordChannelId { get; set; }

        /// <summary>
        /// Google calendar event ID.
        /// </summary>
        public string CalendarEventId { get; set; }

        /// <summary>
        /// Data about creation and modification of event.
        /// </summary>
        public LogData LogData { get; set; }

        public Event() { }

        public Event(Database.Entity.Events.Event entity, bool canManage) : base(entity, canManage)
        {
            if (entity == null)
                return;

            Description = entity.Description;
            Note = entity.Note;
            DiscordChannelId = entity.DiscordChannelId;
            CalendarEventId = entity.CalendarEventId;
            LogData = new LogData(entity);
        }
    }
}
