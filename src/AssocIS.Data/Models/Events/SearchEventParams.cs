﻿using AssocIS.Data.Models.Common;

namespace AssocIS.Data.Models.Events
{
    /// <summary>
    /// Search parameters for event.
    /// </summary>
    public class SearchEventParams : PaginationParams
    {
        /// <summary>
        /// Query search under event name.
        /// </summary>
        public string NameQuery { get; set; }

        /// <summary>
        /// Exclude future events.
        /// </summary>
        public bool ExcludeFuture { get; set; }

        /// <summary>
        /// Ignore ongoing events.
        /// </summary>
        public bool ExcludeOngoing { get; set; }

        /// <summary>
        /// Ignore past events.
        /// </summary>
        public bool ExcludePast { get; set; }
    }
}
