﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Events
{
    /// <summary>
    /// Data for create or update event.
    /// </summary>
    public class UpdateEventParams
    {
        /// <summary>
        /// Name of event
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [StringLength(100, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        public string Name { get; set; }

        /// <summary>
        /// Start of event. Datetime must be in UTC.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [DateLessThan("To", ErrorMessageResourceName = nameof(CommonResources.FromIsAfterEnd), ErrorMessageResourceType = typeof(CommonResources))]
        public DateTime From { get; set; }

        /// <summary>
        /// End of event. If contains null, that implicitly ends at same day when starts at 23:59:59. Datetime must be in UTC.
        /// </summary>
        public DateTime? To { get; set; }

        /// <summary>
        /// Description of event.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Note.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Expected person count.
        /// </summary>
        public int ExpectedCount { get; set; }

        /// <summary>
        /// Real count.
        /// </summary>
        public int? RealCount { get; set; }

        /// <summary>
        /// Discord channel ID.
        /// </summary>
        [StringLength(20, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        public string DiscordChannelId { get; set; }

        /// <summary>
        /// Flag that notify discord bot to create channel. DiscordChannelId property have to contains null.
        /// </summary>
        public bool CreateDiscordChannel { get; set; }

        public Database.Entity.Events.Event ToEntity(long loggedUserId)
        {
            return new Database.Entity.Events.Event()
            {
                CreatedAt = DateTime.Now,
                CreatedById = loggedUserId,
                Description = Description,
                DiscordChannelId = DiscordChannelId,
                ExpectedCount = ExpectedCount,
                From = From,
                Name = Name,
                Note = Note,
                RealCount = RealCount,
                To = To
            };
        }

        public void UpdateEntity(Database.Entity.Events.Event entity, long loggedUserId)
        {
            entity.Description = Description;
            entity.DiscordChannelId = DiscordChannelId;
            entity.ExpectedCount = ExpectedCount;
            entity.From = From;
            entity.To = To;
            entity.Name = Name;
            entity.Note = Note;
            entity.RealCount = RealCount;
            entity.MarkAsModified(loggedUserId);
        }
    }
}
