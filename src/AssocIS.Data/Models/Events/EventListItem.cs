﻿using AssocIS.Common.Extensions;
using System;

namespace AssocIS.Data.Models.Events
{
    /// <summary>
    /// Simplified data about event.
    /// </summary>
    public class EventListItem
    {
        /// <summary>
        /// Unique ID.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Name of event.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Start of event.
        /// </summary>
        public DateTime From { get; set; }

        /// <summary>
        /// End of event.
        /// </summary>
        public DateTime To { get; set; }

        /// <summary>
        /// Expected person count on event.
        /// </summary>
        public int ExpectedCount { get; set; }

        /// <summary>
        /// Real person count on event.
        /// </summary>
        public int? RealCount { get; set; }

        /// <summary>
        /// Flag that describe if user can manage with event.
        /// </summary>
        public bool CanManage { get; set; }

        public EventListItem() { }

        public EventListItem(Database.Entity.Events.Event entity, bool canManage)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Name = entity.Name;
            From = entity.From;
            To = entity.To ?? entity.From.GetEndOfDay();
            ExpectedCount = entity.ExpectedCount;
            RealCount = entity.RealCount;
            CanManage = canManage;
        }
    }
}