﻿using AssocIS.Data.Resources.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Common
{
    /// <summary>
    /// Request for pagination.
    /// </summary>
    public class PaginationParams
    {
        /// <summary>
        /// Maximal count of items at one page. Default value is 50.
        /// </summary>
        [Range(0, 200, ErrorMessageResourceType = typeof(CommonResources), ErrorMessageResourceName = nameof(CommonResources.PaginationLimitExceeded))]
        public int Limit { get; set; } = 50;

        /// <summary>
        /// Page number.
        /// </summary>
        [Range(0, int.MaxValue, ErrorMessageResourceType = typeof(CommonResources), ErrorMessageResourceName = nameof(CommonResources.PageNumOutOfRange))]
        public int Page { get; set; }
    }
}
