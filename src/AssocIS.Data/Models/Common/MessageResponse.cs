﻿namespace AssocIS.Data.Models.Common
{
    /// <summary>
    /// Response that contains only text message.
    /// </summary>
    public class MessageResponse
    {
        /// <summary>
        /// Content.
        /// </summary>
        public string Message { get; set; }

        public MessageResponse(string message)
        {
            Message = message;
        }
    }
}
