﻿using AssocIS.Data.Models.Users;
using System;

namespace AssocIS.Data.Models.Common
{
    /// <summary>
    /// Information about creation and modification of record.
    /// </summary>
    public class LogData
    {
        /// <summary>
        /// When record was created.
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Who record created.
        /// </summary>
        public User CreatedBy { get; set; }

        /// <summary>
        /// When record was last modified.
        /// </summary>
        public DateTime? LastModifiedAt { get; set; }

        /// <summary>
        /// Who record was last modified.
        /// </summary>
        public User LastModifiedBy { get; set; }

        public LogData() { }

        public LogData(Database.Entity.BaseEntities.LoggedEntity entity)
        {
            if (entity == null)
                return;

            CreatedAt = entity.CreatedAt;
            CreatedBy = new(entity.CreatedBy);
            LastModifiedAt = entity.LastModifiedAt == null ? null : entity.LastModifiedAt.Value;
            LastModifiedBy = entity.LastModifiedBy == null ? null : new(entity.LastModifiedBy);
        }
    }
}
