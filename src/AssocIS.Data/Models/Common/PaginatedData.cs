﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssocIS.Data.Models.Common
{
    /// <summary>
    /// Paginated result.
    /// </summary>
    /// <remarks>https://github.com/Misha12/IW5-2020-Team-0007/blob/master/IW5-2020-team0007/MovieDatabase.Data/Models/Common/PaginatedData.cs</remarks>
    public class PaginatedData<TModel>
    {
        /// <summary>
        /// Data
        /// </summary>
        public List<TModel> Data { get; set; }

        /// <summary>
        /// Page number.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Total count of items, independently of pagination
        /// </summary>
        public long TotalItemsCount { get; set; }

        /// <summary>
        /// A flag that the user can request for next page.
        /// </summary>
        public bool CanNext { get; set; }

        /// <summary>
        /// A flag that the user can request for previous page.
        /// </summary>
        public bool CanPrev { get; set; }

        public static async Task<PaginatedData<TModel>> CreateAsync<TEntity>(IQueryable<TEntity> query, PaginationParams request, Converter<TEntity, TModel> itemConverter)
        {
            var result = CreateEmpty(request);
            result.TotalItemsCount = await query.CountAsync();

            var skip = (request.Page == 0 ? 0 : request.Page - 1) * request.Limit;
            result.SetFlags(skip, request.Limit);

            if (result.TotalItemsCount == 0)
            {
                result.Data = new List<TModel>();
                return result;
            }

            query = query.Skip(skip).Take(request.Limit);
            result.Data = (await query.ToListAsync()).ConvertAll(itemConverter);

            return result;
        }

        public static PaginatedData<TModel> Create(List<TModel> data, PaginationParams request)
        {
            var result = CreateEmpty(request);
            result.TotalItemsCount = data.Count;

            var skip = (request.Page == 0 ? 0 : request.Page - 1) * request.Limit;
            result.SetFlags(skip, request.Limit);
            result.Data = data.Skip(skip).Take(request.Limit).ToList();

            return result;
        }

        public static PaginatedData<TModel> CreateEmpty(PaginationParams request)
        {
            if (request.Page <= 1)
                request.Page = 0;

            return new PaginatedData<TModel>()
            {
                Page = request.Page == 0 ? 1 : request.Page
            };
        }

        internal void SetFlags(int skip, int limit)
        {
            CanPrev = skip != 0;
            CanNext = skip + limit < TotalItemsCount;
        }
    }
}
