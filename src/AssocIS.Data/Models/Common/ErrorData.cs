﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace AssocIS.Data.Models.Common
{
    /// <summary>
    /// Information about error.
    /// </summary>
    public class ErrorData
    {
        public List<string> Errors { get; set; }

        public ErrorData() : this(null) { }

        public ErrorData(List<string> errors)
        {
            Errors = errors ?? new List<string>();
        }
    }
}
