﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AssocIS.Data.Models.Teams
{
    /// <summary>
    /// Detailed information about team.
    /// </summary>
    public class Team : TeamListItem
    {
        /// <summary>
        /// Team description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Datetime when team was created. Datetime is in UTC.
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Role ID on discord.
        /// </summary>
        public string DiscordRoleId { get; set; }

        /// <summary>
        /// Groups of team in Google Workspace
        /// </summary>
        public List<string> GoogleGroups { get; set; }

        public Team() { }

        public Team(Database.Entity.Team team, bool canRemove) : base(team, canRemove)
        {
            if (team == null)
                return;

            Description = team.Description;
            CreatedAt = team.CreatedAt;
            DiscordRoleId = team.DiscordRoleId;
            GoogleGroups = team.GoogleGroups.Select(o => o.Email).ToList();
        }
    }
}
