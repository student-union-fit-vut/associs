﻿using AssocIS.Data.Models.Users;

namespace AssocIS.Data.Models.Teams
{
    /// <summary>
    /// Team member.
    /// </summary>
    public class TeamMember : User
    {
        /// <summary>
        /// Flag that the user is team leader.
        /// </summary>
        public bool IsLeader { get; set; }

        public TeamMember() { }

        public TeamMember(Database.Entity.TeamMember teamMember) : base(teamMember?.User)
        {
            if (teamMember == null)
                return;

            IsLeader = teamMember.IsLeader;
        }
    }
}
