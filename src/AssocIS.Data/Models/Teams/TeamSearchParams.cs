﻿using AssocIS.Data.Models.Common;

namespace AssocIS.Data.Models.Teams
{
    /// <summary>
    /// Search request for team.
    /// </summary>
    public class TeamSearchParams : PaginationParams
    {
        /// <summary>
        /// Query search, that must contain.
        /// </summary>
        public string NameQuery { get; set; }

        /// <summary>
        /// Show only empty teams or teams with only leader.
        /// </summary>
        public bool OnlyEmptyTeams { get; set; }
    }
}
