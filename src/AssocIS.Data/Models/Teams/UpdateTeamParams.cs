﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.Team;
using AssocIS.Data.Resources.Templates.EmailTemplates;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AssocIS.Data.Models.Teams
{
    /// <summary>
    /// Request for create or update team.
    /// </summary>
    public class UpdateTeamParams
    {
        /// <summary>
        /// Name of team.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [StringLength(100, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        [UniqueNameCheckValidation(typeof(Database.Entity.Team), nameof(NameChanged), ErrorMessageResourceName = nameof(TeamResources.TeamNameExists), ErrorMessageResourceType = typeof(TeamResources))]
        public string Name { get; set; }

        /// <summary>
        /// Helper property that contains true if team name was changed.
        /// </summary>
        public bool NameChanged { get; set; }

        /// <summary>
        /// Team description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ID of associated Discord role for team.
        /// </summary>
        public string DiscordRoleId { get; set; }

        /// <summary>
        /// If DiscordRoleId is null, and CreateRole is true, create role on discord.
        /// </summary>
        public bool CreateDiscordRole { get; set; }

        /// <summary>
        /// ID of team leader.
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceName = nameof(TeamResources.LeaderMustExists), ErrorMessageResourceType = typeof(TeamResources))]
        public long? LeaderId { get; set; }

        /// <summary>
        /// Groups of team in google workspace.
        /// </summary>
        public List<string> GoogleGroups { get; set; }

        public Database.Entity.Team ToEntity()
        {
            var entity = new Database.Entity.Team()
            {
                DiscordRoleId = DiscordRoleId,
                Description = Description,
                Name = Name,
                CreatedAt = DateTime.Now
            };

            if (LeaderId != null)
            {
                entity.Members.Add(new Database.Entity.TeamMember()
                {
                    IsLeader = true,
                    UserId = LeaderId.Value
                });
            }

            if (GoogleGroups != null)
            {
                foreach (var group in GoogleGroups)
                {
                    entity.GoogleGroups.Add(new Database.Entity.TeamMailingList()
                    {
                        Email = group
                    });
                }
            }

            return entity;
        }

        public void UpdateEntity(Database.Entity.Team entity)
        {
            if (NameChanged) entity.Name = Name;
            entity.Description = Description;
            entity.DiscordRoleId = DiscordRoleId;

            // Downgrade team leader to team member.
            var currentLeader = entity.Members.FirstOrDefault(o => o.IsLeader);
            if (currentLeader != null)
                currentLeader.IsLeader = false;

            if (LeaderId != null)
            {
                // Request contains new team leader. Set it.
                var teamMember = entity.Members.FirstOrDefault(o => o.UserId == LeaderId.Value);

                if (teamMember == null)
                {
                    // Adds new member to team.

                    teamMember = new Database.Entity.TeamMember()
                    {
                        IsLeader = true,
                        UserId = LeaderId.Value
                    };

                    entity.Members.Add(teamMember);
                }
                else
                {
                    // Modify current team member state.
                    teamMember.IsLeader = true;
                }
            }

            if (GoogleGroups != null)
            {
                entity.GoogleGroups = entity.GoogleGroups.Where(o => GoogleGroups.Contains(o.Email)).ToHashSet();

                foreach (var group in GoogleGroups.Where(o => !entity.GoogleGroups.Any(x => x.Email == o)))
                {
                    entity.GoogleGroups.Add(new Database.Entity.TeamMailingList()
                    {
                        Email = group
                    });
                }
            }
        }
    }
}
