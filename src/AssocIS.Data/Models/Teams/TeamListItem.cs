﻿using AssocIS.Data.Models.Users;
using System.Linq;
using System.Security.Claims;

namespace AssocIS.Data.Models.Teams
{
    /// <summary>
    /// Simplified information about team for list.
    /// </summary>
    public class TeamListItem
    {
        /// <summary>
        /// Unique ID.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Team name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Team leader.
        /// </summary>
        public UserInfo Leader { get; set; }

        /// <summary>
        /// Count of members in team.
        /// </summary>
        public int MembersCount { get; set; }

        /// <summary>
        /// Flag that describe if logged user can remove team.
        /// </summary>
        public bool CanRemove { get; set; }

        public TeamListItem() { }

        public TeamListItem(Database.Entity.Team team, bool canRemove)
        {
            if (team == null)
                return;

            Id = team.Id;
            Name = team.Name;

            var leader = team.Members.FirstOrDefault(o => o.IsLeader);
            Leader = leader != null ? new UserInfo(leader.User) : null;

            MembersCount = team.Members.Count;
            CanRemove = canRemove;
        }
    }
}
