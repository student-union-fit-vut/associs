﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.User;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Teams
{
    /// <summary>
    /// Data for add or remove user in team.
    /// </summary>
    public class UpdateTeamMemberParams
    {
        /// <summary>
        /// Team ID
        /// </summary>
        [FromRoute(Name = "teamId")]
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public long TeamId { get; set; }

        /// <summary>
        /// User ID
        /// </summary>
        [FromRoute(Name = "userId")]
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceName = nameof(UserResources.UserNotFound), ErrorMessageResourceType = typeof(UserResources))]
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public long UserId { get; set; }

        public Database.Entity.TeamMember ToEntity(bool isLeader = false)
        {
            return new Database.Entity.TeamMember()
            {
                TeamId = TeamId,
                UserId = UserId,
                IsLeader = isLeader
            };
        }
    }
}
