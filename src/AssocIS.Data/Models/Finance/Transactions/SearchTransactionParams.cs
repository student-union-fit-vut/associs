﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.Common;
using AssocIS.Database.Enums;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AssocIS.Data.Resources.Finance;

namespace AssocIS.Data.Models.Finance.Transactions
{
    /// <summary>
    /// Search parameters in transactions.
    /// </summary>
    public class SearchTransactionParams : PaginationParams, IValidatableObject
    {
        /// <summary>
        /// Start of range when transaction was processed.
        /// </summary>
        [DateLessThan("ProcessedTo", ErrorMessageResourceName = nameof(CommonResources.FromIsAfterEnd), ErrorMessageResourceType = typeof(CommonResources))]
        public DateTime? ProcessedFrom { get; set; }

        /// <summary>
        /// End of range when transaction was processed.
        /// </summary>
        public DateTime? ProcessedTo { get; set; }

        /// <summary>
        /// Start of range of amount.
        /// </summary>
        public decimal? AmountFrom { get; set; }

        /// <summary>
        /// End of range of amount.
        /// </summary>
        public decimal? AmountTo { get; set; }

        /// <summary>
        /// List of transaction types.
        /// </summary>
        public List<TransactionType> TransactionTypes { get; set; }

        /// <summary>
        /// Query search in name of transaction.
        /// </summary>
        public string NameQuery { get; set; }

        /// <summary>
        /// IDs of accounts.
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.Finance.Account), ErrorMessageResourceName = nameof(FinanceResources.AccountNotFound), ErrorMessageResourceType = typeof(FinanceResources))]
        public List<long> AccountIds { get; set; }

        /// <summary>
        /// IDs of suppliers.
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.Finance.Supplier), ErrorMessageResourceName = nameof(FinanceResources.AccountNotFound), ErrorMessageResourceType = typeof(FinanceResources))]
        public List<long> SupplierIds { get; set; }

        /// <summary>
        /// IDs of transaction categories.
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.Finance.TransactionCategory), ErrorMessageResourceName = nameof(FinanceResources.CategoryNotFound), ErrorMessageResourceType = typeof(FinanceResources))]
        public List<long> CategoryIds { get; set; }

        /// <summary>
        /// Search only transaction that contains attachments.
        /// </summary>
        public bool OnlyWithAttachments { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (AmountFrom != null && AmountTo != null && AmountTo < AmountFrom)
            {
                var localizer = validationContext.GetService<IStringLocalizer<CommonResources>>();
                yield return new ValidationResult(localizer[nameof(CommonResources.RangeFromIsAfterEnd)]);
            }
        }
    }
}
