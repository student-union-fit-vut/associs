﻿using AssocIS.Data.Models.Finance.Accounts;
using AssocIS.Data.Models.Finance.Categories;
using AssocIS.Data.Models.Finance.Suppliers;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AssocIS.Data.Models.Finance.Transactions
{
    /// <summary>
    /// List item information about transaction.
    /// </summary>
    public class TransactionListItem
    {
        /// <summary>
        /// Unique ID.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Name of transaction.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Account of transaction.
        /// </summary>
        public AccountListItem Account { get; set; }

        /// <summary>
        /// Supplier data. May contains null.
        /// </summary>
        public SupplierListItem Supplier { get; set; }

        /// <summary>
        /// Amount of money in transaction.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Transaction type (Incoming payment, Outgoing payment).
        /// </summary>
        public TransactionType Type { get; set; }

        /// <summary>
        /// Datetime when transaction was processed. It does not have to match the creation date. Datetime is in UTC.
        /// </summary>
        public DateTime ProcessedAt { get; set; }

        /// <summary>
        /// Categories.
        /// </summary>
        public List<TransactionCategory> Categories { get; set; }

        /// <summary>
        /// Flag that describe, if user can manage transaction.
        /// </summary>
        public bool CanManage { get; set; }

        public TransactionListItem() { }

        public TransactionListItem(Database.Entity.Finance.Transaction entity, bool canManage)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Name = entity.Name;
            Account = new AccountListItem(entity.Account, false);
            Amount = entity.Amount;
            Type = entity.Type;
            ProcessedAt = entity.ProcessedAt;
            Supplier = entity.Supplier == null ? null : new SupplierListItem(entity.Supplier, false);
            Categories = entity.Categories.Select(o => new TransactionCategory(o, 0)).ToList();
            CanManage = canManage;
        }
    }
}
