﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.Finance;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AssocIS.Data.Models.Finance.Transactions
{
    /// <summary>
    /// Data for create or update transaction.
    /// </summary>
    public class UpdateTransactionParams
    {
        /// <summary>
        /// Datetime when transaction was processed. It does not have to match the creation date. Datetime have to be in UTC.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public DateTime ProcessedAt { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public decimal Amount { get; set; }

        /// <summary>
        /// Transaction type (Incoming payment, Outgoing payment).
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public TransactionType Type { get; set; }

        /// <summary>
        /// Name of payment.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string Name { get; set; }

        /// <summary>
        /// Note.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Account ID where money goes.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [IdExistenceValidator(typeof(Database.Entity.Finance.Account), ErrorMessageResourceName = nameof(FinanceResources.Update_AccountNotExists), ErrorMessageResourceType = typeof(FinanceResources))]
        public long AccountId { get; set; }

        /// <summary>
        /// Supplier ID if transaction contains supplier (such as payment for product purchase).
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.Finance.Supplier), ErrorMessageResourceType = typeof(FinanceResources), ErrorMessageResourceName = nameof(FinanceResources.Update_SupplierNotExists))]
        public long? SupplierId { get; set; }

        /// <summary>
        /// Category IDs.
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.Finance.TransactionCategory), ErrorMessageResourceName = nameof(FinanceResources.Update_CategoryNotExists), ErrorMessageResourceType = typeof(FinanceResources))]
        public List<long> Categories { get; set; }

        public Database.Entity.Finance.Transaction ToEntity(long loggedUserId, List<Database.Entity.Finance.TransactionCategory> categories)
        {
            return new Database.Entity.Finance.Transaction()
            {
                AccountId = AccountId,
                Amount = Amount,
                Categories = categories.ToHashSet(),
                CreatedAt = DateTime.Now,
                CreatedById = loggedUserId,
                Name = Name,
                Note = Note,
                ProcessedAt = ProcessedAt,
                SupplierId = SupplierId,
                Type = Type
            };
        }

        public void UpdateEntity(Database.Entity.Finance.Transaction entity, long loggedUserId, List<Database.Entity.Finance.TransactionCategory> categories)
        {
            entity.AccountId = AccountId;
            entity.Amount = Amount;
            entity.Categories = categories.ToHashSet();
            entity.Name = Name;
            entity.Note = Note;
            entity.ProcessedAt = ProcessedAt;
            entity.SupplierId = SupplierId;
            entity.Type = Type;
            entity.MarkAsModified(loggedUserId);
        }
    }
}
