﻿namespace AssocIS.Data.Models.Finance.Transactions
{
    public class TransactionReportItem<TGroup>
    {
        /// <summary>
        /// Grouping value (Type, Supplier, Account, ...)
        /// </summary>
        public TGroup GroupValue { get; set; }

        /// <summary>
        /// Count of items.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        public TransactionReportItem() { }

        public TransactionReportItem(TGroup groupValue, int count, decimal amount)
        {
            GroupValue = groupValue;
            Count = count;
            Amount = amount;
        }
    }
}
