﻿using AssocIS.Data.Models.Common;
using System.Collections.Generic;
using System.Linq;

namespace AssocIS.Data.Models.Finance.Transactions
{
    /// <summary>
    /// Detailed information about transaction.
    /// </summary>
    public class Transaction : TransactionListItem
    {
        /// <summary>
        /// Note.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Attachments.
        /// </summary>
        public List<TransactionAttachment> Attachments { get; set; }

        /// <summary>
        /// Logging data about creation and modification of transaction.
        /// </summary>
        public LogData LogData { get; set; }

        public Transaction() { }

        public Transaction(Database.Entity.Finance.Transaction entity, bool canManage) : base(entity, canManage)
        {
            if (entity == null)
                return;

            Note = entity.Note;
            Attachments = entity.Attachments.Select(o => new TransactionAttachment(o)).ToList();
            LogData = new LogData(entity);
        }
    }
}
