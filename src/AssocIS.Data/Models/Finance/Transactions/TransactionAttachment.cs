﻿using AssocIS.Data.Models.Common;
using AssocIS.Database.Enums;

namespace AssocIS.Data.Models.Finance.Transactions
{
    /// <summary>
    /// Information about attachment.
    /// </summary>
    public class TransactionAttachment
    {
        /// <summary>
        /// Unique ID.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Attachment filename.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Attachment type.
        /// </summary>
        public TransactionAttachmentType Type { get; set; }

        /// <summary>
        /// Log data about creation and last modification.
        /// </summary>
        public LogData LogData { get; set; }

        public TransactionAttachment() { }

        public TransactionAttachment(Database.Entity.Finance.TransactionAttachment entity)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Name = entity.Name;
            Type = entity.Type;
            LogData = new LogData(entity);
        }
    }
}
