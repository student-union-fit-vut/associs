﻿using AssocIS.Data.Resources.Common;
using AssocIS.Database.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace AssocIS.Data.Models.Finance.Transactions
{
    public class CreateTransactionAttachmentParams
    {
        public TransactionAttachmentType Type { get; set; }
        public IFormFile File { get; set; }

        public async Task<Database.Entity.Finance.TransactionAttachment> ToEntityAsync(long transactionId, long loggedUserId)
        {
            using var ms = new MemoryStream();
            await File.CopyToAsync(ms);

            return new Database.Entity.Finance.TransactionAttachment()
            {
                CreatedAt = DateTime.Now,
                CreatedById = loggedUserId,
                Name = File.FileName,
                Type = Type,
                DecompressedData = ms.ToArray(),
                TransactionId = transactionId
            };
        }
    }
}
