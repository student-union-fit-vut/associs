﻿using AssocIS.Data.Resources.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Finance.Accounts
{
    /// <summary>
    /// Data for create or update account.
    /// </summary>
    public class UpdateAccountParams
    {
        /// <summary>
        /// Name of account.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string Name { get; set; }

        /// <summary>
        /// Note.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Account number. If account is not in bank that contains null.
        /// </summary>
        [StringLength(50, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Bank code. If account is not in bank that contains null.
        /// </summary>
        [StringLength(10, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        public string BankCode { get; set; }

        public Database.Entity.Finance.Account ToEntity(long loggedUserId)
        {
            return new Database.Entity.Finance.Account()
            {
                AccountNumber = AccountNumber,
                CreatedAt = DateTime.Now,
                BankCode = BankCode,
                CreatedById = loggedUserId,
                Name = Name,
                Note = Note
            };
        }

        public void UpdateEntity(Database.Entity.Finance.Account entity, long loggedUserId)
        {
            entity.AccountNumber = AccountNumber;
            entity.BankCode = BankCode;
            entity.Name = Name;
            entity.Note = Note;
            entity.MarkAsModified(loggedUserId);
        }
    }
}
