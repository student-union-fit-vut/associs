﻿using AssocIS.Data.Models.Common;

namespace AssocIS.Data.Models.Finance.Accounts
{
    public class Account : AccountListItem
    {
        /// <summary>
        /// Count of transactions.
        /// </summary>
        public int TransactionCount { get; set; }

        /// <summary>
        /// Actual state of account (Amount).
        /// </summary>
        public decimal AccountState { get; set; }

        /// <summary>
        /// Data about creation and modifications.
        /// </summary>
        public LogData LogData { get; set; }

        /// <summary>
        /// Account note.
        /// </summary>
        public string Note { get; set; }

        public Account(Database.Entity.Finance.Account entity, int transactionCount, decimal state,
            bool canRemove) : base(entity, canRemove)
        {
            if (entity == null)
                return;

            TransactionCount = transactionCount;
            AccountState = state;
            LogData = new LogData(entity);
            Note = entity.Note;
        }
    }
}
