﻿using AssocIS.Data.Models.Common;

namespace AssocIS.Data.Models.Finance.Accounts
{
    /// <summary>
    /// Search parameters for account.
    /// </summary>
    public class SearchAccountParams
    {
        /// <summary>
        /// Query search in meeting name.
        /// </summary>
        public string NameQuery { get; set; }

        /// <summary>
        /// Bank code.
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// Account number query. Search compares from start.
        /// </summary>
        public string AccountNumberQuery { get; set; }
    }
}