﻿namespace AssocIS.Data.Models.Finance.Accounts
{
    /// <summary>
    /// Information about account.
    /// </summary>
    public class AccountListItem
    {
        /// <summary>
        /// Unique Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Account name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Account number.
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Bank code.
        /// If account is not bank, contains null.
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// Flag that describe if account can remove.
        /// </summary>
        public bool CanRemove { get; set; }

        public AccountListItem() { }

        public AccountListItem(Database.Entity.Finance.Account entity, bool canRemove)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Name = entity.Name;
            AccountNumber = entity.AccountNumber;
            BankCode = entity.BankCode;
            CanRemove = canRemove;
        }
    }
}
