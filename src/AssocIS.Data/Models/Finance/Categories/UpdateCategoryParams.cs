﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.Finance;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Finance.Categories
{
    /// <summary>
    /// Data for create or update financial category.
    /// </summary>
    public class UpdateCategoryParams
    {
        /// <summary>
        /// Name of category.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [StringLength(100, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        [UniqueNameCheckValidation(typeof(Database.Entity.Finance.TransactionCategory), nameof(NameChanged), ErrorMessageResourceName = nameof(FinanceResources.CategoryNameNotUnique), ErrorMessageResourceType = typeof(FinanceResources))]
        public string Name { get; set; }

        /// <summary>
        /// Flag to notify that name of category was changed.
        /// </summary>
        public bool NameChanged { get; set; }

        /// <summary>
        /// Link to icon of category.
        /// </summary>
        public string Icon { get; set; }

        public Database.Entity.Finance.TransactionCategory ToEntity()
        {
            return new Database.Entity.Finance.TransactionCategory()
            {
                Icon = Icon,
                Name = Name
            };
        }

        public void UpdateEntity(Database.Entity.Finance.TransactionCategory entity)
        {
            entity.Name = Name;
            entity.Icon = Icon;
        }
    }
}
