﻿namespace AssocIS.Data.Models.Finance.Categories
{
    /// <summary>
    /// Category of transaction.
    /// </summary>
    public class TransactionCategory
    {
        /// <summary>
        /// Unique ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Name of category.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Link to icon.
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Count of transaction in this category.
        /// </summary>
        public int TransactionCount { get; set; }

        public TransactionCategory() { }

        public TransactionCategory(Database.Entity.Finance.TransactionCategory entity, int transactionCount = 0)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Name = entity.Name;
            Icon = entity.Icon;
            TransactionCount = transactionCount;
        }
    }
}
