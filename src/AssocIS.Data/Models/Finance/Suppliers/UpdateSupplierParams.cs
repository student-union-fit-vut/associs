﻿using AssocIS.Data.Resources.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Finance.Suppliers
{
    /// <summary>
    /// Data for create or update supplier.
    /// </summary>
    public class UpdateSupplierParams
    {
        /// <summary>
        /// Name of supplier.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string Name { get; set; }

        /// <summary>
        /// Some contact (Email, phone, ...)
        /// </summary>
        public string Contact { get; set; }

        /// <summary>
        /// Link to web (Url).
        /// </summary>
        [Url(ErrorMessageResourceName = nameof(CommonResources.InvalidUrl), ErrorMessageResourceType = typeof(CommonResources))]
        public string WebLink { get; set; }

        /// <summary>
        /// Note to supplier.
        /// </summary>
        public string Note { get; set; }

        public Database.Entity.Finance.Supplier ToEntity(long loggedUserId)
        {
            return new Database.Entity.Finance.Supplier()
            {
                Contact = Contact,
                CreatedAt = DateTime.Now,
                CreatedById = loggedUserId,
                Name = Name,
                Note = Note,
                WebLink = WebLink
            };
        }

        public void UpdateEntity(Database.Entity.Finance.Supplier entity, long loggedUserId)
        {
            entity.Contact = Contact;
            entity.Name = Name;
            entity.Note = Note;
            entity.WebLink = WebLink;
            entity.MarkAsModified(loggedUserId);
        }
    }
}
