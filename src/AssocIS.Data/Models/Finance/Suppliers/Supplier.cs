﻿using AssocIS.Data.Models.Common;

namespace AssocIS.Data.Models.Finance.Suppliers
{
    /// <summary>
    /// Detailed information about suppliler.
    /// </summary>
    public class Supplier : SupplierListItem
    {
        /// <summary>
        /// Contact to suppler (Email, Phone, ...)
        /// </summary>
        public string Contact { get; set; }

        /// <summary>
        /// URL to web of supplier.
        /// </summary>
        public string WebLink { get; set; }

        /// <summary>
        /// Note of supplier.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Information about creation and modification of record.
        /// </summary>
        public LogData LogData { get; set; }

        public Supplier() { }

        public Supplier(Database.Entity.Finance.Supplier entity, bool canRemove) : base(entity, canRemove)
        {
            if (entity == null)
                return;

            Contact = entity.Contact;
            WebLink = entity.WebLink;
            Note = entity.Note;
            LogData = new LogData(entity);
        }
    }
}
