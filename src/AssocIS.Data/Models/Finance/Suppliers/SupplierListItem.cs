﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssocIS.Data.Models.Finance.Suppliers
{
    /// <summary>
    /// List item of supplier.
    /// </summary>
    public class SupplierListItem
    {
        /// <summary>
        /// Unique ID.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Name of supplier.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Flag that describe, if can remove supplier.
        /// </summary>
        public bool CanRemove { get; set; }

        public SupplierListItem() { }

        public SupplierListItem(Database.Entity.Finance.Supplier entity, bool canRemove)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Name = entity.Name;
            CanRemove = canRemove;
        }
    }
}
