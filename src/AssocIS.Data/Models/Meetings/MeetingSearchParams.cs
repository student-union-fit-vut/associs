﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Models.Common;
using AssocIS.Data.Resources.Common;
using AssocIS.Data.Resources.User;
using AssocIS.Database.Entity;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;

namespace AssocIS.Data.Models.Meetings
{
    /// <summary>
    /// Request for search meetings.
    /// </summary>
    public class MeetingSearchParams : PaginationParams
    {
        /// <summary>
        /// Query search in meeting name.
        /// </summary>
        public string NameQuery { get; set; }

        /// <summary>
        /// Start of range when meeting is planned. DateTime have to be in UTC.
        /// </summary>
        [DateLessThan("To", ErrorMessageResourceName = nameof(CommonResources.FromIsAfterEnd), ErrorMessageResourceType = typeof(CommonResources))]
        public DateTime? From { get; set; }

        /// <summary>
        /// End of range when meeting is planned. DateTime have to be in UTC.
        /// </summary>
        public DateTime? To { get; set; }

        /// <summary>
        /// Attendance types (Required, Optional, RequiredForExpected.)
        /// </summary>
        public List<MeetingAttendanceType> AttendanceTypes { get; set; }

        /// <summary>
        /// Meeting states (Drafted, Summoned, Cancelled)
        /// </summary>
        public List<MeetingState> MeetingStates { get; set; }

        /// <summary>
        /// Query search in agenda.
        /// </summary>
        public string AgendaQuery { get; set; }

        /// <summary>
        /// Query search in report.
        /// </summary>
        public string ReportQuery { get; set; }

        /// <summary>
        /// Query search in message.
        /// </summary>
        public string MessageQuery { get; set; }

        /// <summary>
        /// IDs of users that created meeting.
        /// </summary>
        [IdExistenceValidator(typeof(User), ErrorMessageResourceName = nameof(UserResources.UserNotFound), ErrorMessageResourceType = typeof(UserResources))]
        public long? AuthorId { get; set; }
    }
}
