﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.User;
using AssocIS.Database.Enums;

namespace AssocIS.Data.Models.Meetings
{
    /// <summary>
    /// Params object for users meeting presence.
    /// </summary>
    public class SetMeetingPresenceParams
    {
        /// <summary>
        /// User's Id
        /// </summary>
        [IdExistenceValidator(typeof(Database.Entity.User), ErrorMessageResourceName = nameof(UserResources.UserNotFound), ErrorMessageResourceType = typeof(UserResources))]
        public long UserId { get; set; }

        /// <summary>
        /// Presence
        /// </summary>
        public MeetingPresenceType PresenceType { get; set; }

        public Database.Entity.MeetingPresence ToEntity()
        {
            return new Database.Entity.MeetingPresence()
            {
                UserId = UserId,
                PresenceType = PresenceType
            };
        }

        public void UpdateEntity(Database.Entity.MeetingPresence entity)
        {
            entity.PresenceType = PresenceType;
        }
    }
}
