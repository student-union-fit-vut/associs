﻿using AssocIS.Data.Models.Users;
using AssocIS.Database.Enums;
using System.Collections.Generic;

namespace AssocIS.Data.Models.Meetings
{
    /// <summary>
    /// Presence report on meetings for user.
    /// </summary>
    public class UserPresenceStatus
    {
        /// <summary>
        /// Simple information about user.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Unknown count of presence.
        /// </summary>
        public int UnknownCount { get; set; }

        /// <summary>
        /// Count of absence.
        /// </summary>
        public int AbsentCount { get; set; }

        /// <summary>
        /// Count of present.
        /// </summary>
        public int PresentCount { get; set; }

        /// <summary>
        /// Expected presence count.
        /// </summary>
        public int ExpectedCount { get; set; }

        /// <summary>
        /// Apologized count.
        /// </summary>
        public int ApologizedCount { get; set; }

        /// <summary>
        /// Warning about users absence.
        /// </summary>
        public bool IsWarn { get; set; }

        public UserPresenceStatus(Database.Entity.User user)
        {
            User = new User(user);
        }

        public void AddPresence(MeetingPresenceType type)
        {
            switch (type)
            {
                case MeetingPresenceType.Absent:
                    AbsentCount++;
                    break;
                case MeetingPresenceType.Apologized:
                    ApologizedCount++;
                    break;
                case MeetingPresenceType.Expected:
                    ExpectedCount++;
                    break;
                case MeetingPresenceType.Present:
                    PresentCount++;
                    break;
                case MeetingPresenceType.Unknown:
                    UnknownCount++;
                    break;
            }
        }
    }
}
