﻿using AssocIS.Data.Helpers;
using AssocIS.Data.Models.Users;
using AssocIS.Database.Enums;

namespace AssocIS.Data.Models.Meetings
{
    /// <summary>
    /// List item of user presence on meeting.
    /// </summary>
    public class MeetingPresenceListItem
    {
        /// <summary>
        /// User identification.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Current user presence.
        /// </summary>
        public MeetingPresenceType Presence { get; set; }

        public MeetingPresenceListItem() { }

        public MeetingPresenceListItem(Database.Entity.MeetingPresence entity)
        {
            if (entity == null)
                return;

            User = new User(entity.User);
            Presence = MeetingPresenceHelper.CalculatePresence(entity.Meeting, entity, entity.User);
        }
    }
}
