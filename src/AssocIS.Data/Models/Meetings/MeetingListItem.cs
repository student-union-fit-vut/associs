﻿using AssocIS.Data.Helpers;
using AssocIS.Database.Enums;
using System;
using System.Linq;

namespace AssocIS.Data.Models.Meetings
{
    /// <summary>
    /// Simplified information about meeting.
    /// </summary>
    public class MeetingListItem
    {
        /// <summary>
        /// Unique ID.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Name of meeting.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Datetime of meeting. Datetime is in UTC.
        /// </summary>
        public DateTime At { get; set; }

        /// <summary>
        /// Datetime of expected end of meeting. Datetime is in UTC.
        /// </summary>
        public DateTime ExpectedEnd { get; set; }

        /// <summary>
        /// Attendance type (Required, Optional, Required for expected)
        /// </summary>
        public MeetingAttendanceType AttendanceType { get; set; }

        /// <summary>
        /// State of meeting. (Draft, Optional, Cancelled)
        /// </summary>
        public MeetingState State { get; set; }

        /// <summary>
        /// Presence state of current logged user.
        /// </summary>
        public MeetingPresenceType? CurrentUserPresence { get; set; }

        public MeetingListItem() { }

        public MeetingListItem(Database.Entity.Meeting entity, Database.Entity.User member)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Name = entity.Name;
            At = entity.At;
            AttendanceType = entity.AttendanceType;
            State = entity.State;
            ExpectedEnd = entity.ExpectedEnd;

            if (member != null)
            {
                var presence = entity.Presences.FirstOrDefault(o => o.UserId == member.Id);
                CurrentUserPresence = MeetingPresenceHelper.CalculatePresence(entity, presence, member);
            }
        }
    }
}
