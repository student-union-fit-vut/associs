﻿using AssocIS.Data.Infrastructure.Validation;
using AssocIS.Data.Resources.Common;
using AssocIS.Database.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AssocIS.Data.Models.Meetings
{
    /// <summary>
    /// Request for create or update meeting.
    /// </summary>
    public class UpdateMeetingParams
    {
        /// <summary>
        /// Name of meeting.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [StringLength(100, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        public string Name { get; set; }

        /// <summary>
        /// Date time of meeting. Datetime have to be in UTC.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [DateLessThan("ExpectedEnd", ErrorMessageResourceName = nameof(CommonResources.FromIsAfterEnd), ErrorMessageResourceType = typeof(CommonResources))]
        public DateTime At { get; set; }

        /// <summary>
        /// Datetime of expected end of meeting. Datetime have to be in UTC.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public DateTime ExpectedEnd { get; set; }

        /// <summary>
        /// Agenda of meeting. Can edit only if meeting wasn't notified.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public List<string> Agenda { get; set; }

        /// <summary>
        /// Report from meeting.
        /// </summary>
        public string Report { get; set; }

        /// <summary>
        /// Attendance type (MandatoryAttendance, OptionalAttendance, Mandatory attendance for expected).
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public MeetingAttendanceType AttendanceType { get; set; }

        /// <summary>
        /// State of meeting (Draft, Summoned, Cancelled).
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public MeetingState MeetingState { get; set; }

        /// <summary>
        /// User IDs of users that expected on meeting.
        /// </summary>
        public List<long> ExpectedUsers { get; set; }

        /// <summary>
        /// Location, where meeting will be.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Custom message to notification.
        /// </summary>
        public string Message { get; set; }

        public Database.Entity.Meeting ToEntity(long loggedUserId)
        {
            var entity = new Database.Entity.Meeting()
            {
                Agenda = Agenda.Select(o => new Database.Entity.MeetingAgendaItem() { Content = o }).ToHashSet(),
                At = At,
                AttendanceType = AttendanceType,
                CreatedAt = DateTime.Now,
                CreatedById = loggedUserId,
                Name = Name,
                Report = Report,
                State = MeetingState,
                Location = Location,
                Message = Message,
                ExpectedEnd = ExpectedEnd
            };

            if (ExpectedUsers != null)
            {
                entity.Presences = ExpectedUsers.Select(o => new Database.Entity.MeetingPresence()
                {
                    PresenceType = MeetingPresenceType.Expected,
                    UserId = o
                }).ToHashSet();
            }

            return entity;
        }

        public void UpdateEntity(Database.Entity.Meeting entity, long loggedUserId)
        {
            entity.Agenda = Agenda.Select(o => new Database.Entity.MeetingAgendaItem() { Content = o }).ToHashSet();
            entity.At = At;
            entity.AttendanceType = AttendanceType;
            entity.Name = Name;
            entity.Report = Report;
            entity.State = MeetingState;
            entity.Location = Location;
            entity.Message = Message;
            entity.ExpectedEnd = ExpectedEnd;
            entity.MarkAsModified(loggedUserId);
        }
    }
}
