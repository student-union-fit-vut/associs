﻿using AssocIS.Data.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AssocIS.Data.Models.Meetings
{
    /// <summary>
    /// Detailed information about meeting.
    /// </summary>
    public class Meeting : MeetingListItem
    {
        /// <summary>
        /// Date time of last notification about meeting. Datetime is in UTC.
        /// </summary>
        public DateTime? LastNotifiedAt { get; set; }

        /// <summary>
        /// Meeting agenda
        /// </summary>
        public List<string> Agenda { get; set; }

        /// <summary>
        /// Agenda report.
        /// </summary>
        public string Report { get; set; }

        /// <summary>
        /// Data about creation and modification about meeting.
        /// </summary>
        public LogData LogData { get; set; }

        /// <summary>
        /// Location of meeting.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Custom message to notification.
        /// </summary>
        public string Message { get; set; }

        public Meeting(Database.Entity.Meeting meeting, Database.Entity.User loggedUser) : base(meeting, loggedUser)
        {
            if (meeting == null)
                return;

            LastNotifiedAt = meeting.LastNotifiedAt;
            Agenda = meeting.Agenda.Select(o => o.Content).ToList();
            Report = meeting.Report;
            LogData = new LogData(meeting);
            Location = meeting.Location;
            Message = meeting.Message;
        }
    }
}
