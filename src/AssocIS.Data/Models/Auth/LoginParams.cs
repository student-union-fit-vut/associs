﻿using AssocIS.Data.Resources.Common;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Auth
{
    /// <summary>
    /// Data of standard login request.
    /// </summary>
    public class LoginParams
    {
        /// <summary>
        /// Username
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string Username { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string Password { get; set; }

        /// <summary>
        /// Client identification.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string ClientId { get; set; }
    }
}
