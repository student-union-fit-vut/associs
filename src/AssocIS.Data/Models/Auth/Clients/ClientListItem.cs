﻿using AssocIS.Database.Enums;

namespace AssocIS.Data.Models.Auth.Clients
{
    /// <summary>
    /// Authentication client. List item
    /// </summary>
    public class ClientListItem
    {
        /// <summary>
        /// Internal ID of client. Assigned automaticaly.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Client ID
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Client name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Client type (Web, Third-party app, ...)
        /// </summary>
        public ClientType ClientType { get; set; }

        /// <summary>
        /// Flag that describe user wasn't any used.
        /// </summary>
        public bool IsUnused { get; set; }

        public ClientListItem() { }

        public ClientListItem(Database.Entity.Client entity)
        {
            if (entity == null)
                return;

            Id = entity.ID;
            ClientId = entity.ClientId;
            Name = entity.Name;
            ClientType = entity.ClientType;
            IsUnused = entity.LastAccess == null;
        }
    }
}
