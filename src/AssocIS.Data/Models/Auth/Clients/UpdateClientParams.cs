﻿using AssocIS.Data.Resources.Common;
using AssocIS.Database.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Auth.Clients
{
    /// <summary>
    /// Request for create or update client application.
    /// </summary>
    public class UpdateClientParams
    {
        /// <summary>
        /// Name of client application.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [StringLength(100, ErrorMessageResourceName = nameof(CommonResources.MaximumLengthExceeded), ErrorMessageResourceType = typeof(CommonResources))]
        public string Name { get; set; }

        /// <summary>
        /// Client description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Type of client (Web, third-party app, bot, ...)
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public ClientType ClientType { get; set; }

        public Database.Entity.Client ToEntity()
        {
            return new Database.Entity.Client()
            {
                ClientId = Guid.NewGuid().ToString(),
                CreatedAt = DateTime.Now,
                Description = Description,
                Name = Name,
                ClientType = ClientType
            };
        }

        public void UpdateEntity(Database.Entity.Client entity)
        {
            entity.Name = Name;
            entity.Description = Description;
            entity.ClientType = ClientType;
        }
    }
}
