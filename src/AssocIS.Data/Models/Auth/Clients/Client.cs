﻿using System;

namespace AssocIS.Data.Models.Auth.Clients
{
    /// <summary>
    /// Authentication client.
    /// </summary>
    public class Client : ClientListItem
    {
        /// <summary>
        /// Optional description of client.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// UTC DateTime of creation.
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// UTC DateTime of last access.
        /// </summary>
        public DateTime? LastAccess { get; set; }

        public Client() { }

        public Client(Database.Entity.Client entity) : base(entity)
        {
            if (entity == null)
                return;

            Description = entity.Description;
            CreatedAt = entity.CreatedAt;
            LastAccess = entity.LastAccess == null ? null : entity.LastAccess;
        }
    }
}
