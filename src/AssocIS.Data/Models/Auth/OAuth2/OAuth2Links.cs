﻿namespace AssocIS.Data.Models.Auth.OAuth2
{
    /// <summary>
    /// OAuth gateway urls.
    /// </summary>
    public class OAuth2Links
    {
        /// <summary>
        /// Oauth2 gateway to discord.
        /// </summary>
        public string Discord { get; set; }

        /// <summary>
        /// OAuth2 Gateway to Google.
        /// </summary>
        public string Google { get; set; }

        public bool IsAnyAvailable()
        {
            return !string.IsNullOrEmpty(Discord) || !string.IsNullOrEmpty(Google);
        }
    }
}
