﻿using AssocIS.Data.Resources.Common;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Auth.OAuth2
{
    /// <summary>
    /// Data for generation oauth2 links to external services.
    /// </summary>
    public class OAuth2GetLinksParams
    {
        /// <summary>
        /// Redirect url to our service.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        [Url(ErrorMessageResourceName = nameof(CommonResources.InvalidUrl), ErrorMessageResourceType = typeof(CommonResources))]
        public string ClientRedirectUrl { get; set; }
    }
}
