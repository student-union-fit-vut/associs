﻿using AssocIS.Data.Enums;

namespace AssocIS.Data.Models.Auth.OAuth2
{
    /// <summary>
    /// OAuth2 data stored in memory cache.
    /// </summary>
    public class OAuth2SessionData
    {
        /// <summary>
        /// Redirect url to send to our application.
        /// </summary>
        public string RedirectUrl { get; set; }

        /// <summary>
        /// User ID used to login.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Used service.
        /// </summary>
        public OAuth2SupportedServices Service { get; set; }

        public OAuth2SessionData() { }

        public OAuth2SessionData(string redirectUrl)
        {
            RedirectUrl = redirectUrl;
        }
    }
}
