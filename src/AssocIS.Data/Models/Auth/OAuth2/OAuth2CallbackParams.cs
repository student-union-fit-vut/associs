﻿using AssocIS.Data.Resources.Auth.OAuth2;
using AssocIS.Data.Resources.Common;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Caching.Memory;
using AssocIS.Data.Enums;
using NSwag.Annotations;

namespace AssocIS.Data.Models.Auth.OAuth2
{
    /// <summary>
    /// Callback data from oauth gateway.
    /// </summary>
    public class OAuth2CallbackParams : IValidatableObject
    {
        /// <summary>
        /// Authorization code.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string Code { get; set; }

        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public string State { get; set; }

        /// <summary>
        /// Session ID from State.
        /// </summary>
        [OpenApiIgnore]
        public Guid? SessionId => string.IsNullOrEmpty(State) ? (Guid?)null : Guid.Parse(State.Split('|')[0]);

        /// <summary>
        /// Type of used service.
        /// </summary>
        [OpenApiIgnore]
        public OAuth2SupportedServices? Service => string.IsNullOrEmpty(State) ? (OAuth2SupportedServices?)null : Enum.Parse<OAuth2SupportedServices>(State.Split('|')[1]);

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var localizer = validationContext.GetService<IStringLocalizer<OAuth2Resources>>();
            var memoryCache = validationContext.GetService<IMemoryCache>();

            var sessionData = State.Split('|');
            if (sessionData.Length != 2)
                yield return new ValidationResult(localizer[nameof(OAuth2Resources.CallbackInvalidSessionData)]);

            if (!Guid.TryParse(sessionData[0], out Guid sessionGuid))
                yield return new ValidationResult(localizer[nameof(OAuth2Resources.CallbackInvalidSessionData)]);

            if (!memoryCache.TryGetValue(sessionGuid.ToString(), out var _))
                yield return new ValidationResult(localizer[nameof(OAuth2Resources.CallbackInvalidSessionData)]);

            if (!Enum.IsDefined(typeof(OAuth2SupportedServices), sessionData[1]))
                yield return new ValidationResult(localizer[nameof(OAuth2Resources.CallbackInvalidSessionData)]);

            yield return ValidationResult.Success;
        }
    }
}
