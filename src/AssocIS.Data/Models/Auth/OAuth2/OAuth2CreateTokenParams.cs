﻿using AssocIS.Data.Resources.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Models.Auth.OAuth2
{
    /// <summary>
    /// Parameters for oauth2 token generator.
    /// </summary>
    public class OAuth2CreateTokenParams
    {
        /// <summary>
        /// Session ID.
        /// </summary>
        [Required(ErrorMessageResourceName = nameof(CommonResources.RequiredField), ErrorMessageResourceType = typeof(CommonResources))]
        public Guid SessionId { get; set; }
    }
}
