﻿using System;

namespace AssocIS.Data.Models.Auth
{
    /// <summary>
    /// Authentification data.
    /// </summary>
    public class JwtToken
    {
        /// <summary>
        /// JWT Access token
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Token type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// UTC DateTime of AccessToken expiration.
        /// </summary>
        public DateTime ExpiresAt { get; set; }

        /// <summary>
        /// UTC DateTime when AccessToken was created.
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Refresh token.
        /// </summary>
        public string RefreshToken { get; set; }

        public JwtToken(string accessToken, string type, DateTime expiresAt, string refreshToken)
        {
            AccessToken = accessToken;
            Type = type;
            ExpiresAt = expiresAt;
            CreatedAt = DateTime.Now;
            RefreshToken = refreshToken;
        }
    }
}
