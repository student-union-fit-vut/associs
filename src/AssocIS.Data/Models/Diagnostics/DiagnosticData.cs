﻿using System;
using System.Diagnostics;

namespace AssocIS.Data.Models.Diagnostics
{
    /// <summary>
    /// Diagnostics data
    /// </summary>
    public class DiagnosticData
    {
        /// <summary>
        /// Start at datetime in local time.
        /// </summary>
        public DateTime StartAt { get; set; }

        /// <summary>
        /// Uptime.
        /// </summary>
        public TimeSpan Uptime => DateTime.Now - StartAt;

        /// <summary>
        /// Time spent on the CPU.
        /// </summary>
        public TimeSpan CpuTime { get; set; }

        /// <summary>
        /// Used memory in kilobytes.
        /// </summary>
        public long MemoryUsed { get; set; }

        /// <summary>
        /// Requests count to API.
        /// </summary>
        public int RequestsCount { get; set; }

        public DiagnosticData()
        {
            var process = Process.GetCurrentProcess();

            StartAt = process.StartTime;
            CpuTime = process.TotalProcessorTime;
            MemoryUsed = process.WorkingSet64;
        }

        public DiagnosticData(int requestsCount) : this()
        {
            RequestsCount = requestsCount;
        }
    }
}
