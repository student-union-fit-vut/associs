﻿using AssocIS.Database.Entity;
using AssocIS.Database.Enums;
using System;

namespace AssocIS.Data.Helpers
{
    public static class MeetingPresenceHelper
    {
        public static MeetingPresenceType CalculatePresence(Meeting meeting, MeetingPresence presence, User user)
        {
            if (meeting.State != MeetingState.Summoned)
                return MeetingPresenceType.Unknown;

            if (DateTime.Now > meeting.At)
            {
                // Meeting started

                if (meeting.AttendanceType == MeetingAttendanceType.Required)
                {
                    // Member is collaborator and presence is not stored. This members are apologized.
                    if (user.State == UserState.ActiveCollaborator && presence == null)
                        return MeetingPresenceType.Optional;

                    // Meeting is mandatory, but user have not stored presence. => Absent.
                    if (presence == null)
                        return MeetingPresenceType.Absent;
                }

                // Meeting is mandatory for expected users and user is expected => Absent.
                if (meeting.AttendanceType == MeetingAttendanceType.RequiredForExpected && presence != null && presence.PresenceType == MeetingPresenceType.Expected)
                    return MeetingPresenceType.Absent;

                // User have stored presence. Take it.
                if (presence != null)
                    return presence.PresenceType;

                // Meeting started, but presence is optional, or user wans't expected. We can set absent.
                return MeetingPresenceType.Absent;
            }

            // Meeting not started, but we know current state of presence.
            if (presence != null)
                return presence.PresenceType;

            // Meeting is mandatory. All users except active collaborators are expected.
            if (meeting.AttendanceType == MeetingAttendanceType.Required)
            {
                if (user.State == UserState.ActiveCollaborator)
                    return MeetingPresenceType.Optional;

                return MeetingPresenceType.Expected;
            }

            return MeetingPresenceType.Unknown;
        }
    }
}
