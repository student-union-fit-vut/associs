﻿using AssocIS.Database.Enums;
using System;
using System.Linq;

namespace AssocIS.Data.Helpers
{
    public static class AssetsHelper
    {
        public static int CalculateAvailableCount(Database.Entity.Asset entity)
        {
            var unavailableConditions = new Func<bool>[]
            {
                () => (entity.Flags & (long)AssetFlags.LoansDisabled) != 0,
                () => (entity.Flags & (long)AssetFlags.Removed) != 0
            };

            if (unavailableConditions.Any(o => o()))
                return 0;

            var loanedCount = entity.Loans.Count(o => o.ReturnedAt == null);
            return Math.Max(entity.Count - loanedCount, 0);
        }
    }
}
