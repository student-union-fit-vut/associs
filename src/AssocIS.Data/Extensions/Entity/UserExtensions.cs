﻿using AssocIS.Database.Entity;

namespace AssocIS.Data.Extensions.Entity
{
    public static class UserExtensions
    {
        public static string FormatName(this User entity, bool withUsername = false)
        {
            var name = $"{entity.Name} {entity.Surname}";

            return !withUsername ? name : $"{name} ({entity.Username})";
        }
    }
}
