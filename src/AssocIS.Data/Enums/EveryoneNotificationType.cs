﻿namespace AssocIS.Data.Enums
{
    /// <summary>
    /// Type of external service to notification.
    /// </summary>
    public enum EveryoneNotificationType
    {
        /// <summary>
        /// Discord home guild.
        /// </summary>
        Discord
    }
}
