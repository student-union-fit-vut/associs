﻿namespace AssocIS.Data.Enums
{
    public enum OAuth2SupportedServices
    {
        Discord,
        Google
    }
}
