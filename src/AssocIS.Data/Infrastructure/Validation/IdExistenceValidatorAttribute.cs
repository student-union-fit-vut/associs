﻿using AssocIS.Database.Entity;
using AssocIS.Database.Entity.Finance;
using AssocIS.Database.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AssocIS.Data.Infrastructure.Validation
{
    public class IdExistenceValidatorAttribute : ValidationAttribute
    {
        private Type EntityType { get; }

        public IdExistenceValidatorAttribute(Type entityType)
        {
            EntityType = entityType;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                // To validate filled value use required attribute.
                return ValidationResult.Success;
            }
            else if (value is List<long> list)
            {
                foreach (var item in list)
                {
                    var result = CheckValue(item, validationContext);

                    if (!result)
                        return new ValidationResult(FormatErrorMessage(value?.ToString()));
                }
            }
            else if (value is long item)
            {
                var result = CheckValue(item, validationContext);

                if (!result)
                    return new ValidationResult(FormatErrorMessage(value?.ToString()));
            }
            else
            {
                throw new NotSupportedException("Only List<long> or long is allowed type.");
            }

            return ValidationResult.Success;
        }

        private Func<IAssocISRepository, bool> GetExpression(long value)
        {
            if (EntityType == typeof(Asset))
                return repo => repo.Assets.GetAssetsQuery(null, null, null, false).Any(o => o.Id == value);
            else if (EntityType == typeof(User))
                return repo => repo.User.UserExists(value);
            else if (EntityType == typeof(AssetCategory))
                return repo => repo.Assets.GetAssetCategoriesQuery().Any(o => o.Id == value);
            else if (EntityType == typeof(Account))
                return repo => repo.Finance.GetAccountsQuery(null, null, null).Any(o => o.Id == value);
            else if (EntityType == typeof(Supplier))
                return repo => repo.Finance.SupplierExistsAsync(value).Result;
            else if (EntityType == typeof(TransactionCategory))
                return repo => repo.Finance.GetCategoriesQuery().Any(o => o.Id == value);

            throw new NotSupportedException($"Unique validation not supports type {EntityType.FullName}");
        }

        private bool CheckValue(long value, ValidationContext context)
        {
            var expression = GetExpression(Convert.ToInt64(value));
            return ValidationHelper.ProcessDbValidation(context, expression);
        }
    }
}
