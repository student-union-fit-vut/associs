﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Infrastructure.Validation
{
    public class DateLessThanAttribute : ValidationAttribute
    {
        private string ComparsionProperty { get; }

        public DateLessThanAttribute(string comparsionProperty)
        {
            ComparsionProperty = comparsionProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;

            var currentValue = (DateTime)value;
            var property = validationContext.ObjectType.GetProperty(ComparsionProperty);

            if (property == null)
                throw new ArgumentException($"Property with name {ComparsionProperty} not found in model.");

            var comparsionValue = property.GetValue(validationContext.ObjectInstance);

            if (comparsionValue == null)
                return ValidationResult.Success;

            if (currentValue > (DateTime)comparsionValue)
                return new ValidationResult(FormatErrorMessage("Date"));

            return ValidationResult.Success;
        }
    }
}
