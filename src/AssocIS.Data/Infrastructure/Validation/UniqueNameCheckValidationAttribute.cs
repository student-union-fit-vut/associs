﻿using AssocIS.Database.Entity;
using AssocIS.Database.Entity.Finance;
using AssocIS.Database.Services;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AssocIS.Data.Infrastructure.Validation
{
    public class UniqueNameCheckValidationAttribute : ValidationAttribute
    {
        private Type EntityType { get; }
        private string ConditionProperty { get; set; }

        public UniqueNameCheckValidationAttribute(Type entityType, string conditionProperty = null)
        {
            EntityType = entityType;
            ConditionProperty = conditionProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!CanValidate(validationContext))
                return ValidationResult.Success;

            var expression = GetExpression(value?.ToString());
            var result = ValidationHelper.ProcessDbValidation(validationContext, expression);

            if (result)
                return new ValidationResult(FormatErrorMessage(value?.ToString()));

            return ValidationResult.Success;
        }

        private Func<IAssocISRepository, bool> GetExpression(string value)
        {
            if (EntityType == typeof(AssetCategory))
                return repo => repo.Assets.GetAssetCategoriesQuery().Any(o => o.Name == value);
            else if (EntityType == typeof(Team))
                return repo => repo.Team.GetTeamsQuery(null, false).Any(o => o.Name == value);
            else if (EntityType == typeof(User))
                return repo => repo.User.GetUsersQuery(null, false, false, true, null).Any(o => o.Username == value);
            else if (EntityType == typeof(TransactionCategory))
                return repo => repo.Finance.GetCategoriesQuery().Any(o => o.Name == value);

            throw new NotSupportedException($"Unique validation not supports type {EntityType.FullName}");
        }

        private bool CanValidate(ValidationContext context)
        {
            if (string.IsNullOrEmpty(ConditionProperty)) return true;

            var type = context.ObjectInstance.GetType();
            var conditionPropertyValue = type.GetProperty(ConditionProperty).GetValue(context.ObjectInstance)?.ToString();
            _ = bool.TryParse(conditionPropertyValue, out bool propertyValue);

            return propertyValue;
        }
    }
}
