﻿using AssocIS.Database.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.ComponentModel.DataAnnotations;

namespace AssocIS.Data.Infrastructure.Validation
{
    public static class ValidationHelper
    {
        public static bool ProcessDbValidation(ValidationContext context, Func<IAssocISRepository, bool> validationFunc)
        {
            if (validationFunc == null)
                throw new ArgumentNullException(nameof(validationFunc));

            var mainRepository = context.GetService<IAssocISRepository>();
            return validationFunc(mainRepository);
        }
    }
}
