﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace AssocIS.Data.Infrastructure
{
    public class ServiceAttribute : Attribute
    {
        public ServiceLifetime Lifetime { get; }
        public Type InterfaceType { get; }

        public ServiceAttribute(ServiceLifetime lifetime = ServiceLifetime.Scoped, Type interfaceType = null)
        {
            Lifetime = lifetime;
            InterfaceType = interfaceType;
        }
    }
}
