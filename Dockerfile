FROM mcr.microsoft.com/dotnet/sdk:6.0-bullseye-slim AS ApiBuild

EXPOSE 5000
ENV TZ=Europe/Prague
ENV ASPNETCORE_URLS 'http://+:5000'
ENV DOTNET_PRINT_TELEMETRY_MESSAGE 'false'

# Common
RUN mkdir -p /api/AssocIS.Common
COPY "src/AssocIS.Common/AssocIS.Common.csproj" /api/AssocIS.Common
RUN dotnet restore "api/AssocIS.Common/AssocIS.Common.csproj"

# Database
RUN mkdir -p /api/AssocIS.Database
COPY "src/AssocIS.Database/AssocIS.Database.csproj" /api/AssocIS.Database
RUN dotnet restore "api/AssocIS.Database/AssocIS.Database.csproj"

# Data
RUN mkdir -p /api/AssocIS.Data
COPY "src/AssocIS.Data/AssocIS.Data.csproj" /api/AssocIS.Data
RUN dotnet restore "api/AssocIS.Data/AssocIS.Data.csproj"

# Main
RUN mkdir -p /api/AssocIS.Main
COPY "src/AssocIS.Main/AssocIS.Main.csproj" /api/AssocIS.Main
RUN dotnet restore "api/AssocIS.Main/AssocIS.Main.csproj"

# Publish
COPY "src/" /api
RUN mkdir -p /app
RUN dotnet publish /api/AssocIS.Main -c Release -o /app/publish --no-restore

# Client Build
FROM node:15-alpine as ClientBuild
RUN mkdir -p /client
COPY "src/AssocISClient/package.json" /client
WORKDIR /client
RUN npm install
COPY "src/AssocISClient" /client
RUN npm run build -- "--configuration=production"

FROM mcr.microsoft.com/dotnet/aspnet:6.0-bullseye-slim as FinalImage
WORKDIR /app
RUN sed -i'.bak' 's/$/ contrib/' /etc/apt/sources.list
RUN apt update && apt install -y --no-install-recommends tzdata libc6-dev libx11-dev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN mkdir -p /app/wwwroot
COPY --from=ApiBuild /app/publish .
COPY --from=ClientBuild /client/dist/AssocISClient /app/wwwroot
ENTRYPOINT [ "dotnet", "AssocIS.Main.dll" ]
